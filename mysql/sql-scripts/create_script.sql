CREATE TABLE `User`
(
    `id`       int NOT NULL AUTO_INCREMENT ,
    `email`    varchar(50) NOT NULL ,
    `username` varchar(50) NOT NULL ,

    PRIMARY KEY (`id`),
    UNIQUE KEY `ind_55` (`email`),
    UNIQUE KEY `ind_56` (`username`)
);

CREATE TABLE `Follow`
(
    `id`           int NOT NULL AUTO_INCREMENT ,
    `user_id_who`  int NOT NULL ,
    `user_id_whom` int NOT NULL ,

    PRIMARY KEY (`id`),
    UNIQUE KEY `ind_84` (`user_id_who`, `user_id_whom`),
    KEY `fkIdx_75` (`user_id_who`),
    CONSTRAINT `FK_74` FOREIGN KEY `fkIdx_75` (`user_id_who`) REFERENCES `User` (`id`),
    KEY `fkIdx_78` (`user_id_whom`),
    CONSTRAINT `FK_77` FOREIGN KEY `fkIdx_78` (`user_id_whom`) REFERENCES `User` (`id`)
);

CREATE TABLE `Post`
(
    `id`          int NOT NULL AUTO_INCREMENT ,
    `description` varchar(500) NOT NULL ,
    `photo_url`    varchar(2048) NOT NULL ,
    `time`        datetime NOT NULL ,
    `user_id`     int NOT NULL ,

    PRIMARY KEY (`id`),
    KEY `fkIdx_60` (`user_id`),
    CONSTRAINT `FK_59` FOREIGN KEY `fkIdx_60` (`user_id`) REFERENCES `User` (`id`)
);

CREATE TABLE `Comment`
(
    `id`      int NOT NULL AUTO_INCREMENT ,
    `text`    varchar(500) NOT NULL ,
    `time`    datetime NOT NULL ,
    `post_id` int NOT NULL ,
    `user_id` int NOT NULL ,

    PRIMARY KEY (`id`),
    KEY `fkIdx_50` (`post_id`),
    CONSTRAINT `FK_49` FOREIGN KEY `fkIdx_50` (`post_id`) REFERENCES `Post` (`id`),
    KEY `fkIdx_81` (`user_id`),
    CONSTRAINT `FK_80` FOREIGN KEY `fkIdx_81` (`user_id`) REFERENCES `User` (`id`)
);

CREATE TABLE `Like`
(
    `id`      int NOT NULL AUTO_INCREMENT ,
    `user_id` int NOT NULL ,
    `post_id` int NOT NULL ,

    PRIMARY KEY (`id`),
    UNIQUE KEY `ind_88` (`post_id`, `user_id`),
    KEY `fkIdx_63` (`user_id`),
    CONSTRAINT `FK_62` FOREIGN KEY `fkIdx_63` (`user_id`) REFERENCES `User` (`id`),
    KEY `fkIdx_66` (`post_id`),
    CONSTRAINT `FK_65` FOREIGN KEY `fkIdx_66` (`post_id`) REFERENCES `Post` (`id`)
);

CREATE TABLE `User_log`
(
    `id`       int NOT NULL AUTO_INCREMENT ,
    `id_row`   int NOT NULL ,
    `activity` varchar(6) NOT NULL ,
    `time`     datetime NOT NULL ,

    PRIMARY KEY (`id`)
);

CREATE TRIGGER TR_Users_AfterInsert
AFTER INSERT ON `User`
FOR EACH ROW
INSERT INTO `User_log`(id_row, activity, `time`) VALUES (NEW.id, 'insert', now());

CREATE TRIGGER TR_Users_AfterUpdate
AFTER UPDATE ON `User`
FOR EACH ROW
INSERT INTO `User_log`(id_row, activity, `time`) VALUES (OLD.id, 'update', now());

CREATE TRIGGER TR_Users_AfterDelete
AFTER DELETE ON `User`
FOR EACH ROW
INSERT INTO `User_log`(id_row, activity, `time`) VALUES (OLD.id, 'delete', now());
