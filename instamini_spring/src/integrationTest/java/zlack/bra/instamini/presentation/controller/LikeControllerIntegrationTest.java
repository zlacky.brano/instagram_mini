package zlack.bra.instamini.presentation.controller;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import util.AssertLikesIntegrationUtil;
import util.MockJwtAuthenticationUtil;
import zlack.bra.instamini.business.dto.*;
import zlack.bra.instamini.security.jwt.JwtUtil;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@Sql(scripts = "/sql-scripts/insert_script_test.sql", executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
@Sql(scripts = "/sql-scripts/delete_script_test.sql", executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
class LikeControllerIntegrationTest {

    @Autowired
    private MockMvc mockMvc;

    private MockJwtAuthenticationUtil mockJwtAuthenticationUtil;

    @MockBean
    private JwtUtil jwtUtil;

    private final UserDTO loggedUserDTOExample = new UserDTO(10, "olivia.johnson@test.com", "Olivia Johnson");

    private final UserDTO notLoggedUserDTOExample = new UserDTO(6, "thomas.wilson@test.com", "WilT");

    private final List<LikeDTO> likesInDatabase = new ArrayList<>() {
        {
            add(new LikeDTO(1, loggedUserDTOExample,
                    new PostDTO(3, "In est risus, auctor sed, tristique in, tempus sit amet, sem. Fusce consequat. Nulla nisl. Nunc nisl. Duis bibendum, felis sed interdum venenatis, turpis enim blandit mi, in porttitor pede justo eu massa. Donec dapibus. Duis at velit eu est congue elementum. In hac habitasse platea dictumst. Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante. Nulla justo.",
                            "https://res.cloudinary.com/instamini/image/upload/v1630672583/pug-690566_1280_s4wmbu.jpg",
                            LocalDateTime.of(2021, 5, 25, 12, 12, 22),
                            new UserDTO(2, "george.jones@test.com", "_Georgie_"))));
            add(new LikeDTO(2, notLoggedUserDTOExample,
                    new PostDTO(3, "In est risus, auctor sed, tristique in, tempus sit amet, sem. Fusce consequat. Nulla nisl. Nunc nisl. Duis bibendum, felis sed interdum venenatis, turpis enim blandit mi, in porttitor pede justo eu massa. Donec dapibus. Duis at velit eu est congue elementum. In hac habitasse platea dictumst. Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante. Nulla justo.",
                            "https://res.cloudinary.com/instamini/image/upload/v1630672583/pug-690566_1280_s4wmbu.jpg",
                            LocalDateTime.of(2021, 5, 25, 12, 12, 22),
                            new UserDTO(2, "george.jones@test.com", "_Georgie_"))));
            add(new LikeDTO(3, new UserDTO(1, "john.smith@test.com", "JohnyS"),
                    new PostDTO(2, "Nullam orci pede, venenatis non, sodales sed, tincidunt eu, felis. Fusce posuere felis sed lacus. Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl. Nunc rhoncus dui vel sem. Sed sagittis. Nam congue, risus semper porta volutpat, quam pede lobortis ligula, sit amet eleifend pede libero quis orci. Nullam molestie nibh in lectus. Pellentesque at nulla.", "https://res.cloudinary.com/instamini/image/upload/v1630672584/dolphin-203875_1280_n67crm.jpg",
                            LocalDateTime.of(2020, 12, 20, 7, 51, 2),
                            loggedUserDTOExample)));
            add(new LikeDTO(4, new UserDTO(5, "charlie.smith@test.com", ":Charlie:"),
                    new PostDTO(4, "Nulla ac enim. In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem.",
                            "https://res.cloudinary.com/instamini/image/upload/v1630672586/husky-3380548_1280_rman9o.jpg",
                            LocalDateTime.of(2021, 2, 6, 16, 15, 7),
                            loggedUserDTOExample)));
        }
    };


    @BeforeEach
    void setUp() {
        mockJwtAuthenticationUtil = new MockJwtAuthenticationUtil(jwtUtil, loggedUserDTOExample, "exists");
    }

    @Test
    void all401() throws Exception {
        mockMvc.perform(get("/likes")).andExpect(status().isUnauthorized());

        mockJwtAuthenticationUtil.verify(0);
    }

    @Test
    void all200() throws Exception {
        mockJwtAuthenticationUtil.mockReturns();

        ResultActions result = mockMvc.perform(get("/likes")
                        .header("Authorization", "Bearer " + mockJwtAuthenticationUtil.getMockedToken()))
                .andExpect(status().isOk()).andExpect(content().contentType("application/json"));

        for (int i = 0; i < likesInDatabase.size(); i++) {
            AssertLikesIntegrationUtil.assertLikes(result, likesInDatabase.get(i), i);
        }

        mockJwtAuthenticationUtil.verify(1);
    }

    @Test
    void byID401() throws Exception {
        LikeDTO likeDTOToFind = likesInDatabase.get(0);

        mockMvc.perform(get("/likes/{id}", likeDTOToFind.getId())).andExpect(status().isUnauthorized());

        mockJwtAuthenticationUtil.verify(0);
    }

    @Test
    void byID404() throws Exception {
        mockJwtAuthenticationUtil.mockReturns();

        mockMvc.perform(get("/likes/{id}", 0)
                        .header("Authorization", "Bearer " + mockJwtAuthenticationUtil.getMockedToken()))
                .andExpect(status().isNotFound());

        mockJwtAuthenticationUtil.verify(1);
    }

    @Test
    void byID200() throws Exception {
        LikeDTO likeDTOToFind = likesInDatabase.get(0);

        mockJwtAuthenticationUtil.mockReturns();

        ResultActions result = mockMvc.perform(get("/likes/{id}", likeDTOToFind.getId())
                        .header("Authorization", "Bearer " + mockJwtAuthenticationUtil.getMockedToken()))
                .andExpect(status().isOk()).andExpect(content().contentType("application/json"));

        AssertLikesIntegrationUtil.assertLikes(result, likeDTOToFind);

        mockJwtAuthenticationUtil.verify(1);
    }

    @Test
    void create401() throws Exception {
        LikeCreateDTO likeCreateDTO = new LikeCreateDTO(notLoggedUserDTOExample.getId(), likesInDatabase.get(2).getPost().getId());

        mockMvc.perform(post("/likes")
                        .contentType("application/json")
                        .content(likeCreateDTO.toJson()))
                .andExpect(status().isUnauthorized());

        mockJwtAuthenticationUtil.verify(0);
    }

    @Test
    void create403() throws Exception {
        LikeCreateDTO likeCreateDTO = new LikeCreateDTO(notLoggedUserDTOExample.getId(), likesInDatabase.get(2).getPost().getId());

        mockJwtAuthenticationUtil.mockReturns();

        mockMvc.perform(post("/likes")
                        .contentType("application/json")
                        .content(likeCreateDTO.toJson())
                        .header("Authorization", "Bearer " + mockJwtAuthenticationUtil.getMockedToken()))
                .andExpect(status().isForbidden());

        mockJwtAuthenticationUtil.verify(1);
    }

    @Test
    void create404() throws Exception {
        LikeCreateDTO likeCreateDTO = new LikeCreateDTO(loggedUserDTOExample.getId(), 0);

        mockJwtAuthenticationUtil.mockReturns();

        mockMvc.perform(post("/likes")
                        .contentType("application/json")
                        .content(likeCreateDTO.toJson())
                        .header("Authorization", "Bearer " + mockJwtAuthenticationUtil.getMockedToken()))
                .andExpect(status().isNotFound());

        mockJwtAuthenticationUtil.verify(1);
    }

    @Test
    void create201() throws Exception {
        LikeCreateDTO likeCreateDTO = new LikeCreateDTO(loggedUserDTOExample.getId(), likesInDatabase.get(2).getPost().getId());

        mockJwtAuthenticationUtil.mockReturns();

        ResultActions result = mockMvc.perform(post("/likes")
                        .contentType("application/json")
                        .content(likeCreateDTO.toJson())
                        .header("Authorization", "Bearer " + mockJwtAuthenticationUtil.getMockedToken()))
                .andExpect(status().isCreated()).andExpect(content().contentType("application/json"));

        AssertLikesIntegrationUtil.assertLikes(result, likeCreateDTO);

        mockJwtAuthenticationUtil.verify(1);
    }

    @Test
    void create400Valid() throws Exception {
        LikeCreateDTO likeCreateDTO = new LikeCreateDTO(loggedUserDTOExample.getId(), likesInDatabase.get(2).getPost().getId());

        mockJwtAuthenticationUtil.mockReturns();

        mockMvc.perform(post("/likes")
                        .contentType("application/json")
                        .content("{\"userId\":\"" + likeCreateDTO.getUserId() + "\"}") // missing body
                        .header("Authorization", "Bearer " + mockJwtAuthenticationUtil.getMockedToken()))
                .andExpect(status().isBadRequest());

        mockJwtAuthenticationUtil.verify(1);
    }

    @Test
    void create400Unique() throws Exception {
        LikeCreateDTO likeCreateDTO = new LikeCreateDTO(loggedUserDTOExample.getId(), likesInDatabase.get(0).getPost().getId());

        mockJwtAuthenticationUtil.mockReturns();

        mockMvc.perform(post("/likes")
                        .contentType("application/json")
                        .content(likeCreateDTO.toJson())
                        .header("Authorization", "Bearer " + mockJwtAuthenticationUtil.getMockedToken()))
                .andExpect(status().isBadRequest());

        mockJwtAuthenticationUtil.verify(1);
    }

    @Test
    void delete401() throws Exception {
        mockMvc.perform(delete("/likes/{id}", likesInDatabase.get(0).getId()))
                .andExpect(status().isUnauthorized());

        mockJwtAuthenticationUtil.verify(0);
    }

    @Test
    void delete403() throws Exception {
        mockJwtAuthenticationUtil.mockReturns();

        mockMvc.perform(delete("/likes/{id}", likesInDatabase.get(3).getId()) // Deleting like, which does not belong to logged user
                        .header("Authorization", "Bearer " + mockJwtAuthenticationUtil.getMockedToken()))
                .andExpect(status().isForbidden());

        mockJwtAuthenticationUtil.verify(1);
    }

    @Test
    void delete404() throws Exception {
        mockJwtAuthenticationUtil.mockReturns();

        mockMvc.perform(delete("/likes/{id}", 0)
                        .header("Authorization", "Bearer " + mockJwtAuthenticationUtil.getMockedToken()))
                .andExpect(status().isNotFound());

        mockJwtAuthenticationUtil.verify(1);
    }

    @Test
    void delete200() throws Exception {
        mockJwtAuthenticationUtil.mockReturns();

        mockMvc.perform(delete("/likes/{id}", likesInDatabase.get(0).getId())
                        .header("Authorization", "Bearer " + mockJwtAuthenticationUtil.getMockedToken()))
                .andExpect(status().isOk());

        mockJwtAuthenticationUtil.verify(1);
    }

    @Test
    void findNumberOfLikesOnPost401() throws Exception {
        mockMvc.perform(get("/likes/count")
                        .param("post", "3"))
                .andExpect(status().isUnauthorized());

        mockJwtAuthenticationUtil.verify(0);
    }

    @Test
    void findNumberOfLikesOnPost404() throws Exception {
        mockJwtAuthenticationUtil.mockReturns();

        mockMvc.perform(get("/likes/count")
                        .param("post", "0")
                        .header("Authorization", "Bearer " + mockJwtAuthenticationUtil.getMockedToken()))
                .andExpect(status().isNotFound());

        mockJwtAuthenticationUtil.verify(1);
    }

    @Test
    void findNumberOfLikesOnPost200() throws Exception {
        mockJwtAuthenticationUtil.mockReturns();

        ResultActions result = mockMvc.perform(get("/likes/count")
                        .param("post", "3")
                        .header("Authorization", "Bearer " + mockJwtAuthenticationUtil.getMockedToken()))
                .andExpect(status().isOk()).andExpect(content().contentType("application/json"));

        // post with id 1 has 2 likes in database
        result.andExpect(content().string("2"));

        mockJwtAuthenticationUtil.verify(1);
    }

    @Test
    void findLikesOnPost401() throws Exception {
        mockMvc.perform(get("/likes/onPost")
                        .param("post", "3"))
                .andExpect(status().isUnauthorized());

        mockJwtAuthenticationUtil.verify(0);
    }

    @Test
    void findLikesOnPost404() throws Exception {
        mockJwtAuthenticationUtil.mockReturns();

        mockMvc.perform(get("/likes/onPost")
                        .param("post", "0")
                        .header("Authorization", "Bearer " + mockJwtAuthenticationUtil.getMockedToken()))
                .andExpect(status().isNotFound());

        mockJwtAuthenticationUtil.verify(1);
    }

    @Test
    void findLikesOnPost200() throws Exception {
        mockJwtAuthenticationUtil.mockReturns();

        ResultActions result = mockMvc.perform(get("/likes/onPost")
                        .param("post", "3")
                        .header("Authorization", "Bearer " + mockJwtAuthenticationUtil.getMockedToken()))
                .andExpect(status().isOk()).andExpect(content().contentType("application/json"));

        // post with id 1 has 2 likes in database
        AssertLikesIntegrationUtil.assertLikes(result, likesInDatabase.get(0), 0);
        AssertLikesIntegrationUtil.assertLikes(result, likesInDatabase.get(1), 1);

        mockJwtAuthenticationUtil.verify(1);
    }

    @Test
    void findLikeIdByUserAndPost200() throws Exception {
        mockJwtAuthenticationUtil.mockReturns();

        ResultActions result = mockMvc.perform(get("/likes/isLiked/{postId}", likesInDatabase.get(0).getPost().getId())
                        .header("Authorization", "Bearer " + mockJwtAuthenticationUtil.getMockedToken()))
                .andExpect(status().isOk()).andExpect(content().contentType("application/json"));

        Assertions.assertEquals(result.andReturn().getResponse().getContentAsString(), "1");

        mockJwtAuthenticationUtil.verify(1);
    }

    @Test
    void findLikeIdByUserAndPost401() throws Exception {
        mockMvc.perform(get("/follows/isLiked/{postId}", likesInDatabase.get(0).getPost().getId()))
                .andExpect(status().isUnauthorized());

        mockJwtAuthenticationUtil.verify(0);
    }

    @Test
    void findLikeIdByUserAndPost404() throws Exception {
        mockJwtAuthenticationUtil.mockReturns();

        mockMvc.perform(get("/likes/isLiked/{postId}", 0)
                        .header("Authorization", "Bearer " + mockJwtAuthenticationUtil.getMockedToken()))
                .andExpect(status().isNotFound());

        mockJwtAuthenticationUtil.verify(1);
    }
}