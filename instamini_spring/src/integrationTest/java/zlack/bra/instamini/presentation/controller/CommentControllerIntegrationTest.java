package zlack.bra.instamini.presentation.controller;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import util.AssertCommentsIntegrationUtil;
import util.MockJwtAuthenticationUtil;
import zlack.bra.instamini.business.dto.*;
import zlack.bra.instamini.presentation.controller.request.EditTextCommentRequestBody;
import zlack.bra.instamini.security.jwt.JwtUtil;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@Sql(scripts = "/sql-scripts/insert_script_test.sql", executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
@Sql(scripts = "/sql-scripts/delete_script_test.sql", executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
class CommentControllerIntegrationTest {

    @Autowired
    private MockMvc mockMvc;

    private MockJwtAuthenticationUtil mockJwtAuthenticationUtil;

    @MockBean
    private JwtUtil jwtUtil;

    private final UserDTO loggedUserDTOExample = new UserDTO(10, "olivia.johnson@test.com", "Olivia Johnson");

    private final UserDTO notLoggedUserDTOExample = new UserDTO(6, "thomas.wilson@test.com", "WilT");

    private final List<CommentDTO> commentsInDatabase = new ArrayList<>() {
        {
            add(new CommentDTO(1, "In hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus.",
                    LocalDateTime.of(2020, 5, 24, 8, 23, 35),
                    notLoggedUserDTOExample,
                    new PostDTO(1, "Morbi quis tortor id nulla ultrices aliquet. Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui. Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc.",
                            "https://res.cloudinary.com/instamini/image/upload/v1630672586/husky-3380548_1280_rman9o.jpg",
                            LocalDateTime.of(2020, 4, 5, 22, 52, 37),
                            notLoggedUserDTOExample)));
            add(new CommentDTO(2, "Pellentesque ultrices mattis odio.",
                    LocalDateTime.of(2021, 1, 23, 15, 33, 49),
                    loggedUserDTOExample,
                    new PostDTO(1, "Morbi quis tortor id nulla ultrices aliquet. Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui. Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc.",
                            "https://res.cloudinary.com/instamini/image/upload/v1630672586/husky-3380548_1280_rman9o.jpg",
                            LocalDateTime.of(2020, 4, 5, 22, 52, 37),
                            notLoggedUserDTOExample)));
            add(new CommentDTO(3, "Vestibulum quam sapien, varius ut, blandit non, interdum in, ante. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Duis faucibus accumsan odio. Curabitur convallis.",
                    LocalDateTime.of(2020, 9, 9, 7, 6, 13),
                    new UserDTO(8, "oscar.garcia@test.com", "Oscar"),
                    new PostDTO(2, "Nullam orci pede, venenatis non, sodales sed, tincidunt eu, felis. Fusce posuere felis sed lacus. Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl. Nunc rhoncus dui vel sem. Sed sagittis. Nam congue, risus semper porta volutpat, quam pede lobortis ligula, sit amet eleifend pede libero quis orci. Nullam molestie nibh in lectus. Pellentesque at nulla.",
                            "https://res.cloudinary.com/instamini/image/upload/v1630672584/dolphin-203875_1280_n67crm.jpg",
                            LocalDateTime.of(2020, 12, 20, 7, 51, 2),
                            loggedUserDTOExample)));
            add(new CommentDTO(4, "Aenean sit amet justo. Morbi ut odio. Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim. Lorem ipsum dolor sit amet, consectetuer adipiscing elit.",
                    LocalDateTime.of(2021, 1, 11, 11, 0, 57),
                    loggedUserDTOExample,
                    new PostDTO(4, "Nulla ac enim. In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem.",
                            "https://res.cloudinary.com/instamini/image/upload/v1630672586/husky-3380548_1280_rman9o.jpg",
                            LocalDateTime.of(2021, 2, 6, 16, 15, 7),
                            loggedUserDTOExample)));
        }
    };

    @BeforeEach
    void setUp() {
        mockJwtAuthenticationUtil = new MockJwtAuthenticationUtil(jwtUtil, loggedUserDTOExample, "exists");
    }

    @Test
    void all401() throws Exception {
        mockMvc.perform(get("/comments")).andExpect(status().isUnauthorized());

        mockJwtAuthenticationUtil.verify(0);
    }

    @Test
    void all200() throws Exception {
        mockJwtAuthenticationUtil.mockReturns();

        ResultActions result = mockMvc.perform(get("/comments")
                        .header("Authorization", "Bearer " + mockJwtAuthenticationUtil.getMockedToken()))
                .andExpect(status().isOk()).andExpect(content().contentType("application/json"));

        for (int i = 0; i < commentsInDatabase.size(); i++) {
            AssertCommentsIntegrationUtil.assertComments(result, commentsInDatabase.get(i), i);
        }

        mockJwtAuthenticationUtil.verify(1);
    }

    @Test
    void byID401() throws Exception {
        CommentDTO commentDTOToFind = commentsInDatabase.get(0);

        mockMvc.perform(get("/comments/{id}", commentDTOToFind.getId())).andExpect(status().isUnauthorized());

        mockJwtAuthenticationUtil.verify(0);
    }

    @Test
    void byID404() throws Exception {
        mockJwtAuthenticationUtil.mockReturns();

        mockMvc.perform(get("/comments/{id}", 0)
                        .header("Authorization", "Bearer " + mockJwtAuthenticationUtil.getMockedToken()))
                .andExpect(status().isNotFound());

        mockJwtAuthenticationUtil.verify(1);
    }

    @Test
    void byID200() throws Exception {
        CommentDTO commentDTOToFind = commentsInDatabase.get(0);

        mockJwtAuthenticationUtil.mockReturns();

        ResultActions result = mockMvc.perform(get("/comments/{id}", commentDTOToFind.getId())
                        .header("Authorization", "Bearer " + mockJwtAuthenticationUtil.getMockedToken()))
                .andExpect(status().isOk()).andExpect(content().contentType("application/json"));

        AssertCommentsIntegrationUtil.assertComments(result, commentDTOToFind);

        mockJwtAuthenticationUtil.verify(1);
    }

    @Test
    void create401() throws Exception {
        CommentCreateDTO commentCreateDTO = new CommentCreateDTO("test", notLoggedUserDTOExample.getId(), commentsInDatabase.get(2).getPost().getId());

        mockMvc.perform(post("/comments")
                        .contentType("application/json")
                        .content(commentCreateDTO.toJson()))
                .andExpect(status().isUnauthorized());

        mockJwtAuthenticationUtil.verify(0);
    }

    @Test
    void create403() throws Exception {
        CommentCreateDTO commentCreateDTO = new CommentCreateDTO("test", notLoggedUserDTOExample.getId(), commentsInDatabase.get(2).getPost().getId());

        mockJwtAuthenticationUtil.mockReturns();

        mockMvc.perform(post("/comments")
                        .contentType("application/json")
                        .content(commentCreateDTO.toJson())
                        .header("Authorization", "Bearer " + mockJwtAuthenticationUtil.getMockedToken()))
                .andExpect(status().isForbidden());

        mockJwtAuthenticationUtil.verify(1);
    }

    @Test
    void create404() throws Exception {
        CommentCreateDTO commentCreateDTO = new CommentCreateDTO("test", loggedUserDTOExample.getId(), 0);

        mockJwtAuthenticationUtil.mockReturns();

        mockMvc.perform(post("/comments")
                        .contentType("application/json")
                        .content(commentCreateDTO.toJson())
                        .header("Authorization", "Bearer " + mockJwtAuthenticationUtil.getMockedToken()))
                .andExpect(status().isNotFound());

        mockJwtAuthenticationUtil.verify(1);
    }

    @Test
    void create201() throws Exception {
        CommentCreateDTO commentCreateDTO = new CommentCreateDTO("test", loggedUserDTOExample.getId(), commentsInDatabase.get(2).getPost().getId());

        mockJwtAuthenticationUtil.mockReturns();

        ResultActions result = mockMvc.perform(post("/comments")
                        .contentType("application/json")
                        .content(commentCreateDTO.toJson())
                        .header("Authorization", "Bearer " + mockJwtAuthenticationUtil.getMockedToken()))
                .andExpect(status().isCreated()).andExpect(content().contentType("application/json"));

        AssertCommentsIntegrationUtil.assertComments(result, commentCreateDTO);

        mockJwtAuthenticationUtil.verify(1);
    }

    @Test
    void create400Valid() throws Exception {
        CommentCreateDTO commentCreateDTO = new CommentCreateDTO("test", loggedUserDTOExample.getId(), commentsInDatabase.get(2).getPost().getId());

        mockJwtAuthenticationUtil.mockReturns();

        mockMvc.perform(post("/comments")
                        .contentType("application/json")
                        .content("{\"text\":\"" + commentCreateDTO.getText() + "\"}") // missing body
                        .header("Authorization", "Bearer " + mockJwtAuthenticationUtil.getMockedToken()))
                .andExpect(status().isBadRequest());

        mockJwtAuthenticationUtil.verify(1);
    }

    @Test
    void editText401() throws Exception {
        EditTextCommentRequestBody requestBody = new EditTextCommentRequestBody("new text");

        mockMvc.perform(put("/comments/{id}/editText", commentsInDatabase.get(0).getId())
                        .contentType("application/json")
                        .content(requestBody.toJson()))
                .andExpect(status().isUnauthorized());

        mockJwtAuthenticationUtil.verify(0);
    }

    @Test
    void editText403() throws Exception {
        EditTextCommentRequestBody requestBody = new EditTextCommentRequestBody("new text");

        mockJwtAuthenticationUtil.mockReturns();

        mockMvc.perform(put("/comments/{id}/editText", commentsInDatabase.get(0).getId()) // Editing comment, which does not belong to logged user
                        .contentType("application/json")
                        .content(requestBody.toJson())
                        .header("Authorization", "Bearer " + mockJwtAuthenticationUtil.getMockedToken()))
                .andExpect(status().isForbidden());

        mockJwtAuthenticationUtil.verify(1);
    }

    @Test
    void editText404() throws Exception {
        EditTextCommentRequestBody requestBody = new EditTextCommentRequestBody("new text");

        mockJwtAuthenticationUtil.mockReturns();

        mockMvc.perform(put("/comments/{id}/editText", 0)
                        .contentType("application/json")
                        .content(requestBody.toJson())
                        .header("Authorization", "Bearer " + mockJwtAuthenticationUtil.getMockedToken()))
                .andExpect(status().isNotFound());

        mockJwtAuthenticationUtil.verify(1);
    }

    @Test
    void editText200() throws Exception {
        EditTextCommentRequestBody requestBody = new EditTextCommentRequestBody("new text");

        mockJwtAuthenticationUtil.mockReturns();


        ResultActions result = mockMvc.perform(put("/comments/{id}/editText", commentsInDatabase.get(1).getId())
                        .contentType("application/json")
                        .content(requestBody.toJson())
                        .header("Authorization", "Bearer " + mockJwtAuthenticationUtil.getMockedToken()))
                .andExpect(status().isOk()).andExpect(content().contentType("application/json"));

        AssertCommentsIntegrationUtil.assertCommentsInEditTextTest(result, commentsInDatabase.get(1), requestBody.getNewText());

        mockJwtAuthenticationUtil.verify(1);
    }

    @Test
    void editText400Valid() throws Exception {
        mockJwtAuthenticationUtil.mockReturns();

        mockMvc.perform(put("/comments/{id}/editText", commentsInDatabase.get(1).getId())
                        .contentType("application/json")
                        .content("{}") // missing body
                        .header("Authorization", "Bearer " + mockJwtAuthenticationUtil.getMockedToken()))
                .andExpect(status().isBadRequest());

        mockJwtAuthenticationUtil.verify(1);
    }

    @Test
    void delete401() throws Exception {
        mockMvc.perform(delete("/comments/{id}", commentsInDatabase.get(0).getId()))
                .andExpect(status().isUnauthorized());

        mockJwtAuthenticationUtil.verify(0);
    }

    @Test
    void delete403() throws Exception {
        mockJwtAuthenticationUtil.mockReturns();

        mockMvc.perform(delete("/comments/{id}", commentsInDatabase.get(0).getId()) // Deleting comment, which does not belong to logged user
                        .header("Authorization", "Bearer " + mockJwtAuthenticationUtil.getMockedToken()))
                .andExpect(status().isForbidden());

        mockJwtAuthenticationUtil.verify(1);
    }

    @Test
    void delete404() throws Exception {
        mockJwtAuthenticationUtil.mockReturns();

        mockMvc.perform(delete("/comments/{id}", 0)
                        .header("Authorization", "Bearer " + mockJwtAuthenticationUtil.getMockedToken()))
                .andExpect(status().isNotFound());

        mockJwtAuthenticationUtil.verify(1);
    }

    @Test
    void delete200() throws Exception {
        mockJwtAuthenticationUtil.mockReturns();

        mockMvc.perform(delete("/comments/{id}", commentsInDatabase.get(1).getId())
                        .header("Authorization", "Bearer " + mockJwtAuthenticationUtil.getMockedToken()))
                .andExpect(status().isOk());

        mockJwtAuthenticationUtil.verify(1);
    }

    @Test
    void findAllCommentsOnPost401() throws Exception {
        mockMvc.perform(get("/comments/onPost").param("post", loggedUserDTOExample.getId().toString()))
                .andExpect(status().isUnauthorized());

        mockJwtAuthenticationUtil.verify(0);
    }

    @Test
    void findAllCommentsOnPost404() throws Exception {
        mockJwtAuthenticationUtil.mockReturns();

        mockMvc.perform(get("/comments/onPost")
                        .param("post", "0")
                        .header("Authorization", "Bearer " + mockJwtAuthenticationUtil.getMockedToken()))
                .andExpect(status().isNotFound());

        mockJwtAuthenticationUtil.verify(1);
    }

    @Test
    void findAllCommentsOnPost200() throws Exception {
        mockJwtAuthenticationUtil.mockReturns();

        ResultActions result = mockMvc.perform(get("/comments/onPost")
                        .param("post", "1")
                        .header("Authorization", "Bearer " + mockJwtAuthenticationUtil.getMockedToken()))
                .andExpect(status().isOk()).andExpect(content().contentType("application/json"));

        // post with id 1 has 2 comments in database
        AssertCommentsIntegrationUtil.assertComments(result, commentsInDatabase.get(0), 1);
        AssertCommentsIntegrationUtil.assertComments(result, commentsInDatabase.get(1), 0);

        mockJwtAuthenticationUtil.verify(1);
    }
}

