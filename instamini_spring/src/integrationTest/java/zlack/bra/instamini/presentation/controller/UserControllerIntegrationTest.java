package zlack.bra.instamini.presentation.controller;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import util.AssertLikesIntegrationUtil;
import util.AssertUsersIntegrationUtil;
import util.MockJwtAuthenticationUtil;
import zlack.bra.instamini.business.dto.UserDTO;
import zlack.bra.instamini.presentation.controller.request.EditUsernameRequestBody;
import zlack.bra.instamini.security.jwt.JwtUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@Sql(scripts = "/sql-scripts/insert_script_test.sql", executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
@Sql(scripts = "/sql-scripts/delete_script_test.sql", executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
class UserControllerIntegrationTest {

    @Autowired
    private MockMvc mockMvc;

    private MockJwtAuthenticationUtil mockJwtAuthenticationUtil;

    @MockBean
    private JwtUtil jwtUtil;

    private final UserDTO loggedUserDTOExample = new UserDTO(10, "olivia.johnson@test.com", "Olivia Johnson");

    private final List<UserDTO> usersInDatabse = new ArrayList<>() {
        {
            add(new UserDTO(1, "john.smith@test.com", "JohnyS"));
            add(new UserDTO(2, "george.jones@test.com", "_Georgie_"));
            add(new UserDTO(3, "jacob.brown@test.com", "_**JB**_"));
            add(new UserDTO(4, "harry.davies@test.com", "HarryDav"));
            add(new UserDTO(5, "charlie.smith@test.com", ":Charlie:"));
            add(new UserDTO(6, "thomas.wilson@test.com", "WilT"));
            add(new UserDTO(7, "joe.evans@test.com", "JEvans"));
            add(new UserDTO(8, "oscar.garcia@test.com", "Oscar"));
            add(new UserDTO(9, "william.miller@test.com", "WillMill"));
            add(new UserDTO(10, "olivia.johnson@test.com", "Olivia Johnson"));
            add(new UserDTO(11, "emily.murphy@test.com", "Em"));
            add(new UserDTO(12, "ava.byrne@test.com", "Ava"));
        }
    };

    @BeforeEach
    void setUp() {
        mockJwtAuthenticationUtil = new MockJwtAuthenticationUtil(jwtUtil, loggedUserDTOExample, "exists");
    }

    @Test
    void all401() throws Exception {
        mockMvc.perform(get("/users")).andExpect(status().isUnauthorized());

        mockJwtAuthenticationUtil.verify(0);
    }

    @Test
    void all200() throws Exception {
        mockJwtAuthenticationUtil.mockReturns();

        ResultActions result = mockMvc.perform(get("/users")
                        .header("Authorization", "Bearer " + mockJwtAuthenticationUtil.getMockedToken()))
                .andExpect(status().isOk()).andExpect(content().contentType("application/json"));

        for (int i = 0; i < usersInDatabse.size(); i++) {
            AssertUsersIntegrationUtil.assertUsers(result, usersInDatabse.get(i), i);
        }

        mockJwtAuthenticationUtil.verify(1);
    }

    @Test
    void byEmail401() throws Exception {
        UserDTO userDTOToFind = usersInDatabse.get(0);

        mockMvc.perform(get("/users/byEmail").param("email", userDTOToFind.getEmail())).andExpect(status().isUnauthorized());

        mockJwtAuthenticationUtil.verify(0);
    }

    @Test
    void byEmail404() throws Exception {
        mockJwtAuthenticationUtil.mockReturns();

        mockMvc.perform(get("/users/byEmail").param("email", UUID.randomUUID().toString())
                        .header("Authorization", "Bearer " + mockJwtAuthenticationUtil.getMockedToken()))
                .andExpect(status().isNotFound());

        mockJwtAuthenticationUtil.verify(1);
    }

    @Test
    void byEmail200() throws Exception {
        UserDTO userDTOToFind = usersInDatabse.get(0);

        mockJwtAuthenticationUtil.mockReturns();

        ResultActions result = mockMvc.perform(get("/users/byEmail").param("email", userDTOToFind.getEmail())
                        .header("Authorization", "Bearer " + mockJwtAuthenticationUtil.getMockedToken()))
                .andExpect(status().isOk()).andExpect(content().contentType("application/json"));

        AssertUsersIntegrationUtil.assertUsers(result, userDTOToFind);

        mockJwtAuthenticationUtil.verify(1);
    }

    @Test
    void byID401() throws Exception {
        UserDTO userDTOToFind = usersInDatabse.get(0);

        mockMvc.perform(get("/users/{id}", userDTOToFind.getId())).andExpect(status().isUnauthorized());

        mockJwtAuthenticationUtil.verify(0);
    }

    @Test
    void byID404() throws Exception {
        mockJwtAuthenticationUtil.mockReturns();

        mockMvc.perform(get("/users/{id}", 0)
                        .header("Authorization", "Bearer " + mockJwtAuthenticationUtil.getMockedToken()))
                .andExpect(status().isNotFound());

        mockJwtAuthenticationUtil.verify(1);
    }

    @Test
    void byID200() throws Exception {
        UserDTO userDTOToFind = usersInDatabse.get(0);

        mockJwtAuthenticationUtil.mockReturns();

        ResultActions result = mockMvc.perform(get("/users/{id}", userDTOToFind.getId())
                        .header("Authorization", "Bearer " + mockJwtAuthenticationUtil.getMockedToken()))
                .andExpect(status().isOk()).andExpect(content().contentType("application/json"));

        AssertUsersIntegrationUtil.assertUsers(result, userDTOToFind);

        mockJwtAuthenticationUtil.verify(1);
    }

    @Test
    void byPrincipal401() throws Exception {
        mockMvc.perform(get("/users/logged")).andExpect(status().isUnauthorized());

        mockJwtAuthenticationUtil.verify(0);
    }

    @Test
    void byPrincipal200() throws Exception {
        mockJwtAuthenticationUtil.mockReturns();

        ResultActions result = mockMvc.perform(get("/users/logged")
                        .header("Authorization", "Bearer " + mockJwtAuthenticationUtil.getMockedToken()))
                .andExpect(status().isOk()).andExpect(content().contentType("application/json"));

        AssertUsersIntegrationUtil.assertUsers(result, loggedUserDTOExample);

        mockJwtAuthenticationUtil.verify(1);
    }

    @Test
    void editUsername401() throws Exception {
        EditUsernameRequestBody requestBody = new EditUsernameRequestBody("new username");

        mockMvc.perform(put("/users/{id}/editUsername", usersInDatabse.get(0).getId())
                        .contentType("application/json")
                        .content(requestBody.toJson()))
                .andExpect(status().isUnauthorized());

        mockJwtAuthenticationUtil.verify(0);
    }

    @Test
    void editUsername403() throws Exception {
        EditUsernameRequestBody requestBody = new EditUsernameRequestBody("new username");

        mockJwtAuthenticationUtil.mockReturns();

        mockMvc.perform(put("/users/{id}/editUsername", usersInDatabse.get(0).getId()) // Editing username, which does not belong to logged user
                        .contentType("application/json")
                        .content(requestBody.toJson())
                        .header("Authorization", "Bearer " + mockJwtAuthenticationUtil.getMockedToken()))
                .andExpect(status().isForbidden());

        mockJwtAuthenticationUtil.verify(1);
    }

    @Test
    void editUsername200() throws Exception {
        EditUsernameRequestBody requestBody = new EditUsernameRequestBody("new username");

        mockJwtAuthenticationUtil.mockReturns();

        ResultActions result = mockMvc.perform(put("/users/{id}/editUsername", usersInDatabse.get(9).getId())
                        .contentType("application/json")
                        .content(requestBody.toJson())
                        .header("Authorization", "Bearer " + mockJwtAuthenticationUtil.getMockedToken()))
                .andExpect(status().isOk()).andExpect(content().contentType("application/json"));

        AssertUsersIntegrationUtil.assertUsersInEditUsernameTest(result, usersInDatabse.get(9), requestBody.getUsername());

        mockJwtAuthenticationUtil.verify(1);
    }

    @Test
    void editUsername400Valid() throws Exception {
        mockJwtAuthenticationUtil.mockReturns();

        mockMvc.perform(put("/users/{id}/editUsername", usersInDatabse.get(1).getId())
                        .contentType("application/json")
                        .content("{}") // missing body
                        .header("Authorization", "Bearer " + mockJwtAuthenticationUtil.getMockedToken()))
                .andExpect(status().isBadRequest());

        mockJwtAuthenticationUtil.verify(1);
    }

    @Test
    void editUsername400Unique() throws Exception {
        EditUsernameRequestBody requestBody = new EditUsernameRequestBody("JEvans");

        mockJwtAuthenticationUtil.mockReturns();

        mockMvc.perform(put("/users/{id}/editUsername", usersInDatabse.get(9).getId())
                        .contentType("application/json")
                        .content(requestBody.toJson())
                        .header("Authorization", "Bearer " + mockJwtAuthenticationUtil.getMockedToken()))
                .andExpect(status().isBadRequest());

        mockJwtAuthenticationUtil.verify(1);
    }

    @Test
    void delete401() throws Exception {
        mockMvc.perform(delete("/users/{id}", usersInDatabse.get(0).getId()))
                .andExpect(status().isUnauthorized());

        mockJwtAuthenticationUtil.verify(0);
    }

    @Test
    void delete403() throws Exception {
        mockJwtAuthenticationUtil.mockReturns();

        mockMvc.perform(delete("/users/{id}", usersInDatabse.get(0).getId()) // Deleting user, which does not belong to logged user
                        .header("Authorization", "Bearer " + mockJwtAuthenticationUtil.getMockedToken()))
                .andExpect(status().isForbidden());

        mockJwtAuthenticationUtil.verify(1);
    }

    @Test
    void delete200() throws Exception {
        mockJwtAuthenticationUtil.mockReturns();

        mockMvc.perform(delete("/users/{id}", usersInDatabse.get(9).getId())
                        .header("Authorization", "Bearer " + mockJwtAuthenticationUtil.getMockedToken()))
                .andExpect(status().isOk());

        mockJwtAuthenticationUtil.verify(1);
    }

    @Test
    void findAllUsersWhoLikedPost401() throws Exception {
        mockMvc.perform(get("/users/likedPost").param("post", "3"))
                .andExpect(status().isUnauthorized());

        mockJwtAuthenticationUtil.verify(0);
    }

    @Test
    void findAllUsersWhoLikedPost404() throws Exception {
        mockJwtAuthenticationUtil.mockReturns();

        mockMvc.perform(get("/users/likedPost").param("post", "0")
                        .header("Authorization", "Bearer " + mockJwtAuthenticationUtil.getMockedToken()))
                .andExpect(status().isNotFound());

        mockJwtAuthenticationUtil.verify(1);
    }

    @Test
    void findAllUsersWhoLikedPost200() throws Exception {
        mockJwtAuthenticationUtil.mockReturns();

        ResultActions result = mockMvc.perform(get("/users/likedPost").param("post", "3")
                        .header("Authorization", "Bearer " + mockJwtAuthenticationUtil.getMockedToken()))
                .andExpect(status().isOk()).andExpect(content().contentType("application/json"));

        // post with id 3 has 2 user's likes in database
        AssertUsersIntegrationUtil.assertUsers(result, usersInDatabse.get(5), 1);
        AssertUsersIntegrationUtil.assertUsers(result, usersInDatabse.get(9), 0);

        mockJwtAuthenticationUtil.verify(1);
    }

    @Test
    void findAllFollowers401() throws Exception {
        mockMvc.perform(get("/users/followers").param("user", "1"))
                .andExpect(status().isUnauthorized());

        mockJwtAuthenticationUtil.verify(0);
    }

    @Test
    void findAllFollowers404() throws Exception {
        mockJwtAuthenticationUtil.mockReturns();

        mockMvc.perform(get("/users/followers").param("user", "0")
                        .header("Authorization", "Bearer " + mockJwtAuthenticationUtil.getMockedToken()))
                .andExpect(status().isNotFound());

        mockJwtAuthenticationUtil.verify(1);
    }

    @Test
    void findAllFollowers200() throws Exception {
        mockJwtAuthenticationUtil.mockReturns();

        ResultActions result = mockMvc.perform(get("/users/followers").param("user", "1")
                        .header("Authorization", "Bearer " + mockJwtAuthenticationUtil.getMockedToken()))
                .andExpect(status().isOk()).andExpect(content().contentType("application/json"));

        // user with id 2 has 2 followers in database
        AssertUsersIntegrationUtil.assertUsers(result, usersInDatabse.get(1), 0);
        AssertUsersIntegrationUtil.assertUsers(result, usersInDatabse.get(9), 1);

        mockJwtAuthenticationUtil.verify(1);
    }

    @Test
    void findCountOfAllFollowers401() throws Exception {
        mockMvc.perform(get("/users/followers/count").param("user", "2"))
                .andExpect(status().isUnauthorized());

        mockJwtAuthenticationUtil.verify(0);
    }

    @Test
    void findCountOfAllFollowers404() throws Exception {
        mockJwtAuthenticationUtil.mockReturns();

        mockMvc.perform(get("/users/followers/count").param("user", "0")
                        .header("Authorization", "Bearer " + mockJwtAuthenticationUtil.getMockedToken()))
                .andExpect(status().isNotFound());

        mockJwtAuthenticationUtil.verify(1);
    }

    @Test
    void findCountOfAllFollowers200() throws Exception {
        mockJwtAuthenticationUtil.mockReturns();

        ResultActions result = mockMvc.perform(get("/users/followers/count").param("user", "1")
                        .header("Authorization", "Bearer " + mockJwtAuthenticationUtil.getMockedToken()))
                .andExpect(status().isOk()).andExpect(content().contentType("application/json"));

        // user with id 2 follows user in database
        Assertions.assertEquals("2", result.andReturn().getResponse().getContentAsString());

        mockJwtAuthenticationUtil.verify(1);
    }

    @Test
    void findAllFollowing401() throws Exception {
        mockMvc.perform(get("/users/following").param("user", "2"))
                .andExpect(status().isUnauthorized());

        mockJwtAuthenticationUtil.verify(0);
    }

    @Test
    void findAllFollowing404() throws Exception {
        mockJwtAuthenticationUtil.mockReturns();

        mockMvc.perform(get("/users/following").param("user", "0")
                        .header("Authorization", "Bearer " + mockJwtAuthenticationUtil.getMockedToken()))
                .andExpect(status().isNotFound());

        mockJwtAuthenticationUtil.verify(1);
    }

    @Test
    void findAllFollowing200() throws Exception {
        mockJwtAuthenticationUtil.mockReturns();

        ResultActions result = mockMvc.perform(get("/users/following").param("user", "2")
                        .header("Authorization", "Bearer " + mockJwtAuthenticationUtil.getMockedToken()))
                .andExpect(status().isOk()).andExpect(content().contentType("application/json"));

        // user with id 2 follows user in database
        AssertUsersIntegrationUtil.assertUsers(result, usersInDatabse.get(0), 0);

        mockJwtAuthenticationUtil.verify(1);
    }

    @Test
    void findCountOfAllFollowing401() throws Exception {
        mockMvc.perform(get("/users/following/count").param("user", "2"))
                .andExpect(status().isUnauthorized());

        mockJwtAuthenticationUtil.verify(0);
    }

    @Test
    void findCountOfAllFollowing404() throws Exception {
        mockJwtAuthenticationUtil.mockReturns();

        mockMvc.perform(get("/users/following/count").param("user", "0")
                        .header("Authorization", "Bearer " + mockJwtAuthenticationUtil.getMockedToken()))
                .andExpect(status().isNotFound());

        mockJwtAuthenticationUtil.verify(1);
    }

    @Test
    void findCountOfAllFollowing200() throws Exception {
        mockJwtAuthenticationUtil.mockReturns();

        ResultActions result = mockMvc.perform(get("/users/following/count").param("user", "2")
                        .header("Authorization", "Bearer " + mockJwtAuthenticationUtil.getMockedToken()))
                .andExpect(status().isOk()).andExpect(content().contentType("application/json"));

        // user with id 2 follows user in database
        Assertions.assertEquals(result.andReturn().getResponse().getContentAsString(), "1");

        mockJwtAuthenticationUtil.verify(1);
    }
}

