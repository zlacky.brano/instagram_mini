package zlack.bra.instamini.presentation.controller;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import util.AssertFollowsIntegrationUtil;
import util.MockJwtAuthenticationUtil;
import zlack.bra.instamini.business.dto.*;
import zlack.bra.instamini.security.jwt.JwtUtil;

import java.util.ArrayList;
import java.util.List;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@Sql(scripts = "/sql-scripts/insert_script_test.sql", executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
@Sql(scripts = "/sql-scripts/delete_script_test.sql", executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
class FollowControllerIntegrationTest {

    @Autowired
    private MockMvc mockMvc;

    private MockJwtAuthenticationUtil mockJwtAuthenticationUtil;

    @MockBean
    private JwtUtil jwtUtil;

    private final UserDTO loggedUserDTOExample = new UserDTO(10, "olivia.johnson@test.com", "Olivia Johnson");

    private final UserDTO notLoggedUserDTOExample = new UserDTO(6, "thomas.wilson@test.com", "WilT");

    private final List<FollowDTO> followsInDatabase = new ArrayList<>() {
        {
            add(new FollowDTO(1,
                    new UserDTO(1, "john.smith@test.com", "JohnyS"),
                    new UserDTO(2, "george.jones@test.com", "_Georgie_")));
            add(new FollowDTO(2,
                    new UserDTO(2, "george.jones@test.com", "_Georgie_"),
                    new UserDTO(1, "john.smith@test.com", "JohnyS")));
            add(new FollowDTO(3,
                    notLoggedUserDTOExample,
                    new UserDTO(4, "harry.davies@test.com", "HarryDav")));
            add(new FollowDTO(4,
                    loggedUserDTOExample,
                    new UserDTO(1, "john.smith@test.com", "JohnyS")));
        }
    };

    @BeforeEach
    void setUp() {
        mockJwtAuthenticationUtil = new MockJwtAuthenticationUtil(jwtUtil, loggedUserDTOExample, "exists");
    }

    @Test
    void all401() throws Exception {
        mockMvc.perform(get("/follows")).andExpect(status().isUnauthorized());

        mockJwtAuthenticationUtil.verify(0);
    }

    @Test
    void all200() throws Exception {
        mockJwtAuthenticationUtil.mockReturns();

        ResultActions result = mockMvc.perform(get("/follows")
                        .header("Authorization", "Bearer " + mockJwtAuthenticationUtil.getMockedToken()))
                .andExpect(status().isOk()).andExpect(content().contentType("application/json"));

        for (int i = 0; i < followsInDatabase.size(); i++) {
            AssertFollowsIntegrationUtil.assertFollows(result, followsInDatabase.get(i), i);
        }

        mockJwtAuthenticationUtil.verify(1);
    }

    @Test
    void byID401() throws Exception {
        FollowDTO followDTOToFind = followsInDatabase.get(0);

        mockMvc.perform(get("/follows/{id}", followDTOToFind.getId())).andExpect(status().isUnauthorized());

        mockJwtAuthenticationUtil.verify(0);
    }

    @Test
    void byID404() throws Exception {
        mockJwtAuthenticationUtil.mockReturns();

        mockMvc.perform(get("/follows/{id}", 0)
                        .header("Authorization", "Bearer " + mockJwtAuthenticationUtil.getMockedToken()))
                .andExpect(status().isNotFound());

        mockJwtAuthenticationUtil.verify(1);
    }

    @Test
    void byID200() throws Exception {
        FollowDTO followDTOToFind = followsInDatabase.get(0);

        mockJwtAuthenticationUtil.mockReturns();

        ResultActions result = mockMvc.perform(get("/follows/{id}", followDTOToFind.getId())
                        .header("Authorization", "Bearer " + mockJwtAuthenticationUtil.getMockedToken()))
                .andExpect(status().isOk()).andExpect(content().contentType("application/json"));

        AssertFollowsIntegrationUtil.assertFollows(result, followDTOToFind);

        mockJwtAuthenticationUtil.verify(1);
    }

    @Test
    void create401() throws Exception {
        FollowCreateDTO followCreateDTO = new FollowCreateDTO(notLoggedUserDTOExample.getId(), loggedUserDTOExample.getId());

        mockMvc.perform(post("/follows")
                        .contentType("application/json")
                        .content(followCreateDTO.toJson()))
                .andExpect(status().isUnauthorized());

        mockJwtAuthenticationUtil.verify(0);
    }

    @Test
    void create403() throws Exception {
        FollowCreateDTO followCreateDTO = new FollowCreateDTO(notLoggedUserDTOExample.getId(), loggedUserDTOExample.getId());

        mockJwtAuthenticationUtil.mockReturns();

        mockMvc.perform(post("/follows")
                        .contentType("application/json")
                        .content(followCreateDTO.toJson())
                        .header("Authorization", "Bearer " + mockJwtAuthenticationUtil.getMockedToken()))
                .andExpect(status().isForbidden());

        mockJwtAuthenticationUtil.verify(1);
    }

    @Test
    void create404() throws Exception {
        FollowCreateDTO followCreateDTO = new FollowCreateDTO(loggedUserDTOExample.getId(), 0);

        mockJwtAuthenticationUtil.mockReturns();

        mockMvc.perform(post("/follows")
                        .contentType("application/json")
                        .content(followCreateDTO.toJson())
                        .header("Authorization", "Bearer " + mockJwtAuthenticationUtil.getMockedToken()))
                .andExpect(status().isNotFound());

        mockJwtAuthenticationUtil.verify(1);
    }

    @Test
    void create201() throws Exception {
        FollowCreateDTO followCreateDTO = new FollowCreateDTO(loggedUserDTOExample.getId(), notLoggedUserDTOExample.getId());

        mockJwtAuthenticationUtil.mockReturns();

        ResultActions result = mockMvc.perform(post("/follows")
                        .contentType("application/json")
                        .content(followCreateDTO.toJson())
                        .header("Authorization", "Bearer " + mockJwtAuthenticationUtil.getMockedToken()))
                .andExpect(status().isCreated()).andExpect(content().contentType("application/json"));

        AssertFollowsIntegrationUtil.assertFollows(result, followCreateDTO);

        mockJwtAuthenticationUtil.verify(1);
    }

    @Test
    void create400FollowYourself() throws Exception {
        FollowCreateDTO followCreateDTO = new FollowCreateDTO(loggedUserDTOExample.getId(), loggedUserDTOExample.getId());

        mockJwtAuthenticationUtil.mockReturns();

        mockMvc.perform(post("/follows")
                        .contentType("application/json")
                        .content(followCreateDTO.toJson())
                        .header("Authorization", "Bearer " + mockJwtAuthenticationUtil.getMockedToken()))
                .andExpect(status().isBadRequest());

        mockJwtAuthenticationUtil.verify(1);
    }

    @Test
    void create400Valid() throws Exception {
        FollowCreateDTO followCreateDTO = new FollowCreateDTO(loggedUserDTOExample.getId(), notLoggedUserDTOExample.getId());

        mockJwtAuthenticationUtil.mockReturns();

        mockMvc.perform(post("/follows")
                        .contentType("application/json")
                        .content("{\"userWhoId\":\"" + followCreateDTO.getUserWhoId() + "\"}") // missing body
                        .header("Authorization", "Bearer " + mockJwtAuthenticationUtil.getMockedToken()))
                .andExpect(status().isBadRequest());

        mockJwtAuthenticationUtil.verify(1);
    }

    @Test
    void create400Unique() throws Exception {
        FollowCreateDTO followCreateDTO = new FollowCreateDTO(loggedUserDTOExample.getId(), followsInDatabase.get(3).getUserWhom().getId());

        mockJwtAuthenticationUtil.mockReturns();

        mockMvc.perform(post("/follows")
                        .contentType("application/json")
                        .content(followCreateDTO.toJson())
                        .header("Authorization", "Bearer " + mockJwtAuthenticationUtil.getMockedToken()))
                .andExpect(status().isBadRequest());

        mockJwtAuthenticationUtil.verify(1);
    }

    @Test
    void delete401() throws Exception {
        mockMvc.perform(delete("/follows/{id}", followsInDatabase.get(0).getId()))
                .andExpect(status().isUnauthorized());

        mockJwtAuthenticationUtil.verify(0);
    }

    @Test
    void delete403() throws Exception {
        mockJwtAuthenticationUtil.mockReturns();

        mockMvc.perform(delete("/follows/{id}", followsInDatabase.get(0).getId()) // Deleting follow, which does not belong to logged user
                        .header("Authorization", "Bearer " + mockJwtAuthenticationUtil.getMockedToken()))
                .andExpect(status().isForbidden());

        mockJwtAuthenticationUtil.verify(1);
    }

    @Test
    void delete404() throws Exception {
        mockJwtAuthenticationUtil.mockReturns();

        mockMvc.perform(delete("/follows/{id}", 0)
                        .header("Authorization", "Bearer " + mockJwtAuthenticationUtil.getMockedToken()))
                .andExpect(status().isNotFound());

        mockJwtAuthenticationUtil.verify(1);
    }

    @Test
    void delete200() throws Exception {
        mockJwtAuthenticationUtil.mockReturns();

        mockMvc.perform(delete("/follows/{id}", followsInDatabase.get(3).getId())
                        .header("Authorization", "Bearer " + mockJwtAuthenticationUtil.getMockedToken()))
                .andExpect(status().isOk());

        mockJwtAuthenticationUtil.verify(1);
    }

    @Test
    void findFollowIdByUsers200() throws Exception {
        mockJwtAuthenticationUtil.mockReturns();

        ResultActions result = mockMvc.perform(get("/follows/isFollowed/{userIdWhom}", followsInDatabase.get(3).getUserWhom().getId())
                        .header("Authorization", "Bearer " + mockJwtAuthenticationUtil.getMockedToken()))
                .andExpect(status().isOk()).andExpect(content().contentType("application/json"));

        Assertions.assertEquals(result.andReturn().getResponse().getContentAsString(), "4");

        mockJwtAuthenticationUtil.verify(1);
    }

    @Test
    void findFollowIdByUsers401() throws Exception {
        mockMvc.perform(get("/follows/isFollowed/{userIdWhom}", followsInDatabase.get(0).getUserWhom().getId()))
                .andExpect(status().isUnauthorized());

        mockJwtAuthenticationUtil.verify(0);
    }

    @Test
    void findFollowIdByUsers404() throws Exception {
        mockJwtAuthenticationUtil.mockReturns();

        mockMvc.perform(get("/follows/isFollowed/{userIdWhom}", 0)
                        .header("Authorization", "Bearer " + mockJwtAuthenticationUtil.getMockedToken()))
                .andExpect(status().isNotFound());

        mockJwtAuthenticationUtil.verify(1);
    }
}