package zlack.bra.instamini.presentation.controller;

import org.json.JSONObject;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import util.AssertPostsIntegrationUtil;
import util.MockJwtAuthenticationUtil;
import zlack.bra.instamini.business.dto.PostCreateDTO;
import zlack.bra.instamini.business.dto.PostDTO;
import zlack.bra.instamini.business.dto.UserDTO;
import zlack.bra.instamini.business.service.ImageUploadService;
import zlack.bra.instamini.presentation.controller.request.EditPostDescriptionRequestBody;
import zlack.bra.instamini.security.jwt.JwtUtil;

import java.io.FileInputStream;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@SpringBootTest
@AutoConfigureMockMvc
@Sql(scripts = "/sql-scripts/insert_script_test.sql", executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
@Sql(scripts = "/sql-scripts/delete_script_test.sql", executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
class PostControllerIntegrationTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ImageUploadService imageUploadService;

    private MockJwtAuthenticationUtil mockJwtAuthenticationUtil;

    @MockBean
    private JwtUtil jwtUtil;

    private final UserDTO loggedUserDTOExample = new UserDTO(10, "olivia.johnson@test.com", "Olivia Johnson");

    private final UserDTO notLoggedUserDTOExample = new UserDTO(6, "thomas.wilson@test.com", "WilT");

    private final List<PostDTO> postsInDatabase = new ArrayList<>() {
        {
            add(new PostDTO(1, "Morbi quis tortor id nulla ultrices aliquet. Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui. Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc.",
                    "https://res.cloudinary.com/instamini/image/upload/v1630672586/husky-3380548_1280_rman9o.jpg",
                    LocalDateTime.of(2020, 4, 5, 22, 52, 37),
                    notLoggedUserDTOExample));
            add(new PostDTO(2, "Nullam orci pede, venenatis non, sodales sed, tincidunt eu, felis. Fusce posuere felis sed lacus. Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl. Nunc rhoncus dui vel sem. Sed sagittis. Nam congue, risus semper porta volutpat, quam pede lobortis ligula, sit amet eleifend pede libero quis orci. Nullam molestie nibh in lectus. Pellentesque at nulla.",
                    "https://res.cloudinary.com/instamini/image/upload/v1630672584/dolphin-203875_1280_n67crm.jpg",
                    LocalDateTime.of(2020, 12, 20, 7, 51, 2),
                    loggedUserDTOExample));
            add(new PostDTO(3, "In est risus, auctor sed, tristique in, tempus sit amet, sem. Fusce consequat. Nulla nisl. Nunc nisl. Duis bibendum, felis sed interdum venenatis, turpis enim blandit mi, in porttitor pede justo eu massa. Donec dapibus. Duis at velit eu est congue elementum. In hac habitasse platea dictumst. Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante. Nulla justo.",
                    "https://res.cloudinary.com/instamini/image/upload/v1630672583/pug-690566_1280_s4wmbu.jpg",
                    LocalDateTime.of(2021, 5, 25, 12, 12, 22),
                    new UserDTO(2, "george.jones@test.com", "_Georgie_")));
            add(new PostDTO(4, "Nulla ac enim. In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem.",
                    "https://res.cloudinary.com/instamini/image/upload/v1630672586/husky-3380548_1280_rman9o.jpg",
                    LocalDateTime.of(2021, 2, 6, 16, 15, 7),
                    loggedUserDTOExample));
        }
    };

    @BeforeEach
    void setUp() {
        mockJwtAuthenticationUtil = new MockJwtAuthenticationUtil(jwtUtil, loggedUserDTOExample, "exists");
    }

    @Test
    void all401() throws Exception {
        mockMvc.perform(get("/posts")).andExpect(status().isUnauthorized());

        mockJwtAuthenticationUtil.verify(0);
    }

    @Test
    void all200() throws Exception {
        mockJwtAuthenticationUtil.mockReturns();

        ResultActions result = mockMvc.perform(get("/posts")
                        .header("Authorization", "Bearer " + mockJwtAuthenticationUtil.getMockedToken()))
                .andExpect(status().isOk()).andExpect(content().contentType("application/json"));

        for (int i = 0; i < postsInDatabase.size(); i++) {
            AssertPostsIntegrationUtil.assertPosts(result, postsInDatabase.get(i), i);
        }

        mockJwtAuthenticationUtil.verify(1);
    }

    @Test
    void byID401() throws Exception {
        PostDTO postDTOToFind = postsInDatabase.get(0);

        mockMvc.perform(get("/posts/{id}", postDTOToFind.getId())).andExpect(status().isUnauthorized());

        mockJwtAuthenticationUtil.verify(0);
    }

    @Test
    void byID404() throws Exception {
        mockJwtAuthenticationUtil.mockReturns();

        mockMvc.perform(get("/posts/{id}", 0)
                        .header("Authorization", "Bearer " + mockJwtAuthenticationUtil.getMockedToken()))
                .andExpect(status().isNotFound());

        mockJwtAuthenticationUtil.verify(1);
    }

    @Test
    void byID200() throws Exception {
        PostDTO postDTOToFind = postsInDatabase.get(0);

        mockJwtAuthenticationUtil.mockReturns();

        ResultActions result = mockMvc.perform(get("/posts/{id}", postDTOToFind.getId())
                        .header("Authorization", "Bearer " + mockJwtAuthenticationUtil.getMockedToken()))
                .andExpect(status().isOk()).andExpect(content().contentType("application/json"));

        AssertPostsIntegrationUtil.assertPosts(result, postDTOToFind);

        mockJwtAuthenticationUtil.verify(1);
    }

    @Test
    void create401() throws Exception {
        MockMultipartFile imagePart = new MockMultipartFile("image", new FileInputStream(System.getProperty("user.dir") + "/src/integrationTest/resources/image-test/test.png"));
        PostCreateDTO postCreateDTO = new PostCreateDTO("test", notLoggedUserDTOExample.getId(), imagePart);

        mockMvc.perform(multipart("/posts")
                        .file(imagePart)
                        .param("userId", postCreateDTO.getUserId().toString())
                        .param("description", postCreateDTO.getDescription()))
                .andExpect(status().isUnauthorized());

        mockJwtAuthenticationUtil.verify(0);
    }

    @Test
    void create403() throws Exception {
        MockMultipartFile imagePart = new MockMultipartFile("image", new FileInputStream(System.getProperty("user.dir") + "/src/integrationTest/resources/image-test/test.png"));
        PostCreateDTO postCreateDTO = new PostCreateDTO("test", notLoggedUserDTOExample.getId(), imagePart);

        mockJwtAuthenticationUtil.mockReturns();

        mockMvc.perform(multipart("/posts")
                        .file(imagePart)
                        .param("userId", postCreateDTO.getUserId().toString())
                        .param("description", postCreateDTO.getDescription())
                        .header("Authorization", "Bearer " + mockJwtAuthenticationUtil.getMockedToken()))
                .andExpect(status().isForbidden());

        mockJwtAuthenticationUtil.verify(1);
    }

    @Test
    void create201() throws Exception {
        MockMultipartFile imagePart = new MockMultipartFile("image", new FileInputStream(System.getProperty("user.dir") + "/src/integrationTest/resources/image-test/test.png"));
        PostCreateDTO postCreateDTO = new PostCreateDTO("test", loggedUserDTOExample.getId(), imagePart);

        mockJwtAuthenticationUtil.mockReturns();

        ResultActions result = mockMvc.perform(multipart("/posts")
                        .file(imagePart)
                        .param("userId", postCreateDTO.getUserId().toString())
                        .param("description", postCreateDTO.getDescription())
                        .header("Authorization", "Bearer " + mockJwtAuthenticationUtil.getMockedToken()))
                .andExpect(status().isCreated()).andExpect(content().contentType("application/json"));

        try {
            AssertPostsIntegrationUtil.assertPosts(result, postCreateDTO);

            mockJwtAuthenticationUtil.verify(1);
        } finally {
            JSONObject json = new JSONObject(result.andReturn().getResponse().getContentAsString());
            String photoUrl = json.get("photoUrl").toString();
            String publicId = photoUrl.substring(photoUrl.lastIndexOf("/") + 1, photoUrl.lastIndexOf("."));
            imageUploadService.destroy(publicId);
        }
    }

    @Test
    void create400() throws Exception {
        MockMultipartFile imagePart = new MockMultipartFile("image", new FileInputStream(System.getProperty("user.dir") + "/src/integrationTest/resources/image-test/not_image.txt"));
        PostCreateDTO postCreateDTO = new PostCreateDTO("test", loggedUserDTOExample.getId(), imagePart);

        mockJwtAuthenticationUtil.mockReturns();

        mockMvc.perform(multipart("/posts")
                        .file(imagePart)
                        .param("userId", postCreateDTO.getUserId().toString())
                        .param("description", postCreateDTO.getDescription())
                        .header("Authorization", "Bearer " + mockJwtAuthenticationUtil.getMockedToken()))
                .andExpect(status().isBadRequest());

        mockJwtAuthenticationUtil.verify(1);
    }

    @Test
    void create400Valid() throws Exception {
        MockMultipartFile imagePart = new MockMultipartFile("image", new FileInputStream(System.getProperty("user.dir") + "/src/integrationTest/resources/image-test/test.png"));
        PostCreateDTO postCreateDTO = new PostCreateDTO("test", loggedUserDTOExample.getId(), imagePart);

        mockJwtAuthenticationUtil.mockReturns();

        mockMvc.perform(multipart("/posts")
                        .file(imagePart)
                        .param("userId", postCreateDTO.getUserId().toString())
                        .header("Authorization", "Bearer " + mockJwtAuthenticationUtil.getMockedToken()))
                .andExpect(status().isBadRequest());

        mockJwtAuthenticationUtil.verify(1);
    }

    @Test
    void editDescription401() throws Exception {
        EditPostDescriptionRequestBody requestBody = new EditPostDescriptionRequestBody("new description");

        mockMvc.perform(put("/posts/{id}/editDescription", postsInDatabase.get(0).getId())
                        .contentType("application/json")
                        .content(requestBody.toJson()))
                .andExpect(status().isUnauthorized());

        mockJwtAuthenticationUtil.verify(0);
    }

    @Test
    void editDescription403() throws Exception {
        EditPostDescriptionRequestBody requestBody = new EditPostDescriptionRequestBody("new description");

        mockJwtAuthenticationUtil.mockReturns();

        mockMvc.perform(put("/posts/{id}/editDescription", postsInDatabase.get(0).getId()) // Editing post, which does not belong to logged user
                        .contentType("application/json")
                        .content(requestBody.toJson())
                        .header("Authorization", "Bearer " + mockJwtAuthenticationUtil.getMockedToken()))
                .andExpect(status().isForbidden());

        mockJwtAuthenticationUtil.verify(1);
    }

    @Test
    void editDescription404() throws Exception {
        EditPostDescriptionRequestBody requestBody = new EditPostDescriptionRequestBody("new description");

        mockJwtAuthenticationUtil.mockReturns();

        mockMvc.perform(put("/posts/{id}/editDescription", 0)
                        .contentType("application/json")
                        .content(requestBody.toJson())
                        .header("Authorization", "Bearer " + mockJwtAuthenticationUtil.getMockedToken()))
                .andExpect(status().isNotFound());

        mockJwtAuthenticationUtil.verify(1);
    }

    @Test
    void editDescription200() throws Exception {
        EditPostDescriptionRequestBody requestBody = new EditPostDescriptionRequestBody("new description");

        mockJwtAuthenticationUtil.mockReturns();


        ResultActions result = mockMvc.perform(put("/posts/{id}/editDescription", postsInDatabase.get(1).getId())
                        .contentType("application/json")
                        .content(requestBody.toJson())
                        .header("Authorization", "Bearer " + mockJwtAuthenticationUtil.getMockedToken()))
                .andExpect(status().isOk()).andExpect(content().contentType("application/json"));

        AssertPostsIntegrationUtil.assertPostsInEditDescriptionTest(result, postsInDatabase.get(1), requestBody.getNewDescription());

        mockJwtAuthenticationUtil.verify(1);
    }

    @Test
    void editDescription400Valid() throws Exception {
        mockJwtAuthenticationUtil.mockReturns();

        mockMvc.perform(put("/posts/{id}/editDescription", postsInDatabase.get(1).getId())
                        .contentType("application/json")
                        .content("{}") // missing body
                        .header("Authorization", "Bearer " + mockJwtAuthenticationUtil.getMockedToken()))
                .andExpect(status().isBadRequest());

        mockJwtAuthenticationUtil.verify(1);
    }

    @Test
    void delete401() throws Exception {
        mockMvc.perform(delete("/posts/{id}", postsInDatabase.get(0).getId()))
                .andExpect(status().isUnauthorized());

        mockJwtAuthenticationUtil.verify(0);
    }

    @Test
    void delete403() throws Exception {
        mockJwtAuthenticationUtil.mockReturns();

        mockMvc.perform(delete("/posts/{id}", postsInDatabase.get(0).getId()) // Deleting post, which does not belong to logged user
                        .header("Authorization", "Bearer " + mockJwtAuthenticationUtil.getMockedToken()))
                .andExpect(status().isForbidden());

        mockJwtAuthenticationUtil.verify(1);
    }

    @Test
    void delete404() throws Exception {
        mockJwtAuthenticationUtil.mockReturns();

        mockMvc.perform(delete("/posts/{id}", 0)
                        .header("Authorization", "Bearer " + mockJwtAuthenticationUtil.getMockedToken()))
                .andExpect(status().isNotFound());

        mockJwtAuthenticationUtil.verify(1);
    }

    @Test
    void delete200() throws Exception {
        mockJwtAuthenticationUtil.mockReturns();

        mockMvc.perform(delete("/posts/{id}", postsInDatabase.get(1).getId())
                        .header("Authorization", "Bearer " + mockJwtAuthenticationUtil.getMockedToken()))
                .andExpect(status().isOk());

        mockJwtAuthenticationUtil.verify(1);
    }

    @Test
    void findAllPostsOfUser401() throws Exception {
        mockMvc.perform(get("/posts/ofUser").param("user", loggedUserDTOExample.getId().toString()))
                .andExpect(status().isUnauthorized());

        mockJwtAuthenticationUtil.verify(0);
    }

    @Test
    void findAllPostsOfUser404() throws Exception {
        mockJwtAuthenticationUtil.mockReturns();

        mockMvc.perform(get("/posts/ofUser")
                        .param("user", "0")
                        .header("Authorization", "Bearer " + mockJwtAuthenticationUtil.getMockedToken()))
                .andExpect(status().isNotFound());

        mockJwtAuthenticationUtil.verify(1);
    }

    @Test
    void findAllPostsOfUser200() throws Exception {
        mockJwtAuthenticationUtil.mockReturns();

        ResultActions result = mockMvc.perform(get("/posts/ofUser")
                        .param("user", loggedUserDTOExample.getId().toString())
                        .header("Authorization", "Bearer " + mockJwtAuthenticationUtil.getMockedToken()))
                .andExpect(status().isOk()).andExpect(content().contentType("application/json"));

        // logged user has 2 posts in database
        AssertPostsIntegrationUtil.assertPosts(result, postsInDatabase.get(3), 0);
        AssertPostsIntegrationUtil.assertPosts(result, postsInDatabase.get(1), 1);

        mockJwtAuthenticationUtil.verify(1);
    }

    @Test
    void findCountOfAllPostsOfUser401() throws Exception {
        mockMvc.perform(get("/posts/count").param("user", loggedUserDTOExample.getId().toString()))
                .andExpect(status().isUnauthorized());

        mockJwtAuthenticationUtil.verify(0);
    }

    @Test
    void findCountOfAllPostsOfUser404() throws Exception {
        mockJwtAuthenticationUtil.mockReturns();

        mockMvc.perform(get("/posts/count")
                        .param("user", "0")
                        .header("Authorization", "Bearer " + mockJwtAuthenticationUtil.getMockedToken()))
                .andExpect(status().isNotFound());

        mockJwtAuthenticationUtil.verify(1);
    }

    @Test
    void findCountOfAllPostsOfUser200() throws Exception {
        mockJwtAuthenticationUtil.mockReturns();

        ResultActions result = mockMvc.perform(get("/posts/count")
                        .param("user", loggedUserDTOExample.getId().toString())
                        .header("Authorization", "Bearer " + mockJwtAuthenticationUtil.getMockedToken()))
                .andExpect(status().isOk()).andExpect(content().contentType("application/json"));

        result.andExpect(content().string("2")); // logged user has 2 posts in database

        mockJwtAuthenticationUtil.verify(1);
    }
}