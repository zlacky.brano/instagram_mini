package util;

import org.springframework.test.web.servlet.ResultActions;
import zlack.bra.instamini.business.dto.FollowCreateDTO;
import zlack.bra.instamini.business.dto.FollowDTO;
import zlack.bra.instamini.business.dto.PostCreateDTO;
import zlack.bra.instamini.business.dto.PostDTO;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

public class AssertFollowsIntegrationUtil {

    public static void assertFollows(ResultActions result, FollowDTO follow, int indexInResult) throws Exception {
        result.andExpect(jsonPath("$["+indexInResult+"].id").value(follow.getId()))
                .andExpect(jsonPath("$["+indexInResult+"].userWho").value(follow.getUserWho()))
                .andExpect(jsonPath("$["+indexInResult+"].userWhom").value(follow.getUserWhom()));
    }

    public static void assertFollows(ResultActions result, FollowDTO follow) throws Exception {
        result.andExpect(jsonPath("$.id").value(follow.getId()))
                .andExpect(jsonPath("$.userWho").value(follow.getUserWho()))
                .andExpect(jsonPath("$.userWhom").value(follow.getUserWhom()));
    }

    public static void assertFollows(ResultActions result, FollowCreateDTO followCreateDTO) throws Exception {
        result.andExpect(jsonPath("$.id").isNotEmpty())
                .andExpect(jsonPath("$.userWho.id").value(followCreateDTO.getUserWhoId()))
                .andExpect(jsonPath("$.userWhom.id").value(followCreateDTO.getUserWhomId()));
    }
}
