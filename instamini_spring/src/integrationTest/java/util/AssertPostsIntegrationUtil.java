package util;

import org.springframework.test.web.servlet.ResultActions;
import zlack.bra.instamini.business.dto.PostCreateDTO;
import zlack.bra.instamini.business.dto.PostDTO;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

public class AssertPostsIntegrationUtil {

    public static void assertPosts(ResultActions result, PostDTO post, int indexInResult) throws Exception {
        result.andExpect(jsonPath("$["+indexInResult+"].id").value(post.getId()))
                .andExpect(jsonPath("$["+indexInResult+"].description").value(post.getDescription()))
                .andExpect(jsonPath("$["+indexInResult+"].time").value(post.getTime().toString()))
                .andExpect(jsonPath("$["+indexInResult+"].user").value(post.getUser()))
                .andExpect(jsonPath("$["+indexInResult+"].photoUrl").value(post.getPhotoUrl()));
    }

    public static void assertPosts(ResultActions result, PostDTO post) throws Exception {
        result.andExpect(jsonPath("$.id").value(post.getId()))
                .andExpect(jsonPath("$.description").value(post.getDescription()))
                .andExpect(jsonPath("$.time").value(post.getTime().toString()))
                .andExpect(jsonPath("$.user").value(post.getUser()))
                .andExpect(jsonPath("$.photoUrl").value(post.getPhotoUrl()));
    }

    public static void assertPostsInEditDescriptionTest(ResultActions result, PostDTO post, String newDescription) throws Exception {
        result.andExpect(jsonPath("$.id").value(post.getId()))
                .andExpect(jsonPath("$.description").value(newDescription))
                .andExpect(jsonPath("$.time").value(post.getTime().toString()))
                .andExpect(jsonPath("$.user").value(post.getUser()))
                .andExpect(jsonPath("$.photoUrl").value(post.getPhotoUrl()));
    }

    public static void assertPosts(ResultActions result, PostCreateDTO postCreateDTO) throws Exception {
        result.andExpect(jsonPath("$.id").isNotEmpty())
                .andExpect(jsonPath("$.description").value(postCreateDTO.getDescription()))
                .andExpect(jsonPath("$.time").isNotEmpty())
                .andExpect(jsonPath("$.user.id").value(postCreateDTO.getUserId()))
                .andExpect(jsonPath("$.photoUrl").isNotEmpty());
    }
}
