package util;

import org.springframework.test.web.servlet.ResultActions;
import zlack.bra.instamini.business.dto.PostCreateDTO;
import zlack.bra.instamini.business.dto.PostDTO;
import zlack.bra.instamini.business.dto.UserCreateDTO;
import zlack.bra.instamini.business.dto.UserDTO;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

public class AssertUsersIntegrationUtil {

    public static void assertUsers(ResultActions result, UserDTO user, int indexInResult) throws Exception {
        result.andExpect(jsonPath("$["+indexInResult+"].id").value(user.getId()))
                .andExpect(jsonPath("$["+indexInResult+"].username").value(user.getUsername()))
                .andExpect(jsonPath("$["+indexInResult+"].email").value(user.getEmail()));
    }

    public static void assertUsers(ResultActions result, UserDTO user) throws Exception {
        result.andExpect(jsonPath("$.id").value(user.getId()))
                .andExpect(jsonPath("$.username").value(user.getUsername()))
                .andExpect(jsonPath("$.email").value(user.getEmail()));
    }

    public static void assertUsersInEditUsernameTest(ResultActions result, UserDTO user, String newUsername) throws Exception {
        result.andExpect(jsonPath("$.id").value(user.getId()))
                .andExpect(jsonPath("$.username").value(newUsername))
                .andExpect(jsonPath("$.email").value(user.getEmail()));
    }

    public static void assertUsers(ResultActions result, UserCreateDTO userCreateDTO) throws Exception {
        result.andExpect(jsonPath("$.id").isNotEmpty())
                .andExpect(jsonPath("$.username").value(userCreateDTO.getUsername()))
                .andExpect(jsonPath("$.email").value(userCreateDTO.getEmail()));
    }
}
