package util;

import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.jupiter.api.Assertions;
import org.springframework.test.web.servlet.ResultActions;
import zlack.bra.instamini.business.dto.FollowCreateDTO;
import zlack.bra.instamini.business.dto.FollowDTO;
import zlack.bra.instamini.business.dto.LikeCreateDTO;
import zlack.bra.instamini.business.dto.LikeDTO;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

public class AssertLikesIntegrationUtil {

    public static void assertLikes(ResultActions result, LikeDTO like, int indexInResult) throws Exception {
        JSONArray json = new JSONArray(result.andReturn().getResponse().getContentAsString());
        JSONObject likeObject = json.getJSONObject(indexInResult);
        JSONObject postObject = likeObject.getJSONObject("post");
        JSONObject userOfPostObject = postObject.getJSONObject("user");

        Assertions.assertEquals(postObject.get("id"), like.getPost().getId());
        Assertions.assertEquals(postObject.get("photoUrl"), like.getPost().getPhotoUrl());
        Assertions.assertEquals(postObject.get("description"), like.getPost().getDescription());
        Assertions.assertEquals(postObject.get("time"), like.getPost().getTime().toString());

        Assertions.assertEquals(userOfPostObject.get("id"), like.getPost().getUser().getId());
        Assertions.assertEquals(userOfPostObject.get("username"), like.getPost().getUser().getUsername());
        Assertions.assertEquals(userOfPostObject.get("email"), like.getPost().getUser().getEmail());

        result.andExpect(jsonPath("$["+indexInResult+"].id").value(like.getId()))
                .andExpect(jsonPath("$["+indexInResult+"].user").value(like.getUser()));
    }

    public static void assertLikes(ResultActions result, LikeDTO like) throws Exception {
        JSONObject json = new JSONObject(result.andReturn().getResponse().getContentAsString());
        JSONObject postObject = json.getJSONObject("post");
        JSONObject userOfPostObject = postObject.getJSONObject("user");

        Assertions.assertEquals(postObject.get("id"), like.getPost().getId());
        Assertions.assertEquals(postObject.get("photoUrl"), like.getPost().getPhotoUrl());
        Assertions.assertEquals(postObject.get("description"), like.getPost().getDescription());
        Assertions.assertEquals(postObject.get("time"), like.getPost().getTime().toString());

        Assertions.assertEquals(userOfPostObject.get("id"), like.getPost().getUser().getId());
        Assertions.assertEquals(userOfPostObject.get("username"), like.getPost().getUser().getUsername());
        Assertions.assertEquals(userOfPostObject.get("email"), like.getPost().getUser().getEmail());

        result.andExpect(jsonPath("$.id").value(like.getId()))
                .andExpect(jsonPath("$.user").value(like.getUser()));
    }

    public static void assertLikes(ResultActions result, LikeCreateDTO likeCreateDTO) throws Exception {
        result.andExpect(jsonPath("$.id").isNotEmpty())
                .andExpect(jsonPath("$.user.id").value(likeCreateDTO.getUserId()))
                .andExpect(jsonPath("$.post.id").value(likeCreateDTO.getPostId()));
    }
}
