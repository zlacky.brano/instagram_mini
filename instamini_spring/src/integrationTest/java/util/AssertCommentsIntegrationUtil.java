package util;

import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.jupiter.api.Assertions;
import org.springframework.test.web.servlet.ResultActions;
import zlack.bra.instamini.business.dto.CommentCreateDTO;
import zlack.bra.instamini.business.dto.CommentDTO;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

public class AssertCommentsIntegrationUtil {

    public static void assertComments(ResultActions result, CommentDTO comment, int indexInResult) throws Exception {
        JSONArray json = new JSONArray(result.andReturn().getResponse().getContentAsString());
        JSONObject commentObject = json.getJSONObject(indexInResult);
        JSONObject postObject = commentObject.getJSONObject("post");
        JSONObject userOfPostObject = postObject.getJSONObject("user");

        Assertions.assertEquals(postObject.get("id"), comment.getPost().getId());
        Assertions.assertEquals(postObject.get("photoUrl"), comment.getPost().getPhotoUrl());
        Assertions.assertEquals(postObject.get("description"), comment.getPost().getDescription());
        Assertions.assertEquals(postObject.get("time"), comment.getPost().getTime().toString());

        Assertions.assertEquals(userOfPostObject.get("id"), comment.getPost().getUser().getId());
        Assertions.assertEquals(userOfPostObject.get("username"), comment.getPost().getUser().getUsername());
        Assertions.assertEquals(userOfPostObject.get("email"), comment.getPost().getUser().getEmail());

        result.andExpect(jsonPath("$["+indexInResult+"].id").value(comment.getId()))
                .andExpect(jsonPath("$["+indexInResult+"].text").value(comment.getText()))
                .andExpect(jsonPath("$["+indexInResult+"].time").value(comment.getTime().toString()))
                .andExpect(jsonPath("$["+indexInResult+"].user").value(comment.getUser()));
    }

    public static void assertComments(ResultActions result, CommentDTO comment) throws Exception {
        JSONObject json = new JSONObject(result.andReturn().getResponse().getContentAsString());
        JSONObject postObject = json.getJSONObject("post");
        JSONObject userOfPostObject = postObject.getJSONObject("user");

        Assertions.assertEquals(postObject.get("id"), comment.getPost().getId());
        Assertions.assertEquals(postObject.get("photoUrl"), comment.getPost().getPhotoUrl());
        Assertions.assertEquals(postObject.get("description"), comment.getPost().getDescription());
        Assertions.assertEquals(postObject.get("time"), comment.getPost().getTime().toString());

        Assertions.assertEquals(userOfPostObject.get("id"), comment.getPost().getUser().getId());
        Assertions.assertEquals(userOfPostObject.get("username"), comment.getPost().getUser().getUsername());
        Assertions.assertEquals(userOfPostObject.get("email"), comment.getPost().getUser().getEmail());

        result.andExpect(jsonPath("$.id").value(comment.getId()))
                .andExpect(jsonPath("$.text").value(comment.getText()))
                .andExpect(jsonPath("$.time").value(comment.getTime().toString()))
                .andExpect(jsonPath("$.user").value(comment.getUser()));
    }

    public static void assertComments(ResultActions result, CommentCreateDTO commentCreateDTO) throws Exception {
        result.andExpect(jsonPath("$.id").isNotEmpty())
                .andExpect(jsonPath("$.text").value(commentCreateDTO.getText()))
                .andExpect(jsonPath("$.time").isNotEmpty())
                .andExpect(jsonPath("$.user.id").value(commentCreateDTO.getUserId()))
                .andExpect(jsonPath("$.post.id").value(commentCreateDTO.getPostId()));
    }

    public static void assertCommentsInEditTextTest(ResultActions result, CommentDTO comment, String newText) throws Exception {
        JSONObject json = new JSONObject(result.andReturn().getResponse().getContentAsString());
        JSONObject postObject = json.getJSONObject("post");
        JSONObject userOfPostObject = postObject.getJSONObject("user");

        Assertions.assertEquals(postObject.get("id"), comment.getPost().getId());
        Assertions.assertEquals(postObject.get("photoUrl"), comment.getPost().getPhotoUrl());
        Assertions.assertEquals(postObject.get("description"), comment.getPost().getDescription());
        Assertions.assertEquals(postObject.get("time"), comment.getPost().getTime().toString());

        Assertions.assertEquals(userOfPostObject.get("id"), comment.getPost().getUser().getId());
        Assertions.assertEquals(userOfPostObject.get("username"), comment.getPost().getUser().getUsername());
        Assertions.assertEquals(userOfPostObject.get("email"), comment.getPost().getUser().getEmail());

        result.andExpect(jsonPath("$.id").value(comment.getId()))
                .andExpect(jsonPath("$.text").value(newText))
                .andExpect(jsonPath("$.time").value(comment.getTime().toString()))
                .andExpect(jsonPath("$.user").value(comment.getUser()));
    }
}
