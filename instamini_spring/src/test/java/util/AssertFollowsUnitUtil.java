package util;

import org.junit.jupiter.api.Assertions;
import zlack.bra.instamini.business.dto.FollowDTO;
import zlack.bra.instamini.business.service.impl.UserService;
import zlack.bra.instamini.data.entity.Follow;

public class AssertFollowsUnitUtil {

    public static void assertFollows(Follow follow1, FollowDTO follow2, UserService userService) {
        Assertions.assertEquals(follow1.getId(), follow2.getId());
        Assertions.assertEquals(userService.toDTO(follow1.getUserWho()), follow2.getUserWho());
        Assertions.assertEquals(userService.toDTO(follow1.getUserWhom()), follow2.getUserWhom());
    }

    public static void assertFollows(FollowDTO follow1, FollowDTO follow2) {
        Assertions.assertEquals(follow1.getId(), follow2.getId());
        Assertions.assertEquals(follow1.getUserWho(), follow2.getUserWho());
        Assertions.assertEquals(follow1.getUserWhom(), follow2.getUserWhom());
    }
}
