package util.logged;

import org.springframework.security.oauth2.client.authentication.OAuth2AuthenticationToken;
import org.springframework.security.oauth2.core.user.OAuth2User;
import zlack.bra.instamini.business.dto.UserDTO;
import zlack.bra.instamini.security.oauth.CustomOAuth2User;

import static zlack.bra.instamini.constant.Constants.ONLY_PROVIDER;

public class LoggedUser {

    public static final UserDTO user = new UserDTO(666, "test", "test");

    public static OAuth2AuthenticationToken getOauth2AuthenticationToken() {
        OAuth2User oAuth2User = new CustomOAuth2User(user.getId(),
                user.getUsername(), user.getEmail());
        return new OAuth2AuthenticationToken(oAuth2User,
                oAuth2User.getAuthorities(), ONLY_PROVIDER);
    }
}
