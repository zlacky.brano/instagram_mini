package zlack.bra.instamini.presentation.controller.util;

import junit.framework.AssertionFailedError;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import org.springframework.security.oauth2.client.authentication.OAuth2AuthenticationToken;
import org.springframework.security.oauth2.core.user.OAuth2User;
import org.springframework.web.server.ResponseStatusException;
import zlack.bra.instamini.business.dto.UserDTO;
import zlack.bra.instamini.security.oauth.CustomOAuth2User;

import static org.junit.jupiter.api.Assertions.*;
import static zlack.bra.instamini.constant.Constants.ONLY_PROVIDER;

class AuthorizationUtilTest {

    private final UserDTO loggedUser = new UserDTO(666, "test", "test");

    private final String message = "Test";

    private final CustomOAuth2User oAuth2User = new CustomOAuth2User(loggedUser.getId(),
            loggedUser.getUsername(), loggedUser.getEmail());
    private final OAuth2AuthenticationToken oAuth2AuthenticationToken = new OAuth2AuthenticationToken(oAuth2User,
            oAuth2User.getAuthorities(), ONLY_PROVIDER);

    @Test
    void getUserFromPrincipal() {
        CustomOAuth2User user = AuthorizationUtil.getUserFromPrincipal(oAuth2AuthenticationToken);
        Assertions.assertEquals(user.getAuthorities(), oAuth2User.getAuthorities());
        Assertions.assertEquals(user.getEmail(), oAuth2User.getEmail());
        Assertions.assertEquals(user.getId(), oAuth2User.getId());
        Assertions.assertEquals(user.getAttributes(), oAuth2User.getAttributes());
        Assertions.assertEquals(user.getName(), oAuth2User.getName());
    }

    @Test
    void checkIfAuthorizedOrElseThrow() {
        try {
            AuthorizationUtil.checkIfAuthorizedOrElseThrow(oAuth2AuthenticationToken, 666, message);
        } catch (ResponseStatusException e) {
            throw new AssertionFailedError();
        }
    }

    @Test
    void checkIfAuthorizedOrElseThrowForbidden() {
        try {
            AuthorizationUtil.checkIfAuthorizedOrElseThrow(oAuth2AuthenticationToken, 1, message);
            throw new AssertionFailedError();
        } catch (ResponseStatusException e) {
            Assertions.assertEquals(e.getStatus(), HttpStatus.FORBIDDEN);
            Assertions.assertEquals(e.getReason(), message);
        }
    }
}