package zlack.bra.instamini.presentation.controller;

import junit.framework.AssertionFailedError;
import org.hibernate.exception.ConstraintViolationException;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;
import util.AssertLikesUnitUtil;
import util.logged.LoggedUser;
import zlack.bra.instamini.business.dto.*;
import zlack.bra.instamini.business.exception.NotFoundException;
import zlack.bra.instamini.business.service.impl.LikeService;

import java.sql.SQLException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static zlack.bra.instamini.constant.Constants.DUPLICATE_ENTRY;

@RunWith(MockitoJUnitRunner.class)
class LikeControllerTest {

    private AutoCloseable autoCloseable;

    @InjectMocks
    private LikeController likeController;

    @Mock
    private LikeService likeService;

    private UserDTO loggedUser;

    private final LocalDateTime localDateTime = LocalDateTime.of(LocalDate.of(2000, 12, 12), LocalTime.of(10, 10, 10));

    @BeforeEach
    void setUp() {
        autoCloseable = MockitoAnnotations.openMocks(this);
        loggedUser = LoggedUser.user;
    }

    @AfterEach
    void tearDown() throws Exception {
        autoCloseable.close();
    }

    @Test
    void all() {
        List<LikeDTO> likes = new ArrayList<>();
        int numberOfItems = 3;

        for (int i = 1; i <= numberOfItems; i++) {
            UserDTO userDTO = new UserDTO(i, "1user" + i, "1user" + i);
            PostDTO postDTO = new PostDTO(i, "description" + i, "photoUrl" + i, localDateTime, userDTO);

            LikeDTO like = new LikeDTO(i, userDTO, postDTO);
            likes.add(like);
        }

        Mockito.when(likeService.findAll()).thenReturn(likes);

        List<LikeDTO> likesDTO = likeController.all();

        Assertions.assertEquals(likes.size(), likesDTO.size());
        for (int i = 0; i < likesDTO.size(); i++) {
            AssertLikesUnitUtil.assertLikes(likes.get(i), likesDTO.get(i));
        }
        Mockito.verify(likeService, Mockito.times(1)).findAll();
    }

    @Test
    void allEmpty() {
        List<LikeDTO> likes = new ArrayList<>();

        Mockito.when(likeService.findAll()).thenReturn(likes);

        List<LikeDTO> likesDTO = likeController.all();

        Assertions.assertEquals(likes.size(), likesDTO.size());
        Mockito.verify(likeService, Mockito.times(1)).findAll();
    }

    @Test
    void byID() {
        UserDTO userDTO = new UserDTO(1, "1user", "1user");
        PostDTO postDTO = new PostDTO(2, "description", "photoUrl", localDateTime, userDTO);

        LikeDTO like = new LikeDTO(3, userDTO, postDTO);

        Mockito.when(likeService.findById(like.getId())).thenReturn(Optional.of(like));

        LikeDTO likeResult = likeController.byID(like.getId());

        AssertLikesUnitUtil.assertLikes(like, likeResult);

        Mockito.verify(likeService, Mockito.times(1)).findById(like.getId());
    }

    @Test
    void byIDNotFound() {
        Integer likeId = 1;

        Mockito.when(likeService.findById(likeId)).thenReturn(Optional.empty());

        try {
            likeController.byID(likeId);
            throw new AssertionFailedError();
        } catch (ResponseStatusException e) {
            Assertions.assertEquals(e.getStatus(), HttpStatus.NOT_FOUND);
            Mockito.verify(likeService, Mockito.times(1)).findById(likeId);
        }
    }

    @Test
    void create() throws NotFoundException {
        Integer postId = 5;
        LikeCreateDTO likeCreateDTO = new LikeCreateDTO(loggedUser.getId(), postId);

        PostDTO postDTO = new PostDTO(2, "description", "photoUrl", localDateTime, loggedUser);

        Integer likeId = 1;
        LikeDTO likeDTO = new LikeDTO(likeId, loggedUser, postDTO);

        Mockito.when(likeService.create(likeCreateDTO)).thenReturn(likeDTO);

        LikeDTO likeDTOOutput = (LikeDTO) likeController.create(likeCreateDTO, LoggedUser.getOauth2AuthenticationToken()).getBody();

        Assertions.assertNotNull(likeDTOOutput);
        AssertLikesUnitUtil.assertLikes(likeDTO, likeDTOOutput);

        Mockito.verify(likeService, Mockito.times(1)).create(likeCreateDTO);
    }

    @Test
    void createNotFound() throws NotFoundException {
        Integer userIdWhom = 5;
        LikeCreateDTO likeCreateDTO = new LikeCreateDTO(loggedUser.getId(), userIdWhom);

        Mockito.when(likeService.create(likeCreateDTO)).thenThrow(new NotFoundException(""));

        try {
            likeController.create(likeCreateDTO, LoggedUser.getOauth2AuthenticationToken());
            throw new AssertionFailedError();
        } catch (ResponseStatusException e) {
            Assertions.assertEquals(e.getStatus(), HttpStatus.NOT_FOUND);
            Mockito.verify(likeService, Mockito.times(1)).create(likeCreateDTO);
        }
    }

    @Test
    void createForbidden() throws NotFoundException {
        Integer notLoggedUserId = 2;
        Integer postId = 1;
        LikeCreateDTO likeCreateDTO = new LikeCreateDTO(notLoggedUserId, postId);

        UserDTO notLoggedUserDTO = new UserDTO(notLoggedUserId, "test", "test");
        PostDTO postDTO = new PostDTO(postId, "description", "photoUrl", localDateTime, loggedUser);

        Integer likeId = 1;
        LikeDTO likeDTO = new LikeDTO(likeId, notLoggedUserDTO, postDTO);

        Mockito.when(likeService.create(likeCreateDTO)).thenReturn(likeDTO);

        try {
            likeController.create(likeCreateDTO, LoggedUser.getOauth2AuthenticationToken());
            throw new AssertionFailedError();
        } catch (ResponseStatusException e) {
            Assertions.assertEquals(e.getStatus(), HttpStatus.FORBIDDEN);
            Mockito.verify(likeService, Mockito.times(0)).create(likeCreateDTO);
        }
    }

    @Test
    void createDuplicateEntry() throws NotFoundException {
        Integer userIdWhom = 5;
        LikeCreateDTO likeCreateDTO = new LikeCreateDTO(loggedUser.getId(), userIdWhom);

        Mockito.when(likeService.create(likeCreateDTO)).thenThrow(new ConstraintViolationException("test", new SQLException("Duplicate entry"), null));

        try {
            likeController.create(likeCreateDTO, LoggedUser.getOauth2AuthenticationToken());
            throw new AssertionFailedError();
        } catch (ResponseStatusException e) {
            Assertions.assertEquals(HttpStatus.BAD_REQUEST, e.getStatus());
            Assertions.assertEquals(DUPLICATE_ENTRY, e.getReason());
            Mockito.verify(likeService, Mockito.times(1)).create(likeCreateDTO);
        }
    }

    @Test
    void delete() throws NotFoundException {
        Integer likeId = 1;
        Integer postId = 2;
        PostDTO postDTO = new PostDTO(postId, "description", "photoUrl", localDateTime, loggedUser);

        Optional<LikeDTO> likeDTOOptional = Optional.of(new LikeDTO(likeId, loggedUser, postDTO));

        Mockito.when(likeService.findById(likeId)).thenReturn(likeDTOOptional);
        likeController.delete(likeId, LoggedUser.getOauth2AuthenticationToken());

        Mockito.verify(likeService, Mockito.times(1)).findById(likeId);
        Mockito.verify(likeService, Mockito.times(1)).deleteById(likeId);
    }

    @Test
    void deleteNotFound() throws NotFoundException {
        Integer likeId = 1;

        Mockito.when(likeService.findById(likeId)).thenReturn(Optional.empty());
        Mockito.doThrow(new NotFoundException("")).when(likeService).deleteById(likeId);

        try {
            likeController.delete(likeId, LoggedUser.getOauth2AuthenticationToken());
            throw new AssertionFailedError();
        } catch (ResponseStatusException e) {
            Assertions.assertEquals(e.getStatus(), HttpStatus.NOT_FOUND);
            Mockito.verify(likeService, Mockito.times(1)).findById(likeId);
            Mockito.verify(likeService, Mockito.times(1)).deleteById(likeId);
        }
    }

    @Test
    void deleteForbidden() throws NotFoundException {
        Integer notLoggedUserId = 5;
        Integer postId = 84;
        UserDTO notLoggedUserDTO = new UserDTO(notLoggedUserId, "1user", "1user");
        PostDTO postDTO = new PostDTO(postId, "description", "photoUrl", localDateTime, notLoggedUserDTO);

        Integer likeId = 5;
        Optional<LikeDTO> likeDTOOptional = Optional.of(new LikeDTO(likeId, notLoggedUserDTO, postDTO));

        Mockito.when(likeService.findById(likeId)).thenReturn(likeDTOOptional);
        try {
            likeController.delete(likeId, LoggedUser.getOauth2AuthenticationToken());
            throw new AssertionFailedError();
        } catch (ResponseStatusException e) {
            Assertions.assertEquals(e.getStatus(), HttpStatus.FORBIDDEN);
            Mockito.verify(likeService, Mockito.times(1)).findById(likeId);
            Mockito.verify(likeService, Mockito.times(0)).deleteById(likeId);
        }
    }

    @Test
    void findNumberOfLikesOnPost() throws NotFoundException {
        Integer postId = 1;
        Integer numberOfItems = 5;

        Mockito.when(likeService.findNumberOfLikesOnPost(postId)).thenReturn(numberOfItems);

        Integer numberOfItemsOutput = likeService.findNumberOfLikesOnPost(postId);

        Assertions.assertEquals(numberOfItems, numberOfItemsOutput);

        Mockito.verify(likeService, Mockito.times(1)).findNumberOfLikesOnPost(postId);
    }

    @Test
    void findNumberOfLikesOnPostNotFound() throws NotFoundException {
        Integer postId = 1;

        Mockito.when(likeService.findNumberOfLikesOnPost(postId)).thenThrow(new NotFoundException(""));
        try {
            likeController.findNumberOfLikesOnPost(postId);
            throw new AssertionFailedError();
        } catch (ResponseStatusException e) {
            Assertions.assertEquals(e.getStatus(), HttpStatus.NOT_FOUND);
            Mockito.verify(likeService, Mockito.times(1)).findNumberOfLikesOnPost(postId);
        }
    }

    @Test
    void findLikesOnPost() throws NotFoundException {
        Integer postId = 20;

        List<LikeDTO> likesDTO = new ArrayList<>();
        int numberOfItems = 5;
        for (int i = 1; i <= numberOfItems; i++) {
            UserDTO userDTO = new UserDTO(i, "test" + i, "test" + i);
            PostDTO postDTO = new PostDTO(i, "description" + i, "photoUrl" + i, localDateTime, userDTO);
            LikeDTO like = new LikeDTO(i, userDTO, postDTO);
            likesDTO.add(like);
        }

        Mockito.when(likeService.findLikesOnPost(postId)).thenReturn(likesDTO);

        List<LikeDTO> outputLikesDTO = likeController.findLikesOnPost(postId);

        Assertions.assertEquals(likesDTO.size(), outputLikesDTO.size());

        for (int i = 0; i < numberOfItems; i++) {
            AssertLikesUnitUtil.assertLikes(likesDTO.get(i), outputLikesDTO.get(i));
        }
        Mockito.verify(likeService, Mockito.times(1)).findLikesOnPost(postId);
    }

    @Test
    void findLikesOnPostNotFound() throws NotFoundException {
        Integer postId = 20;

        Mockito.when(likeService.findLikesOnPost(postId)).thenThrow(new NotFoundException(""));

        try {
            likeController.findLikesOnPost(postId);
            throw new AssertionFailedError();
        } catch (ResponseStatusException e) {
            Assertions.assertEquals(e.getStatus(), HttpStatus.NOT_FOUND);
            Mockito.verify(likeService, Mockito.times(1)).findLikesOnPost(postId);
        }
    }

    @Test
    void findLikeIdByUserAndPost() throws NotFoundException {
        Integer postId = 1;

        Mockito.when(likeService.findLikeIdByUserAndPost(loggedUser.getId(), postId)).thenThrow(new NotFoundException(""));

        try {
            likeController.findLikeIdByUserAndPost(postId, LoggedUser.getOauth2AuthenticationToken());
            throw new AssertionFailedError();
        } catch (ResponseStatusException e) {
            Assertions.assertEquals(e.getStatus(), HttpStatus.NOT_FOUND);
            Mockito.verify(likeService, Mockito.times(1)).findLikeIdByUserAndPost(loggedUser.getId(), postId);
        }
    }
}
