package zlack.bra.instamini.presentation.controller;

import junit.framework.AssertionFailedError;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;
import util.AssertCommentsUnitUtil;
import util.logged.LoggedUser;
import zlack.bra.instamini.business.dto.*;
import zlack.bra.instamini.business.exception.NotFoundException;
import zlack.bra.instamini.business.service.impl.CommentService;
import zlack.bra.instamini.presentation.controller.request.EditTextCommentRequestBody;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


@RunWith(MockitoJUnitRunner.class)
class CommentControllerTest {

    private AutoCloseable autoCloseable;

    @InjectMocks
    private CommentController commentController;

    @Mock
    private CommentService commentService;

    private UserDTO loggedUser;

    private final LocalDateTime localDateTime = LocalDateTime.of(LocalDate.of(2000, 12, 12), LocalTime.of(10, 10, 10));

    @BeforeEach
    void setUp() {
        autoCloseable = MockitoAnnotations.openMocks(this);
        loggedUser = LoggedUser.user;
    }

    @AfterEach
    void tearDown() throws Exception {
        autoCloseable.close();
    }

    @Test
    void all() {
        List<CommentDTO> comments = new ArrayList<>();
        int numberOfItems = 3;

        for (int i = 1; i <= numberOfItems; i++) {
            UserDTO userDTO = new UserDTO(i, "user" + i, "user" + i);
            CommentDTO comment = new CommentDTO(i, "text-test" + i, localDateTime, userDTO,
                    new PostDTO(i, "description-test", "photoUrl", localDateTime, userDTO));
            comments.add(comment);
        }

        Mockito.when(commentService.findAll()).thenReturn(comments);

        List<CommentDTO> commentsDTO = commentController.all();

        Assertions.assertEquals(comments.size(), commentsDTO.size());
        for (int i = 0; i < commentsDTO.size(); i++) {
            AssertCommentsUnitUtil.assertComments(comments.get(i), commentsDTO.get(i));
        }
        Mockito.verify(commentService, Mockito.times(1)).findAll();
    }

    @Test
    void allEmpty() {
        List<CommentDTO> comments = new ArrayList<>();

        Mockito.when(commentService.findAll()).thenReturn(comments);

        List<CommentDTO> commentsDTO = commentController.all();

        Assertions.assertEquals(comments.size(), commentsDTO.size());
        Mockito.verify(commentService, Mockito.times(1)).findAll();
    }

    @Test
    void byID() {
        UserDTO user = new UserDTO(1, "test", "test");
        PostDTO post = new PostDTO(1, "description", "photo url", localDateTime, user);
        CommentDTO comment = new CommentDTO(1, "text", localDateTime, user, post);

        Mockito.when(commentService.findById(comment.getId())).thenReturn(Optional.of(comment));

        CommentDTO commentResult = commentController.byID(comment.getId());

        AssertCommentsUnitUtil.assertComments(comment, commentResult);

        Mockito.verify(commentService, Mockito.times(1)).findById(comment.getId());
    }

    @Test
    void byIDNotFound() {
        Integer commentId = 1;

        Mockito.when(commentService.findById(commentId)).thenReturn(Optional.empty());

        try {
            commentController.byID(commentId);
            throw new AssertionFailedError();
        } catch (ResponseStatusException e) {
            Assertions.assertEquals(e.getStatus(), HttpStatus.NOT_FOUND);
            Mockito.verify(commentService, Mockito.times(1)).findById(commentId);
        }
    }

    @Test
    void create() throws NotFoundException {
        Integer postId = 1;

        CommentCreateDTO commentCreateDTO = new CommentCreateDTO("test", loggedUser.getId(), postId);

        PostDTO postDTO = new PostDTO(postId, "description", "photoUrl", localDateTime, loggedUser);

        Integer commentId = 5;
        CommentDTO commentDTO = new CommentDTO(commentId, commentCreateDTO.getText(), localDateTime, loggedUser, postDTO);

        Mockito.when(commentService.create(commentCreateDTO)).thenReturn(commentDTO);

        CommentDTO commentDTOOutput = (CommentDTO) commentController.create(commentCreateDTO, LoggedUser.getOauth2AuthenticationToken()).getBody();

        Assertions.assertNotNull(commentDTOOutput);
        AssertCommentsUnitUtil.assertComments(commentDTO, commentDTOOutput);

        Mockito.verify(commentService, Mockito.times(1)).create(commentCreateDTO);
    }

    @Test
    void createNotFound() throws NotFoundException {
        Integer postId = 1;
        CommentCreateDTO commentCreateDTO = new CommentCreateDTO("test", loggedUser.getId(), postId);

        Mockito.when(commentService.create(commentCreateDTO)).thenThrow(new NotFoundException(""));

        try {
            commentController.create(commentCreateDTO, LoggedUser.getOauth2AuthenticationToken());
            throw new AssertionFailedError();
        } catch (ResponseStatusException e) {
            Assertions.assertEquals(e.getStatus(), HttpStatus.NOT_FOUND);
            Mockito.verify(commentService, Mockito.times(1)).create(commentCreateDTO);
        }
    }

    @Test
    void createForbidden() throws NotFoundException {
        Integer postId = 1;
        Integer notLoggedUserId = 1;
        UserDTO userDTO = new UserDTO(notLoggedUserId, "test", "test");
        CommentCreateDTO commentCreateDTO = new CommentCreateDTO("test", userDTO.getId(), postId);

        PostDTO postDTO = new PostDTO(postId, "description", "photoUrl", localDateTime, loggedUser);

        Integer commentId = 5;
        CommentDTO commentDTO = new CommentDTO(commentId, commentCreateDTO.getText(), localDateTime, userDTO, postDTO);

        Mockito.when(commentService.create(commentCreateDTO)).thenReturn(commentDTO);

        try {
            commentController.create(commentCreateDTO, LoggedUser.getOauth2AuthenticationToken());
            throw new AssertionFailedError();
        } catch (ResponseStatusException e) {
            Assertions.assertEquals(e.getStatus(), HttpStatus.FORBIDDEN);
            Mockito.verify(commentService, Mockito.times(0)).create(commentCreateDTO);
        }
    }

    @Test
    void editText() throws NotFoundException {
        Integer postId = 1;

        PostDTO postDTO = new PostDTO(postId, "description", "photoUrl", localDateTime, loggedUser);

        Integer commentId = 5;
        CommentDTO commentDTO = new CommentDTO(commentId, "old text", localDateTime, loggedUser, postDTO);

        EditTextCommentRequestBody requestBody = new EditTextCommentRequestBody("new text");

        CommentDTO newCommentDTO = new CommentDTO(commentId, requestBody.getNewText(), localDateTime, loggedUser, postDTO);

        Mockito.when(commentService.findById(commentId)).thenReturn(Optional.of(commentDTO));
        Mockito.when(commentService.editText(commentId, requestBody.getNewText())).thenReturn(newCommentDTO);

        CommentDTO commentDTOOutput = commentController.editText(commentId, requestBody, LoggedUser.getOauth2AuthenticationToken());

        Assertions.assertNotNull(commentDTOOutput);
        AssertCommentsUnitUtil.assertComments(newCommentDTO, commentDTOOutput);

        Mockito.verify(commentService, Mockito.times(1)).editText(commentId, requestBody.getNewText());
        Mockito.verify(commentService, Mockito.times(1)).findById(commentId);
    }

    @Test
    void editTextNotFound() throws NotFoundException {
        Integer commentId = 1;
        EditTextCommentRequestBody requestBody = new EditTextCommentRequestBody("new text");

        Mockito.when(commentService.findById(commentId)).thenReturn(Optional.empty());
        Mockito.when(commentService.editText(commentId, requestBody.getNewText())).thenThrow(new NotFoundException(""));

        try {
            commentController.editText(commentId, requestBody, LoggedUser.getOauth2AuthenticationToken());
            throw new AssertionFailedError();
        } catch (ResponseStatusException e) {
            Assertions.assertEquals(e.getStatus(), HttpStatus.NOT_FOUND);
            Mockito.verify(commentService, Mockito.times(1)).editText(commentId, requestBody.getNewText());
            Mockito.verify(commentService, Mockito.times(1)).findById(commentId);
        }
    }

    @Test
    void editTextForbidden() throws NotFoundException {
        Integer postId = 1;
        Integer notLoggedUserId = 1;
        UserDTO notLoggedUserDTO = new UserDTO(notLoggedUserId, "test", "test");

        PostDTO postDTO = new PostDTO(postId, "description", "photoUrl", localDateTime, loggedUser);

        Integer commentId = 5;
        CommentDTO commentDTO = new CommentDTO(commentId, "old text", localDateTime, notLoggedUserDTO, postDTO);

        EditTextCommentRequestBody requestBody = new EditTextCommentRequestBody("new text");

        CommentDTO newCommentDTO = new CommentDTO(commentId, requestBody.getNewText(), localDateTime, notLoggedUserDTO, postDTO);

        Mockito.when(commentService.findById(commentId)).thenReturn(Optional.of(commentDTO));
        Mockito.when(commentService.editText(commentId, requestBody.getNewText())).thenReturn(newCommentDTO);

        try {
            commentController.editText(commentId, requestBody, LoggedUser.getOauth2AuthenticationToken());
            throw new AssertionFailedError();
        } catch (ResponseStatusException e) {
            Assertions.assertEquals(e.getStatus(), HttpStatus.FORBIDDEN);
            Mockito.verify(commentService, Mockito.times(0)).editText(commentId, requestBody.getNewText());
            Mockito.verify(commentService, Mockito.times(1)).findById(commentId);
        }
    }

    @Test
    void delete() throws NotFoundException {
        Integer commentId = 1;
        Integer postId = 5;
        PostDTO postDTO = new PostDTO(postId, "description", "photoUrl", localDateTime, loggedUser);
        Optional<CommentDTO> commentDTOOptional = Optional.of(new CommentDTO(commentId, "text", localDateTime, loggedUser, postDTO));

        Mockito.when(commentService.findById(commentId)).thenReturn(commentDTOOptional);
        commentController.delete(commentId, LoggedUser.getOauth2AuthenticationToken());

        Mockito.verify(commentService, Mockito.times(1)).findById(commentId);
        Mockito.verify(commentService, Mockito.times(1)).deleteById(commentId);
    }

    @Test
    void deleteNotFound() throws NotFoundException {
        Integer commentId = 1;

        Mockito.when(commentService.findById(commentId)).thenReturn(Optional.empty());
        Mockito.doThrow(new NotFoundException("")).when(commentService).deleteById(commentId);

        try {
            commentController.delete(commentId, LoggedUser.getOauth2AuthenticationToken());
            throw new AssertionFailedError();
        } catch (ResponseStatusException e) {
            Assertions.assertEquals(e.getStatus(), HttpStatus.NOT_FOUND);
            Mockito.verify(commentService, Mockito.times(1)).findById(commentId);
            Mockito.verify(commentService, Mockito.times(1)).deleteById(commentId);
        }
    }

    @Test
    void deleteForbidden() throws NotFoundException {
        Integer postId = 1;
        Integer userId = 1;

        UserDTO userDTO = new UserDTO(userId, "test", "test");
        PostDTO postDTO = new PostDTO(postId, "description", "photoUrl", localDateTime, loggedUser);

        Integer commentId = 5;
        Optional<CommentDTO> commentDTOOptional = Optional.of(new CommentDTO(commentId, "text", localDateTime, userDTO, postDTO));

        Mockito.when(commentService.findById(commentId)).thenReturn(commentDTOOptional);
        try {
            commentController.delete(commentId, LoggedUser.getOauth2AuthenticationToken());
            throw new AssertionFailedError();
        } catch (ResponseStatusException e) {
            Assertions.assertEquals(e.getStatus(), HttpStatus.FORBIDDEN);
            Mockito.verify(commentService, Mockito.times(1)).findById(commentId);
            Mockito.verify(commentService, Mockito.times(0)).deleteById(commentId);
        }
    }

    @Test
    void findAllCommentsOnPost() throws NotFoundException {
        Integer postId = 20;

        List<CommentDTO> commentsDTO = new ArrayList<>();
        int numberOfItems = 5;
        for (int i = 1; i <= numberOfItems; i++) {
            UserDTO userDTO = new UserDTO(i, "test" + i, "test" + i);
            PostDTO postDTO = new PostDTO(i, "description" + i, "photoUrl" + i, localDateTime, userDTO);
            CommentDTO comment = new CommentDTO(i, "text" + i, localDateTime, userDTO, postDTO);
            commentsDTO.add(comment);
        }

        Mockito.when(commentService.findAllCommentsOnPost(postId)).thenReturn(commentsDTO);

        List<CommentDTO> outputCommentsDTO = commentController.findAllCommentsOnPost(postId);

        Assertions.assertEquals(commentsDTO.size(), outputCommentsDTO.size());

        for (int i = 0; i < numberOfItems; i++) {
            AssertCommentsUnitUtil.assertComments(commentsDTO.get(i), outputCommentsDTO.get(i));
        }
        Mockito.verify(commentService, Mockito.times(1)).findAllCommentsOnPost(postId);
    }

    @Test
    void findAllCommentsOnPostNotFound() throws NotFoundException {
        Integer postId = 20;

        Mockito.when(commentService.findAllCommentsOnPost(postId)).thenThrow(new NotFoundException(""));

        try {
            commentController.findAllCommentsOnPost(postId);
            throw new AssertionFailedError();
        } catch (ResponseStatusException e) {
            Assertions.assertEquals(e.getStatus(), HttpStatus.NOT_FOUND);
            Mockito.verify(commentService, Mockito.times(1)).findAllCommentsOnPost(postId);
        }
    }
}