package zlack.bra.instamini.business.service.impl;

import junit.framework.AssertionFailedError;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.test.util.ReflectionTestUtils;
import util.AssertFollowsUnitUtil;
import zlack.bra.instamini.business.dto.FollowCreateDTO;
import zlack.bra.instamini.business.dto.FollowDTO;
import zlack.bra.instamini.business.exception.FollowYourselfException;
import zlack.bra.instamini.business.exception.NotFoundException;
import zlack.bra.instamini.data.entity.Follow;
import zlack.bra.instamini.data.entity.User;
import zlack.bra.instamini.data.repository.FollowRepository;
import zlack.bra.instamini.data.repository.PostRepository;
import zlack.bra.instamini.data.repository.UserRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


@RunWith(MockitoJUnitRunner.class)
class FollowServiceTest {

    private AutoCloseable autoCloseable;

    private UserService userService;
    private FollowService followService;

    @Mock
    private FollowRepository followRepository;

    @Mock
    private UserRepository userRepository;

    @Mock
    private PostRepository postRepository;


    @BeforeEach
    void setUp() {
        autoCloseable = MockitoAnnotations.openMocks(this);
        userService = new UserService(userRepository, postRepository, "test", "test", 0, "test");
        followService = new FollowService(followRepository, userRepository, userService);
    }

    @AfterEach
    void tearDown() throws Exception {
        autoCloseable.close();
    }

    @Test
    void create() {
        Integer userWhoId = 1;
        Integer userWhomId = 2;
        FollowCreateDTO followCreateDTO = new FollowCreateDTO(userWhoId, userWhomId);

        User userWho = new User("test", "test", null, null, null, null, null);
        User userWhom = new User("test", "test", null, null, null, null, null);

        ReflectionTestUtils.setField(userWho, "id", userWhoId);
        ReflectionTestUtils.setField(userWhom, "id", userWhomId);

        Integer followId = 1;
        Follow follow = new Follow(userWho, userWhom);
        ReflectionTestUtils.setField(follow, "id", followId);

        Mockito.when(followRepository.save(Mockito.any())).thenReturn(follow);
        Mockito.when(userRepository.findById(userWhoId)).thenReturn(Optional.of(userWho));
        Mockito.when(userRepository.findById(userWhomId)).thenReturn(Optional.of(userWhom));

        FollowDTO followDTO;
        try {
            followDTO = followService.create(followCreateDTO);
        } catch (FollowYourselfException | NotFoundException e) {
            throw new AssertionFailedError();
        }

        Assertions.assertNotNull(followDTO);
        AssertFollowsUnitUtil.assertFollows(follow, followDTO, userService);

        Mockito.verify(followRepository, Mockito.times(1)).save(Mockito.any());
        Mockito.verify(userRepository, Mockito.times(1)).findById(userWhoId);
        Mockito.verify(userRepository, Mockito.times(1)).findById(userWhomId);
    }

    @Test
    void createNotFoundUserWho() {
        Integer userWhoId = 1;
        Integer userWhomId = 2;
        FollowCreateDTO followCreateDTO = new FollowCreateDTO(userWhoId, userWhomId);

        User userWho = new User("test", "test", null, null, null, null, null);
        User userWhom = new User("test", "test", null, null, null, null, null);

        ReflectionTestUtils.setField(userWho, "id", userWhoId);
        ReflectionTestUtils.setField(userWhom, "id", userWhomId);

        Integer followId = 1;
        Follow follow = new Follow(userWho, userWhom);
        ReflectionTestUtils.setField(follow, "id", followId);

        Mockito.when(followRepository.save(Mockito.any())).thenReturn(follow);
        Mockito.when(userRepository.findById(userWhoId)).thenReturn(Optional.empty());
        Mockito.when(userRepository.findById(userWhomId)).thenReturn(Optional.of(userWhom));

        try {
            followService.create(followCreateDTO);
            throw new AssertionFailedError();
        } catch (FollowYourselfException e) {
            throw new AssertionFailedError();
        } catch (NotFoundException e) {
            Mockito.verify(followRepository, Mockito.times(0)).save(Mockito.any());
            Mockito.verify(userRepository, Mockito.times(1)).findById(userWhoId);
            Mockito.verify(userRepository, Mockito.times(1)).findById(userWhomId);
        }
    }

    @Test
    void createNotFoundUserWhom() {
        Integer userWhoId = 1;
        Integer userWhomId = 2;
        FollowCreateDTO followCreateDTO = new FollowCreateDTO(userWhoId, userWhomId);

        User userWho = new User("test", "test", null, null, null, null, null);
        User userWhom = new User("test", "test", null, null, null, null, null);

        ReflectionTestUtils.setField(userWho, "id", userWhoId);
        ReflectionTestUtils.setField(userWhom, "id", userWhomId);

        Integer followId = 1;
        Follow follow = new Follow(userWho, userWhom);
        ReflectionTestUtils.setField(follow, "id", followId);

        Mockito.when(followRepository.save(Mockito.any())).thenReturn(follow);
        Mockito.when(userRepository.findById(userWhoId)).thenReturn(Optional.of(userWho));
        Mockito.when(userRepository.findById(userWhomId)).thenReturn(Optional.empty());

        try {
            followService.create(followCreateDTO);
            throw new AssertionFailedError();
        } catch (FollowYourselfException e) {
            throw new AssertionFailedError();
        } catch (NotFoundException e) {
            Mockito.verify(followRepository, Mockito.times(0)).save(Mockito.any());
            Mockito.verify(userRepository, Mockito.times(1)).findById(userWhoId);
            Mockito.verify(userRepository, Mockito.times(1)).findById(userWhomId);
        }
    }

    @Test
    void createFollowYourself() {
        Integer userId = 2;
        FollowCreateDTO followCreateDTO = new FollowCreateDTO(userId, userId);

        User user = new User("test", "test", null, null, null, null, null);

        ReflectionTestUtils.setField(user, "id", userId);

        Integer followId = 1;
        Follow follow = new Follow(user, user);
        ReflectionTestUtils.setField(follow, "id", followId);

        Mockito.when(followRepository.save(Mockito.any())).thenReturn(follow);
        Mockito.when(userRepository.findById(userId)).thenReturn(Optional.of(user));

        try {
            followService.create(followCreateDTO);
            throw new AssertionFailedError();
        } catch (FollowYourselfException e) {
            Mockito.verify(followRepository, Mockito.times(0)).save(Mockito.any());
            Mockito.verify(userRepository, Mockito.times(2)).findById(userId);
        } catch (NotFoundException e) {
            throw new AssertionFailedError();
        }
    }

    @Test
    void findAll() {
        List<Follow> follows = new ArrayList<>();
        int numberOfItems = 3;
        int idOfUser = 1;

        for (int i = 1; i <= numberOfItems; i++) {
            User userWho = new User("1test" + i, "1test" + i, null, null, null, null, null);
            User userWhom = new User("2test" + i, "2test" + i, null, null, null, null, null);
            ReflectionTestUtils.setField(userWho, "id", idOfUser++);
            ReflectionTestUtils.setField(userWhom, "id", idOfUser++);

            Follow follow = new Follow(userWho, userWhom);
            ReflectionTestUtils.setField(follow, "id", i);
            follows.add(follow);
        }

        Mockito.when(followRepository.findAll()).thenReturn(follows);

        List<FollowDTO> followsDTO = followService.findAll();

        Assertions.assertEquals(follows.size(), followsDTO.size());
        for (int i = 0; i < followsDTO.size(); i++) {
            AssertFollowsUnitUtil.assertFollows(follows.get(i), followsDTO.get(i), userService);
        }
        Mockito.verify(followRepository, Mockito.times(1)).findAll();
    }

    @Test
    void findAllEmpty() {
        List<Follow> follows = new ArrayList<>();

        Mockito.when(followRepository.findAll()).thenReturn(follows);

        List<FollowDTO> followsDTO = followService.findAll();

        Assertions.assertEquals(follows.size(), followsDTO.size());
        Mockito.verify(followRepository, Mockito.times(1)).findAll();
    }

    @Test
    void findByIds() {
        List<Integer> ids = new ArrayList<>();
        List<Follow> follows = new ArrayList<>();
        int numberOfItems = 3;
        int idOfUser = 1;

        for (int i = 1; i <= numberOfItems; i++) {
            User userWho = new User("1test" + i, "1test" + i, null, null, null, null, null);
            User userWhom = new User("2test" + i, "2test" + i, null, null, null, null, null);
            ReflectionTestUtils.setField(userWho, "id", idOfUser++);
            ReflectionTestUtils.setField(userWhom, "id", idOfUser++);

            Follow follow = new Follow(userWho, userWhom);
            ReflectionTestUtils.setField(follow, "id", i);
            follows.add(follow);
            ids.add(i);
        }

        Mockito.when(followRepository.findAllById(ids)).thenReturn(follows);

        List<FollowDTO> followsDTO = followService.findByIds(ids);

        Assertions.assertEquals(follows.size(), followsDTO.size());

        for (int i = 0; i < followsDTO.size(); i++) {
            AssertFollowsUnitUtil.assertFollows(follows.get(i), followsDTO.get(i), userService);
        }
        Mockito.verify(followRepository, Mockito.times(1)).findAllById(ids);
    }

    @Test
    void findByIdsEmpty() {
        List<Integer> ids = new ArrayList<>();
        List<Follow> follows = new ArrayList<>();

        Mockito.when(followRepository.findAllById(ids)).thenReturn(follows);

        List<FollowDTO> followsDTO = followService.findByIds(ids);

        Assertions.assertEquals(follows.size(), followsDTO.size());

        Mockito.verify(followRepository, Mockito.times(1)).findAllById(ids);
    }

    @Test
    void findById() {
        Integer userWhoId = 1;
        Integer userWhomId = 2;
        User userWho = new User("1test", "1test", null, null, null, null, null);
        User userWhom = new User("2test", "2test", null, null, null, null, null);
        ReflectionTestUtils.setField(userWho, "id", userWhoId);
        ReflectionTestUtils.setField(userWhom, "id", userWhomId);

        Integer followId = 1;
        Follow follow = new Follow(userWho, userWhom);
        ReflectionTestUtils.setField(follow, "id", followId);

        Mockito.when(followRepository.findById(followId)).thenReturn(Optional.of(follow));

        Optional<FollowDTO> followDTOOptional = followService.findById(followId);

        if (followDTOOptional.isPresent()) {
            FollowDTO followDTO = followDTOOptional.get();
            AssertFollowsUnitUtil.assertFollows(follow, followDTO, userService);
        }
        else {
            throw new AssertionFailedError();
        }
        Mockito.verify(followRepository, Mockito.times(1)).findById(followId);
    }

    @Test
    void findByIdNotFound() {
        Integer followId = 1;

        Mockito.when(followRepository.findById(followId)).thenReturn(Optional.empty());

        Optional<FollowDTO> followDTOOptional = followService.findById(followId);

        if (followDTOOptional.isPresent()) {
            throw new AssertionFailedError();
        } else {
            Mockito.verify(followRepository, Mockito.times(1)).findById(followId);
        }
    }

    @Test
    void toDTO() {
        Integer userWhoId = 1;
        Integer userWhomId = 2;
        User userWho = new User("1test", "1test", null, null, null, null, null);
        User userWhom = new User("2test", "2test", null, null, null, null, null);
        ReflectionTestUtils.setField(userWho, "id", userWhoId);
        ReflectionTestUtils.setField(userWhom, "id", userWhomId);

        Integer followId = 1;
        Follow follow = new Follow(userWho, userWhom);
        ReflectionTestUtils.setField(follow, "id", followId);

        FollowDTO followDTO = followService.toDTO(follow);

        AssertFollowsUnitUtil.assertFollows(follow, followDTO, userService);
    }

    @Test
    void toDTOOptional() {
        Integer userWhoId = 1;
        Integer userWhomId = 2;
        User userWho = new User("1test", "1test", null, null, null, null, null);
        User userWhom = new User("2test", "2test", null, null, null, null, null);
        ReflectionTestUtils.setField(userWho, "id", userWhoId);
        ReflectionTestUtils.setField(userWhom, "id", userWhomId);

        Integer followId = 1;
        Follow follow = new Follow(userWho, userWhom);
        ReflectionTestUtils.setField(follow, "id", followId);

        Optional<FollowDTO> followDTO = followService.toDTO(Optional.of(follow));

        if (followDTO.isEmpty()) {
            throw new AssertionFailedError();
        }

        AssertFollowsUnitUtil.assertFollows(follow, followDTO.get(), userService);
    }

    @Test
    void deleteById() {
        Integer followId = 1;

        Mockito.when(followRepository.existsById(followId)).thenReturn(true);
        try {
            followService.deleteById(followId);
        } catch (NotFoundException e) {
            throw new AssertionFailedError();
        }

        Mockito.when(followRepository.existsById(followId)).thenReturn(false);
        try {
            followService.deleteById(followId);
            throw new AssertionFailedError();
        } catch (NotFoundException ignored) { }

        Mockito.verify(followRepository, Mockito.times(2)).existsById(followId);
        Mockito.verify(followRepository, Mockito.times(1)).deleteById(followId);
    }

    @Test
    void findFollowIdByUsers() {
        Integer followId = 1;
        Integer userId1 = 1;
        Integer userId2 = 2;

        Mockito.when(followRepository.findFollowIdByUsers(userId1, userId2)).thenReturn(Optional.of(followId));

        Integer resultId;
        try {
            resultId = followService.findFollowIdByUsers(userId1, userId2);
        } catch (NotFoundException e) {
            throw new AssertionFailedError();
        }

        Assertions.assertEquals(followId, resultId);
        Mockito.verify(followRepository, Mockito.times(1)).findFollowIdByUsers(userId1, userId2);
    }

    @Test
    void findFollowIdByUsersNotFound() {
        Integer userId1 = 1;
        Integer userId2 = 2;

        Mockito.when(followRepository.findFollowIdByUsers(userId1, userId2)).thenReturn(Optional.empty());

        try {
            followService.findFollowIdByUsers(userId1, userId2);
            throw new AssertionFailedError();
        } catch (NotFoundException e) {
            Mockito.verify(followRepository, Mockito.times(1)).findFollowIdByUsers(userId1, userId2);
        }
    }
}