package zlack.bra.instamini.business.service.impl;

import junit.framework.AssertionFailedError;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.test.util.ReflectionTestUtils;
import util.AssertCommentsUnitUtil;
import zlack.bra.instamini.business.dto.*;
import zlack.bra.instamini.business.exception.NotFoundException;
import zlack.bra.instamini.business.service.ImageUploadService;
import zlack.bra.instamini.data.entity.Comment;
import zlack.bra.instamini.data.entity.Post;
import zlack.bra.instamini.data.entity.User;
import zlack.bra.instamini.data.repository.CommentRepository;
import zlack.bra.instamini.data.repository.PostRepository;
import zlack.bra.instamini.data.repository.UserRepository;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RunWith(MockitoJUnitRunner.class)
class CommentServiceTest {

    private AutoCloseable autoCloseable;

    private UserService userService;
    private PostService postService;
    private CommentService commentService;

    @Mock
    private CommentRepository commentRepository;

    @Mock
    private PostRepository postRepository;

    @Mock
    private UserRepository userRepository;

    @Mock
    private ImageUploadService imageUploadService;

    private final LocalDateTime localDateTime = LocalDateTime.of(LocalDate.of(2000, 12, 12), LocalTime.of(10, 10, 10));

    @BeforeEach
    void setUp() {
        autoCloseable = MockitoAnnotations.openMocks(this);
        userService = new UserService(userRepository, postRepository, "test", "test", 0, "test");
        postService = new PostService(postRepository, userRepository, userService, imageUploadService);
        commentService = new CommentService(commentRepository, userService, postService, postRepository, userRepository);
    }

    @AfterEach
    void tearDown() throws Exception {
        autoCloseable.close();
    }

    @Test
    void editText() {
        Integer userId = 1;
        Integer postId = 1;
        User user = new User("user1", "user1", null, null, null, null, null);
        Post post = new Post("test", "test", localDateTime, user);
        ReflectionTestUtils.setField(user, "id", userId);
        ReflectionTestUtils.setField(post, "id", postId);

        Comment commentToEdit = new Comment("old text", localDateTime, user, post);

        Integer commentId = 1;
        ReflectionTestUtils.setField(commentToEdit, "id", commentId);

        String newText = "new text";

        Mockito.when(commentRepository.findById(commentId)).thenReturn(Optional.of(commentToEdit));

        CommentDTO editedComment;
        try {
            editedComment = commentService.editText(commentId, newText);
        } catch (NotFoundException e) {
            throw new AssertionFailedError();
        }

        Assertions.assertNotNull(editedComment);
        AssertCommentsUnitUtil.assertCommentsInEditTextTest(commentToEdit, editedComment, userService, postService, newText);

        Mockito.verify(commentRepository, Mockito.times(1)).findById(commentId);
    }

    @Test
    void editTextNotFound() {
        Integer id = 1;
        String newText = "new text";

        Mockito.when(commentRepository.findById(id)).thenReturn(Optional.empty());

        try {
            commentService.editText(id, newText);
            throw new AssertionFailedError();
        } catch (NotFoundException e) {
            Mockito.verify(commentRepository, Mockito.times(1)).findById(id);
        }
    }

    @Test
    void create() {
        Integer userId = 1;
        Integer postId = 1;
        CommentCreateDTO commentCreateDTO = new CommentCreateDTO("text", userId, postId);

        User user = new User("user1", "user1", null, null, null, null, null);
        Post post = new Post("test", "test", localDateTime, user);
        ReflectionTestUtils.setField(user, "id", userId);
        ReflectionTestUtils.setField(post, "id", postId);

        Comment comment = new Comment("text", localDateTime, user, post);

        Integer commentId = 1;
        ReflectionTestUtils.setField(comment, "id", commentId);

        Mockito.when(commentRepository.save(Mockito.any())).thenReturn(comment);
        Mockito.when(userRepository.findById(userId)).thenReturn(Optional.of(user));
        Mockito.when(postRepository.findById(postId)).thenReturn(Optional.of(post));

        CommentDTO commentDTO;
        try {
            commentDTO = commentService.create(commentCreateDTO);
        } catch (NotFoundException e) {
            throw new AssertionFailedError();
        }

        Assertions.assertNotNull(commentDTO);
        AssertCommentsUnitUtil.assertComments(comment, commentDTO, userService, postService);

        Mockito.verify(commentRepository, Mockito.times(1)).save(Mockito.any());
        Mockito.verify(userRepository, Mockito.times(1)).findById(userId);
        Mockito.verify(postRepository, Mockito.times(1)).findById(postId);
    }

    @Test
    void createUserNotFound() {
        Integer userId = 1;
        Integer postId = 1;
        CommentCreateDTO commentCreateDTO = new CommentCreateDTO("text", userId, postId);

        User user = new User("user1", "user1", null, null, null, null, null);
        Post post = new Post("test", "test", localDateTime, user);
        ReflectionTestUtils.setField(user, "id", userId);
        ReflectionTestUtils.setField(post, "id", postId);

        Comment comment = new Comment("text", localDateTime, user, post);

        Integer commentId = 1;
        ReflectionTestUtils.setField(comment, "id", commentId);

        Mockito.when(commentRepository.save(Mockito.any())).thenReturn(comment);
        Mockito.when(userRepository.findById(userId)).thenReturn(Optional.empty());
        Mockito.when(postRepository.findById(postId)).thenReturn(Optional.of(post));

        try {
            commentService.create(commentCreateDTO);
            throw new AssertionFailedError();
        } catch (NotFoundException e) {
            Mockito.verify(commentRepository, Mockito.times(0)).save(Mockito.any());
            Mockito.verify(userRepository, Mockito.times(1)).findById(userId);
            Mockito.verify(postRepository, Mockito.times(1)).findById(postId);
        }
    }

    @Test
    void createPostNotFound() {
        Integer userId = 1;
        Integer postId = 1;
        CommentCreateDTO commentCreateDTO = new CommentCreateDTO("text", userId, postId);

        User user = new User("user1", "user1", null, null, null, null, null);
        Post post = new Post("test", "test", localDateTime, user);
        ReflectionTestUtils.setField(user, "id", userId);
        ReflectionTestUtils.setField(post, "id", postId);

        Comment comment = new Comment("text", localDateTime, user, post);

        Integer commentId = 1;
        ReflectionTestUtils.setField(comment, "id", commentId);

        Mockito.when(commentRepository.save(Mockito.any())).thenReturn(comment);
        Mockito.when(userRepository.findById(userId)).thenReturn(Optional.of(user));
        Mockito.when(postRepository.findById(postId)).thenReturn(Optional.empty());

        try {
            commentService.create(commentCreateDTO);
            throw new AssertionFailedError();
        } catch (NotFoundException e) {
            Mockito.verify(commentRepository, Mockito.times(0)).save(Mockito.any());
            Mockito.verify(userRepository, Mockito.times(1)).findById(userId);
            Mockito.verify(postRepository, Mockito.times(1)).findById(postId);
        }
    }

    @Test
    void findAll() {
        List<Comment> comments = new ArrayList<>();
        int numberOfItems = 3;
        Integer userId = 1;
        Integer postId = 1;

        for (int i = 1; i <= numberOfItems; i++) {
            User user = new User("user1", "user1", null, null, null, null, null);
            Post post = new Post("test", "test", localDateTime, user);
            ReflectionTestUtils.setField(user, "id", userId);
            ReflectionTestUtils.setField(post, "id", postId);

            Comment comment = new Comment("text", localDateTime, user, post);

            Integer commentId = 1;
            ReflectionTestUtils.setField(comment, "id", commentId);
            comments.add(comment);
        }

        Mockito.when(commentRepository.findAll()).thenReturn(comments);

        List<CommentDTO> commentsDTO = commentService.findAll();

        Assertions.assertEquals(comments.size(), commentsDTO.size());
        for (int i = 0; i < commentsDTO.size(); i++) {
            AssertCommentsUnitUtil.assertComments(comments.get(i), commentsDTO.get(i), userService, postService);
        }
        Mockito.verify(commentRepository, Mockito.times(1)).findAll();
    }

    @Test
    void findAllEmpty() {
        List<Comment> comments = new ArrayList<>();

        Mockito.when(commentRepository.findAll()).thenReturn(comments);

        List<CommentDTO> commentsDTO = commentService.findAll();

        Assertions.assertEquals(comments.size(), commentsDTO.size());

        Mockito.verify(commentRepository, Mockito.times(1)).findAll();
    }

    @Test
    void findByIds() {
        List<Integer> ids = new ArrayList<>();
        List<Comment> comments = new ArrayList<>();
        int numberOfItems = 3;
        Integer userId = 1;
        Integer postId = 1;

        for (int i = 1; i <= numberOfItems; i++) {
            User user = new User("user1", "user1", null, null, null, null, null);
            Post post = new Post("test", "test", localDateTime, user);
            ReflectionTestUtils.setField(user, "id", userId);
            ReflectionTestUtils.setField(post, "id", postId);

            Comment comment = new Comment("text", localDateTime, user, post);

            Integer commentId = 1;
            ReflectionTestUtils.setField(comment, "id", commentId);
            comments.add(comment);
            ids.add(i);
        }

        Mockito.when(commentRepository.findAllById(ids)).thenReturn(comments);

        List<CommentDTO> commentsDTO = commentService.findByIds(ids);

        Assertions.assertEquals(comments.size(), commentsDTO.size());

        for (int i = 0; i < commentsDTO.size(); i++) {
            AssertCommentsUnitUtil.assertComments(comments.get(i), commentsDTO.get(i), userService, postService);
        }
        Mockito.verify(commentRepository, Mockito.times(1)).findAllById(ids);
    }

    @Test
    void findByIdsEmpty() {
        List<Integer> ids = new ArrayList<>();
        List<Comment> comments = new ArrayList<>();

        Mockito.when(commentRepository.findAllById(ids)).thenReturn(comments);

        List<CommentDTO> commentsDTO = commentService.findByIds(ids);

        Assertions.assertEquals(comments.size(), commentsDTO.size());

        Mockito.verify(commentRepository, Mockito.times(1)).findAllById(ids);
    }

    @Test
    void findById() {
        Integer userId = 1;
        Integer postId = 2;
        User user = new User("user1", "user1", null, null, null, null, null);
        Post post = new Post("test", "test", localDateTime, user);
        ReflectionTestUtils.setField(user, "id", userId);
        ReflectionTestUtils.setField(post, "id", postId);

        Comment comment = new Comment("text", localDateTime, user, post);

        Integer commentId = 1;
        ReflectionTestUtils.setField(comment, "id", commentId);

        Mockito.when(commentRepository.findById(commentId)).thenReturn(Optional.of(comment));

        Optional<CommentDTO> commentDTOOptional = commentService.findById(commentId);

        if (commentDTOOptional.isPresent()) {
            CommentDTO commentDTO = commentDTOOptional.get();
            AssertCommentsUnitUtil.assertComments(comment, commentDTO, userService, postService);
        } else {
            throw new AssertionFailedError();
        }
        Mockito.verify(commentRepository, Mockito.times(1)).findById(commentId);
    }

    @Test
    void findByIdNotFound() {
        Integer commentId = 1;

        Mockito.when(commentRepository.findById(commentId)).thenReturn(Optional.empty());

        Optional<CommentDTO> commentDTOOptional = commentService.findById(commentId);

        if (commentDTOOptional.isPresent()) {
            throw new AssertionFailedError();
        } else {
            Mockito.verify(commentRepository, Mockito.times(1)).findById(commentId);
        }
    }

    @Test
    void toDTO() {
        Integer userId = 1;
        Integer postId = 2;
        User user = new User("user1", "user1", null, null, null, null, null);
        Post post = new Post("test", "test", localDateTime, user);
        ReflectionTestUtils.setField(user, "id", userId);
        ReflectionTestUtils.setField(post, "id", postId);

        Comment comment = new Comment("text", localDateTime, user, post);

        Integer commentId = 1;
        ReflectionTestUtils.setField(comment, "id", commentId);

        CommentDTO commentDTO = commentService.toDTO(comment);

        AssertCommentsUnitUtil.assertComments(comment, commentDTO, userService, postService);
    }

    @Test
    void toDTOOptional() {
        Integer userId = 1;
        Integer postId = 2;
        User user = new User("user1", "user1", null, null, null, null, null);
        Post post = new Post("test", "test", localDateTime, user);
        ReflectionTestUtils.setField(user, "id", userId);
        ReflectionTestUtils.setField(post, "id", postId);

        Comment comment = new Comment("text", localDateTime, user, post);

        Integer commentId = 1;
        ReflectionTestUtils.setField(comment, "id", commentId);

        Optional<CommentDTO> commentDTO = commentService.toDTO(Optional.of(comment));

        if (commentDTO.isEmpty()) {
            throw new AssertionFailedError();
        }

        AssertCommentsUnitUtil.assertComments(comment, commentDTO.get(), userService, postService);
    }

    @Test
    void deleteById() {
        Integer commentId = 1;

        Mockito.when(commentRepository.existsById(commentId)).thenReturn(true);
        try {
            commentService.deleteById(commentId);
        } catch (NotFoundException e) {
            throw new AssertionFailedError();
        }

        Mockito.when(commentRepository.existsById(commentId)).thenReturn(false);
        try {
            commentService.deleteById(commentId);
            throw new AssertionFailedError();
        } catch (NotFoundException ignored) {
        }

        Mockito.verify(commentRepository, Mockito.times(2)).existsById(commentId);
        Mockito.verify(commentRepository, Mockito.times(1)).deleteById(commentId);
    }

    @Test
    void findAllCommentsOnPost() {
        Integer postId = 1;
        Integer userId = 1;
        User user = new User("test", "test", null, null, null, null, null);
        Post post = new Post("test", "test", localDateTime, user);
        ReflectionTestUtils.setField(user, "id", userId);
        ReflectionTestUtils.setField(post, "id", postId);

        List<Comment> comments = new ArrayList<>();
        int numberOfComments = 5;
        for (int i = 1; i <= numberOfComments; i++) {
            Integer commentId = i;
            Comment comment = new Comment("text" + i, localDateTime, user, post);
            ReflectionTestUtils.setField(comment, "id", commentId);
            comments.add(comment);
        }

        Mockito.when(commentRepository.findAllCommentsOnPost(postId)).thenReturn(comments);
        Mockito.when(postRepository.existsById(postId)).thenReturn(true);

        List<CommentDTO> commentsDTO;
        try {
            commentsDTO = commentService.findAllCommentsOnPost(postId);
        } catch (NotFoundException e) {
            throw new AssertionFailedError();
        }

        Assertions.assertEquals(comments.size(), commentsDTO.size());

        for (int i = 0; i < numberOfComments; i++) {
            AssertCommentsUnitUtil.assertComments(comments.get(i), commentsDTO.get(i), userService, postService);
        }
        Mockito.verify(commentRepository, Mockito.times(1)).findAllCommentsOnPost(postId);
        Mockito.verify(postRepository, Mockito.times(1)).existsById(postId);
    }

    @Test
    void findAllCommentsOnPostNotFound() {
        Integer postId = 1;
        List<Comment> comments = new ArrayList<>();

        Mockito.when(commentRepository.findAllCommentsOnPost(postId)).thenReturn(comments);
        Mockito.when(postRepository.existsById(postId)).thenReturn(false);

        try {
            commentService.findAllCommentsOnPost(postId);
            throw new AssertionFailedError();
        } catch (NotFoundException e) {
            Mockito.verify(commentRepository, Mockito.times(0)).findAllCommentsOnPost(postId);
            Mockito.verify(postRepository, Mockito.times(1)).existsById(postId);
        }
    }
}