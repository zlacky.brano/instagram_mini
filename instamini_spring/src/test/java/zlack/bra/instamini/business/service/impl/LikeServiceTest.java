package zlack.bra.instamini.business.service.impl;

import junit.framework.AssertionFailedError;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.test.util.ReflectionTestUtils;
import util.AssertLikesUnitUtil;
import zlack.bra.instamini.business.dto.*;
import zlack.bra.instamini.business.exception.NotFoundException;
import zlack.bra.instamini.business.service.ImageUploadService;
import zlack.bra.instamini.data.entity.Like;
import zlack.bra.instamini.data.entity.Post;
import zlack.bra.instamini.data.entity.User;
import zlack.bra.instamini.data.repository.LikeRepository;
import zlack.bra.instamini.data.repository.PostRepository;
import zlack.bra.instamini.data.repository.UserRepository;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


@RunWith(MockitoJUnitRunner.class)
class LikeServiceTest {

    private AutoCloseable autoCloseable;

    private PostService postService;
    private UserService userService;
    private LikeService likeService;

    @Mock
    private ImageUploadService imageUploadService;

    @Mock
    private LikeRepository likeRepository;

    @Mock
    private PostRepository postRepository;

    @Mock
    private UserRepository userRepository;

    private final LocalDateTime localDateTime = LocalDateTime.of(LocalDate.of(2000, 12, 12), LocalTime.of(10, 10, 10));

    @BeforeEach
    void setUp() {
        autoCloseable = MockitoAnnotations.openMocks(this);
        userService = new UserService(userRepository, postRepository, "test", "test", 0, "test");
        postService = new PostService(postRepository, userRepository, userService, imageUploadService);
        likeService = new LikeService(likeRepository, postRepository, userRepository, postService, userService);
    }

    @AfterEach
    void tearDown() throws Exception {
        autoCloseable.close();
    }

    @Test
    void create() {
        Integer userId = 1;
        Integer postId = 2;
        LikeCreateDTO likeCreateDTO = new LikeCreateDTO(userId, postId);

        User user = new User("test", "test", null, null, null, null, null);
        Post post = new Post("test", "test", localDateTime, user);

        ReflectionTestUtils.setField(user, "id", userId);
        ReflectionTestUtils.setField(post, "id", postId);

        Integer likeId = 1;
        Like like = new Like(user, post);
        ReflectionTestUtils.setField(like, "id", likeId);

        Mockito.when(likeRepository.save(Mockito.any())).thenReturn(like);
        Mockito.when(userRepository.findById(userId)).thenReturn(Optional.of(user));
        Mockito.when(postRepository.findById(postId)).thenReturn(Optional.of(post));

        LikeDTO likeDTO;
        try {
            likeDTO = likeService.create(likeCreateDTO);
        } catch (NotFoundException e) {
            throw new AssertionFailedError();
        }

        Assertions.assertNotNull(likeDTO);
        AssertLikesUnitUtil.assertLikes(like, likeDTO, userService, postService);

        Mockito.verify(likeRepository, Mockito.times(1)).save(Mockito.any());
        Mockito.verify(userRepository, Mockito.times(1)).findById(userId);
        Mockito.verify(postRepository, Mockito.times(1)).findById(postId);
    }

    @Test
    void createUserNotFound() {
        Integer userId = 1;
        Integer postId = 2;
        LikeCreateDTO likeCreateDTO = new LikeCreateDTO(userId, postId);

        User user = new User("test", "test", null, null, null, null, null);
        Post post = new Post("test", "test", localDateTime, user);

        ReflectionTestUtils.setField(user, "id", userId);
        ReflectionTestUtils.setField(post, "id", postId);

        Integer likeId = 1;
        Like like = new Like(user, post);
        ReflectionTestUtils.setField(like, "id", likeId);

        Mockito.when(likeRepository.save(Mockito.any())).thenReturn(like);
        Mockito.when(userRepository.findById(userId)).thenReturn(Optional.empty());
        Mockito.when(postRepository.findById(postId)).thenReturn(Optional.of(post));

        try {
            likeService.create(likeCreateDTO);
            throw new AssertionFailedError();
        } catch (NotFoundException e) {
            Mockito.verify(likeRepository, Mockito.times(0)).save(Mockito.any());
            Mockito.verify(userRepository, Mockito.times(1)).findById(userId);
            Mockito.verify(postRepository, Mockito.times(1)).findById(postId);
        }
    }

    @Test
    void createPostNotFound() {
        Integer userId = 1;
        Integer postId = 2;
        LikeCreateDTO likeCreateDTO = new LikeCreateDTO(userId, postId);

        User user = new User("test", "test", null, null, null, null, null);
        Post post = new Post("test", "test", localDateTime, user);

        ReflectionTestUtils.setField(user, "id", userId);
        ReflectionTestUtils.setField(post, "id", postId);

        Integer likeId = 1;
        Like like = new Like(user, post);
        ReflectionTestUtils.setField(like, "id", likeId);

        Mockito.when(likeRepository.save(Mockito.any())).thenReturn(like);
        Mockito.when(userRepository.findById(userId)).thenReturn(Optional.of(user));
        Mockito.when(postRepository.findById(postId)).thenReturn(Optional.empty());

        try {
            likeService.create(likeCreateDTO);
            throw new AssertionFailedError();
        } catch (NotFoundException e) {
            Mockito.verify(likeRepository, Mockito.times(0)).save(Mockito.any());
            Mockito.verify(userRepository, Mockito.times(1)).findById(userId);
            Mockito.verify(postRepository, Mockito.times(1)).findById(postId);
        }
    }

    @Test
    void findAll() {
        List<Like> likes = new ArrayList<>();
        int numberOfItems = 3;
        int idOfUser = 1;
        int idOfPost = 1;

        for (int i = 1; i <= numberOfItems; i++) {
            User user = new User("test" + i, "test" + i, null, null, null, null, null);
            Post post = new Post("test" + i, "test" + i, localDateTime, user);
            ReflectionTestUtils.setField(user, "id", idOfUser++);
            ReflectionTestUtils.setField(post, "id", idOfPost++);

            Like like = new Like(user, post);
            ReflectionTestUtils.setField(like, "id", i);
            likes.add(like);
        }

        Mockito.when(likeRepository.findAll()).thenReturn(likes);

        List<LikeDTO> likesDTO = likeService.findAll();

        Assertions.assertEquals(likes.size(), likesDTO.size());
        for (int i = 0; i < likesDTO.size(); i++) {
            AssertLikesUnitUtil.assertLikes(likes.get(i), likesDTO.get(i), userService, postService);
        }
        Mockito.verify(likeRepository, Mockito.times(1)).findAll();
    }

    @Test
    void findAllEmpty() {
        List<Like> likes = new ArrayList<>();

        Mockito.when(likeRepository.findAll()).thenReturn(likes);

        List<LikeDTO> likesDTO = likeService.findAll();

        Assertions.assertEquals(likes.size(), likesDTO.size());
        Mockito.verify(likeRepository, Mockito.times(1)).findAll();
    }

    @Test
    void findByIds() {
        List<Integer> ids = new ArrayList<>();
        List<Like> likes = new ArrayList<>();
        int numberOfItems = 3;
        int idOfUser = 1;
        int idOfPost = 1;

        for (int i = 1; i <= numberOfItems; i++) {
            User user = new User("test" + i, "test" + i, null, null, null, null, null);
            Post post = new Post("test" + i, "test" + i, localDateTime, user);
            ReflectionTestUtils.setField(user, "id", idOfUser++);
            ReflectionTestUtils.setField(post, "id", idOfPost++);

            Like like = new Like(user, post);
            ReflectionTestUtils.setField(like, "id", i);
            likes.add(like);
            ids.add(i);
        }

        Mockito.when(likeRepository.findAllById(ids)).thenReturn(likes);

        List<LikeDTO> likesDTO = likeService.findByIds(ids);

        Assertions.assertEquals(likes.size(), likesDTO.size());

        for (int i = 0; i < likesDTO.size(); i++) {
            AssertLikesUnitUtil.assertLikes(likes.get(i), likesDTO.get(i), userService, postService);
        }
        Mockito.verify(likeRepository, Mockito.times(1)).findAllById(ids);
    }

    @Test
    void findByIdsEmpty() {
        List<Integer> ids = new ArrayList<>();
        List<Like> likes = new ArrayList<>();

        Mockito.when(likeRepository.findAllById(ids)).thenReturn(likes);

        List<LikeDTO> likesDTO = likeService.findByIds(ids);

        Assertions.assertEquals(likes.size(), likesDTO.size());

        Mockito.verify(likeRepository, Mockito.times(1)).findAllById(ids);
    }

    @Test
    void findById() {
        Integer userId = 1;
        Integer postId = 2;
        User user = new User("test", "test", null, null, null, null, null);
        Post post = new Post("test", "test", localDateTime, user);
        ReflectionTestUtils.setField(user, "id", userId);
        ReflectionTestUtils.setField(post, "id", postId);

        Integer likeId = 3;
        Like like = new Like(user, post);
        ReflectionTestUtils.setField(like, "id", likeId);

        Mockito.when(likeRepository.findById(likeId)).thenReturn(Optional.of(like));

        Optional<LikeDTO> likeDTOOptional = likeService.findById(likeId);

        if (likeDTOOptional.isPresent()) {
            LikeDTO likeDTO = likeDTOOptional.get();
            AssertLikesUnitUtil.assertLikes(like, likeDTO, userService, postService);
        } else {
            throw new AssertionFailedError();
        }
        Mockito.verify(likeRepository, Mockito.times(1)).findById(likeId);
    }

    @Test
    void findByIdNotFound() {
        Integer likeId = 1;

        Mockito.when(likeRepository.findById(likeId)).thenReturn(Optional.empty());

        Optional<LikeDTO> likeDTOOptional = likeService.findById(likeId);

        if (likeDTOOptional.isPresent()) {
            throw new AssertionFailedError();
        } else {
            Mockito.verify(likeRepository, Mockito.times(1)).findById(likeId);
        }
    }

    @Test
    void toDTO() {
        Integer userId = 1;
        Integer postId = 2;
        User user = new User("test", "test", null, null, null, null, null);
        Post post = new Post("test", "test", localDateTime, user);
        ReflectionTestUtils.setField(user, "id", userId);
        ReflectionTestUtils.setField(post, "id", postId);

        Integer likeId = 3;
        Like like = new Like(user, post);
        ReflectionTestUtils.setField(like, "id", likeId);

        LikeDTO likeDTO = likeService.toDTO(like);

        AssertLikesUnitUtil.assertLikes(like, likeDTO, userService, postService);
    }

    @Test
    void toDTOOptional() {
        Integer userId = 1;
        Integer postId = 2;
        User user = new User("test", "test", null, null, null, null, null);
        Post post = new Post("test", "test", localDateTime, user);
        ReflectionTestUtils.setField(user, "id", userId);
        ReflectionTestUtils.setField(post, "id", postId);

        Integer likeId = 3;
        Like like = new Like(user, post);
        ReflectionTestUtils.setField(like, "id", likeId);

        Optional<LikeDTO> likeDTO = likeService.toDTO(Optional.of(like));

        if (likeDTO.isEmpty()) {
            throw new AssertionFailedError();
        }

        AssertLikesUnitUtil.assertLikes(like, likeDTO.get(), userService, postService);
    }

    @Test
    void deleteById() {
        Integer likeId = 1;

        Mockito.when(likeRepository.existsById(likeId)).thenReturn(true);
        try {
            likeService.deleteById(likeId);
        } catch (NotFoundException e) {
            throw new AssertionFailedError();
        }

        Mockito.when(likeRepository.existsById(likeId)).thenReturn(false);
        try {
            likeService.deleteById(likeId);
            throw new AssertionFailedError();
        } catch (NotFoundException ignored) {
        }

        Mockito.verify(likeRepository, Mockito.times(2)).existsById(likeId);
        Mockito.verify(likeRepository, Mockito.times(1)).deleteById(likeId);
    }

    @Test
    void findLikesOnPost() {
        Integer postId = 1;
        Integer userId = 1;
        User user = new User("test", "test", null, null, null, null, null);
        Post post = new Post("test", "test", localDateTime, user);
        ReflectionTestUtils.setField(user, "id", userId);
        ReflectionTestUtils.setField(post, "id", postId);

        List<Like> likes = new ArrayList<>();
        int numberOfLikes = 5;
        for (int i = 1; i <= numberOfLikes; i++) {
            Integer likeId = i;
            Like like = new Like(user, post);
            ReflectionTestUtils.setField(like, "id", likeId);

            likes.add(like);
        }

        Mockito.when(likeRepository.findLikesOnPost(postId)).thenReturn(likes);
        Mockito.when(postRepository.existsById(postId)).thenReturn(true);

        List<LikeDTO> likesDTO;
        try {
            likesDTO = likeService.findLikesOnPost(postId);
        } catch (NotFoundException e) {
            throw new AssertionFailedError();
        }

        Assertions.assertEquals(likes.size(), likesDTO.size());

        for (int i = 0; i < numberOfLikes; i++) {
            AssertLikesUnitUtil.assertLikes(likes.get(i), likesDTO.get(i), userService, postService);
        }
        Mockito.verify(likeRepository, Mockito.times(1)).findLikesOnPost(postId);
        Mockito.verify(postRepository, Mockito.times(1)).existsById(postId);
    }

    @Test
    void findLikesOnPostNotFound() {
        Integer postId = 1;
        List<Like> likes = new ArrayList<>();

        Mockito.when(likeRepository.findLikesOnPost(postId)).thenReturn(likes);
        Mockito.when(postRepository.existsById(postId)).thenReturn(false);

        try {
            likeService.findLikesOnPost(postId);
            throw new AssertionFailedError();
        } catch (NotFoundException e) {
            Mockito.verify(likeRepository, Mockito.times(0)).findLikesOnPost(postId);
            Mockito.verify(postRepository, Mockito.times(1)).existsById(postId);
        }
    }

    @Test
    void findNumberOfLikesOnPost() {
        Integer postId = 1;
        Integer numberOfPosts = 5;

        Mockito.when(likeRepository.findNumberOfLikesOnPost(postId)).thenReturn(numberOfPosts);
        Mockito.when(postRepository.existsById(postId)).thenReturn(true);

        Integer numberOfPostsOutput;
        try {
            numberOfPostsOutput = likeService.findNumberOfLikesOnPost(postId);
        } catch (NotFoundException e) {
            throw new AssertionFailedError();
        }

        Assertions.assertEquals(numberOfPosts, numberOfPostsOutput);

        Mockito.verify(likeRepository, Mockito.times(1)).findNumberOfLikesOnPost(postId);
        Mockito.verify(postRepository, Mockito.times(1)).existsById(postId);
    }

    @Test
    void findNumberOfLikesOnPostNotFound() {
        Integer postId = 1;

        Mockito.when(likeRepository.findNumberOfLikesOnPost(postId)).thenReturn(0);
        Mockito.when(postRepository.existsById(postId)).thenReturn(false);

        try {
            likeService.findNumberOfLikesOnPost(postId);
            throw new AssertionFailedError();
        } catch (NotFoundException e) {
            Mockito.verify(likeRepository, Mockito.times(0)).findNumberOfLikesOnPost(postId);
            Mockito.verify(postRepository, Mockito.times(1)).existsById(postId);
        }
    }

    @Test
    void findLikeIdByUserAndPost() {
        Integer likeId = 1;
        Integer userId = 1;
        Integer postId = 2;

        Mockito.when(likeRepository.findLikeIdByUserAndPost(userId, postId)).thenReturn(Optional.of(likeId));

        Integer resultId;
        try {
            resultId = likeService.findLikeIdByUserAndPost(userId, postId);
        } catch (NotFoundException e) {
            throw new AssertionFailedError();
        }

        Assertions.assertEquals(likeId, resultId);
        Mockito.verify(likeRepository, Mockito.times(1)).findLikeIdByUserAndPost(userId, postId);
    }

    @Test
    void findLikeIdByUserAndPostNotFound() {
        Integer userId = 1;
        Integer postId = 2;

        Mockito.when(likeRepository.findLikeIdByUserAndPost(userId, postId)).thenReturn(Optional.empty());

        try {
            likeService.findLikeIdByUserAndPost(userId, postId);
            throw new AssertionFailedError();
        } catch (NotFoundException e) {
            Mockito.verify(likeRepository, Mockito.times(1)).findLikeIdByUserAndPost(userId, postId);
        }
    }
}