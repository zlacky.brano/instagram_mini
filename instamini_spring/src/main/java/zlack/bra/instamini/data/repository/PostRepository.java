package zlack.bra.instamini.data.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import zlack.bra.instamini.data.entity.Post;

import java.util.List;

@Repository
public interface PostRepository extends JpaRepository<Post, Integer> {
    @Query(
            value = "SELECT * FROM Post as p WHERE p.user_id = :idUser ORDER BY p.time DESC",
            nativeQuery = true
    )
    List<Post> findAllPostsOfUser(Integer idUser);

    @Query(
            value = "SELECT COUNT(*) FROM Post as p WHERE p.user_id = :idUser",
            nativeQuery = true
    )
    Integer findCountOfAllPostsOfUser(Integer idUser);
}
