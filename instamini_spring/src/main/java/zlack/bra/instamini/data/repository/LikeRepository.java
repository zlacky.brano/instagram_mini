package zlack.bra.instamini.data.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import zlack.bra.instamini.data.entity.Like;

import java.util.List;
import java.util.Optional;

@Repository
public interface LikeRepository extends JpaRepository<Like, Integer> {
    @Query(
            value = "SELECT COUNT(*) FROM `Like` as l WHERE l.post_id = :idPost",
            nativeQuery = true
    )
    Integer findNumberOfLikesOnPost(Integer idPost);

    @Query(
            value = "SELECT * FROM `Like` as l WHERE l.post_id = :idPost",
            nativeQuery = true
    )
    List<Like> findLikesOnPost(Integer idPost);

    @Query(
            value = "SELECT id FROM `Like` WHERE user_id = :userId AND post_id = :postId",
            nativeQuery = true
    )
    Optional<Integer> findLikeIdByUserAndPost(Integer userId, Integer postId);
}
