package zlack.bra.instamini.data.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import zlack.bra.instamini.data.entity.User;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Integer> {
    @Query(
            value = "SELECT u.id, u.username, u.email FROM `Like` as l JOIN `User` as u ON(l.user_id = u.id) WHERE l.post_id = :idPost",
            nativeQuery = true
    )
    List<User> findAllUsersWhoLikedPost(Integer idPost);

    @Query(
            value = "SELECT u.id, u.username, u.email FROM Follow as f JOIN `User` as u ON(f.user_id_who = u.id) WHERE f.user_id_whom=:id",
            nativeQuery = true
    )
    List<User> findAllFollowers(Integer id);

    @Query(
            value = "SELECT u.id, u.username, u.email FROM Follow as f JOIN `User` as u ON(f.user_id_whom = u.id) WHERE f.user_id_who=:id",
            nativeQuery = true
    )
    List<User> findAllFollowing(Integer id);

    @Query(
            value = "SELECT * FROM `User` as u WHERE u.email = :email",
            nativeQuery = true
    )
    Optional<User> findByEmail(String email);

    @Query(
            value = "SELECT COUNT(*) FROM Follow as f JOIN `User` as u ON(f.user_id_who = u.id) WHERE f.user_id_whom=:id",
            nativeQuery = true
    )
    Integer findCountOfAllFollowers(Integer id);

    @Query(
            value = "SELECT COUNT(*) FROM Follow as f JOIN `User` as u ON(f.user_id_whom = u.id) WHERE f.user_id_who=:id",
            nativeQuery = true
    )
    Integer findCountOfAllFollowing(Integer id);

}
