package zlack.bra.instamini.data.entity;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;

import static zlack.bra.instamini.constant.Constants.MAX_LENGTH_DESCRIPTION_AND_TEXT;

@Setter
@Getter
@NoArgsConstructor
@Entity
@Table(name = "Comment")
public class Comment {

    @Setter(value = AccessLevel.NONE)
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Size(max = MAX_LENGTH_DESCRIPTION_AND_TEXT)
    @NotBlank
    @Column(nullable = false)
    private String text;

    @NotNull
    @Column(columnDefinition = "TIMESTAMP", nullable = false)
    private LocalDateTime time;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "user_id", nullable = false)
    private User user;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "post_id", nullable = false)
    private Post post;

    public Comment(String text, LocalDateTime time, User user, Post post) {
        this.text = text;
        this.time = time;
        this.user = user;
        this.post = post;
    }

    @Override
    public String toString() {
        return "Comment{" +
                "id=" + id +
                ", text='" + text + '\'' +
                ", time=" + time +
                ", user=" + user +
                ", post=" + post +
                '}';
    }
}
