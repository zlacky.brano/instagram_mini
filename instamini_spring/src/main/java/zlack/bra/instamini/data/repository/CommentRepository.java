package zlack.bra.instamini.data.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import zlack.bra.instamini.data.entity.Comment;

import java.util.List;

@Repository
public interface CommentRepository extends JpaRepository<Comment, Integer> {
    @Query(
            value = "SELECT * FROM Comment as c WHERE c.post_id = :idPost ORDER BY c.time DESC",
            nativeQuery = true
    )
    List<Comment> findAllCommentsOnPost(Integer idPost);
}
