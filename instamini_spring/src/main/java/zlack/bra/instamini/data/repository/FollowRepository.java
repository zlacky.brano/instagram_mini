package zlack.bra.instamini.data.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import zlack.bra.instamini.data.entity.Follow;

import java.util.Optional;

@Repository
public interface FollowRepository extends JpaRepository<Follow, Integer> {
    @Query(
            value = "SELECT id FROM Follow WHERE user_id_who = :userIdWho AND user_id_whom = :userIdWhom",
            nativeQuery = true
    )
    Optional<Integer> findFollowIdByUsers(Integer userIdWho, Integer userIdWhom);
}
