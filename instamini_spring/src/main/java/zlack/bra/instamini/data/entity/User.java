package zlack.bra.instamini.data.entity;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.util.List;

import static zlack.bra.instamini.constant.Constants.MAX_LENGTH_USERNAME_AND_EMAIL;

@Setter
@Getter
@NoArgsConstructor
@Entity
public class User {

    @Setter(value = AccessLevel.NONE)
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Size(max = MAX_LENGTH_USERNAME_AND_EMAIL)
    @Email
    @NotBlank
    @Column(nullable = false, unique = true)
    private String email;

    @Size(max = MAX_LENGTH_USERNAME_AND_EMAIL)
    @NotBlank
    @Column(nullable = false, unique = true)
    private String username;

    @OneToMany(mappedBy = "userWho", cascade = CascadeType.REMOVE)
    private List<Follow> following;

    @OneToMany(mappedBy = "userWhom", cascade = CascadeType.REMOVE)
    private List<Follow> followed;

    @OneToMany(mappedBy = "user", cascade = CascadeType.REMOVE)
    private List<Post> posts;

    @OneToMany(mappedBy = "user", cascade = CascadeType.REMOVE)
    private List<Like> likes;

    @OneToMany(mappedBy = "user", cascade = CascadeType.REMOVE)
    private List<Comment> comments;

    public User(String email, String username,
                List<Follow> following, List<Follow> followed, List<Post> posts, List<Like> likes, List<Comment> comments) {
        this.email = email;
        this.username = username;
        this.following = following;
        this.followed = followed;
        this.posts = posts;
        this.likes = likes;
        this.comments = comments;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", email='" + email + '\'' +
                ", username='" + username + '\'' +
                ", following=" + following +
                ", followed=" + followed +
                ", posts=" + posts +
                '}';
    }
}
