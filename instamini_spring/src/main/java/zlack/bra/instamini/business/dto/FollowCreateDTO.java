package zlack.bra.instamini.business.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class FollowCreateDTO {

    @NotNull
    private Integer userWhoId;

    @NotNull
    private Integer userWhomId;

    public String toJson() {
        return "{" +
                "\"userWhoId\": \"" + userWhoId + "\"," +
                "\"userWhomId\": \"" + userWhomId + "\"" +
                "}";
    }
}
