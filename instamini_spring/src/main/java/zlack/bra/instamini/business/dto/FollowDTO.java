package zlack.bra.instamini.business.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class FollowDTO {

    private Integer id;
    private UserDTO userWho;
    private UserDTO userWhom;
}
