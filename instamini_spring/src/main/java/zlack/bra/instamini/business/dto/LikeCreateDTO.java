package zlack.bra.instamini.business.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class LikeCreateDTO {

    @NotNull
    private Integer userId;

    @NotNull
    private Integer postId;

    public String toJson() {
        return "{" +
                "\"userId\": \"" + userId + "\"," +
                "\"postId\": \"" + postId + "\"" +
                "}";
    }
}
