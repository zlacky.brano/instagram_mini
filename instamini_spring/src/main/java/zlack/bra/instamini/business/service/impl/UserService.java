package zlack.bra.instamini.business.service.impl;

import lombok.extern.slf4j.Slf4j;
import org.apache.http.HttpHost;
import org.elasticsearch.ElasticsearchException;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import zlack.bra.instamini.business.dto.UserCreateDTO;
import zlack.bra.instamini.business.dto.UserDTO;
import zlack.bra.instamini.business.service.UserServiceInt;
import zlack.bra.instamini.data.entity.User;
import zlack.bra.instamini.business.exception.NotFoundException;
import zlack.bra.instamini.data.repository.PostRepository;
import zlack.bra.instamini.data.repository.UserRepository;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

@Slf4j
@Service
@Transactional
public class UserService implements UserServiceInt {

    private final UserRepository userRepository;
    private final PostRepository postRepository;

    private final String usersIndex;

    private final RestHighLevelClient client;

    @Autowired
    public UserService(UserRepository userRepository, PostRepository postRepository,
                       @Value("${elasticsearch.index.users}") String usersIndex,
                       @Value("${elasticsearch.host.hostname}") String hostname,
                       @Value("${elasticsearch.host.port}") Integer port,
                       @Value("${elasticsearch.host.scheme}") String scheme) {
        this.userRepository = userRepository;
        this.postRepository = postRepository;
        this.usersIndex = usersIndex;
        this.client = new RestHighLevelClient(RestClient.builder(new HttpHost(hostname, port, scheme)));
    }

    @Override
    public List<UserDTO> searchUsers(String usernamePrefix) {
        log.info("Collecting users with elastic search by given prefix.");
        QueryBuilder matchQueryBuilder = QueryBuilders.prefixQuery("username", usernamePrefix)
                .caseInsensitive(true);
        SearchSourceBuilder sourceBuilder = new SearchSourceBuilder().query(matchQueryBuilder);
        SearchRequest searchRequest = new SearchRequest(usersIndex);
        searchRequest.source(sourceBuilder);
        SearchResponse searchResponse;
        try {
            searchResponse = client.search(searchRequest, RequestOptions.DEFAULT);
        } catch (IOException e) {
            log.warn("Exception in searching users with elastic search.");
            throw new ElasticsearchException(e);
        }

        List<UserDTO> result = new ArrayList<>();
        SearchHit[] searchHits = searchResponse.getHits().getHits();
        for (SearchHit hit : searchHits) {
            Map<String, Object> sourceAsMap = hit.getSourceAsMap();
            UserDTO userDTO = new UserDTO();
            userDTO.setId((Integer) sourceAsMap.get("id"));
            userDTO.setUsername((String) sourceAsMap.get("username"));
            userDTO.setEmail((String) sourceAsMap.get("email"));
            result.add(userDTO);
        }

        return result;
    }

    @Override
    public UserDTO editUsername(Integer id, String username) throws NotFoundException {
        Optional<User> userOptional = userRepository.findById(id);
        if (userOptional.isEmpty()) {
            log.warn("Trying to edit username to user, which does not exist.");
            throw new NotFoundException("No such user.");
        }

        User user = userOptional.get();
        user.setUsername(username);

        log.info("Username of user: " + userOptional.get().getEmail() + " is going to be edited.");

        return toDTO(user);
    }

    @Override
    public UserDTO create(UserCreateDTO userCreateDTO) {
        UserDTO dto = toDTO(userRepository.save(new User(userCreateDTO.getEmail(), userCreateDTO.getUsername(), null, null, null, null, null)));

        log.info("User " + userCreateDTO.getEmail() + " has been created.");

        return dto;
    }

    @Override
    public List<UserDTO> findAll() {
        log.info("Collecting all users.");
        return userRepository.findAll().stream().map(this::toDTO).collect(Collectors.toList());
    }

    @Override
    public List<UserDTO> findByIds(List<Integer> ids) {
        log.info("Collecting all users with specified ids.");
        return userRepository.findAllById(ids).stream().map(this::toDTO).collect(Collectors.toList());
    }

    @Override
    public Optional<UserDTO> findById(Integer id) {
        log.info("Finding one specified user by id.");
        return toDTO(userRepository.findById(id));
    }

    public Optional<UserDTO> findByEmail(String email) {
        log.info("Finding one specified user by email.");
        return toDTO(userRepository.findByEmail(email));
    }

    @Override
    public UserDTO toDTO(User user) {
        return new UserDTO(user.getId(), user.getEmail(), user.getUsername());
    }

    @Override
    public Optional<UserDTO> toDTO(Optional<User> user) {
        if (user.isEmpty()) {
            return Optional.empty();
        }
        return Optional.of(toDTO(user.get()));
    }

    @Override
    public void deleteById(Integer id) throws NotFoundException {
        if (userRepository.existsById(id)) {
            userRepository.deleteById(id);
            log.info("User with id: " + id + " has been deleted.");
        } else {
            log.warn("Trying to delete user, that does not exist.");
            throw new NotFoundException("No such user.");
        }
    }

    @Override
    public List<UserDTO> findAllUsersWhoLikedPost(Integer idPost) throws NotFoundException {
        if (postRepository.existsById(idPost)) {
            log.info("Collecting all users, which liked post: " + idPost + ".");
            return userRepository.findAllUsersWhoLikedPost(idPost).stream().map(this::toDTO).collect(Collectors.toList());
        }
        log.warn("Trying to find all users, which liked post");
        throw new NotFoundException("No such post.");
    }

    @Override
    public List<UserDTO> findAllFollowers(Integer id) throws NotFoundException {
        if (!userRepository.existsById(id)) {
            throw new NotFoundException("No such user.");
        }
        log.info("Collecting all users, which follow user with id: " + id + ".");
        return userRepository.findAllFollowers(id).stream().map(this::toDTO).collect(Collectors.toList());
    }

    @Override
    public List<UserDTO> findAllFollowing(Integer id) throws NotFoundException {
        if (!userRepository.existsById(id)) {
            throw new NotFoundException("No such user.");
        }
        log.info("Collecting all users, that user with id: " + id + " follows.");
        return userRepository.findAllFollowing(id).stream().map(this::toDTO).collect(Collectors.toList());
    }

    @Override
    public Integer findCountOfAllFollowers(Integer id) throws NotFoundException {
        if (!userRepository.existsById(id)) {
            throw new NotFoundException("No such user.");
        }
        log.info("Finding count of all users, which follow user with id: " + id + ".");
        return userRepository.findCountOfAllFollowers(id);
    }

    @Override
    public Integer findCountOfAllFollowing(Integer id) throws NotFoundException {
        if (!userRepository.existsById(id)) {
            throw new NotFoundException("No such user.");
        }
        log.info("Finding count of all users, that user with id: " + id + " follows.");
        return userRepository.findCountOfAllFollowing(id);
    }
}
