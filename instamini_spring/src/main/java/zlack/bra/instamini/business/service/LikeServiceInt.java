package zlack.bra.instamini.business.service;

import zlack.bra.instamini.business.dto.LikeCreateDTO;
import zlack.bra.instamini.business.dto.LikeDTO;
import zlack.bra.instamini.business.exception.NotFoundException;
import zlack.bra.instamini.data.entity.Like;

import java.util.List;
import java.util.Optional;

public interface LikeServiceInt {

    /**
     * Creates new like
     * @param likeCreateDTO object with necessary values to create like
     * @return created like
     * @throws NotFoundException if user or post does not exist
     */
    LikeDTO create(LikeCreateDTO likeCreateDTO) throws NotFoundException;

    /**
     * Finds all likes
     * @return all likes
     */
    List<LikeDTO> findAll();

    /**
     * Finds likes by ids
     * @param ids
     * @return found likes
     */
    List<LikeDTO> findByIds(List<Integer> ids);

    /**
     * Find like by id
     * @param id
     * @return found like
     */
    Optional<LikeDTO> findById(Integer id);

    /**
     * Converts Like to LikeDTO
     * @param like to be converted
     * @return converted like
     */
    LikeDTO toDTO(Like like);

    /**
     * Converts Optional-Like to Optional-LikeDTO
     * @param like to be converted
     * @return converted like
     */
    Optional<LikeDTO> toDTO(Optional<Like> like);

    /**
     * Deletes like by id
     * @param id
     * @throws NotFoundException if like does not exist
     */
    void deleteById(Integer id) throws NotFoundException;

    /**
     * Finds number of likes on post
     * @param idPost id of post
     * @return found number of likes on post
     * @throws NotFoundException if post does not exist
     */
    Integer findNumberOfLikesOnPost(Integer idPost) throws NotFoundException;

    /**
     * Finds likes on post
     * @param idPost id of post
     * @return found likes on post
     * @throws NotFoundException if post does not exist
     */
    List<LikeDTO> findLikesOnPost(Integer idPost) throws NotFoundException;

    /**
     * Finds follow id
     * @param userId
     * @param postId
     * @return found id of follow
     * @throws NotFoundException if like does not exist
     */
    Integer findLikeIdByUserAndPost(Integer userId, Integer postId) throws NotFoundException;

}