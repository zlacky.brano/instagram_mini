package zlack.bra.instamini.business.service;

import zlack.bra.instamini.business.dto.UserCreateDTO;
import zlack.bra.instamini.business.dto.UserDTO;
import zlack.bra.instamini.business.exception.NotFoundException;
import zlack.bra.instamini.data.entity.User;

import java.util.List;
import java.util.Optional;

public interface UserServiceInt {

    /**
     * Finds users by given prefix
     * @param usernamePrefix
     * @return found users
     */
    List<UserDTO> searchUsers(String usernamePrefix);

    /**
     * Edits username of user
     * @param id of user
     * @param username new username
     * @return updated user
     * @throws NotFoundException if user was not found
     */
    UserDTO editUsername(Integer id, String username) throws NotFoundException;

    /**
     * Creates new user
     * @param userCreateDTO object with necessary values to create user
     * @return created user
     */
    UserDTO create(UserCreateDTO userCreateDTO);

    /**
     * Finds all users
     * @return all users
     */
    List<UserDTO> findAll();

    /**
     * Finds users by ids
     * @param ids
     * @return found users
     */
    List<UserDTO> findByIds(List<Integer> ids);

    /**
     * Finds user by id
     * @param id
     * @return found user
     */
    Optional<UserDTO> findById(Integer id);

    /**
     * Finds user by email
     * @param email
     * @return found user
     */
    Optional<UserDTO> findByEmail(String email);

    /**
     * Converts User to UserDTO
     * @param user to be converted
     * @return converted user
     */
    UserDTO toDTO(User user);

    /**
     * Converts Optional-User to Optional-UserDTO
     * @param user to be converted
     * @return converted user
     */
    Optional<UserDTO> toDTO(Optional<User> user);

    /**
     * Deletes user by id
     * @param id of user
     * @throws NotFoundException if user does not exist
     */
    void deleteById(Integer id) throws NotFoundException;

    /**
     * Finds all users who liked post
     * @param idPost id of post
     * @return found users
     * @throws NotFoundException if post does not exist
     */
    List<UserDTO> findAllUsersWhoLikedPost(Integer idPost) throws NotFoundException;

    /**
     * Finds all followers of user
     * @param id of user
     * @return found users
     * @throws NotFoundException if user does not exist
     */
    List<UserDTO> findAllFollowers(Integer id) throws NotFoundException;

    /**
     * Finds all users who user with id is following
     * @param id of user
     * @return found users
     * @throws NotFoundException if user does not exist
     */
    List<UserDTO> findAllFollowing(Integer id) throws NotFoundException;

    /**
     * Finds number of all followers
     * @param id of user
     * @return number of all followers
     * @throws NotFoundException if user does not exist
     */
    Integer findCountOfAllFollowers(Integer id) throws NotFoundException;

    /**
     * Finds number of all users who user with id is following
     * @param id of user
     * @return number of all followers
     * @throws NotFoundException if user does not exist
     */
    Integer findCountOfAllFollowing(Integer id) throws NotFoundException;
}
