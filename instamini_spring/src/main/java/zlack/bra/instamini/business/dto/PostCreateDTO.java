package zlack.bra.instamini.business.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.IOException;
import java.util.Arrays;

import static zlack.bra.instamini.constant.Constants.MAX_LENGTH_DESCRIPTION_AND_TEXT;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PostCreateDTO {

    @NotBlank
    @Size(max = MAX_LENGTH_DESCRIPTION_AND_TEXT)
    private String description;

    @NotNull
    private Integer userId;

    @NotNull
    private MultipartFile image;
}
