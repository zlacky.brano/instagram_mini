package zlack.bra.instamini.business.service;

import zlack.bra.instamini.business.dto.CommentCreateDTO;
import zlack.bra.instamini.business.dto.CommentDTO;
import zlack.bra.instamini.business.exception.NotFoundException;
import zlack.bra.instamini.data.entity.Comment;

import java.util.List;
import java.util.Optional;

public interface CommentServiceInt {

    /**
     * Edits text of comment
     * @param id of comment
     * @param text new text
     * @return upldated comment if form of DTO
     * @throws NotFoundException if comment does not exist
     */
    CommentDTO editText(Integer id, String text) throws NotFoundException;

    /**
     * Creates new comment
     * @param commentCreateDTO object holding all necessary values for creating comment
     * @return created comment
     * @throws NotFoundException if post or user does not exist
     */
    CommentDTO create(CommentCreateDTO commentCreateDTO) throws NotFoundException;

    /**
     * Finds all comments
     * @return all comments
     */
    List<CommentDTO> findAll();

    /**
     * Finds comments by ids
     * @param ids with these we will find comments
     * @return found comments
     */
    List<CommentDTO> findByIds(List<Integer> ids);

    /**
     * Finds comment by id
     * @param id with this we will find comment
     * @return optional of found comment
     */
    Optional<CommentDTO> findById(Integer id);

    /**
     * Converts Comment to CommentDTO
     * @param comment to be converted
     * @return converted comment
     */
    CommentDTO toDTO(Comment comment);

    /**
     * Converts Optional-Comment to Optional-CommentDTO
     * @param comment to be converted
     * @return converted comment
     */
    Optional<CommentDTO> toDTO(Optional<Comment> comment);

    /**
     * Deletes comment by id
     * @param id with this we will find and delete comment
     * @throws NotFoundException if comment with given id does not exist
     */
    void deleteById(Integer id) throws NotFoundException;

    /**
     * Finds all comments on post
     * @param idPost find comments on post with this id
     * @return found comments
     * @throws NotFoundException if post does not exist
     */
    List<CommentDTO> findAllCommentsOnPost(Integer idPost) throws NotFoundException;
}
