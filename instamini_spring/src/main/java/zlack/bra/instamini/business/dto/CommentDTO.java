package zlack.bra.instamini.business.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CommentDTO {

    private Integer id;
    private String text;
    private LocalDateTime time;
    private UserDTO user;
    private PostDTO post;
}
