package zlack.bra.instamini.business.service;

import zlack.bra.instamini.business.dto.PostCreateDTO;
import zlack.bra.instamini.business.dto.PostDTO;
import zlack.bra.instamini.business.exception.NotFoundException;
import zlack.bra.instamini.business.exception.UploadFailException;
import zlack.bra.instamini.data.entity.Post;

import java.util.List;
import java.util.Optional;

public interface PostServiceInt {

    /**
     * Edits description of post
     * @param id of post
     * @param description new description
     * @return updated post
     * @throws NotFoundException if post does not exist
     */
    PostDTO editDescription(Integer id, String description) throws NotFoundException;

    /**
     * Creates new post
     * @param postCreateDTO object with necessary values to create psot
     * @return created post
     * @throws NotFoundException if user does not exist
     * @throws UploadFailException if upload fails
     */
    PostDTO create(PostCreateDTO postCreateDTO) throws NotFoundException, UploadFailException;

    /**
     * Find all posts
     * @return all posts
     */
    List<PostDTO> findAll();

    /**
     * Find posts by ids
     * @param ids
     * @return found posts
     */
    List<PostDTO> findByIds(List<Integer> ids);

    /**
     * Find post by id
     * @param id
     * @return found post
     */
    Optional<PostDTO> findById(Integer id);

    /**
     * Converts Post to PostDTO
     * @param post to be converted
     * @return converted post
     */
    PostDTO toDTO(Post post);

    /**
     * Converts Optional-Post to Optional-PostDTO
     * @param post to be converted
     * @return converted post
     */
    Optional<PostDTO> toDTO(Optional<Post> post);

    /**
     * Deletes post by id
     * @param id
     * @throws NotFoundException if post does not exist
     */
    void deleteById(Integer id) throws NotFoundException;

    /**
     * Finds all posts of user
     * @param idUser id of user
     * @return found posts of user
     * @throws NotFoundException if user does not exist
     */
    List<PostDTO> findAllPostsOfUser(Integer idUser) throws NotFoundException;

    /**
     * Finds count of all posts of user
     * @param idUser id of user
     * @return counts of posts of user
     * @throws NotFoundException if user does not exist
     */
    Integer findCountOfAllPostsOfUser(Integer idUser) throws NotFoundException;
}
