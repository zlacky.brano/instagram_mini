package zlack.bra.instamini.handler;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

import javax.validation.ConstraintViolationException;

@ControllerAdvice
@Slf4j
public class ConstraintViolationExceptionHandler {

    /**
     * Handles ConstraintViolationExceptionHandler, when its thrown and returns appropriate response
     * @param ex thrown ConstraintViolationException
     * @return appropriate response
     */
    @ExceptionHandler({ConstraintViolationException.class})
    @ResponseStatus(value=HttpStatus.BAD_REQUEST)
    protected ResponseEntity<Object> handleValidationFailure(ConstraintViolationException ex) {
        log.warn("ConstraintViolationException: " + ex.getMessage());

        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(ex.getMessage());
    }
}
