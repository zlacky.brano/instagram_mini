package zlack.bra.instamini.security.jwt;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.client.authentication.OAuth2AuthenticationToken;
import org.springframework.security.oauth2.core.user.OAuth2User;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;
import zlack.bra.instamini.business.dto.UserDTO;
import zlack.bra.instamini.business.service.impl.UserService;
import zlack.bra.instamini.security.oauth.CustomOAuth2User;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static zlack.bra.instamini.constant.Constants.ONLY_PROVIDER;

@Component
@Slf4j
public class JwtRequestFilter extends OncePerRequestFilter {

    @Autowired
    private UserService userService;

    @Autowired
    private JwtUtil jwtUtil;

    private static final List<String> skipFilterUrls = Arrays.asList("/oauth2/**", "/actuator/**");

    /**
     * Method, which tells what URLs shouldn't be filtered in this particular filter
     * @param request HttpServletRequest
     * @return true or false, which depends if request URL matches skip URL
     * @throws ServletException
     */
    @Override
    protected boolean shouldNotFilter(HttpServletRequest request) throws ServletException {
        return skipFilterUrls.stream().anyMatch(url -> new AntPathRequestMatcher(url).matches(request));
    }

    /**
     * Filter to validate JWT token and create SecurityContext if necessary
     * @param request HttpServletRequest
     * @param response HttpServletResponse
     * @param filterChain FilterChain
     * @throws ServletException if an I/O error occurs during the processing of the request
     * @throws IOException if the processing fails for any other reason
     */
    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {

        String authorizationHeader = request.getHeader("Authorization");

        String email = null;
        String jwt = null;

        if (authorizationHeader != null && authorizationHeader.startsWith("Bearer ")) {
            jwt = authorizationHeader.substring(7);
            if (jwtUtil.validateToken(jwt)) {
                email = jwtUtil.extractEmail(jwt);
            }
        }

        if (email != null && SecurityContextHolder.getContext().getAuthentication() == null) {
            Optional<UserDTO> userDTOOptional = userService.findByEmail(email);
            if (userDTOOptional.isPresent()) {
                OAuth2User oAuth2User = new CustomOAuth2User(userDTOOptional.get().getId(),
                        userDTOOptional.get().getUsername(), email);
                OAuth2AuthenticationToken oAuth2AuthenticationToken = new OAuth2AuthenticationToken(oAuth2User,
                        oAuth2User.getAuthorities(), ONLY_PROVIDER);
                SecurityContextHolder.getContext().setAuthentication(oAuth2AuthenticationToken);
            }
            else {
                log.warn("Could not set user authentication in security context.");
            }
        }
        else {
            log.warn("Could not set user authentication in security context.");
            SecurityContextHolder.clearContext();
        }

        filterChain.doFilter(request, response);
    }
}
