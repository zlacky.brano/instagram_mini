package zlack.bra.instamini.presentation.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.oauth2.client.authentication.OAuth2AuthenticationToken;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import zlack.bra.instamini.business.dto.UserDTO;
import zlack.bra.instamini.business.exception.NotFoundException;
import zlack.bra.instamini.business.service.impl.UserService;
import zlack.bra.instamini.presentation.controller.request.EditUsernameRequestBody;
import zlack.bra.instamini.presentation.controller.util.AuthorizationUtil;
import zlack.bra.instamini.security.oauth.CustomOAuth2User;

import javax.validation.Valid;
import java.security.Principal;
import java.util.List;
import java.util.Optional;

import static zlack.bra.instamini.presentation.controller.util.CheckForUnique.checkForUniqueConstraintViolation;

@RestController
public class UserController {

    private final UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    /**
     * Finds all users
     * @return all users
     */
    @GetMapping("/users")
    public List<UserDTO> all() {
        return userService.findAll();
    }

    /**
     * Finds users by prefix
     * @param prefix
     * @return found users
     */
    @GetMapping("/users/search")
    public List<UserDTO> searchUsers(@RequestParam String prefix) {
        return userService.searchUsers(prefix);
    }

    /**
     * Finds user by id
     * @param id of user
     * @return found user. NOT_FOUND, if user does not exist.
     */
    @GetMapping("/users/{id}")
    public UserDTO byID(@PathVariable Integer id) {
        Optional<UserDTO> optionalUserDTO;
        optionalUserDTO = userService.findById(id);

        if (optionalUserDTO.isPresent()) {
            return optionalUserDTO.get();
        }
        else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "No such user.");
        }
    }

    /**
     * Finds user by email
     * @param email
     * @return found user. NOT_FOUND, if user does not exist.
     */
    @GetMapping(value = "/users/byEmail", params = {"email"})
    public UserDTO byEmail(@RequestParam String email) {
        Optional<UserDTO> optionalUserDTO;
        optionalUserDTO = userService.findByEmail(email);

        if (optionalUserDTO.isPresent()) {
            return optionalUserDTO.get();
        }
        else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "No such user.");
        }
    }

    /**
     * Finds user by principal (logged user)
     * @param principal represents logged user
     * @return found user. NOT_FOUND, if user does not exist.
     */
    @GetMapping(value = "/users/logged")
    public UserDTO byPrincipal(Principal principal) {
        CustomOAuth2User customOAuth2User = AuthorizationUtil.getUserFromPrincipal(principal);
        if (customOAuth2User.getEmail() == null) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Principal's email is null.");
        }
        Optional<UserDTO> optionalUserDTO;
        optionalUserDTO = userService.findByEmail(customOAuth2User.getEmail());

        if (optionalUserDTO.isPresent()) {
            return optionalUserDTO.get();
        }
        else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "No such user.");
        }
    }

    /**
     * Edits username of user
     * @param id of user
     * @param requestBody new username
     * @param principal represents logged user
     * @return updated user
     */
    @PutMapping("/users/{id}/editUsername")
    public UserDTO editUsername(@PathVariable Integer id,
                                @RequestBody @Valid EditUsernameRequestBody requestBody,
                                Principal principal) {
        AuthorizationUtil.checkIfAuthorizedOrElseThrow(principal, id,
                "Can't edit username on profile, which does not belong to you.");
        try {
            return userService.editUsername(id, requestBody.getUsername());
        } catch (NotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (Exception e) {
            throw checkForUniqueConstraintViolation(e);
        }
    }

    /**
     * Deletes user by id
     * @param id of user
     * @param principal represents logged user
     * Can return NOT_FOUND, if user does not exist
     */
    @DeleteMapping("/users/{id}")
    public void delete(@PathVariable Integer id, Principal principal) {
        AuthorizationUtil.checkIfAuthorizedOrElseThrow(principal, id,
                "Can't delete profile, which does not belong to you.");
        try {
            userService.deleteById(id);
        } catch (NotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    /**
     * Finds all users who liked post
     * @param post id
     * @return found users. NOT_FOUND, if post does not exist
     */
    @GetMapping(value = "/users/likedPost", params = {"post"})
    public List<UserDTO> findAllUsersWhoLikedPost(@RequestParam Integer post) {
        try {
            return userService.findAllUsersWhoLikedPost(post);
        } catch (NotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    /**
     * Finds all followers of user
     * @param user id
     * @return found users. NOT_FOUND, if user does not exist
     */
    @GetMapping(value = "/users/followers", params = {"user"})
    public List<UserDTO> findAllFollowers(@RequestParam Integer user) {
        try {
            return userService.findAllFollowers(user);
        } catch (NotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    /**
     * Finds all users, who user is following
     * @param user id
     * @return found users. NOT_FOUND, if user does not exist
     */
    @GetMapping(value = "/users/following", params = {"user"})
    public List<UserDTO> findAllFollowing(@RequestParam Integer user) {
        try {
            return userService.findAllFollowing(user);
        } catch (NotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    /**
     * Finds count of all followers
     * @param user id
     * @return found count. NOT_FOUND, if user does not exist
     */
    @GetMapping(value = "/users/followers/count", params = {"user"})
    public Integer findCountOfAllFollowers(@RequestParam Integer user) {
        try {
            return userService.findCountOfAllFollowers(user);
        } catch (NotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    /**
     * Finds count of all users, who user is following
     * @param user id
     * @return found count. NOT_FOUND, if user does not exist
     */
    @GetMapping(value = "/users/following/count", params = {"user"})
    public Integer findCountOfAllFollowing(@RequestParam Integer user) {
        try {
            return userService.findCountOfAllFollowing(user);
        } catch (NotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
}
