package zlack.bra.instamini.presentation.controller.util;

import org.springframework.http.HttpStatus;
import org.springframework.security.oauth2.client.authentication.OAuth2AuthenticationToken;
import org.springframework.web.server.ResponseStatusException;
import zlack.bra.instamini.security.oauth.CustomOAuth2User;

import java.security.Principal;

/**
 * Helps with authorization
 */
public class AuthorizationUtil {

    /**
     * Gets user from principal
     * @param principal logged user
     * @return CustomOauth2User
     */
    public static CustomOAuth2User getUserFromPrincipal(Principal principal) {
        OAuth2AuthenticationToken oAuth2AuthenticationToken = (OAuth2AuthenticationToken) principal;
        return (CustomOAuth2User) oAuth2AuthenticationToken.getPrincipal();
    }

    /**
     * Throws ResponseStatusException, if user is not allowed to use resource
     * @param principal logged user
     * @param userId owner's id
     * @param message error message, if exception is hthrown
     */
    public static void checkIfAuthorizedOrElseThrow(Principal principal, Integer userId, String message) {
        if (!getUserFromPrincipal(principal).getId().equals(userId)) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, message);
        }
    }
}
