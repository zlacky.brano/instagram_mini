package zlack.bra.instamini.presentation.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.client.authentication.OAuth2AuthenticationToken;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import zlack.bra.instamini.business.dto.CommentCreateDTO;
import zlack.bra.instamini.business.dto.CommentDTO;
import zlack.bra.instamini.business.dto.PostDTO;
import zlack.bra.instamini.business.exception.NotFoundException;
import zlack.bra.instamini.business.service.impl.CommentService;
import zlack.bra.instamini.presentation.controller.request.EditTextCommentRequestBody;
import zlack.bra.instamini.presentation.controller.util.AuthorizationUtil;
import zlack.bra.instamini.security.oauth.CustomOAuth2User;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import java.security.Principal;
import java.util.List;
import java.util.Optional;

import static zlack.bra.instamini.constant.Constants.ROOT_URL;

@RestController
public class CommentController {

    private final CommentService commentService;

    @Autowired
    public CommentController(CommentService commentService) {
        this.commentService = commentService;
    }

    /**
     * Returns all comments
     * @return all comments
     */
    @GetMapping("/comments")
    public List<CommentDTO> all() {
        return commentService.findAll();
    }

    /**
     * Finds comment by id
     * @param id of comment
     * @return found comment or status code NOT_FOUND, if it does not exist
     */
    @GetMapping("/comments/{id}")
    public CommentDTO byID(@PathVariable Integer id) {
        Optional<CommentDTO> optionalCommentDTO;
        optionalCommentDTO = commentService.findById(id);

        if (optionalCommentDTO.isPresent()) {
            return optionalCommentDTO.get();
        }
        else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "No such comment.");
        }
    }

    /**
     * Creates new comment
     * @param commentCreateDTO object with necessary values to create comment
     * @param principal represents logged user
     * @return created comment or status code NOT_FOUND, if user or post does not exist
     */
    @PostMapping("/comments")
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<?> create(@RequestBody @Valid CommentCreateDTO commentCreateDTO, Principal principal) {
        AuthorizationUtil.checkIfAuthorizedOrElseThrow(principal, commentCreateDTO.getUserId(),
                "Can't create comment for other user.");

        CommentDTO commentDTO;
        try {
            commentDTO = commentService.create(commentCreateDTO);
        } catch (NotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
        return ResponseEntity.created(Link.of(ROOT_URL + "comments/" + commentDTO.getId()).toUri()).body(commentDTO);
    }

    /**
     * Edits text of comment
     * @param id of comment
     * @param requestBody new text
     * @param principal represents logged user
     * @return updated comment or status code NOT_FOUND, if comment does not exist
     */
    @PutMapping("/comments/{id}/editText")
    public CommentDTO editText(@PathVariable Integer id, @RequestBody @Valid EditTextCommentRequestBody requestBody, Principal principal) {
        Optional<CommentDTO> commentDTOOptional = commentService.findById(id);
        commentDTOOptional.ifPresent(commentDTO -> AuthorizationUtil.checkIfAuthorizedOrElseThrow(principal,
                commentDTO.getUser().getId(), "Can't edit text of comment, which does not belong to you."));

        try {
            return commentService.editText(id, requestBody.getNewText());
        } catch (NotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    /**
     * Deletes comment by id
     * @param id of comment
     * @param principal represents logged user
     * Can return NOT_FOUND, if comment does not exist
     */
    @DeleteMapping("/comments/{id}")
    public void delete(@PathVariable Integer id, Principal principal) {
        Optional<CommentDTO> commentDTOOptional = commentService.findById(id);
        commentDTOOptional.ifPresent(commentDTO -> AuthorizationUtil.checkIfAuthorizedOrElseThrow(principal,
                commentDTO.getUser().getId(), "Can't delete comment, which does not belong to you."));

        try {
            commentService.deleteById(id);
        } catch (NotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    /**
     * Finds all comments on post
     * @param post id
     * @return found comments or status code NOT_FOUND, if post does not exist
     */
    @GetMapping(value = "/comments/onPost", params = {"post"})
    public List<CommentDTO> findAllCommentsOnPost(@RequestParam Integer post)  {
        try {
            return commentService.findAllCommentsOnPost(post);
        } catch (NotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
}
