package zlack.bra.instamini.presentation.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.client.authentication.OAuth2AuthenticationToken;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import zlack.bra.instamini.business.dto.LikeCreateDTO;
import zlack.bra.instamini.business.dto.LikeDTO;
import zlack.bra.instamini.business.dto.PostDTO;
import zlack.bra.instamini.business.exception.NotFoundException;
import zlack.bra.instamini.business.service.impl.LikeService;
import zlack.bra.instamini.presentation.controller.util.AuthorizationUtil;
import zlack.bra.instamini.security.oauth.CustomOAuth2User;

import javax.validation.Valid;
import java.security.Principal;
import java.util.List;
import java.util.Optional;

import static zlack.bra.instamini.constant.Constants.ROOT_URL;
import static zlack.bra.instamini.presentation.controller.util.CheckForUnique.checkForUniqueConstraintViolation;

@RestController
public class LikeController {

    private final LikeService likeService;

    @Autowired
    public LikeController(LikeService likeService) {
        this.likeService = likeService;
    }

    /**
     * Finds all likes
     * @return all likes
     */
    @GetMapping("/likes")
    public List<LikeDTO> all() {
        return likeService.findAll();
    }

    /**
     * Finds like by id
     * @param id of like
     * @return found like or status code NOT_FOUND, if like does not exist
     */
    @GetMapping("/likes/{id}")
    public LikeDTO byID(@PathVariable Integer id) {
        Optional<LikeDTO> optionalLikeDTO;
        optionalLikeDTO = likeService.findById(id);

        if (optionalLikeDTO.isPresent()) {
            return optionalLikeDTO.get();
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "No such like.");
        }
    }

    /**
     * Creates new like
     * @param likeCreateDTO object with necessary values to create like
     * @param principal represents logged user
     * @return created like. NOT_FOUND, if post or user does not exist. BAD_REQUEST, if unique constraint was violated
     */
    @PostMapping("/likes")
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<?> create(@RequestBody @Valid LikeCreateDTO likeCreateDTO, Principal principal) {
        AuthorizationUtil.checkIfAuthorizedOrElseThrow(principal, likeCreateDTO.getUserId(),
                "Can't create like for other user.");

        LikeDTO likeDTO;
        try {
            likeDTO = likeService.create(likeCreateDTO);
        } catch (NotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (Exception e) {
            throw checkForUniqueConstraintViolation(e);
        }
        return ResponseEntity.created(Link.of(ROOT_URL + "likes/" + likeDTO.getId()).toUri()).body(likeDTO);
    }

    /**
     * Deletes like by id
     * @param id of like
     * @param principal represents logged user
     * Can return NOT_FOUND, if like does not exist
     */
    @DeleteMapping("/likes/{id}")
    public void delete(@PathVariable Integer id, Principal principal) {
        Optional<LikeDTO> likeDTOOptional = likeService.findById(id);
        likeDTOOptional.ifPresent(likeDTO -> AuthorizationUtil.checkIfAuthorizedOrElseThrow(principal,
                likeDTO.getUser().getId(), "Can't delete like, which does not belong to you."));

        try {
            likeService.deleteById(id);
        } catch (NotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    /**
     * Finds number of likes on post
     * @param post id
     * @return number of likes on post. NOT_FOUND, if post does not exist
     */
    @GetMapping(value = "/likes/count", params = {"post"})
    public Integer findNumberOfLikesOnPost(@RequestParam Integer post) {
        try {
            return likeService.findNumberOfLikesOnPost(post);
        } catch (NotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    /**
     * Finds likes on post
     * @param post id
     * @return likes on post. NOT_FOUND, if post does not exist
     */
    @GetMapping(value = "/likes/onPost", params = {"post"})
    public List<LikeDTO> findLikesOnPost(@RequestParam Integer post) {
        try {
            return likeService.findLikesOnPost(post);
        } catch (NotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    /**
     * Finds like id by logged user and post
     * @param postId id of post
     * @param principal represents logged user
     * @return found like id. NOT_FOUND, if like does not exist
     */
    @GetMapping("/likes/isLiked/{postId}")
    public Integer findLikeIdByUserAndPost(@PathVariable Integer postId, Principal principal) {
        CustomOAuth2User customOAuth2User = AuthorizationUtil.getUserFromPrincipal(principal);
        try {
            return likeService.findLikeIdByUserAndPost(customOAuth2User.getId(), postId);
        } catch (NotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

}
