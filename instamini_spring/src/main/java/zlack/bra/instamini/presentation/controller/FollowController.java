package zlack.bra.instamini.presentation.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.client.authentication.OAuth2AuthenticationToken;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import zlack.bra.instamini.business.dto.FollowCreateDTO;
import zlack.bra.instamini.business.dto.FollowDTO;
import zlack.bra.instamini.business.dto.LikeDTO;
import zlack.bra.instamini.business.exception.FollowYourselfException;
import zlack.bra.instamini.business.exception.NotFoundException;
import zlack.bra.instamini.business.service.impl.FollowService;
import zlack.bra.instamini.presentation.controller.util.AuthorizationUtil;
import zlack.bra.instamini.security.oauth.CustomOAuth2User;

import javax.validation.Valid;
import java.security.Principal;
import java.util.List;
import java.util.Optional;

import static zlack.bra.instamini.constant.Constants.ROOT_URL;
import static zlack.bra.instamini.presentation.controller.util.CheckForUnique.checkForUniqueConstraintViolation;

@RestController
public class FollowController {

    private final FollowService followService;

    @Autowired
    public FollowController(FollowService followService) {
        this.followService = followService;
    }

    /**
     * Finds all follows
     * @return all follows
     */
    @GetMapping("/follows")
    public List<FollowDTO> all() {
        return followService.findAll();
    }

    /**
     * Finds follow by id
     * @param id of follow
     * @return found follow or status code NOT_FOUND, if follow does not exist
     */
    @GetMapping("/follows/{id}")
    public FollowDTO byID(@PathVariable Integer id) {
        Optional<FollowDTO> optionalFollowDTO;
        optionalFollowDTO = followService.findById(id);

        if (optionalFollowDTO.isPresent()) {
            return optionalFollowDTO.get();
        }
        else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "No such follow.");
        }
    }

    /**
     * Creates new follow
     * @param followCreateDTO object with necessary values to create follow
     * @param principal represents logged user
     * @return created follow. NOT_FOUND, if any of users does not exist. BAD_REQUEST, if both users are same or unique constraint was violated
     */
    @PostMapping("/follows")
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<?> create(@RequestBody @Valid FollowCreateDTO followCreateDTO, Principal principal) {
        AuthorizationUtil.checkIfAuthorizedOrElseThrow(principal, followCreateDTO.getUserWhoId(),
                "Can't create follow for other user.");

        FollowDTO followDTO;
        try {
            followDTO = followService.create(followCreateDTO);
        } catch (NotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (FollowYourselfException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        } catch (Exception e) {
            throw checkForUniqueConstraintViolation(e);
        }
        return ResponseEntity.created(Link.of(ROOT_URL + "follows/" + followDTO.getId()).toUri()).body(followDTO);
    }

    /**
     * Deletes follow by id
     * @param id of user
     * @param principal represents logged user
     * Can return NOT_FOUND, if follow does not exist
     */
    @DeleteMapping("/follows/{id}")
    public void delete(@PathVariable Integer id, Principal principal) {
        Optional<FollowDTO> followDTOOptional = followService.findById(id);
        followDTOOptional.ifPresent(followDTO -> AuthorizationUtil.checkIfAuthorizedOrElseThrow(principal,
                followDTO.getUserWho().getId(), "Can't delete follow, which does not belong to you."));

        try {
            followService.deleteById(id);
        } catch (NotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    /**
     * Finds follow id by principal's id and userIdWhom
     * @param userIdWhom od userWhom
     * @param principal represents logged user
     * @return follow id or status code NOT_FOUND, if follow does not exist
     */
    @GetMapping("/follows/isFollowed/{userIdWhom}")
    public Integer findFollowIdByUsers(@PathVariable Integer userIdWhom, Principal principal) {
        CustomOAuth2User customOAuth2User = AuthorizationUtil.getUserFromPrincipal(principal);
        try {
            return followService.findFollowIdByUsers(customOAuth2User.getId(), userIdWhom);
        } catch (NotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
}
