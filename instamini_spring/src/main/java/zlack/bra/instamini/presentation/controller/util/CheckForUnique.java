package zlack.bra.instamini.presentation.controller.util;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

import static zlack.bra.instamini.constant.Constants.DUPLICATE_ENTRY;

@Slf4j
public class CheckForUnique {

    /**
     * Checks, if there was ConstraintViolationException and if so returns appropriate response
     * @param e exception
     * @return ResponseStatusException
     */
    public static ResponseStatusException checkForUniqueConstraintViolation(Exception e) {
        for (Throwable t = e.getCause(); t != null; t = t.getCause()) {
            log.error("Exception: " + t);
            if (t.getMessage().startsWith("Duplicate entry") || t.getMessage().startsWith("Unique index")) {
                return new ResponseStatusException(HttpStatus.BAD_REQUEST, DUPLICATE_ENTRY);
            }
        }
        return new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage());
    }
}
