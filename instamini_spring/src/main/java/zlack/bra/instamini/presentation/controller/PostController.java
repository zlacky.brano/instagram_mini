package zlack.bra.instamini.presentation.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.client.authentication.OAuth2AuthenticationToken;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import zlack.bra.instamini.business.dto.PostCreateDTO;
import zlack.bra.instamini.business.dto.PostDTO;
import zlack.bra.instamini.business.exception.NotFoundException;
import zlack.bra.instamini.business.exception.UploadFailException;
import zlack.bra.instamini.business.service.impl.PostService;
import zlack.bra.instamini.presentation.controller.request.EditPostDescriptionRequestBody;
import zlack.bra.instamini.presentation.controller.util.AuthorizationUtil;
import zlack.bra.instamini.security.oauth.CustomOAuth2User;

import javax.validation.Valid;
import java.security.Principal;
import java.util.List;
import java.util.Optional;

import static zlack.bra.instamini.constant.Constants.NOT_AN_IMAGE;
import static zlack.bra.instamini.constant.Constants.ROOT_URL;

@RestController
public class PostController {

    private final PostService postService;

    @Autowired
    public PostController(PostService postService) {
        this.postService = postService;
    }

    /**
     * Finds all posts
     * @return all posts
     */
    @GetMapping("/posts")
    public List<PostDTO> all() {
        return postService.findAll();
    }

    /**
     * Finds post by id
     * @param id of post
     * @return found post. NOT_FOUND, if post does not exist
     */
    @GetMapping("/posts/{id}")
    public PostDTO byID(@PathVariable Integer id) {
        Optional<PostDTO> optionalPostDTO;
        optionalPostDTO = postService.findById(id);

        if (optionalPostDTO.isPresent()) {
            return optionalPostDTO.get();
        }
        else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "No such post.");
        }
    }

    /**
     * Creates new post
     * @param postCreateDTO object with necessary values to create post
     * @param principal represents logged user
     * @return created post. NOT_FOUND, if user was not found. BAD_REQUEST, if file is not an image
     */
    @PostMapping(value = "/posts", consumes = { MediaType.MULTIPART_FORM_DATA_VALUE })
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<?> create(@ModelAttribute @Valid PostCreateDTO postCreateDTO, Principal principal) {
        AuthorizationUtil.checkIfAuthorizedOrElseThrow(principal, postCreateDTO.getUserId(),
                "Can't create post on profile, which does not belong to you.");

        PostDTO postDTO;
        try {
            postDTO = postService.create(postCreateDTO);
        } catch (NotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UploadFailException e) {
            if (e.getMessage().equals(NOT_AN_IMAGE)) {
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
            }
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage());
        }
        return ResponseEntity.created(Link.of(ROOT_URL + "posts/" + postDTO.getId()).toUri()).body(postDTO);
    }

    /**
     * Edits description of post
     * @param id of post
     * @param requestBody new description
     * @param principal represents logged user
     * @return updated post. NOT_FOUND, if post does not exist
     */
    @PutMapping("/posts/{id}/editDescription")
    public PostDTO editDescription(@PathVariable Integer id,
                                   @RequestBody @Valid EditPostDescriptionRequestBody requestBody,
                                   Principal principal) {
        Optional<PostDTO> postDTOOptional = postService.findById(id);
        postDTOOptional.ifPresent(postDTO -> AuthorizationUtil.checkIfAuthorizedOrElseThrow(principal,
                postDTO.getUser().getId(), "Can't edit description on post, which does not belong to you."));

        try {
            return postService.editDescription(id, requestBody.getNewDescription());
        } catch (NotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    /**
     * Deletes post by id
     * @param id of post
     * @param principal represents logged user
     * Can return NOT_FOUND, if post does not exist
     */
    @DeleteMapping("/posts/{id}")
    public void delete(@PathVariable Integer id, Principal principal) {
        Optional<PostDTO> postDTOOptional = postService.findById(id);
        postDTOOptional.ifPresent(postDTO -> AuthorizationUtil.checkIfAuthorizedOrElseThrow(principal,
                postDTO.getUser().getId(), "Can't delete post, which does not belong to you."));

        try {
            postService.deleteById(id);
        } catch (NotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    /**
     * Finds all posts of user
     * @param user id
     * @return found posts. NOT_FOUND, if user was not found
     */
    @GetMapping(value = "/posts/ofUser", params = {"user"})
    public List<PostDTO> findAllPostsOfUser(@RequestParam Integer user) {
        try {
            return postService.findAllPostsOfUser(user);
        } catch (NotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    /**
     * Finds number of all posts of user
     * @param user id
     * @return found number of posts of user
     */
    @GetMapping(value = "/posts/count", params = {"user"})
    public Integer findCountOfAllPostsOfUser(@RequestParam Integer user) {
        try {
            return postService.findCountOfAllPostsOfUser(user);
        } catch (NotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
}
