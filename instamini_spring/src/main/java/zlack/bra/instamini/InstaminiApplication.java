package zlack.bra.instamini;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class InstaminiApplication {

	public static void main(String[] args) {
		SpringApplication.run(InstaminiApplication.class, args);
	}

}
