import './App.css';
import {Route, Switch} from "react-router";
import AfterLoginHandler from "../login/oauth2/AfterLoginHandler";
import Home from "../home/Home";
import React, {useEffect, useState} from "react";
import Header from "../header/Header";
import PrivateRoute from "../common/PrivateRoute";
import Profile from "../profile/Profile"
import Logout from "../login/Logout";
import Search from "../search/Search";
import {TOKEN_NAME, USER_ID} from "../constant/constants";
import NotFound from "../common/request/error/NotFound";
import Modal from "react-modal";
import ServerError from "../common/request/error/ServerError";
import BadRequest from "../common/request/error/BadRequest";
import UnsupportedMedia from "../common/request/error/UnsupportedMedia";
import Forbidden from "../common/request/error/Forbidden";
import NoResponse from "../common/request/error/NoResponse";

Modal.setAppElement('#root');

function App() {
    const [authenticated, setAuthenticated] = useState(false);
    const [loggedUser, setLoggedUser] = useState(null);

    useEffect(() => {
        console.log("App's useEffect called.");
        if (localStorage.getItem(TOKEN_NAME) && localStorage.getItem(USER_ID)) {
            setAuthenticated(true);
            setLoggedUser(localStorage.getItem(USER_ID));
        }
    }, [])

    return (
        <div className="App">
            <div className="App-header">
                <Header authenticated={authenticated} loggedUser={loggedUser}/>
            </div>
            <div className="App-body">
                <Switch>
                    <Route exact path="/">
                        <Home/>
                    </Route>
                    <PrivateRoute exact path="/profile/:id" component={Profile} loggedUser={loggedUser}/>
                    <PrivateRoute path="/search" component={Search}/>
                    <Route path="/oauth2/redirect">
                        <AfterLoginHandler setAuthenticated={setAuthenticated} setLoggedUser={setLoggedUser}/>
                    </Route>
                    <Route path="/logout">
                        <Logout setAuthenticated={setAuthenticated} setLoggedUser={setLoggedUser}/>
                    </Route>
                    <Route path="/serverError" component={ServerError}/>
                    <Route path="/badRequest" component={BadRequest}/>
                    <Route path="/unsupportedMedia" component={UnsupportedMedia}/>
                    <Route path="/forbidden" component={Forbidden}/>
                    <Route path="/noResponse" component={NoResponse}/>
                    <Route component={NotFound}/>
                </Switch>
            </div>
        </div>
    );
}

export default App;
