import React from 'react';
import Login from "../login/Login";
import {Nav, NavBtnLink, NavMenu, NavLink} from "./HeaderElements";

const Header = ({authenticated, loggedUser}) => {
    if (authenticated) {
        const url = "/profile/" + loggedUser;
        return (
            <>
                <Nav>
                    <NavLink to="/">
                        <h1>Instamini</h1>
                    </NavLink>
                    <NavMenu>
                        <NavLink to="/search">
                            Search
                        </NavLink>
                        <NavLink to={url}>
                            Profile
                        </NavLink>
                        <NavBtnLink to="/logout">
                            Logout
                        </NavBtnLink>
                    </NavMenu>
                </Nav>
            </>
        );
    } else {
        return (
            <>
                <Nav>
                    <NavLink to="/">
                        <h1>Instamini</h1>
                    </NavLink>
                    <NavMenu>
                        <Login/>
                    </NavMenu>
                </Nav>
            </>
        );
    }
}

export default Header;