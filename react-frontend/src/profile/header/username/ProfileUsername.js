import React from 'react';
import EditUsername from "./edit/EditUsername";
import './ProfileUsername.css';

const ProfileUsername = ({userId, username, setUser, loggedUser}) => {
    if (userId === parseInt(loggedUser, 10)) {
        return (
            <div className="Profile-username">
                <h2 className="Profile-username-h2">{username}</h2>
                <EditUsername setUser={setUser} loggedUser={loggedUser}/>
            </div>
        )
    }
    return (
        <div className="Profile-username">
            <h1> {username} </h1>
        </div>
    )
}

export default ProfileUsername;