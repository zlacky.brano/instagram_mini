import React from "react";

const ErrorUsernameEdit = ({wrongInput}) => {
    if (wrongInput) {
        return (
            <h4 className="Error-message"> Username must not be blank! </h4>
        )
    }
    return null;
}

export default ErrorUsernameEdit;