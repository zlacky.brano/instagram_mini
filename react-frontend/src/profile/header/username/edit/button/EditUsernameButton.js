import './EditUsernameButton.css';
import useApi from "../../../../../api/useApi";
import React, {useState} from "react";
import useErrorCatcher from "../../../../../api/useErrorCatcher";
import {Redirect} from "react-router";
import {DUPLICATE_ENTRY} from "../../../../../constant/constants";

const EditUsernameButton = ({username, setUser,
                                setWrongInput, loggedUser, setModalIsOpen}) => {
    const [loading, setLoading] = useState(false);
    const { editUsername } = useApi();
    const [redirect, setRedirect] = useState(null);
    const [duplicate, setDuplicate] = useState(false);
    const { responseErrorCatcher } = useErrorCatcher();

    const handleEditUsername = async () => {
        if (username.trim().length !== 0) {
            setLoading(true);
            try {
		const editUsernameRequestBody = {
		    username: username
		}
                const {data} = await editUsername(loggedUser, editUsernameRequestBody);
                setUser(data);
                setModalIsOpen(false);
            } catch (e) {
                const result = responseErrorCatcher(e.response, true, false);
                if (result === DUPLICATE_ENTRY) {
                    setDuplicate(true);
                    setLoading(false);
                } else {
                    setRedirect(result);
                }
            }
        }
        else {
            setWrongInput(true);
        }
    }

    if (redirect) {
        return <Redirect to={redirect}/>;
    } else if (duplicate) {
        return (
            <div>
                <h4 className="Error-message"> This username is taken! </h4>
                <button className="Username-edit-button" onClick={handleEditUsername} disabled={loading}>
                    Edit
                </button>
            </div>
        )
    }

    return (
        <div>
            <button className="Username-edit-button" onClick={handleEditUsername} disabled={loading}>
                Edit
            </button>
        </div>
    )
}

export default EditUsernameButton;
