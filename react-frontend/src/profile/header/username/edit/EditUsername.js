import './EditUsername.css';
import Modal from "react-modal";
import React, {useState} from "react";
import EditUsernameModalBody from "./EditUsernameModalBody";

const customStyles = {
    overlay: {
        background: 'rgba(0, 0, 0, 0.8)',
    },
    content: {
        bottom: 'auto',
        minHeight: '10rem',
        left: '50%',
        right: 'auto',
        top: '50%',
        transform: 'translate(-50%,-50%)',
        minWidth: '450px'
    },
};

const EditUsername = ({setUser, loggedUser}) => {
    const [modalIsOpen, setModalIsOpen] = useState(false);

    return (
        <div>
            <button className="Edit-username-button" onClick={() => setModalIsOpen(prev => !prev)}>
                Edit
            </button>
            <Modal isOpen={modalIsOpen}
                   shouldCloseOnOverlayClick={true}
                   onRequestClose={() => setModalIsOpen(false)}
                   style={customStyles}
                   closeTimeoutMS={300}>
                <EditUsernameModalBody setUser={setUser} loggedUser={loggedUser} setModalIsOpen={setModalIsOpen}/>
            </Modal>
        </div>
    )
}

export default EditUsername;