import {MAX_CHARACTERS_USERNAME} from "../../../../constant/constants";
import React, {useState} from "react";
import './EditUsernameModalBody.css';
import EditUsernameButton from "./button/EditUsernameButton";
import ErrorUsernameEdit from "./ErrorUsernameEdit";

const EditUsernameModalBody = ({setUser, loggedUser, setModalIsOpen}) => {
    const [username, setUsername] = useState("");
    const [wrongInput, setWrongInput] = useState(false);
    const [numberOfCharacters, setNumberOfCharacters] = useState(0);

    const handleTextAreaChange = (event) => {
        setUsername(event.target.value);
        setNumberOfCharacters(event.target.value.length);
    }

    return (
        <div>
            <div>
                <textarea className="Username-text-area" placeholder="New username"
                          onChange={handleTextAreaChange} value={username} maxLength={MAX_CHARACTERS_USERNAME}/>
                <h5 className="Username-text-area-h5">{numberOfCharacters}/{MAX_CHARACTERS_USERNAME}</h5>
            </div>
            <ErrorUsernameEdit wrongInput={wrongInput}/>
            <EditUsernameButton username={username} setUser={setUser}
                                setWrongInput={setWrongInput} loggedUser={loggedUser} setModalIsOpen={setModalIsOpen}/>
        </div>
    )

}

export default EditUsernameModalBody;