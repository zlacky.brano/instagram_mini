import React, {useEffect, useState} from "react";
import useApi from "../../../api/useApi";
import './FollowsList.css';
import Username from "../../../common/username/Username";
import {Redirect} from "react-router";
import useErrorCatcher from "../../../api/useErrorCatcher";

const FollowersModalBody = ({userId}) => {
    const [followers, setFollowers] = useState([]);
    const {getFollowersOfUser} = useApi();
    const [redirect, setRedirect] = useState(null);
    const {responseErrorCatcher} = useErrorCatcher();

    const getFollowers = async (userId) => {
        const {data} = await getFollowersOfUser(userId);
        return data;
    }

    useEffect(() => {
        console.log("FollowersModalBody's useEffect called.");
        getFollowers(userId).then(data => {
            setFollowers(data);
        }).catch(e => {
            const redirectTo = responseErrorCatcher(e.response, false, false);
            setRedirect(redirectTo);
        });

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [userId]);

    if (redirect) {
        return <Redirect to={redirect}/>;
    }

    return (
        <div className="Follows-list">
            <h1>Followers</h1>
            {
                followers.map(follower =>
                    <Username key={follower.id} user={follower}/>
                )
            }
        </div>
    )
}

export default FollowersModalBody;