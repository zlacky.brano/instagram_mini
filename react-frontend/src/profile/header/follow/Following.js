import Modal from "react-modal";
import FollowingModalBody from "./FollowingModalBody";
import React, {useEffect, useState} from "react";
import useApi from "../../../api/useApi";
import './Follows.css';
import useErrorCatcher from "../../../api/useErrorCatcher";
import {Redirect} from "react-router";

const customStyles = {
    overlay: {
        background: 'rgba(0, 0, 0, 0.8)',
    },
    content: {
        top: '40%',
        left: '40%',
        right: '40%',
        bottom: '40%',
        border: 'none',
        padding: '0px',
    },
};

const Following = ({userId}) => {
    const [numberOfFollowing, setNumberOfFollowing] = useState(0);
    const [followingModalIsOpen, setFollowingModalIsOpen] = useState(false);
    const {getCountOfFollowing} = useApi();
    const [redirect, setRedirect] = useState(null);
    const {responseErrorCatcher} = useErrorCatcher();

    const countOfFollowing = async (userId) => {
        const {data} = await getCountOfFollowing(userId);
        return data;
    }

    useEffect(() => {
        console.log("Following's useEffect called.");
        let isMounted = true;
        countOfFollowing(userId).then(data => {
            if (isMounted) {
                setNumberOfFollowing(data);
            }
        }).catch(e => {
            const redirectTo = responseErrorCatcher(e.response, false, false);
            if (isMounted) {
                setRedirect(redirectTo);
            }
        });
        return () => {
            isMounted = false
        };
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [userId])

    if (redirect) {
        return <Redirect to={redirect}/>;
    }
    return (
        <div className="Following">
            <h3 onClick={() => setFollowingModalIsOpen(prev => !prev)}> {numberOfFollowing} Following </h3>
            <Modal isOpen={followingModalIsOpen}
                   shouldCloseOnOverlayClick={true}
                   onRequestClose={() => setFollowingModalIsOpen(false)}
                   style={customStyles}
                   closeTimeoutMS={300}>
                <FollowingModalBody userId={userId}/>
            </Modal>
        </div>
    )
}

export default Following;