import Modal from "react-modal";
import FollowersModalBody from "./FollowersModalBody";
import React, {useEffect, useState} from "react";
import useApi from "../../../api/useApi";
import './Follows.css';
import useErrorCatcher from "../../../api/useErrorCatcher";
import {Redirect} from "react-router";

const customStyles = {
    overlay: {
        background: 'rgba(0, 0, 0, 0.8)',
    },
    content: {
        top: '40%',
        left: '40%',
        right: '40%',
        bottom: '40%',
        border: 'none',
        padding: '0px',
    },
};

const Followers = ({userId, numberOfFollowers, setNumberOfFollowers}) => {
    const [followersModalIsOpen, setFollowersModalIsOpen] = useState(false);
    const [redirect, setRedirect] = useState(null);
    const {responseErrorCatcher} = useErrorCatcher();
    const {getCountOfFollowers} = useApi();

    const countOfFollowers = async (userId) => {
        const {data} = await getCountOfFollowers(userId);
        return data;
    }

    useEffect(() => {
        console.log("Follower's useEffect called.");
        let isMounted = true;
        countOfFollowers(userId).then(data => {
            if (isMounted) {
                setNumberOfFollowers(data);
            }
        }).catch(e => {
            const redirectTo = responseErrorCatcher(e.response, false, false);
            if (isMounted) {
                setRedirect(redirectTo);
            }
        })
        return () => {
            isMounted = false
        };
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [userId])

    if (redirect) {
        return <Redirect to={redirect}/>;
    }

    return (
        <div className="Followers">
            <h3 onClick={() => setFollowersModalIsOpen(prev => !prev)}>{numberOfFollowers} Followers </h3>
            <Modal isOpen={followersModalIsOpen}
                   shouldCloseOnOverlayClick={true}
                   onRequestClose={() => setFollowersModalIsOpen(false)}
                   style={customStyles}
                   closeTimeoutMS={300}>
                <FollowersModalBody userId={userId}/>
            </Modal>
        </div>
    )
}

export default Followers;