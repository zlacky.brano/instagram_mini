import React from 'react';
import './FollowButton.css';
import useApi from "../../../../api/useApi";
import {useEffect, useState} from "react";
import useErrorCatcher from "../../../../api/useErrorCatcher";
import {Redirect} from "react-router";
import {LIKE_OR_FOLLOW_NOT_FOUND} from "../../../../constant/constants";

const FollowButton = ({loggedUser, userId, setNumberOfFollowers}) => {
    const {postFollow, deleteFollow, doesUserFollow} = useApi();
    const [loading, setLoading] = useState(false);
    const [followId, setFollowId] = useState(-1);
    const [redirect, setRedirect] = useState(null);
    const {responseErrorCatcher} = useErrorCatcher();

    const checkIfUserIsFollowing = async () => {
        const {data} = await doesUserFollow(userId);
        return data;
    }

    useEffect(() => {
        console.log("FollowButton's useEffect called.")
        let isMounted = true;
        setFollowId(-1);
        if (userId !== parseInt(loggedUser, 10)) {
            checkIfUserIsFollowing().then(data => {
                if (!data.length && isMounted) {
                    setFollowId(data);
                }
            }).catch(e => {
                const result = responseErrorCatcher(e.response, false, true);
                if (isMounted && result !== LIKE_OR_FOLLOW_NOT_FOUND) {
                    setRedirect(result);
                }
            });
        }
        return () => {
            isMounted = false
        };
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [userId])

    const newFollow = async () => {
        setLoading(true);
        const followData = {
            userWhoId: loggedUser,
            userWhomId: userId
        }
        try {
            const {data} = await postFollow(followData);
            setNumberOfFollowers(prev => prev + 1);
            setFollowId(data.id);
            setLoading(false);
        } catch (e) {
            const redirectTo = responseErrorCatcher(e.response, false, false);
            setRedirect(redirectTo);
        }
    }

    const deleteFollowOfUser = async () => {
        setLoading(true);
        try {
            await deleteFollow(followId);
            setNumberOfFollowers(prev => prev - 1);
            setFollowId(-1);
            setLoading(false);
        } catch (e) {
            const redirectTo = responseErrorCatcher(e.response, false, false);
            setRedirect(redirectTo);
        }
    }

    if (redirect) {
        return <Redirect to={redirect}/>;
    } else if (userId === parseInt(loggedUser, 10)) {
        return null;
    } else if (followId > 0) {
        return (
            <div>
                <button className="Followed-button" onClick={deleteFollowOfUser} disabled={loading}>
                    Follow
                </button>
            </div>
        )
    } else {
        return (
            <div>
                <button className="Follow-button" onClick={newFollow} disabled={loading}>
                    Follow
                </button>
            </div>
        )
    }
}

export default FollowButton;
