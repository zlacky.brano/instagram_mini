import React, {useEffect, useState} from "react";
import useApi from "../../../api/useApi";
import './FollowsList.css';
import Username from "../../../common/username/Username";
import useErrorCatcher from "../../../api/useErrorCatcher";
import {Redirect} from "react-router";

const FollowingModalBody = ({userId}) => {
    const [followings, setFollowing] = useState([]);
    const {getFollowingUsers} = useApi();
    const [redirect, setRedirect] = useState(null);
    const {responseErrorCatcher} = useErrorCatcher();

    const getFollows = async (userId) => {
        const {data} = await getFollowingUsers(userId);
        return data;
    }

    useEffect(() => {
        console.log("FollowingModalBody's useEffect called.");
        getFollows(userId).then(data => {
            setFollowing(data);
        }).catch(e => {
            const redirectTo = responseErrorCatcher(e.response, false, false);
            setRedirect(redirectTo);
        });
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [userId]);

    if (redirect) {
        return <Redirect to={redirect}/>;
    }

    return (
        <div className="Follows-list">
            <h1>Following</h1>
            {
                followings.map(following =>
                    <Username key={following.id} user={following}/>
                )
            }
        </div>
    )
}

export default FollowingModalBody;