import React, {useState} from "react";
import './ProfileHeader.css';
import Followers from "./follow/Followers";
import Following from "./follow/Following";
import ProfileUsername from "./username/ProfileUsername";
import FollowButton from "./follow/button/FollowButton";
import DeleteProfileButton from "../delete/DeleteProfileButton";

const ProfileHeader = ({user, setUser, loggedUser, numberOfPosts}) => {
    const [numberOfFollowers, setNumberOfFollowers] = useState(0);

    return (
        <div className="Profile-header">
            <div>
                <ProfileUsername userId={user.id} username={user.username} setUser={setUser}
                                     loggedUser={loggedUser}/>
                <h5 className="Email"> Email: {user.email} </h5>
                <div className="Info-body" key={user.id}>
                    <h3> {numberOfPosts} Posts </h3>
                    <Followers userId={user.id} numberOfFollowers={numberOfFollowers}
                               setNumberOfFollowers={setNumberOfFollowers}/>
                    <Following userId={user.id}/>
                </div>
                <FollowButton loggedUser={loggedUser} userId={user.id} setNumberOfFollowers={setNumberOfFollowers}/>
                <DeleteProfileButton userId={user.id} loggedUser={loggedUser}/>
            </div>
        </div>
    );
}

export default ProfileHeader;