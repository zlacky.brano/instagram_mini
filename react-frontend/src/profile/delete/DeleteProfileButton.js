import './DeleteProfileButton.css';
import useApi from "../../api/useApi";
import React, {useState} from "react";
import {Redirect} from "react-router";
import useErrorCatcher from "../../api/useErrorCatcher";

const DeleteProfileButton = ({userId, loggedUser}) => {
    const [loading, setLoading] = useState(false);
    const [shouldLogout, setShouldLogout] = useState(false);
    const {deleteUser} = useApi();
    const [redirect, setRedirect] = useState(null);
    const {responseErrorCatcher} = useErrorCatcher();

    if (userId !== parseInt(loggedUser, 10)) {
        return null;
    }

    const handleDeleteProfile = async () => {
        setLoading(true);
        try {
            await deleteUser(userId);
            setShouldLogout(true);
        } catch (e) {
            const redirectTo = responseErrorCatcher(e.response, false, false);
            setRedirect(redirectTo);
        }
    }

    if (redirect) {
        return <Redirect to={redirect}/>;
    } else if (shouldLogout) {
        return <Redirect to={{
            pathname: "/logout",
        }}/>;
    }

    return (
        <div className="Delete-account-div">
            <button className="Delete-account-button" onClick={handleDeleteProfile} disabled={loading}>
                Delete Account
            </button>
        </div>
    )
}

export default DeleteProfileButton;