import React, {useEffect, useState} from 'react';
import './Profile.css';
import ProfileHeader from "./header/ProfileHeader";
import PostList from "./post/PostList";
import useApi from "../api/useApi";
import Loading from "../common/loading/Loading";
import {Redirect, useParams} from "react-router";
import useErrorCatcher from "../api/useErrorCatcher";

const Profile = ({loggedUser}) => {
    const [posts, setPosts] = useState([]);
    const [user, setUser] = useState(null);
    const [isLoading, setLoading] = useState(true);
    const {id} = useParams();
    const {getUser} = useApi();
    const [redirect, setRedirect] = useState(null);
    const {responseErrorCatcher} = useErrorCatcher();

    const getUserInfo = async (id) => {
        const {data} = await getUser(id);
        return data;
    }

    useEffect(() => {
        console.log("Profile's useEffect called.");
        let isMounted = true;
        getUserInfo(id).then(data => {
            if (isMounted) {
                setUser(data);
                setLoading(false);
            }
        }).catch(e => {
            const redirectTo = responseErrorCatcher(e.response, false, false);
            if (isMounted) {
                setRedirect(redirectTo);
            }
        });
        return () => {
            isMounted = false;
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [id]);

    if (redirect) {
        return <Redirect to={redirect}/>;
    } else if (isLoading) {
        return <Loading/>;
    }

    return (
        <div className="Profile-body">
            <ProfileHeader user={user} setUser={setUser} loggedUser={loggedUser} numberOfPosts={posts.length}/>
            <PostList user={user} loggedUser={loggedUser} posts={posts} setPosts={setPosts}/>
        </div>
    );
}

export default Profile;