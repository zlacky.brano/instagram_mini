import React from "react";

const ErrorComment = ({wrongInput}) => {
    if (wrongInput) {
        return (
            <h4 className="Error-message"> Comment is blank! </h4>
        )
    }
    return null;
}

export default ErrorComment;