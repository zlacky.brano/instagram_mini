import React, {useEffect, useState} from "react";
import useApi from "../../../api/useApi";
import './Comments.css';
import Comment from "./Comment";
import CommentTextArea from "./CommentTextArea";
import useErrorCatcher from "../../../api/useErrorCatcher";
import {Redirect} from "react-router";

const Comments = ({postId, loggedUser}) => {
    const [comments, setComments] = useState([]);
    const {getCommentsOnPost} = useApi();
    const [redirect, setRedirect] = useState(null);
    const {responseErrorCatcher} = useErrorCatcher();

    const getAllCommentsOnPost = async () => {
        const {data} = await getCommentsOnPost(postId);
        return data;
    }

    useEffect(() => {
        console.log("Comment's useEffect called.");
        getAllCommentsOnPost().then(data => {
            setComments(data);
        }).catch(e => {
            const redirectTo = responseErrorCatcher(e.response, false, false);
            setRedirect(redirectTo);
        });
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [postId]);

    if (redirect) {
        return <Redirect to={redirect}/>;
    }

    return (
        <div>
            <div className="Comment-list">
                <h1>Comments:</h1>
                <CommentTextArea postId={postId} setComments={setComments} loggedUser={loggedUser}/>
                {
                    comments.map(comment =>
                        <Comment key={comment.id} comment={comment}
                                 loggedUser={loggedUser} comments={comments} setComments={setComments}/>
                    )
                }
            </div>
        </div>
    )
}

export default Comments;