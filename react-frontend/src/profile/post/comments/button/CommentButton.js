import React from 'react';
import './CommentButton.css';
import useApi from "../../../../api/useApi";
import {useState} from "react";
import {Redirect} from "react-router";
import useErrorCatcher from "../../../../api/useErrorCatcher";

const CommentButton = ({postId, comment, setComment, setComments, setNumberOfCharacters, loggedUser, setWrongInput}) => {
    const [loading, setLoading] = useState(false);
    const {postComment} = useApi();
    const [redirect, setRedirect] = useState(null);
    const {responseErrorCatcher} = useErrorCatcher();

    const handleCommentSubmit = async () => {
        if (comment.trim().length !== 0) {
            setLoading(true);
            const commentData = {
                text: comment,
                postId: postId,
                userId: loggedUser
            }
            try {
                const {data} = await postComment(commentData);
                setComment("");
                setNumberOfCharacters(0);
                setComments(prev => [data].concat(prev));
                setWrongInput(false);
                setLoading(false);
            } catch (e) {
                const redirectTo = responseErrorCatcher(e.response, false, false);
                setRedirect(redirectTo);
            }
        }
        else {
            setWrongInput(true);
        }
    }

    if (redirect) {
        return <Redirect to={redirect}/>;
    }

    return (
        <div>
            <button className="Comment-button" onClick={handleCommentSubmit} disabled={loading}>
                Comment
            </button>
        </div>
    )
}

export default CommentButton;