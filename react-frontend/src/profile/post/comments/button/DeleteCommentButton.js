import React, {useState} from "react";
import './DeleteCommentButton.css';
import useApi from "../../../../api/useApi";
import useErrorCatcher from "../../../../api/useErrorCatcher";
import {Redirect} from "react-router";

const DeleteCommentButton = ({userId, comment, loggedUser, comments, setComments}) => {
    const [loading, setLoading] = useState(false);
    const [redirect, setRedirect] = useState(null);
    const {deleteComment} = useApi();
    const {responseErrorCatcher} = useErrorCatcher();

    const deleteCommentOnPost = async () => {
        setLoading(true);
        try {
            await deleteComment(comment.id)
            const newComments = [...comments];
            const index = comments.indexOf(comment);
            newComments.splice(index, 1);
            setComments(newComments);
        } catch (e) {
            const redirectTo = responseErrorCatcher(e.response, false, false);
            setRedirect(redirectTo);
        }
    }

    if (redirect) {
        return <Redirect to={redirect}/>;
    } else if (userId !== parseInt(loggedUser, 10)) {
        return null;
    } else {
        return (
            <div>
                <button className="Delete-comment-button" onClick={deleteCommentOnPost} disabled={loading}>
                    Delete
                </button>
            </div>
        )
    }
}

export default DeleteCommentButton;