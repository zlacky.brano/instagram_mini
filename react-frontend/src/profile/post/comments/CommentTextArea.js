import React, {useState} from "react";
import CommentButton from "./button/CommentButton";
import {MAX_CHARACTERS_TEXT_COMMENT_OR_DESCRIPTION} from "../../../constant/constants";
import './CommentTextArea.css';
import ErrorComment from "./ErrorComment";

const CommentTextArea = ({postId, setComments, loggedUser}) => {
    const [comment, setComment] = useState("");
    const [wrongInput, setWrongInput] = useState(false);
    const [numberOfCharacters, setNumberOfCharacters] = useState(0);

    const handleTextAreaChange = (event) => {
        setComment(event.target.value);
        setNumberOfCharacters(event.target.value.length);
    }

    return (
        <div>
            <div>
                <textarea className="Comment-text-area" placeholder="Add comment"
                          onChange={handleTextAreaChange} value={comment} maxLength={MAX_CHARACTERS_TEXT_COMMENT_OR_DESCRIPTION}/>
                <h5 className="Comment-text-area-h5">{numberOfCharacters}/{MAX_CHARACTERS_TEXT_COMMENT_OR_DESCRIPTION}</h5>
            </div>
            <ErrorComment wrongInput={wrongInput}/>
            <CommentButton postId={postId} comment={comment} setComment={setComment} setComments={setComments}
                           setNumberOfCharacters={setNumberOfCharacters} loggedUser={loggedUser} setWrongInput={setWrongInput}/>
        </div>
    )
}

export default CommentTextArea;