import React from "react";
import './Comment.css';
import Username from "../../../common/username/Username";
import DeleteCommentButton from "./button/DeleteCommentButton";

const Comment = ({comment, loggedUser, comments, setComments}) => {
    return (
        <div className="Comment-body">
            <Username user={comment.user}/>
            <div>
                <h6 className="Comment-h6"> {comment.time} </h6>
                <h4 className="Comment-h4"> {comment.text} </h4>
            </div>
            <DeleteCommentButton userId={comment.user.id} comment={comment}
                                 loggedUser={loggedUser} comments={comments} setComments={setComments}/>
        </div>
    )
}

export default Comment;