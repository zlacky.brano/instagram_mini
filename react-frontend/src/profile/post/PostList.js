import React, {useEffect, useState} from "react";
import './PostList.css';
import useApi from "../../api/useApi";
import Post from "./Post";
import NewPostButton from "./new/button/NewPostButton";
import useErrorCatcher from "../../api/useErrorCatcher";
import {Redirect} from "react-router";

const PostList = ({user, loggedUser, posts, setPosts}) => {
    const [loading, setLoading] = useState(true);
    const {getPosts} = useApi();
    const [redirect, setRedirect] = useState(null);
    const {responseErrorCatcher} = useErrorCatcher();

    const getPostsOfUser = async (userId) => {
        const {data} = await getPosts(userId);
        return data;
    }

    useEffect(() => {
        console.log("ProfileList's useEffect called.");
        let isMounted = true;
        getPostsOfUser(user.id).then(data => {
            if (isMounted) {
                setPosts(data);
                setLoading(false);
            }
        }).catch(e => {
            const redirectTo = responseErrorCatcher(e.response, false, false);
            if (isMounted) {
                setRedirect(redirectTo);
            }
        });
        return () => {
            isMounted = false;
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [user.id]);

    if (redirect) {
        return <Redirect to={redirect}/>;
    } else if (loading) {
        return null;
    }

    return (
        <div className="Post-body">
            <NewPostButton userId={user.id} loggedUser={loggedUser} setPosts={setPosts}/>
            <div className="Post-list">
                {
                    posts.map(post =>
                        <Post key={post.id} post={post} photoUrl={post.photoUrl} loggedUser={loggedUser}
                              setPosts={setPosts} posts={posts}/>
                    )
                }
            </div>
        </div>
    );
}

export default PostList;