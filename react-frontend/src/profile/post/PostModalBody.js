import React from "react";
import './PostModalBody.css';
import Likes from "./likes/Likes";
import Comments from "./comments/Comments";
import DeletePostButton from "./delete/DeletePostButton";

const PostModalBody = ({post, loggedUser, setPosts, posts}) => {
    return (
        <div className="Post-modal-body">
            <div className="Image-body">
                <img className="Image" alt="Post" src={post.photoUrl}/>
            </div>
            <div className="Description-body">
                <h1> Description: </h1>
                <h4 className="Description-h4"> {post.description} </h4>
                <h5> {post.time} </h5>
                <DeletePostButton post={post} loggedUser={loggedUser}
                                  ownerId={post.user.id} setPosts={setPosts} posts={posts}/>
                <Likes postId={post.id} loggedUser={loggedUser}/>
                <Comments postId={post.id} loggedUser={loggedUser}/>
            </div>
        </div>
    )
}

export default PostModalBody;