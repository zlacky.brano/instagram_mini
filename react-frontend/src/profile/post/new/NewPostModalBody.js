import './NewPostModalBody.css';
import {MAX_CHARACTERS_TEXT_COMMENT_OR_DESCRIPTION} from "../../../constant/constants";
import React, {useState} from "react";
import NewPostDropZone from "./NewPostDropZone";
import SavePostButton from "./button/SavePostButton";
import ErrorPost from "./ErrorPost";

const NewPostModalBody = ({loggedUser, setModalIsOpen, setPosts}) => {
    const [description, setDescription] = useState("");
    const [numberOfCharacters, setNumberOfCharacters] = useState(0);
    const [images, setImages] = useState([]);
    const [wrongInput, setWrongInput] = useState(false);

    const handleTextAreaChange = (event) => {
        setDescription(event.target.value);
        setNumberOfCharacters(event.target.value.length);
    }

    return (
        <div className="New-post-modal-body">
            <h2> Description: </h2>
            <textarea className="New-post-textarea" placeholder="Add description"
                      onChange={handleTextAreaChange} value={description} maxLength={MAX_CHARACTERS_TEXT_COMMENT_OR_DESCRIPTION}/>
            <h5 className="Comment-text-area-h5">{numberOfCharacters}/{MAX_CHARACTERS_TEXT_COMMENT_OR_DESCRIPTION}</h5>
            <NewPostDropZone images={images} setImages={setImages}/>
            <ErrorPost wrongInput={wrongInput}/>
            <SavePostButton images={images} description={description} loggedUser={loggedUser}
                            setWrongInput={setWrongInput} setModalIsOpen={setModalIsOpen} setPosts={setPosts}/>
        </div>
    )
}

export default NewPostModalBody;