import React from 'react';
import {useDropzone} from 'react-dropzone';
import './NewPostDropZone.css';

const NewPostDropZone = ({images, setImages}) => {
    const {getRootProps, getInputProps} = useDropzone({
        accept: "image/*",
        onDrop: (files) => {
            if (files.length === 0 || files.length > 1) {
                return null;
            }
            setImages(
                files.map((file) =>
                    Object.assign(file, {
                        preview: URL.createObjectURL(file),
                    }))
            )
        }

    })

    const imagesPreview = images.map((image) => (
        <div key={image.name}>
            <div>
                <img src={image.preview} style={{width: "40%"}} alt="Preview"/>
            </div>
        </div>
    ))

    return (
        <div>
            <div {...getRootProps()}>
                <input {...getInputProps()}/>
                <h1 className="Dropzone-h2"> Drop image here </h1>
            </div>
            <div className="Dropzone-preview">
                {imagesPreview}
            </div>
        </div>
    )
}

export default NewPostDropZone;