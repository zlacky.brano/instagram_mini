import React from 'react';
import './SavePostButton.css';
import useApi from "../../../../api/useApi";
import {useState} from "react";
import {Redirect} from "react-router";
import useErrorCatcher from "../../../../api/useErrorCatcher";

const SavePostButton = ({images, description, loggedUser, setWrongInput, setModalIsOpen, setPosts}) => {
    const {savePost} = useApi();
    const [loading, setLoading] = useState(false);
    const [redirect, setRedirect] = useState(null);
    const {responseErrorCatcher} = useErrorCatcher();

    const handleSavePost = async () => {
        if (images.length !== 0 && description.trim().length !== 0) {
            setLoading(true);
            const formData = new FormData();
            formData.append('image', images[0]);
            formData.append('description', description);
            formData.append('userId', loggedUser);
            try {
                const {data} = await savePost(formData);
                setModalIsOpen(false);
                setPosts(prev => [data].concat(prev));
            } catch (e) {
                const redirectTo = responseErrorCatcher(e.response, false, false);
                setRedirect(redirectTo);
            }
        } else {
            setWrongInput(true);
        }
    }

    if (redirect) {
        return <Redirect to={redirect}/>;
    }

    return (
        <div>
            <button className="Save-post-button" onClick={handleSavePost} disabled={loading}>
                Save
            </button>
        </div>
    )
}

export default SavePostButton;