import React from 'react';
import './NewPostButton.css';
import {useState} from "react";
import Modal from 'react-modal';
import NewPostModalBody from "../NewPostModalBody";

const customStyles = {
    overlay: {
        background: 'rgba(0, 0, 0, 0.8)',
    },
    content: {
        top: '10%',
        left: '30%',
        right: '30%',
        bottom: '10%',
        border: 'none',
        padding: '0px',
    },
};

const NewPostButton = ({userId, loggedUser, setPosts}) => {
    const [modalIsOpen, setModalIsOpen] = useState(false);

    if (userId !== parseInt(loggedUser, 10)) {
        return null;
    }

    return (
        <div>
            <button className="New-post-button" onClick={() => setModalIsOpen(prev => !prev)}>
                New Post
            </button>
            <Modal isOpen={modalIsOpen}
                   shouldCloseOnOverlayClick={true}
                   onRequestClose={() => setModalIsOpen(false)}
                   style={customStyles}
                   closeTimeoutMS={300}>
                <NewPostModalBody loggedUser={loggedUser} setModalIsOpen={setModalIsOpen} setPosts={setPosts}/>
            </Modal>
        </div>
    )
}

export default NewPostButton;