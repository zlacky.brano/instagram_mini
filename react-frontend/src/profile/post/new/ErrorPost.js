import React from "react";

const ErrorPost = ({wrongInput}) => {
    if (wrongInput) {
        return (
            <h4 className="Error-message"> Description is blank or image is missing! </h4>
        )
    }
    return null;
}

export default ErrorPost;