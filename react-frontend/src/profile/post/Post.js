import React, {useState} from "react";
import Modal from "react-modal";
import './Post.css';
import PostModalBody from "./PostModalBody";

const customStyles = {
    overlay: {
        background: 'rgba(0, 0, 0, 0.8)',
    },
    content: {
        top: '5%',
        left: '5%',
        right: '5%',
        bottom: '5%',
        border: 'none',
        padding: '0px',
    },
};

const Post = ({post, photoUrl, loggedUser, setPosts, posts}) => {
    const [modalIsOpen, setModalIsOpen] = useState(false);

    return (
        <>
            <img className="Post" alt="Post" src={photoUrl} onClick={() => setModalIsOpen(prev => !prev)}/>
            <Modal isOpen={modalIsOpen}
                   shouldCloseOnOverlayClick={true}
                   onRequestClose={() => setModalIsOpen(false)}
                   style={customStyles}
                   closeTimeoutMS={300}>
                <PostModalBody post={post} loggedUser={loggedUser}
                               setPosts={setPosts} posts={posts}/>
            </Modal>
        </>
    )
}

export default Post;