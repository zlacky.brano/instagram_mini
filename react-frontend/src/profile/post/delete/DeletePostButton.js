import React from 'react';
import './DeletePostButton.css';
import useApi from "../../../api/useApi";
import {useState} from "react";
import useErrorCatcher from "../../../api/useErrorCatcher";
import {Redirect} from "react-router";

const DeletePostButton = ({post, loggedUser, ownerId, posts, setPosts}) => {
    const [loading, setLoading] = useState(false);
    const {deletePost} = useApi();
    const [redirect, setRedirect] = useState(null);
    const {responseErrorCatcher} = useErrorCatcher();

    if (ownerId !== parseInt(loggedUser, 10)) {
        return null;
    }

    const handleDelete = async () => {
        setLoading(true);
        try {
            await deletePost(post.id);
            const newPosts = [...posts];
            const index = posts.indexOf(post);
            newPosts.splice(index, 1);
            setPosts(newPosts);
        } catch (e) {
            const redirectTo = responseErrorCatcher(e.response, false, false);
            setRedirect(redirectTo);
        }
    }

    if (redirect) {
        return <Redirect to={redirect}/>;
    }

    return (
        <div>
            <button className="Delete-post-button" onClick={handleDelete} disabled={loading}>
                Delete Post
            </button>
        </div>
    )
}

export default DeletePostButton;