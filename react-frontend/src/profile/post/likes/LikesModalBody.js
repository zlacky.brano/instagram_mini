import React, {useEffect, useState} from "react";
import useApi from "../../../api/useApi";
import './LikesModalBody.css';
import Username from "../../../common/username/Username";
import useErrorCatcher from "../../../api/useErrorCatcher";
import {Redirect} from "react-router";

const LikesModalBody = ({postId}) => {
    const [likes, setLikes] = useState([]);
    const {getLikesOnPost} = useApi();
    const [redirect, setRedirect] = useState(null);
    const {responseErrorCatcher} = useErrorCatcher();

    const getLikes = async (postId) => {
        const {data} = await getLikesOnPost(postId);
        return data;
    }

    useEffect(() => {
        console.log("LikesModalBody's useEffect called.");
        getLikes(postId).then(data => {
            setLikes(data);
        }).catch(e => {
            const redirectTo = responseErrorCatcher(e.response, false, false);
            setRedirect(redirectTo);
        });
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [postId]);

    if (redirect) {
        return <Redirect to={redirect}/>;
    }

    return (
        <div className="Likes-list">
            <h1>Likes</h1>
            {
                likes.map(like =>
                    <Username key={like.user.id} user={like.user}/>
                )
            }
        </div>
    )
}

export default LikesModalBody;