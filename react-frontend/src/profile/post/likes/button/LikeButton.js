import React from 'react';
import './LikeButton.css';
import useApi from "../../../../api/useApi";
import {useEffect, useState} from "react";
import useErrorCatcher from "../../../../api/useErrorCatcher";
import {Redirect} from "react-router";
import {LIKE_OR_FOLLOW_NOT_FOUND} from "../../../../constant/constants";

const LikeButton = ({postId, setNumberOfLikes, loggedUser}) => {
    const {postLike, doesUserLikePost, deleteLike} = useApi();
    const [loading, setLoading] = useState(false);
    const [likeId, setLikeId] = useState(-1);
    const [redirect, setRedirect] = useState(null);
    const {responseErrorCatcher} = useErrorCatcher();

    const checkIfPostIsLiked = async () => {
        const {data} = await doesUserLikePost(postId);
        return data;
    }

    useEffect(() => {
        console.log("LikeButton's useEffect called.")
        setLikeId(-1);
        checkIfPostIsLiked().then(data => {
            if (!data.length) {
                setLikeId(data);
            }
        }).catch(e => {
            const result = responseErrorCatcher(e.response, false, true);
            if (result !== LIKE_OR_FOLLOW_NOT_FOUND) {
                setRedirect(result);
            }
        });
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [postId])

    const newLike = async () => {
        setLoading(true);
        const likeData = {
            postId: postId,
            userId: loggedUser
        }
        try {
            const {data} = await postLike(likeData);
            setNumberOfLikes(prev => prev + 1);
            setLikeId(data.id);
            setLoading(false);
        } catch (e) {
            const redirectTo = responseErrorCatcher(e.response, false, false);
            setRedirect(redirectTo);
        }
    }

    const deleteLikeOnPost = async () => {
        setLoading(true);
        try {
            await deleteLike(likeId);
            setNumberOfLikes(prev => prev - 1);
            setLikeId(-1);
            setLoading(false);
        } catch (e) {
            const redirectTo = responseErrorCatcher(e.response, false, false);
            setRedirect(redirectTo);
        }
    }

    if (redirect) {
        return <Redirect to={redirect}/>;
    } else if (likeId > 0) {
        return (
            <div>
                <button className="Liked-button" onClick={deleteLikeOnPost} disabled={loading}>
                    Like
                </button>
            </div>
        )
    } else {
        return (
            <div>
                <button className="Like-button" onClick={newLike} disabled={loading}>
                    Like
                </button>
            </div>
        )
    }
}

export default LikeButton;