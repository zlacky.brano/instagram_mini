import thumbsUp from "../../../img/thumbs-up.webp";
import React, {useEffect, useState} from "react";
import Modal from "react-modal";
import './Likes.css';
import useApi from "../../../api/useApi";
import LikesModalBody from "./LikesModalBody";
import LikeButton from "./button/LikeButton";
import useErrorCatcher from "../../../api/useErrorCatcher";
import {Redirect} from "react-router";

const customStyles = {
    overlay: {
        background: 'rgba(0, 0, 0, 0.8)',
    },
    content: {
        top: '40%',
        left: '40%',
        right: '40%',
        bottom: '40%',
        border: 'none',
        padding: '0px',
    },
};

const Likes = ({postId, loggedUser}) => {
    const [modalIsOpen, setModalIsOpen] = useState(false);
    const [numberOfLikes, setNumberOfLikes] = useState(0);
    const {getNumberOfLikesOnPost} = useApi();
    const [redirect, setRedirect] = useState(null);
    const {responseErrorCatcher} = useErrorCatcher();

    const getNumberOfLikesOnThisPost = async (postId) => {
        const {data} = await getNumberOfLikesOnPost(postId);
        return data;
    }

    useEffect(() => {
        console.log("Like's useEffect called.");
        getNumberOfLikesOnThisPost(postId).then(data => {
            setNumberOfLikes(data);
        }).catch(e => {
            const redirectTo = responseErrorCatcher(e.response, false, false);
            setRedirect(redirectTo);
        });
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [postId]);

    if (redirect) {
        return <Redirect to={redirect}/>;
    }

    return (
        <div>
            <div className="Like-block" onClick={() => setModalIsOpen(prev => !prev)}>
                <img className="Like" alt="Like" src={thumbsUp}/>
                <h3 className="Like-h3"> {numberOfLikes} </h3>
            </div>
            <LikeButton postId={postId} setNumberOfLikes={setNumberOfLikes} loggedUser={loggedUser}/>
            <Modal isOpen={modalIsOpen}
                   shouldCloseOnOverlayClick={true}
                   onRequestClose={() => setModalIsOpen(false)}
                   style={customStyles}
                   closeTimeoutMS={300}>
                <LikesModalBody postId={postId}/>
            </Modal>
        </div>
    )
}

export default Likes;