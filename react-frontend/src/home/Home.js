import React from "react";
import './Home.css'

function Home() {
    return (
        <div>
            <header className="Welcome-body">
                <h1>
                    Welcome to Instamini.
                </h1>
            </header>
        </div>
    );
}

export default Home;