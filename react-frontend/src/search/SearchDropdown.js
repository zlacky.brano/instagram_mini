import './SearchDropdown.css';
import useApi from "../api/useApi";
import React, {useEffect, useRef, useState} from "react";
import {Redirect} from "react-router";
import useErrorCatcher from "../api/useErrorCatcher";
import Username from "../common/username/Username";

const SearchDropdown = () => {
    const [users, setUsers] = useState([]);
    const [redirect, setRedirect] = useState(null);
    const searchInputRef = useRef(null);
    const {searchUser} = useApi();
    const {responseErrorCatcher} = useErrorCatcher();

    useEffect(() => {
        console.log("SearchDropdown's useEffect called.")

        function handleClickOutside(event) {
            if (searchInputRef.current && !searchInputRef.current.contains(event.target)
                && event.target.tagName.toLowerCase() !== 'a') {
                setUsers([]);
            }
        }

        document.addEventListener("mousedown", handleClickOutside);
        return () => {
            document.removeEventListener("mousedown", handleClickOutside);
        }

    }, [searchInputRef]);

    const handleInputChange = async (event) => {
        try {
            const prefix = encodeURIComponent(event.target.value);
            const {data} = await searchUser(prefix);
            setUsers(data);
        } catch (e) {
            const redirectTo = responseErrorCatcher(e.response, false, false);
            setRedirect(redirectTo);
        }
    }

    if (redirect) {
        return <Redirect to={redirect}/>;
    }

    return (
        <div className="Search-dropdown-body">
            <input type="text" placeholder="Search..." className="Input-search"
                   onChange={handleInputChange} ref={searchInputRef} onClick={handleInputChange}/>
            {users.length > 0
                ?
                <ul className="Search-list">
                    {
                        users.map(user =>
                            <Username key={user.id} user={user}/>
                        )
                    }
                </ul>
                :
                null
            }

        </div>
    )
}

export default SearchDropdown;