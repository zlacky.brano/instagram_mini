import React from "react";
import './Search.css';
import SearchDropdown from "./SearchDropdown";

function Search() {
    return (
        <div className="Search-body">
            <h1>Search user</h1>
            <SearchDropdown/>
        </div>
    );
}

export default Search;