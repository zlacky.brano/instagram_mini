import axios from "axios";
import {API_BASE_URL, FIND_LOGGED_USER_RESOURCE, TOKEN_NAME} from "../constant/constants";

const useApi = () => {
    const axiosRequest = axios.create({
        baseURL: API_BASE_URL,
        headers: {
            'Content-type': 'application/json',
            Authorization: {
                toString () {
                    return `Bearer ${localStorage.getItem(TOKEN_NAME)}`
                }
            }
        }
    });

    return {
        getCountOfFollowers: userId => axiosRequest.get(`/users/followers/count?user=${userId}`),
        getCountOfFollowing: userId => axiosRequest.get(`/users/following/count?user=${userId}`),
        getUser: userId => axiosRequest.get(`/users/${userId}`),
        getLoggedUser: () => axiosRequest.get(FIND_LOGGED_USER_RESOURCE),
        getPosts: userId => axiosRequest.get(`/posts/ofUser?user=${userId}`),
        getNumberOfLikesOnPost: postId => axiosRequest.get(`/likes/count?post=${postId}`),
        getCommentsOnPost: postId => axiosRequest.get(`/comments/onPost?post=${postId}`),
        getLikesOnPost: postId => axiosRequest.get(`/likes/onPost?post=${postId}`),
        getFollowersOfUser: userId => axiosRequest.get(`/users/followers?user=${userId}`),
        getFollowingUsers: userId => axiosRequest.get(`/users/following?user=${userId}`),
        postLike: like => axiosRequest.post("/likes", like),
        doesUserLikePost: postId => axiosRequest.get(`/likes/isLiked/${postId}`),
        deleteLike: likeId => axiosRequest.delete(`/likes/${likeId}`),
        postComment: comment => axiosRequest.post("/comments", comment),
        editUsername: (userId, editUsernameRequestBody) => axiosRequest.put(`/users/${userId}/editUsername`, editUsernameRequestBody),
        postFollow: follow => axiosRequest.post("/follows", follow),
        deleteFollow: followId => axiosRequest.delete(`/follows/${followId}`),
        doesUserFollow: userIdWhom => axiosRequest.get(`/follows/isFollowed/${userIdWhom}`),
        deleteComment: commentId => axiosRequest.delete(`/comments/${commentId}`),
        deletePost: postId => axiosRequest.delete(`/posts/${postId}`),
        deleteUser: userId => axiosRequest.delete(`/users/${userId}`),
        savePost: post => axiosRequest.post("/posts", post),
        searchUser: prefix => axiosRequest.get(`/users/search?prefix=${prefix}`)
    };
}

export default useApi;
