import {DUPLICATE_ENTRY, LIKE_OR_FOLLOW_NOT_FOUND} from "../constant/constants";

const useErrorCatcher = () => {
    return {
        responseErrorCatcher: (response, checkForUniqueConstraintViolation, allowNotFound) => {
            if (!response) {
                return "/noResponse";
            }
            else if (response.status === 401) {
                return "/logout";
            }
            else if (response.status === 500) {
                return "/serverError";
            }
            else if (response.status === 404) {
                if (allowNotFound) {
                    return LIKE_OR_FOLLOW_NOT_FOUND;
                }
                return "/notFound";
            }
            else if (response.status === 400) {
                if (checkForUniqueConstraintViolation && response.data.message === DUPLICATE_ENTRY) {
                    return DUPLICATE_ENTRY;
                }
                return "/badRequest";
            }
            else if (response.status === 415) {
                return "/unsupportedMedia";
            }
            else if (response.status === 403) {
                return "/forbidden"
            }
            else {
                return "/";
            }

        }
    }

}

export default useErrorCatcher;
