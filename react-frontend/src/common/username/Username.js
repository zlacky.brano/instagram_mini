import React from 'react';
import {Link} from "react-router-dom";
import './Username.css';

const Username = ({user}) => {
    const url = "/profile/" + user.id;
    return (
        <div className="Username">
            <Link to={url} className="Link">{user.username}</Link>
        </div>
    )
}

export default Username;