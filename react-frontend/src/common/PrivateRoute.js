import React from 'react';
import { Route, Redirect } from "react-router-dom";
import {TOKEN_NAME} from "../constant/constants";

const PrivateRoute = ({component: Component, ...rest}) => (
    <Route
        {...rest}
        render={props =>
            localStorage.getItem(TOKEN_NAME) ? (
                <Component {...rest} {...props} />
            ) : (
                <Redirect
                    to={{
                        pathname: '/',
                        state: {from: props.location}
                    }}
                />
            )
        }
    />
);

export default PrivateRoute;