import React from 'react';

function ServerError() {
    return (
        <div className="Request-error-body">
            <h1>
                500
            </h1>
            <div>
                I am sorry. Error on server side occurred.
            </div>
        </div>
    );

}

export default ServerError;