import React from 'react';

function Forbidden() {
    return (
        <div className="Request-error-body">
            <h1>
                403
            </h1>
            <div>
                Forbidden.
            </div>
        </div>
    );

}

export default Forbidden;