import React from 'react';

function NotFound() {
    return (
        <div className="Request-error-body">
            <h1>
                404
            </h1>
            <div>
                The Page you're looking for was not found.
            </div>
        </div>
    );

}

export default NotFound;