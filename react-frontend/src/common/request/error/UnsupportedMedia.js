import React from 'react';

function UnsupportedMedia() {
    return (
        <div className="Request-error-body">
            <h1>
                415
            </h1>
            <div>
                Unsupported media error.
            </div>
        </div>
    );

}

export default UnsupportedMedia;