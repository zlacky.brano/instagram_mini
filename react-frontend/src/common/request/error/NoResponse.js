import React from 'react';

function NoResponse() {
    return (
        <div className="Request-error-body">
            <h1>
                Server didn't respond.
            </h1>
        </div>
    );

}

export default NoResponse;