import React from 'react';

function BadRequest() {
    return (
        <div className="Request-error-body">
            <h1>
                400
            </h1>
            <div>
                Bad Request.
            </div>
        </div>
    );

}

export default BadRequest;