import React, {useEffect, useState} from 'react';
import {Redirect} from "react-router";
import {TOKEN_NAME, USER_ID} from "../../constant/constants";
import useApi from "../../api/useApi";
import Loading from "../../common/loading/Loading";
import useErrorCatcher from "../../api/useErrorCatcher";

const AfterLoginHandler = ({setAuthenticated, setLoggedUser}) => {
    const url = new URL(window.location.href);
    const token = url.searchParams.get('token');
    const [isLoading, setLoading] = useState(true);
    const {getLoggedUser} = useApi();
    const [redirect, setRedirect] = useState(null);
    const {responseErrorCatcher} = useErrorCatcher();

    const getLoggedUserInfo = async () => {
        const {data} = await getLoggedUser();
        return data;
    }

    useEffect(() => {
        console.log("AfterLoginHandler's useEffect called.");
        if (token) {
            localStorage.setItem(TOKEN_NAME, token);
            setAuthenticated(true);
            getLoggedUserInfo().then(data => {
                localStorage.setItem(USER_ID, data.id);
                setLoggedUser(data.id);
                setLoading(false);
            }).catch(e => {
                const redirectTo = responseErrorCatcher(e.response, false, false);
                setRedirect(redirectTo);
            });
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [token]);

    if (redirect) {
        return <Redirect to={redirect}/>;
    } else if (isLoading) {
        return <Loading/>;
    } else if (token) {
        return <Redirect to={{
            pathname: "/profile/" + localStorage.getItem(USER_ID),
        }}/>;
    } else {
        return <Redirect to={{
            pathname: "/",
            state: {
                error: "Token was not found"
            }
        }}/>;
    }
}

export default AfterLoginHandler;