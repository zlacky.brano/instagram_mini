import {Redirect} from "react-router";
import React, {useEffect} from "react";
import {TOKEN_NAME, USER_ID} from "../constant/constants";

function Logout({setAuthenticated, setLoggedUser}) {
    useEffect(() => {
        localStorage.removeItem(TOKEN_NAME);
        localStorage.removeItem(USER_ID);
        setAuthenticated(false);
        setLoggedUser(null);
    });

    return (
        <Redirect to={{
            pathname: "/"
        }}/>
    )
}

export default Logout;