import React from 'react';
import {API_BASE_URL, GOOGLE_AUTH_RESOURCE} from "../constant/constants";
import googleSignIn from './../img/google-sign-in.png';

function Login() {
    return (
        <div className="Login">
            <a className="Login-button" href={API_BASE_URL + GOOGLE_AUTH_RESOURCE}>
                <img src={googleSignIn} width="100%" height="45px" alt="Google"/>
            </a>
        </div>
    );

}

export default Login;