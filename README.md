# Instamini
Inspired by Instagram

![Diagram](https://res.cloudinary.com/instamini/image/upload/v1632777932/Diagram_y0fhog.png)

# Run

```sh
$ git clone https://gitlab.com/zlacky.brano/instagram_mini.git
$ cd instagram_mini
(Command below is Optional, default is spring.) 
$ bash ./pick-framework.sh <name> (micronaut/spring/quarkus)
(If you already started this docker-compose.yml at least once and you want to change framework to another one, then you have to rebuild it with command below)
$ docker-compose build instamini
$ docker-compose up

Type localhost:3001 in web browser

Grafana (localhost:3000):
	Login: admin
	Password: admin
```

# Run tests without running docker-compose

```sh
$ git clone https://gitlab.com/zlacky.brano/instagram_mini.git
$ cd instagram_mini
$ ./gradlew check
```

# Tech

## Core

* [Java 11]
* [Spring] / [Quarkus] / [Micronaut]

[Java 11]: <https://www.java.com/en/>
[Spring]: <https://spring.io/>
[Quarkus]: <https://quarkus.io/>
[Micronaut]: <https://micronaut.io/>

## Persistence

* [MySQL]
* [Spring Data JPA] / [Micronaut Data] / [Quarkus Hibernate ORM Panache]
* [H2 Database] (for testing)

[MySQL]: <https://www.mysql.com/>
[Spring Data JPA]: <https://spring.io/projects/spring-data-jpa>
[Micronaut Data]: <https://guides.micronaut.io/latest/micronaut-data-jdbc-repository-gradle-java.html>
[Quarkus Hibernate ORM Panache]: <https://quarkus.io/guides/hibernate-orm-panache>
[H2 Database]: <https://www.h2database.com>

## Security

* [Google OAuth2]
* [JWT]
* [Spring Security] / [Quarkus Security] / [Micronaut Security]

[Google OAuth2]: <https://developers.google.com/identity/protocols/oauth2>
[JWT]: <https://jwt.io/>
[Spring Security]: <https://spring.io/projects/spring-security>
[Quarkus Security]: <https://quarkus.io/guides/security-customization>
[Micronaut Security]: <https://micronaut-projects.github.io/micronaut-security/latest/guide/>

## Observability

* [Prometheus]
* [Grafana]

[Prometheus]: <https://prometheus.io/>
[Grafana]: <https://grafana.com/>

## Simple UI - for demonstration purposes

* [React]

[React]: <https://reactjs.org/>

## Other

### Containerization

* [Docker]

[Docker]: <https://www.docker.com/>

### Image management

* [Cloudinary]

[Cloudinary]: <https://cloudinary.com/>

### Full-text search

* [Elasticsearch]

[Elasticsearch]: <https://www.elastic.co/>

### Indexing (index data from MySQL to Elasticsearch)

* [Logstash]

[Logstash]: <https://www.elastic.co/logstash/>

# Screenshots
![screenshot_1]
![screenshot_2]
![screenshot_3]
![screenshot_4]
![screenshot_5]
![screenshot_6]
![screenshot_7]


[screenshot_1]: https://res.cloudinary.com/instamini/image/upload/v1632695252/1.png "Home page"
[screenshot_2]: https://res.cloudinary.com/instamini/image/upload/v1632695252/2.png "Search page"
[screenshot_3]: https://res.cloudinary.com/instamini/image/upload/v1632695252/3.png "Profile"
[screenshot_4]: https://res.cloudinary.com/instamini/image/upload/v1632695252/4.png "Other's profile"
[screenshot_5]: https://res.cloudinary.com/instamini/image/upload/v1632695252/5.png "Followers Modal Body"
[screenshot_6]: https://res.cloudinary.com/instamini/image/upload/v1632695252/6.png "Post Modal Body"
[screenshot_7]: https://res.cloudinary.com/instamini/image/upload/v1633352698/7_xgbfq4.png "Grafana example"
