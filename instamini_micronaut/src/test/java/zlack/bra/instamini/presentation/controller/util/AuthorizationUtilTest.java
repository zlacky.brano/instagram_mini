package zlack.bra.instamini.presentation.controller.util;

import io.micronaut.http.HttpStatus;
import io.micronaut.http.exceptions.HttpStatusException;
import junit.framework.AssertionFailedError;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import util.logged.LoggedUser;
import zlack.bra.instamini.business.dto.UserDTO;
import zlack.bra.instamini.business.service.impl.UserService;
import zlack.bra.instamini.controller.util.AuthorizationUtil;

import java.util.Optional;


@RunWith(MockitoJUnitRunner.class)
class AuthorizationUtilTest {

    private AutoCloseable autoCloseable;

    private UserDTO loggedUser;

    private AuthorizationUtil authorizationUtil;

    private String message = "test";

    @Mock
    private UserService userService;

    @BeforeEach
    void setUp() {
        autoCloseable = MockitoAnnotations.openMocks(this);
        loggedUser = LoggedUser.user;
        authorizationUtil = new AuthorizationUtil(userService);
    }

    @AfterEach
    void tearDown() throws Exception {
        autoCloseable.close();
    }

    @Test
    void getUserFromPrincipal() {
        Mockito.when(userService.findByEmail(LoggedUser.getPrincipal().getName())).thenReturn(Optional.of(loggedUser));

        UserDTO user = authorizationUtil.getUserFromPrincipal(LoggedUser.getPrincipal());
        Assertions.assertEquals(loggedUser.getUsername(), user.getUsername());
        Assertions.assertEquals(loggedUser.getEmail(), user.getEmail());
        Assertions.assertEquals(loggedUser.getId(), user.getId());

        Mockito.verify(userService, Mockito.times(1)).findByEmail(LoggedUser.getPrincipal().getName());
    }

    @Test
    void getUserFromPrincipalBadRequest() {
        Mockito.when(userService.findByEmail(LoggedUser.getPrincipal().getName())).thenReturn(Optional.empty());

        try {
            authorizationUtil.getUserFromPrincipal(LoggedUser.getPrincipal());
            throw new AssertionFailedError();
        } catch (HttpStatusException e) {
            Assertions.assertEquals(e.getStatus(), HttpStatus.BAD_REQUEST);
        }

        Mockito.verify(userService, Mockito.times(1)).findByEmail(LoggedUser.getPrincipal().getName());
    }

    @Test
    void checkIfAuthorizedOrElseThrow() {
        Mockito.when(userService.findByEmail(LoggedUser.getPrincipal().getName())).thenReturn(Optional.of(loggedUser));

        try {
            authorizationUtil.checkIfAuthorizedOrElseThrow(LoggedUser.getPrincipal(), 666, message);
        } catch (HttpStatusException e) {
            throw new AssertionFailedError();
        }

        Mockito.verify(userService, Mockito.times(1)).findByEmail(LoggedUser.getPrincipal().getName());
    }

    @Test
    void checkIfAuthorizedOrElseThrowForbidden() {
        Mockito.when(userService.findByEmail(LoggedUser.getPrincipal().getName())).thenReturn(Optional.of(loggedUser));

        try {
            authorizationUtil.checkIfAuthorizedOrElseThrow(LoggedUser.getPrincipal(), 1, message);
            throw new AssertionFailedError();
        } catch (HttpStatusException e) {
            Assertions.assertEquals(e.getStatus(), HttpStatus.FORBIDDEN);
            Assertions.assertEquals(e.getMessage(), message);
        }

        Mockito.verify(userService, Mockito.times(1)).findByEmail(LoggedUser.getPrincipal().getName());
    }
}
