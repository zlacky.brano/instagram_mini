package zlack.bra.instamini.presentation.controller;

import io.micronaut.http.HttpStatus;
import io.micronaut.http.exceptions.HttpStatusException;
import junit.framework.AssertionFailedError;
import org.hibernate.exception.ConstraintViolationException;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import util.AssertFollowsUnitUtil;
import util.logged.LoggedUser;
import zlack.bra.instamini.business.dto.FollowCreateDTO;
import zlack.bra.instamini.business.dto.FollowDTO;
import zlack.bra.instamini.business.dto.UserDTO;
import zlack.bra.instamini.business.exception.FollowYourselfException;
import zlack.bra.instamini.business.exception.NotFoundException;
import zlack.bra.instamini.business.service.impl.FollowService;
import zlack.bra.instamini.business.service.impl.UserService;
import zlack.bra.instamini.controller.FollowController;
import zlack.bra.instamini.controller.util.AuthorizationUtil;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


@RunWith(MockitoJUnitRunner.class)
class FollowControllerTest {

    private AutoCloseable autoCloseable;

    private FollowController followController;

    @Mock
    private FollowService followService;

    @Mock
    private UserService userService;

    private UserDTO loggedUser;

    @BeforeEach
    void setUp() {
        autoCloseable = MockitoAnnotations.openMocks(this);
        loggedUser = LoggedUser.user;
        followController = new FollowController(followService, new AuthorizationUtil(userService));
    }

    @AfterEach
    void tearDown() throws Exception {
        autoCloseable.close();
    }

    @Test
    void all() {
        List<FollowDTO> follows = new ArrayList<>();
        int numberOfItems = 3;

        for (int i = 1; i <= numberOfItems; i++) {
            UserDTO userWhoDTO = new UserDTO(i, "1user" + i, "1user" + i);
            UserDTO userWhomDTO = new UserDTO(i, "2user" + i, "2user" + i);

            FollowDTO follow = new FollowDTO(i, userWhoDTO, userWhomDTO);
            follows.add(follow);
        }

        Mockito.when(followService.findAll()).thenReturn(follows);

        List<FollowDTO> followsDTO = followController.all();

        Assertions.assertEquals(follows.size(), followsDTO.size());
        for (int i = 0; i < followsDTO.size(); i++) {
            AssertFollowsUnitUtil.assertFollows(follows.get(i), followsDTO.get(i));
        }
        Mockito.verify(followService, Mockito.times(1)).findAll();
    }

    @Test
    void allEmpty() {
        List<FollowDTO> follows = new ArrayList<>();

        Mockito.when(followService.findAll()).thenReturn(follows);

        List<FollowDTO> followsDTO = followController.all();

        Assertions.assertEquals(follows.size(), followsDTO.size());
        Mockito.verify(followService, Mockito.times(1)).findAll();
    }

    @Test
    void byID() {
        UserDTO userWhoDTO = new UserDTO(1, "1user", "1user");
        UserDTO userWhomDTO = new UserDTO(2, "2user", "2user");

        FollowDTO follow = new FollowDTO(1, userWhoDTO, userWhomDTO);

        Mockito.when(followService.findById(follow.getId())).thenReturn(Optional.of(follow));

        FollowDTO followResult = followController.byID(follow.getId());

        AssertFollowsUnitUtil.assertFollows(follow, followResult);

        Mockito.verify(followService, Mockito.times(1)).findById(follow.getId());
    }

    @Test
    void byIDNotFound() {
        Integer followId = 1;

        Mockito.when(followService.findById(followId)).thenReturn(Optional.empty());

        try {
            followController.byID(followId);
            throw new AssertionFailedError();
        } catch (HttpStatusException e) {
            Assertions.assertEquals(e.getStatus(), HttpStatus.NOT_FOUND);
            Mockito.verify(followService, Mockito.times(1)).findById(followId);
        }
    }

    @Test
    void create() throws NotFoundException, FollowYourselfException {
        Integer userIdWhom = 5;
        FollowCreateDTO followCreateDTO = new FollowCreateDTO(loggedUser.getId(), userIdWhom);

        UserDTO userWhomDTO = new UserDTO(userIdWhom, "2user", "2user");

        Integer followId = 1;
        FollowDTO followDTO = new FollowDTO(followId, loggedUser, userWhomDTO);

        Mockito.when(userService.findByEmail(loggedUser.getEmail())).thenReturn(Optional.of(loggedUser));
        Mockito.when(followService.create(followCreateDTO)).thenReturn(followDTO);

        Optional<FollowDTO> followDTOOutput = (Optional<FollowDTO>) followController.create(followCreateDTO, LoggedUser.getPrincipal()).getBody();

        Assertions.assertTrue(followDTOOutput.isPresent());
        AssertFollowsUnitUtil.assertFollows(followDTO, followDTOOutput.get());

        Mockito.verify(followService, Mockito.times(1)).create(followCreateDTO);
        Mockito.verify(userService, Mockito.times(1)).findByEmail(loggedUser.getEmail());
    }


    @Test
    void createNotFound() throws NotFoundException, FollowYourselfException {
        Integer userIdWhom = 5;
        FollowCreateDTO followCreateDTO = new FollowCreateDTO(loggedUser.getId(), userIdWhom);

        Mockito.when(userService.findByEmail(loggedUser.getEmail())).thenReturn(Optional.of(loggedUser));
        Mockito.when(followService.create(followCreateDTO)).thenThrow(new NotFoundException(""));

        try {
            followController.create(followCreateDTO, LoggedUser.getPrincipal());
            throw new AssertionFailedError();
        } catch (HttpStatusException e) {
            Assertions.assertEquals(e.getStatus(), HttpStatus.NOT_FOUND);
            Mockito.verify(followService, Mockito.times(1)).create(followCreateDTO);
            Mockito.verify(userService, Mockito.times(1)).findByEmail(loggedUser.getEmail());
        }
    }

    @Test
    void createForbidden() throws NotFoundException, FollowYourselfException {
        Integer userIdWho = 2;
        Integer userIdWhom = 1;
        FollowCreateDTO followCreateDTO = new FollowCreateDTO(userIdWho, userIdWhom);

        UserDTO userWhoDTO = new UserDTO(userIdWho, "test", "test");
        UserDTO userWhomDTO = new UserDTO(userIdWhom, "test", "test");

        Integer followId = 1;
        FollowDTO followDTO = new FollowDTO(followId, userWhoDTO, userWhomDTO);

        Mockito.when(userService.findByEmail(loggedUser.getEmail())).thenReturn(Optional.of(loggedUser));
        Mockito.when(followService.create(followCreateDTO)).thenReturn(followDTO);

        try {
            followController.create(followCreateDTO, LoggedUser.getPrincipal());
            throw new AssertionFailedError();
        } catch (HttpStatusException e) {
            Assertions.assertEquals(e.getStatus(), HttpStatus.FORBIDDEN);
            Mockito.verify(followService, Mockito.times(0)).create(followCreateDTO);
            Mockito.verify(userService, Mockito.times(1)).findByEmail(loggedUser.getEmail());
        }
    }

    @Test
    void createDuplicateEntry() throws NotFoundException, FollowYourselfException {
        Integer userIdWhom = 5;
        FollowCreateDTO followCreateDTO = new FollowCreateDTO(loggedUser.getId(), userIdWhom);

        Mockito.when(userService.findByEmail(loggedUser.getEmail())).thenReturn(Optional.of(loggedUser));
        Mockito.when(followService.create(followCreateDTO)).thenThrow(new ConstraintViolationException("test", new SQLException("Duplicate entry"), null));

        try {
            followController.create(followCreateDTO, LoggedUser.getPrincipal());
            throw new AssertionFailedError();
        } catch (HttpStatusException e) {
            Assertions.assertEquals(HttpStatus.BAD_REQUEST, e.getStatus());
            Mockito.verify(followService, Mockito.times(1)).create(followCreateDTO);
        }
    }

    @Test
    void createBadRequest() throws NotFoundException, FollowYourselfException {
        FollowCreateDTO followCreateDTO = new FollowCreateDTO(loggedUser.getId(), loggedUser.getId());

        Mockito.when(userService.findByEmail(loggedUser.getEmail())).thenReturn(Optional.of(loggedUser));
        Mockito.when(followService.create(followCreateDTO)).thenThrow(new FollowYourselfException(""));

        try {
            followController.create(followCreateDTO, LoggedUser.getPrincipal());
            throw new AssertionFailedError();
        } catch (HttpStatusException e) {
            Assertions.assertEquals(e.getStatus(), HttpStatus.BAD_REQUEST);
            Mockito.verify(followService, Mockito.times(1)).create(followCreateDTO);
            Mockito.verify(userService, Mockito.times(1)).findByEmail(loggedUser.getEmail());
        }
    }

    @Test
    void delete() throws NotFoundException {
        Integer followId = 1;
        UserDTO userWhomDTO = new UserDTO(2, "2user", "2user");

        Optional<FollowDTO> followDTOOptional = Optional.of(new FollowDTO(followId, loggedUser, userWhomDTO));

        Mockito.when(userService.findByEmail(loggedUser.getEmail())).thenReturn(Optional.of(loggedUser));
        Mockito.when(followService.findById(followId)).thenReturn(followDTOOptional);
        followController.delete(followId, LoggedUser.getPrincipal());

        Mockito.verify(followService, Mockito.times(1)).findById(followId);
        Mockito.verify(followService, Mockito.times(1)).deleteById(followId);
        Mockito.verify(userService, Mockito.times(1)).findByEmail(loggedUser.getEmail());
    }

    @Test
    void deleteNotFound() throws NotFoundException {
        Integer followId = 1;

        Mockito.when(followService.findById(followId)).thenReturn(Optional.empty());
        Mockito.doThrow(new NotFoundException("")).when(followService).deleteById(followId);

        try {
            followController.delete(followId, LoggedUser.getPrincipal());
            throw new AssertionFailedError();
        } catch (HttpStatusException e) {
            Assertions.assertEquals(e.getStatus(), HttpStatus.NOT_FOUND);
            Mockito.verify(followService, Mockito.times(1)).findById(followId);
            Mockito.verify(followService, Mockito.times(1)).deleteById(followId);
            Mockito.verify(userService, Mockito.times(0)).findByEmail(loggedUser.getEmail());
        }
    }

    @Test
    void deleteForbidden() throws NotFoundException {
        UserDTO userWhoDTO = new UserDTO(1, "1user", "1user");
        UserDTO userWhomDTO = new UserDTO(2, "2user", "2user");

        Integer followId = 5;
        Optional<FollowDTO> followDTOOptional = Optional.of(new FollowDTO(followId, userWhoDTO, userWhomDTO));

        Mockito.when(userService.findByEmail(loggedUser.getEmail())).thenReturn(Optional.of(loggedUser));
        Mockito.when(followService.findById(followId)).thenReturn(followDTOOptional);
        try {
            followController.delete(followId, LoggedUser.getPrincipal());
            throw new AssertionFailedError();
        } catch (HttpStatusException e) {
            Assertions.assertEquals(e.getStatus(), HttpStatus.FORBIDDEN);
            Mockito.verify(followService, Mockito.times(1)).findById(followId);
            Mockito.verify(followService, Mockito.times(0)).deleteById(followId);
            Mockito.verify(userService, Mockito.times(1)).findByEmail(loggedUser.getEmail());
        }
    }

    @Test
    void findFollowIdByUsers() throws NotFoundException {
        Integer followId = 1;
        Integer userId1 = 1;
        Integer userId2 = 2;

        Mockito.when(followService.findFollowIdByUsers(userId1, userId2)).thenReturn(followId);

        Integer resultId;
        try {
            resultId = followService.findFollowIdByUsers(userId1, userId2);
        } catch (NotFoundException e) {
            throw new AssertionFailedError();
        }

        Assertions.assertEquals(followId, resultId);
        Mockito.verify(followService, Mockito.times(1)).findFollowIdByUsers(userId1, userId2);
    }

    @Test
    void findFollowIdByUsersNotFound() throws NotFoundException {
        Integer userIdWhom = 1;

        Mockito.when(userService.findByEmail(loggedUser.getEmail())).thenReturn(Optional.of(loggedUser));
        Mockito.when(followService.findFollowIdByUsers(loggedUser.getId(), userIdWhom)).thenThrow(new NotFoundException(""));

        try {
            followController.findFollowIdByUsers(userIdWhom, LoggedUser.getPrincipal());
            throw new AssertionFailedError();
        } catch (HttpStatusException e) {
            Assertions.assertEquals(HttpStatus.NOT_FOUND, e.getStatus());
            Mockito.verify(followService, Mockito.times(1)).findFollowIdByUsers(loggedUser.getId(), userIdWhom);
            Mockito.verify(userService, Mockito.times(1)).findByEmail(loggedUser.getEmail());
        }
    }
}
