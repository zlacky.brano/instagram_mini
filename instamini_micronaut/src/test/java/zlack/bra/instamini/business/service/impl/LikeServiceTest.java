package zlack.bra.instamini.business.service.impl;

import io.micronaut.validation.validator.Validator;
import junit.framework.AssertionFailedError;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import util.AssertLikesUnitUtil;
import util.fieldsetter.FieldSetter;
import zlack.bra.instamini.business.dto.LikeCreateDTO;
import zlack.bra.instamini.business.dto.LikeDTO;
import zlack.bra.instamini.business.exception.NotFoundException;
import zlack.bra.instamini.business.service.ImageUploadService;
import zlack.bra.instamini.business.service.util.PostChildrenDelete;
import zlack.bra.instamini.business.service.util.UserChildrenDelete;
import zlack.bra.instamini.data.entity.Like;
import zlack.bra.instamini.data.entity.Post;
import zlack.bra.instamini.data.entity.User;
import zlack.bra.instamini.data.repository.LikeRepository;
import zlack.bra.instamini.data.repository.PostRepository;
import zlack.bra.instamini.data.repository.UserRepository;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


@RunWith(MockitoJUnitRunner.class)
class LikeServiceTest {

    private AutoCloseable autoCloseable;

    private PostService postService;
    private UserService userService;
    private LikeService likeService;

    @Mock
    private ImageUploadService imageUploadService;

    @Mock
    private LikeRepository likeRepository;

    @Mock
    private PostRepository postRepository;

    @Mock
    private UserRepository userRepository;

    @Mock
    private UserChildrenDelete userChildrenDelete;

    @Mock
    private PostChildrenDelete postChildrenDelete;

    @Mock
    private Validator validator;

    private final LocalDateTime localDateTime = LocalDateTime.of(LocalDate.of(2000, 12, 12), LocalTime.of(10, 10, 10));

    private final List<String> httpHosts = new ArrayList<>() {
        {
            add("test");
        }
    };

    @BeforeEach
    void setUp() {
        autoCloseable = MockitoAnnotations.openMocks(this);
        userService = new UserService(userRepository, postRepository, "test", httpHosts, userChildrenDelete);
        postService = new PostService(postRepository, userRepository, userService, imageUploadService, validator, postChildrenDelete);
        likeService = new LikeService(likeRepository, postRepository, userRepository, postService, userService);
    }

    @AfterEach
    void tearDown() throws Exception {
        autoCloseable.close();
    }

    @Test
    void create() throws NoSuchFieldException, IllegalAccessException {
        Integer userId = 1;
        Integer postId = 2;
        LikeCreateDTO likeCreateDTO = new LikeCreateDTO(userId, postId);

        User user = new User("test", "test", null, null, null, null, null);
        Post post = new Post("test", "test", localDateTime, user);

        FieldSetter.setUserId(user, userId);
        FieldSetter.setPostId(post, postId);

        Integer likeId = 1;
        Like like = new Like(user, post);
        FieldSetter.setLikeId(like, likeId);

        Mockito.when(likeRepository.save(Mockito.any())).thenReturn(like);
        Mockito.when(userRepository.findById(userId)).thenReturn(Optional.of(user));
        Mockito.when(postRepository.findById(postId)).thenReturn(Optional.of(post));

        LikeDTO likeDTO;
        try {
            likeDTO = likeService.create(likeCreateDTO);
        } catch (NotFoundException e) {
            throw new AssertionFailedError();
        }

        Assertions.assertNotNull(likeDTO);
        AssertLikesUnitUtil.assertLikes(like, likeDTO, userService, postService);

        Mockito.verify(likeRepository, Mockito.times(1)).save(Mockito.any());
        Mockito.verify(userRepository, Mockito.times(1)).findById(userId);
        Mockito.verify(postRepository, Mockito.times(1)).findById(postId);
    }

    @Test
    void createUserNotFound() throws NoSuchFieldException, IllegalAccessException {
        Integer userId = 1;
        Integer postId = 2;
        LikeCreateDTO likeCreateDTO = new LikeCreateDTO(userId, postId);

        User user = new User("test", "test", null, null, null, null, null);
        Post post = new Post("test", "test", localDateTime, user);

        FieldSetter.setUserId(user, userId);
        FieldSetter.setPostId(post, postId);

        Integer likeId = 1;
        Like like = new Like(user, post);
        FieldSetter.setLikeId(like, likeId);

        Mockito.when(likeRepository.save(Mockito.any())).thenReturn(like);
        Mockito.when(userRepository.findById(userId)).thenReturn(Optional.empty());
        Mockito.when(postRepository.findById(postId)).thenReturn(Optional.of(post));

        try {
            likeService.create(likeCreateDTO);
            throw new AssertionFailedError();
        } catch (NotFoundException e) {
            Mockito.verify(likeRepository, Mockito.times(0)).save(Mockito.any());
            Mockito.verify(userRepository, Mockito.times(1)).findById(userId);
            Mockito.verify(postRepository, Mockito.times(1)).findById(postId);
        }
    }

    @Test
    void createPostNotFound() throws NoSuchFieldException, IllegalAccessException {
        Integer userId = 1;
        Integer postId = 2;
        LikeCreateDTO likeCreateDTO = new LikeCreateDTO(userId, postId);

        User user = new User("test", "test", null, null, null, null, null);
        Post post = new Post("test", "test", localDateTime, user);

        FieldSetter.setUserId(user, userId);
        FieldSetter.setPostId(post, postId);

        Integer likeId = 1;
        Like like = new Like(user, post);
        FieldSetter.setLikeId(like, likeId);

        Mockito.when(likeRepository.save(Mockito.any())).thenReturn(like);
        Mockito.when(userRepository.findById(userId)).thenReturn(Optional.of(user));
        Mockito.when(postRepository.findById(postId)).thenReturn(Optional.empty());

        try {
            likeService.create(likeCreateDTO);
            throw new AssertionFailedError();
        } catch (NotFoundException e) {
            Mockito.verify(likeRepository, Mockito.times(0)).save(Mockito.any());
            Mockito.verify(userRepository, Mockito.times(1)).findById(userId);
            Mockito.verify(postRepository, Mockito.times(1)).findById(postId);
        }
    }

    @Test
    void findAll() throws NoSuchFieldException, IllegalAccessException {
        List<Like> likes = new ArrayList<>();
        int numberOfItems = 3;
        int idOfUser = 1;
        int idOfPost = 1;

        for (int i = 1; i <= numberOfItems; i++) {
            User user = new User("test" + i, "test" + i, null, null, null, null, null);
            Post post = new Post("test" + i, "test" + i, localDateTime, user);
            FieldSetter.setUserId(user, idOfUser++);
            FieldSetter.setPostId(post, idOfPost++);

            Like like = new Like(user, post);
            FieldSetter.setLikeId(like, i);
            likes.add(like);
        }

        Mockito.when(likeRepository.findAll()).thenReturn(likes);

        List<LikeDTO> likesDTO = likeService.findAll();

        Assertions.assertEquals(likes.size(), likesDTO.size());
        for (int i = 0; i < likesDTO.size(); i++) {
            AssertLikesUnitUtil.assertLikes(likes.get(i), likesDTO.get(i), userService, postService);
        }
        Mockito.verify(likeRepository, Mockito.times(1)).findAll();
    }

    @Test
    void findAllEmpty() {
        List<Like> likes = new ArrayList<>();

        Mockito.when(likeRepository.findAll()).thenReturn(likes);

        List<LikeDTO> likesDTO = likeService.findAll();

        Assertions.assertEquals(likes.size(), likesDTO.size());
        Mockito.verify(likeRepository, Mockito.times(1)).findAll();
    }

    @Test
    void findById() throws NoSuchFieldException, IllegalAccessException {
        Integer userId = 1;
        Integer postId = 2;
        User user = new User("test", "test", null, null, null, null, null);
        Post post = new Post("test", "test", localDateTime, user);
        FieldSetter.setUserId(user, userId);
        FieldSetter.setPostId(post, postId);

        Integer likeId = 3;
        Like like = new Like(user, post);
        FieldSetter.setLikeId(like, likeId);

        Mockito.when(likeRepository.findById(likeId)).thenReturn(Optional.of(like));

        Optional<LikeDTO> likeDTOOptional = likeService.findById(likeId);

        if (likeDTOOptional.isPresent()) {
            LikeDTO likeDTO = likeDTOOptional.get();
            AssertLikesUnitUtil.assertLikes(like, likeDTO, userService, postService);
        } else {
            throw new AssertionFailedError();
        }
        Mockito.verify(likeRepository, Mockito.times(1)).findById(likeId);
    }

    @Test
    void findByIdNotFound() {
        Integer likeId = 1;

        Mockito.when(likeRepository.findById(likeId)).thenReturn(Optional.empty());

        Optional<LikeDTO> likeDTOOptional = likeService.findById(likeId);

        if (likeDTOOptional.isPresent()) {
            throw new AssertionFailedError();
        } else {
            Mockito.verify(likeRepository, Mockito.times(1)).findById(likeId);
        }
    }

    @Test
    void toDTO() throws NoSuchFieldException, IllegalAccessException {
        Integer userId = 1;
        Integer postId = 2;
        User user = new User("test", "test", null, null, null, null, null);
        Post post = new Post("test", "test", localDateTime, user);
        FieldSetter.setUserId(user, userId);
        FieldSetter.setPostId(post, postId);

        Integer likeId = 3;
        Like like = new Like(user, post);
        FieldSetter.setLikeId(like, likeId);

        LikeDTO likeDTO = likeService.toDTO(like);

        AssertLikesUnitUtil.assertLikes(like, likeDTO, userService, postService);
    }

    @Test
    void toDTOOptional() throws NoSuchFieldException, IllegalAccessException {
        Integer userId = 1;
        Integer postId = 2;
        User user = new User("test", "test", null, null, null, null, null);
        Post post = new Post("test", "test", localDateTime, user);
        FieldSetter.setUserId(user, userId);
        FieldSetter.setPostId(post, postId);

        Integer likeId = 3;
        Like like = new Like(user, post);
        FieldSetter.setLikeId(like, likeId);

        Optional<LikeDTO> likeDTO = likeService.toDTO(Optional.of(like));

        if (likeDTO.isEmpty()) {
            throw new AssertionFailedError();
        }

        AssertLikesUnitUtil.assertLikes(like, likeDTO.get(), userService, postService);
    }

    @Test
    void deleteById() {
        Integer likeId = 1;

        Mockito.when(likeRepository.existsById(likeId)).thenReturn(true);
        try {
            likeService.deleteById(likeId);
        } catch (NotFoundException e) {
            throw new AssertionFailedError();
        }

        Mockito.when(likeRepository.existsById(likeId)).thenReturn(false);
        try {
            likeService.deleteById(likeId);
            throw new AssertionFailedError();
        } catch (NotFoundException ignored) {
        }

        Mockito.verify(likeRepository, Mockito.times(2)).existsById(likeId);
        Mockito.verify(likeRepository, Mockito.times(1)).deleteById(likeId);
    }

    @Test
    void findLikesOnPost() throws NoSuchFieldException, IllegalAccessException {
        Integer postId = 1;
        Integer userId = 1;
        User user = new User("test", "test", null, null, null, null, null);
        Post post = new Post("test", "test", localDateTime, user);
        FieldSetter.setUserId(user, userId);
        FieldSetter.setPostId(post, postId);

        List<Like> likes = new ArrayList<>();
        int numberOfLikes = 5;
        for (int i = 1; i <= numberOfLikes; i++) {
            Integer likeId = i;
            Like like = new Like(user, post);
            FieldSetter.setLikeId(like, likeId);

            likes.add(like);
        }

        Mockito.when(likeRepository.findLikesOnPost(postId)).thenReturn(likes);
        Mockito.when(postRepository.existsById(postId)).thenReturn(true);

        List<LikeDTO> likesDTO;
        try {
            likesDTO = likeService.findLikesOnPost(postId);
        } catch (NotFoundException e) {
            throw new AssertionFailedError();
        }

        Assertions.assertEquals(likes.size(), likesDTO.size());

        for (int i = 0; i < numberOfLikes; i++) {
            AssertLikesUnitUtil.assertLikes(likes.get(i), likesDTO.get(i), userService, postService);
        }
        Mockito.verify(likeRepository, Mockito.times(1)).findLikesOnPost(postId);
        Mockito.verify(postRepository, Mockito.times(1)).existsById(postId);
    }

    @Test
    void findLikesOnPostNotFound() {
        Integer postId = 1;
        List<Like> likes = new ArrayList<>();

        Mockito.when(likeRepository.findLikesOnPost(postId)).thenReturn(likes);
        Mockito.when(postRepository.existsById(postId)).thenReturn(false);

        try {
            likeService.findLikesOnPost(postId);
            throw new AssertionFailedError();
        } catch (NotFoundException e) {
            Mockito.verify(likeRepository, Mockito.times(0)).findLikesOnPost(postId);
            Mockito.verify(postRepository, Mockito.times(1)).existsById(postId);
        }
    }

    @Test
    void findNumberOfLikesOnPost() {
        Integer postId = 1;
        Long numberOfPosts = Long.valueOf(5);

        Mockito.when(likeRepository.findNumberOfLikesOnPost(postId)).thenReturn(numberOfPosts);
        Mockito.when(postRepository.existsById(postId)).thenReturn(true);

        Long numberOfPostsOutput;
        try {
            numberOfPostsOutput = likeService.findNumberOfLikesOnPost(postId);
        } catch (NotFoundException e) {
            throw new AssertionFailedError();
        }

        Assertions.assertEquals(numberOfPosts, numberOfPostsOutput);

        Mockito.verify(likeRepository, Mockito.times(1)).findNumberOfLikesOnPost(postId);
        Mockito.verify(postRepository, Mockito.times(1)).existsById(postId);
    }

    @Test
    void findNumberOfLikesOnPostNotFound() {
        Integer postId = 1;

        Mockito.when(likeRepository.findNumberOfLikesOnPost(postId)).thenReturn(Long.valueOf(0));
        Mockito.when(postRepository.existsById(postId)).thenReturn(false);

        try {
            likeService.findNumberOfLikesOnPost(postId);
            throw new AssertionFailedError();
        } catch (NotFoundException e) {
            Mockito.verify(likeRepository, Mockito.times(0)).findNumberOfLikesOnPost(postId);
            Mockito.verify(postRepository, Mockito.times(1)).existsById(postId);
        }
    }

    @Test
    void findLikeIdByUserAndPost() {
        Integer likeId = 1;
        Integer userId = 1;
        Integer postId = 2;

        Mockito.when(likeRepository.findLikeIdByUserAndPost(userId, postId)).thenReturn(Optional.of(likeId));

        Integer resultId;
        try {
            resultId = likeService.findLikeIdByUserAndPost(userId, postId);
        } catch (NotFoundException e) {
            throw new AssertionFailedError();
        }

        Assertions.assertEquals(likeId, resultId);
        Mockito.verify(likeRepository, Mockito.times(1)).findLikeIdByUserAndPost(userId, postId);
    }

    @Test
    void findLikeIdByUserAndPostNotFound() {
        Integer userId = 1;
        Integer postId = 2;

        Mockito.when(likeRepository.findLikeIdByUserAndPost(userId, postId)).thenReturn(Optional.empty());

        try {
            likeService.findLikeIdByUserAndPost(userId, postId);
            throw new AssertionFailedError();
        } catch (NotFoundException e) {
            Mockito.verify(likeRepository, Mockito.times(1)).findLikeIdByUserAndPost(userId, postId);
        }
    }
}
