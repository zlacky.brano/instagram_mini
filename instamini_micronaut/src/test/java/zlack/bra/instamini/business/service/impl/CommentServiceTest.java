package zlack.bra.instamini.business.service.impl;

import io.micronaut.context.annotation.Requires;
import io.micronaut.validation.validator.Validator;
import jakarta.inject.Inject;
import junit.framework.AssertionFailedError;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import util.AssertCommentsUnitUtil;
import util.fieldsetter.FieldSetter;
import zlack.bra.instamini.business.dto.CommentCreateDTO;
import zlack.bra.instamini.business.dto.CommentDTO;
import zlack.bra.instamini.business.exception.NotFoundException;
import zlack.bra.instamini.business.service.ImageUploadService;
import zlack.bra.instamini.business.service.util.PostChildrenDelete;
import zlack.bra.instamini.business.service.util.UserChildrenDelete;
import zlack.bra.instamini.data.entity.Comment;
import zlack.bra.instamini.data.entity.Post;
import zlack.bra.instamini.data.entity.User;
import zlack.bra.instamini.data.repository.CommentRepository;
import zlack.bra.instamini.data.repository.PostRepository;
import zlack.bra.instamini.data.repository.UserRepository;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


@RunWith(MockitoJUnitRunner.class)
class CommentServiceTest {

    private AutoCloseable autoCloseable;

    private UserService userService;
    private PostService postService;
    private CommentService commentService;

    @Mock
    private CommentRepository commentRepository;

    @Mock
    private PostRepository postRepository;

    @Mock
    private UserRepository userRepository;

    @Mock
    private ImageUploadService imageUploadService;

    @Mock
    private UserChildrenDelete userChildrenDelete;

    @Mock
    private PostChildrenDelete postChildrenDelete;

    @Mock
    private Validator validator;

    private final List<String> httpHosts = new ArrayList<>() {
        {
            add("test");
        }
    };

    private final LocalDateTime localDateTime = LocalDateTime.of(LocalDate.of(2000, 12, 12), LocalTime.of(10, 10, 10));

    @BeforeEach
    void setUp() {
        autoCloseable = MockitoAnnotations.openMocks(this);
        userService = new UserService(userRepository, postRepository, "test", httpHosts, userChildrenDelete);
        postService = new PostService(postRepository, userRepository, userService, imageUploadService, validator, postChildrenDelete);
        commentService = new CommentService(commentRepository, userService, postService, postRepository, userRepository);
    }

    @AfterEach
    void tearDown() throws Exception {
        autoCloseable.close();
    }

    @Test
    void editText() throws NoSuchFieldException, IllegalAccessException {
        Integer userId = 1;
        Integer postId = 1;
        User user = new User("user1", "user1", null, null, null, null, null);
        Post post = new Post("test", "test", localDateTime, user);
        FieldSetter.setUserId(user, userId);
        FieldSetter.setPostId(post, postId);

        Comment commentToEdit = new Comment("old text", localDateTime, user, post);

        Integer commentId = 1;
        FieldSetter.setCommentId(commentToEdit, commentId);

        String newText = "new text";

        Mockito.when(commentRepository.findById(commentId)).thenReturn(Optional.of(commentToEdit));

        CommentDTO editedComment;
        try {
            editedComment = commentService.editText(commentId, newText);
        } catch (NotFoundException e) {
            throw new AssertionFailedError();
        }

        Assertions.assertNotNull(editedComment);
        AssertCommentsUnitUtil.assertCommentsInEditTextTest(commentToEdit, editedComment, userService, postService, newText);

        Mockito.verify(commentRepository, Mockito.times(1)).findById(commentId);
    }

    @Test
    void editTextNotFound() {
        Integer id = 1;
        String newText = "new text";

        Mockito.when(commentRepository.findById(id)).thenReturn(Optional.empty());

        try {
            commentService.editText(id, newText);
            throw new AssertionFailedError();
        } catch (NotFoundException e) {
            Mockito.verify(commentRepository, Mockito.times(1)).findById(id);
        }
    }

    @Test
    void create() throws NoSuchFieldException, IllegalAccessException {
        Integer userId = 1;
        Integer postId = 1;
        CommentCreateDTO commentCreateDTO = new CommentCreateDTO("text", userId, postId);

        User user = new User("user1", "user1", null, null, null, null, null);
        Post post = new Post("test", "test", localDateTime, user);
        FieldSetter.setUserId(user, userId);
        FieldSetter.setPostId(post, postId);

        Comment comment = new Comment("text", localDateTime, user, post);

        Integer commentId = 1;
        FieldSetter.setCommentId(comment, commentId);

        Mockito.when(commentRepository.save(Mockito.any())).thenReturn(comment);
        Mockito.when(userRepository.findById(userId)).thenReturn(Optional.of(user));
        Mockito.when(postRepository.findById(postId)).thenReturn(Optional.of(post));

        CommentDTO commentDTO;
        try {
            commentDTO = commentService.create(commentCreateDTO);
        } catch (NotFoundException e) {
            throw new AssertionFailedError();
        }

        Assertions.assertNotNull(commentDTO);
        AssertCommentsUnitUtil.assertComments(comment, commentDTO, userService, postService);

        Mockito.verify(commentRepository, Mockito.times(1)).save(Mockito.any());
        Mockito.verify(userRepository, Mockito.times(1)).findById(userId);
        Mockito.verify(postRepository, Mockito.times(1)).findById(postId);
    }

    @Test
    void createUserNotFound() throws NoSuchFieldException, IllegalAccessException {
        Integer userId = 1;
        Integer postId = 1;
        CommentCreateDTO commentCreateDTO = new CommentCreateDTO("text", userId, postId);

        User user = new User("user1", "user1", null, null, null, null, null);
        Post post = new Post("test", "test", localDateTime, user);
        FieldSetter.setUserId(user, userId);
        FieldSetter.setPostId(post, postId);

        Comment comment = new Comment("text", localDateTime, user, post);

        Integer commentId = 1;
        FieldSetter.setCommentId(comment, commentId);

        Mockito.when(commentRepository.save(Mockito.any())).thenReturn(comment);
        Mockito.when(userRepository.findById(userId)).thenReturn(Optional.empty());
        Mockito.when(postRepository.findById(postId)).thenReturn(Optional.of(post));

        try {
            commentService.create(commentCreateDTO);
            throw new AssertionFailedError();
        } catch (NotFoundException e) {
            Mockito.verify(commentRepository, Mockito.times(0)).save(Mockito.any());
            Mockito.verify(userRepository, Mockito.times(1)).findById(userId);
            Mockito.verify(postRepository, Mockito.times(1)).findById(postId);
        }
    }

    @Test
    void createPostNotFound() throws NoSuchFieldException, IllegalAccessException {
        Integer userId = 1;
        Integer postId = 1;
        CommentCreateDTO commentCreateDTO = new CommentCreateDTO("text", userId, postId);

        User user = new User("user1", "user1", null, null, null, null, null);
        Post post = new Post("test", "test", localDateTime, user);
        FieldSetter.setUserId(user, userId);
        FieldSetter.setPostId(post, postId);

        Comment comment = new Comment("text", localDateTime, user, post);

        Integer commentId = 1;
        FieldSetter.setCommentId(comment, commentId);

        Mockito.when(commentRepository.save(Mockito.any())).thenReturn(comment);
        Mockito.when(userRepository.findById(userId)).thenReturn(Optional.of(user));
        Mockito.when(postRepository.findById(postId)).thenReturn(Optional.empty());

        try {
            commentService.create(commentCreateDTO);
            throw new AssertionFailedError();
        } catch (NotFoundException e) {
            Mockito.verify(commentRepository, Mockito.times(0)).save(Mockito.any());
            Mockito.verify(userRepository, Mockito.times(1)).findById(userId);
            Mockito.verify(postRepository, Mockito.times(1)).findById(postId);
        }
    }

    @Test
    void findAll() throws NoSuchFieldException, IllegalAccessException {
        List<Comment> comments = new ArrayList<>();
        int numberOfItems = 3;
        Integer userId = 1;
        Integer postId = 1;

        for (int i = 1; i <= numberOfItems; i++) {
            User user = new User("user1", "user1", null, null, null, null, null);
            Post post = new Post("test", "test", localDateTime, user);
            FieldSetter.setUserId(user, userId);
            FieldSetter.setPostId(post, postId);

            Comment comment = new Comment("text", localDateTime, user, post);

            Integer commentId = 1;
            FieldSetter.setCommentId(comment, commentId);
            comments.add(comment);
        }

        Mockito.when(commentRepository.findAll()).thenReturn(comments);

        List<CommentDTO> commentsDTO = commentService.findAll();

        Assertions.assertEquals(comments.size(), commentsDTO.size());
        for (int i = 0; i < commentsDTO.size(); i++) {
            AssertCommentsUnitUtil.assertComments(comments.get(i), commentsDTO.get(i), userService, postService);
        }
        Mockito.verify(commentRepository, Mockito.times(1)).findAll();
    }

    @Test
    void findAllEmpty() {
        List<Comment> comments = new ArrayList<>();

        Mockito.when(commentRepository.findAll()).thenReturn(comments);

        List<CommentDTO> commentsDTO = commentService.findAll();

        Assertions.assertEquals(comments.size(), commentsDTO.size());

        Mockito.verify(commentRepository, Mockito.times(1)).findAll();
    }

    @Test
    void findById() throws NoSuchFieldException, IllegalAccessException {
        Integer userId = 1;
        Integer postId = 2;
        User user = new User("user1", "user1", null, null, null, null, null);
        Post post = new Post("test", "test", localDateTime, user);
        FieldSetter.setUserId(user, userId);
        FieldSetter.setPostId(post, postId);

        Comment comment = new Comment("text", localDateTime, user, post);

        Integer commentId = 1;
        FieldSetter.setCommentId(comment, commentId);

        Mockito.when(commentRepository.findById(commentId)).thenReturn(Optional.of(comment));

        Optional<CommentDTO> commentDTOOptional = commentService.findById(commentId);

        if (commentDTOOptional.isPresent()) {
            CommentDTO commentDTO = commentDTOOptional.get();
            AssertCommentsUnitUtil.assertComments(comment, commentDTO, userService, postService);
        } else {
            throw new AssertionFailedError();
        }
        Mockito.verify(commentRepository, Mockito.times(1)).findById(commentId);
    }

    @Test
    void findByIdNotFound() {
        Integer commentId = 1;

        Mockito.when(commentRepository.findById(commentId)).thenReturn(Optional.empty());

        Optional<CommentDTO> commentDTOOptional = commentService.findById(commentId);

        if (commentDTOOptional.isPresent()) {
            throw new AssertionFailedError();
        } else {
            Mockito.verify(commentRepository, Mockito.times(1)).findById(commentId);
        }
    }

    @Test
    void toDTO() throws NoSuchFieldException, IllegalAccessException {
        Integer userId = 1;
        Integer postId = 2;
        User user = new User("user1", "user1", null, null, null, null, null);
        Post post = new Post("test", "test", localDateTime, user);
        FieldSetter.setUserId(user, userId);
        FieldSetter.setPostId(post, postId);

        Comment comment = new Comment("text", localDateTime, user, post);

        Integer commentId = 1;
        FieldSetter.setCommentId(comment, commentId);

        CommentDTO commentDTO = commentService.toDTO(comment);

        AssertCommentsUnitUtil.assertComments(comment, commentDTO, userService, postService);
    }

    @Test
    void toDTOOptional() throws NoSuchFieldException, IllegalAccessException {
        Integer userId = 1;
        Integer postId = 2;
        User user = new User("user1", "user1", null, null, null, null, null);
        Post post = new Post("test", "test", localDateTime, user);
        FieldSetter.setUserId(user, userId);
        FieldSetter.setPostId(post, postId);

        Comment comment = new Comment("text", localDateTime, user, post);

        Integer commentId = 1;
        FieldSetter.setCommentId(comment, commentId);

        Optional<CommentDTO> commentDTO = commentService.toDTO(Optional.of(comment));

        if (commentDTO.isEmpty()) {
            throw new AssertionFailedError();
        }

        AssertCommentsUnitUtil.assertComments(comment, commentDTO.get(), userService, postService);
    }

    @Test
    void deleteById() {
        Integer commentId = 1;

        Mockito.when(commentRepository.existsById(commentId)).thenReturn(true);
        try {
            commentService.deleteById(commentId);
        } catch (NotFoundException e) {
            throw new AssertionFailedError();
        }

        Mockito.when(commentRepository.existsById(commentId)).thenReturn(false);
        try {
            commentService.deleteById(commentId);
            throw new AssertionFailedError();
        } catch (NotFoundException ignored) {
        }

        Mockito.verify(commentRepository, Mockito.times(2)).existsById(commentId);
        Mockito.verify(commentRepository, Mockito.times(1)).deleteById(commentId);
    }

    @Test
    void findAllCommentsOnPost() throws NoSuchFieldException, IllegalAccessException {
        Integer postId = 1;
        Integer userId = 1;
        User user = new User("test", "test", null, null, null, null, null);
        Post post = new Post("test", "test", localDateTime, user);
        FieldSetter.setUserId(user, userId);
        FieldSetter.setPostId(post, postId);

        List<Comment> comments = new ArrayList<>();
        int numberOfComments = 5;
        for (int i = 1; i <= numberOfComments; i++) {
            Integer commentId = i;
            Comment comment = new Comment("text" + i, localDateTime, user, post);
            FieldSetter.setCommentId(comment, commentId);
            comments.add(comment);
        }

        Mockito.when(commentRepository.findAllCommentsOnPost(postId)).thenReturn(comments);
        Mockito.when(postRepository.existsById(postId)).thenReturn(true);

        List<CommentDTO> commentsDTO;
        try {
            commentsDTO = commentService.findAllCommentsOnPost(postId);
        } catch (NotFoundException e) {
            throw new AssertionFailedError();
        }

        Assertions.assertEquals(comments.size(), commentsDTO.size());

        for (int i = 0; i < numberOfComments; i++) {
            AssertCommentsUnitUtil.assertComments(comments.get(i), commentsDTO.get(i), userService, postService);
        }
        Mockito.verify(commentRepository, Mockito.times(1)).findAllCommentsOnPost(postId);
        Mockito.verify(postRepository, Mockito.times(1)).existsById(postId);
    }

    @Test
    void findAllCommentsOnPostNotFound() {
        Integer postId = 1;
        List<Comment> comments = new ArrayList<>();

        Mockito.when(commentRepository.findAllCommentsOnPost(postId)).thenReturn(comments);
        Mockito.when(postRepository.existsById(postId)).thenReturn(false);

        try {
            commentService.findAllCommentsOnPost(postId);
            throw new AssertionFailedError();
        } catch (NotFoundException e) {
            Mockito.verify(commentRepository, Mockito.times(0)).findAllCommentsOnPost(postId);
            Mockito.verify(postRepository, Mockito.times(1)).existsById(postId);
        }
    }
}