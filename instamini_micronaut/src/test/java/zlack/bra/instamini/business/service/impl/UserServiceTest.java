package zlack.bra.instamini.business.service.impl;

import junit.framework.AssertionFailedError;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import util.AssertUsersUnitUtil;
import util.fieldsetter.FieldSetter;
import zlack.bra.instamini.business.dto.UserCreateDTO;
import zlack.bra.instamini.business.dto.UserDTO;
import zlack.bra.instamini.business.exception.NotFoundException;
import zlack.bra.instamini.business.service.util.UserChildrenDelete;
import zlack.bra.instamini.data.entity.User;
import zlack.bra.instamini.data.repository.PostRepository;
import zlack.bra.instamini.data.repository.UserRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


class UserServiceTest {

    private AutoCloseable autoCloseable;

    private UserService userService;

    @Mock
    private UserRepository userRepository;

    @Mock
    private PostRepository postRepository;

    @Mock
    private UserChildrenDelete userChildrenDelete;

    private final List<String> httpHosts = new ArrayList<>() {
        {
            add("test");
        }
    };

    @BeforeEach
    void setUp() {
        autoCloseable = MockitoAnnotations.openMocks(this);
        userService = new UserService(userRepository, postRepository, "test",  httpHosts, userChildrenDelete);
    }

    @AfterEach
    void tearDown() throws Exception {
        autoCloseable.close();
    }

    @Test
    void editUsername() throws NoSuchFieldException, IllegalAccessException {
        User userToEdit = new User("test", "old username", null, null, null, null, null);

        Integer id = 1;
        FieldSetter.setUserId(userToEdit, id);

        String newUsername = "new username";

        Mockito.when(userRepository.findById(id)).thenReturn(Optional.of(userToEdit));

        UserDTO editedUser;
        try {
            editedUser = userService.editUsername(id, newUsername);
        } catch (NotFoundException e) {
            throw new AssertionFailedError();
        }

        Assertions.assertNotNull(editedUser);
        AssertUsersUnitUtil.assertUsersInEditUsernameTest(userToEdit, editedUser, newUsername);

        Mockito.verify(userRepository, Mockito.times(1)).findById(id);
    }

    @Test
    void editUsernameNotFound() {
        Integer id = 1;
        String newUsername = "new username";

        Mockito.when(userRepository.findById(id)).thenReturn(Optional.empty());

        try {
            userService.editUsername(id, newUsername);
            throw new AssertionFailedError();
        } catch (NotFoundException e) {
            Mockito.verify(userRepository, Mockito.times(1)).findById(id);
        }
    }

    @Test
    void create() throws NoSuchFieldException, IllegalAccessException {
        Integer userId = 1;
        UserCreateDTO userCreateDTO = new UserCreateDTO("test", "test");

        User user = new User(userCreateDTO.getUsername(), userCreateDTO.getEmail(), null, null, null, null, null);
        FieldSetter.setUserId(user, userId);

        Mockito.when(userRepository.save(Mockito.any())).thenReturn(user);

        UserDTO userDTO = userService.create(userCreateDTO);

        Assertions.assertNotNull(userDTO);
        AssertUsersUnitUtil.assertUsers(user, userDTO);

        Mockito.verify(userRepository, Mockito.times(1)).save(Mockito.any());
    }

    @Test
    void findAll() throws NoSuchFieldException, IllegalAccessException {
        List<User> users = new ArrayList<>();
        int numberOfItems = 3;
        for (int i = 1; i <= numberOfItems; i++) {
            User user = new User("1test" + i, "1test" + i, null, null, null, null, null);
            FieldSetter.setUserId(user, i);

            users.add(user);
        }

        Mockito.when(userRepository.findAll()).thenReturn(users);

        List<UserDTO> usersDTO = userService.findAll();

        Assertions.assertEquals(users.size(), usersDTO.size());
        for (int i = 0; i < usersDTO.size(); i++) {
            AssertUsersUnitUtil.assertUsers(users.get(i), usersDTO.get(i));
        }
        Mockito.verify(userRepository, Mockito.times(1)).findAll();
    }

    @Test
    void findAllEmpty() {
        List<User> users = new ArrayList<>();

        Mockito.when(userRepository.findAll()).thenReturn(users);

        List<UserDTO> usersDTO = userService.findAll();

        Assertions.assertEquals(users.size(), usersDTO.size());

        Mockito.verify(userRepository, Mockito.times(1)).findAll();
    }

    @Test
    void findById() throws NoSuchFieldException, IllegalAccessException {
        Integer userId = 1;
        User user = new User("test", "test", null, null, null, null, null);
        FieldSetter.setUserId(user, userId);

        Mockito.when(userRepository.findById(userId)).thenReturn(Optional.of(user));

        Optional<UserDTO> userDTOOptional = userService.findById(userId);

        if (userDTOOptional.isPresent()) {
            UserDTO userDTO = userDTOOptional.get();
            AssertUsersUnitUtil.assertUsers(user, userDTO);
        } else {
            throw new AssertionFailedError();
        }
        Mockito.verify(userRepository, Mockito.times(1)).findById(userId);
    }

    @Test
    void findByIdNotFound() {
        Integer userId = 1;

        Mockito.when(userRepository.findById(userId)).thenReturn(Optional.empty());

        Optional<UserDTO> userDTOOptional = userService.findById(userId);

        if (userDTOOptional.isPresent()) {
            throw new AssertionFailedError();
        } else {
            Mockito.verify(userRepository, Mockito.times(1)).findById(userId);
        }
    }

    @Test
    void findByEmail() throws NoSuchFieldException, IllegalAccessException {
        Integer userId = 1;
        User user = new User("test", "test", null, null, null, null, null);
        FieldSetter.setUserId(user, userId);

        Mockito.when(userRepository.findByEmail(user.getEmail())).thenReturn(Optional.of(user));

        Optional<UserDTO> userDTOOptional = userService.findByEmail(user.getEmail());

        if (userDTOOptional.isPresent()) {
            UserDTO userDTO = userDTOOptional.get();
            AssertUsersUnitUtil.assertUsers(user, userDTO);
        } else {
            throw new AssertionFailedError();
        }
        Mockito.verify(userRepository, Mockito.times(1)).findByEmail(user.getEmail());
    }

    @Test
    void findByEmailNotFound() {
        String userEmail = "test";

        Mockito.when(userRepository.findByEmail(userEmail)).thenReturn(Optional.empty());

        Optional<UserDTO> userDTOOptional = userService.findByEmail(userEmail);

        if (userDTOOptional.isPresent()) {
            throw new AssertionFailedError();
        } else {
            Mockito.verify(userRepository, Mockito.times(1)).findByEmail(userEmail);
        }
    }

    @Test
    void toDTO() throws NoSuchFieldException, IllegalAccessException {
        Integer userId = 1;
        User user = new User("test", "test", null, null, null, null, null);
        FieldSetter.setUserId(user, userId);

        UserDTO userDTO = userService.toDTO(user);

        AssertUsersUnitUtil.assertUsers(user, userDTO);
    }

    @Test
    void toDTOOptional() throws NoSuchFieldException, IllegalAccessException {
        Integer userId = 1;
        User user = new User("test", "test", null, null, null, null, null);
        FieldSetter.setUserId(user, userId);

        Optional<UserDTO> userDTO = userService.toDTO(Optional.of(user));

        if (userDTO.isEmpty()) {
            throw new AssertionFailedError();
        }

        AssertUsersUnitUtil.assertUsers(user, userDTO.get());
    }

    @Test
    void deleteById() throws NoSuchFieldException, IllegalAccessException {
        Integer userId = 1;
        User user = new User("test", "test", null, null, null, null, null);
        FieldSetter.setUserId(user, userId);

        Mockito.when(userRepository.findById(userId)).thenReturn(Optional.of(user));
        try {
            userService.deleteById(userId);
        } catch (NotFoundException e) {
            throw new AssertionFailedError();
        }

        Mockito.when(userRepository.findById(userId)).thenReturn(Optional.empty());
        try {
            userService.deleteById(userId);
            throw new AssertionFailedError();
        } catch (NotFoundException ignored) {
        }

        Mockito.verify(userRepository, Mockito.times(2)).findById(userId);
        Mockito.verify(userChildrenDelete, Mockito.times(1)).deleteChildren(user);
        Mockito.verify(userRepository, Mockito.times(1)).deleteById(userId);
    }

    @Test
    void findAllUsersWhoLikedPost() throws NoSuchFieldException, IllegalAccessException {
        Integer postId = 1;

        List<User> users = new ArrayList<>();
        int numberOfItems = 5;
        for (int i = 1; i <= numberOfItems; i++) {
            Integer userId = i;
            User user = new User("test" + i, "test" + i, null, null, null, null, null);
            FieldSetter.setUserId(user, userId);

            users.add(user);
        }

        Mockito.when(userRepository.findAllUsersWhoLikedPost(postId)).thenReturn(users);
        Mockito.when(postRepository.existsById(postId)).thenReturn(true);

        List<UserDTO> usersDTO;
        try {
            usersDTO = userService.findAllUsersWhoLikedPost(postId);
        } catch (NotFoundException e) {
            throw new AssertionFailedError();
        }

        Assertions.assertEquals(users.size(), usersDTO.size());
        for (int i = 0; i < usersDTO.size(); i++) {
            AssertUsersUnitUtil.assertUsers(users.get(i), usersDTO.get(i));
        }
        Mockito.verify(userRepository, Mockito.times(1)).findAllUsersWhoLikedPost(postId);
        Mockito.verify(postRepository, Mockito.times(1)).existsById(postId);
    }

    @Test
    void findAllUsersWhoLikedPostNotFound() {
        Integer postId = 1;

        List<User> users = new ArrayList<>();

        Mockito.when(userRepository.findAllUsersWhoLikedPost(postId)).thenReturn(users);
        Mockito.when(postRepository.existsById(postId)).thenReturn(false);

        try {
            userService.findAllUsersWhoLikedPost(postId);
            throw new AssertionFailedError();
        } catch (NotFoundException e) {
            Mockito.verify(userRepository, Mockito.times(0)).findAllUsersWhoLikedPost(postId);
            Mockito.verify(postRepository, Mockito.times(1)).existsById(postId);

        }
    }

    @Test
    void findAllFollowers() throws NoSuchFieldException, IllegalAccessException {
        Integer userId = 1;

        List<User> users = new ArrayList<>();
        int numberOfItems = 5;
        for (int i = 1; i <= numberOfItems; i++) {
            User user = new User("test" + i, "test" + i, null, null, null, null, null);
            FieldSetter.setUserId(user, i);

            users.add(user);
        }

        Mockito.when(userRepository.findAllFollowers(userId)).thenReturn(users);
        Mockito.when(userRepository.existsById(userId)).thenReturn(true);

        List<UserDTO> usersDTO;
        try {
            usersDTO = userService.findAllFollowers(userId);
        } catch (NotFoundException e) {
            throw new AssertionFailedError();
        }

        Assertions.assertEquals(users.size(), usersDTO.size());
        for (int i = 0; i < usersDTO.size(); i++) {
            AssertUsersUnitUtil.assertUsers(users.get(i), usersDTO.get(i));
        }
        Mockito.verify(userRepository, Mockito.times(1)).findAllFollowers(userId);
        Mockito.verify(userRepository, Mockito.times(1)).existsById(userId);
    }

    @Test
    void findAllFollowersNotFound() {
        Integer userId = 1;

        List<User> users = new ArrayList<>();

        Mockito.when(userRepository.findAllFollowers(userId)).thenReturn(users);
        Mockito.when(userRepository.existsById(userId)).thenReturn(false);

        try {
            userService.findAllFollowers(userId);
            throw new AssertionFailedError();
        } catch (NotFoundException e) {
            Mockito.verify(userRepository, Mockito.times(0)).findAllFollowers(userId);
            Mockito.verify(userRepository, Mockito.times(1)).existsById(userId);
        }
    }

    @Test
    void findCountOfAllFollowers() {
        Integer userId = 1;

        Long numberOfItems = 5L;

        Mockito.when(userRepository.findCountOfAllFollowers(userId)).thenReturn(numberOfItems);
        Mockito.when(userRepository.existsById(userId)).thenReturn(true);

        Long resultNumberOfItems;
        try {
            resultNumberOfItems = userService.findCountOfAllFollowers(userId);
        } catch (NotFoundException e) {
            throw new AssertionFailedError();
        }
        Assertions.assertEquals(numberOfItems, resultNumberOfItems);

        Mockito.verify(userRepository, Mockito.times(1)).findCountOfAllFollowers(userId);
        Mockito.verify(userRepository, Mockito.times(1)).existsById(userId);
    }

    @Test
    void findCountOfAllFollowersNotFound() {
        Integer userId = 1;

        Long numberOfItems = 5L;

        Mockito.when(userRepository.findCountOfAllFollowers(userId)).thenReturn(numberOfItems);
        Mockito.when(userRepository.existsById(userId)).thenReturn(false);

        try {
            userService.findCountOfAllFollowers(userId);
            throw new AssertionFailedError();
        } catch (NotFoundException e) {
            Mockito.verify(userRepository, Mockito.times(0)).findCountOfAllFollowers(userId);
            Mockito.verify(userRepository, Mockito.times(1)).existsById(userId);
        }
    }

    @Test
    void findAllFollowing() throws NoSuchFieldException, IllegalAccessException {
        Integer userId = 1;

        List<User> users = new ArrayList<>();
        int numberOfItems = 5;
        for (int i = 1; i <= numberOfItems; i++) {
            User user = new User("test" + i, "test" + i, null, null, null, null, null);
            FieldSetter.setUserId(user, i);

            users.add(user);
        }

        Mockito.when(userRepository.findAllFollowing(userId)).thenReturn(users);
        Mockito.when(userRepository.existsById(userId)).thenReturn(true);

        List<UserDTO> usersDTO;
        try {
            usersDTO = userService.findAllFollowing(userId);
        } catch (NotFoundException e) {
            throw new AssertionFailedError();
        }

        Assertions.assertEquals(users.size(), usersDTO.size());
        for (int i = 0; i < usersDTO.size(); i++) {
            AssertUsersUnitUtil.assertUsers(users.get(i), usersDTO.get(i));
        }
        Mockito.verify(userRepository, Mockito.times(1)).findAllFollowing(userId);
        Mockito.verify(userRepository, Mockito.times(1)).existsById(userId);
    }

    @Test
    void findAllFollowingNotFound() {
        Integer userId = 1;

        List<User> users = new ArrayList<>();

        Mockito.when(userRepository.findAllFollowing(userId)).thenReturn(users);
        Mockito.when(userRepository.existsById(userId)).thenReturn(false);

        try {
            userService.findAllFollowing(userId);
            throw new AssertionFailedError();
        } catch (NotFoundException e) {
            Mockito.verify(userRepository, Mockito.times(0)).findAllFollowing(userId);
            Mockito.verify(userRepository, Mockito.times(1)).existsById(userId);
        }
    }

    @Test
    void findCountOfAllFollowing() {
        Integer userId = 1;

        Long numberOfItems = 5L;

        Mockito.when(userRepository.findCountOfAllFollowing(userId)).thenReturn(numberOfItems);
        Mockito.when(userRepository.existsById(userId)).thenReturn(true);

        Long resultNumberOfItems;
        try {
            resultNumberOfItems = userService.findCountOfAllFollowing(userId);
        } catch (NotFoundException e) {
            throw new AssertionFailedError();
        }
        Assertions.assertEquals(numberOfItems, resultNumberOfItems);

        Mockito.verify(userRepository, Mockito.times(1)).findCountOfAllFollowing(userId);
        Mockito.verify(userRepository, Mockito.times(1)).existsById(userId);
    }

    @Test
    void findCountOfAllFollowingNotFound() {
        Integer userId = 1;

        Long numberOfItems = 5L;

        Mockito.when(userRepository.findCountOfAllFollowing(userId)).thenReturn(numberOfItems);
        Mockito.when(userRepository.existsById(userId)).thenReturn(false);

        try {
            userService.findCountOfAllFollowing(userId);
            throw new AssertionFailedError();
        } catch (NotFoundException e) {
            Mockito.verify(userRepository, Mockito.times(0)).findCountOfAllFollowing(userId);
            Mockito.verify(userRepository, Mockito.times(1)).existsById(userId);
        }
    }
}
