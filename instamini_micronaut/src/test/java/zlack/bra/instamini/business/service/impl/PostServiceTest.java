package zlack.bra.instamini.business.service.impl;

import io.micronaut.http.MediaType;
import io.micronaut.validation.validator.Validator;
import junit.framework.AssertionFailedError;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import util.AssertPostsUnitUtil;
import util.fieldsetter.FieldSetter;
import util.image.MockFileUpload;
import zlack.bra.instamini.business.dto.PostCreateDTO;
import zlack.bra.instamini.business.dto.PostDTO;
import zlack.bra.instamini.business.exception.NotFoundException;
import zlack.bra.instamini.business.exception.UploadFailException;
import zlack.bra.instamini.business.service.ImageUploadService;
import zlack.bra.instamini.business.service.util.PostChildrenDelete;
import zlack.bra.instamini.business.service.util.UserChildrenDelete;
import zlack.bra.instamini.data.entity.Post;
import zlack.bra.instamini.data.entity.User;
import zlack.bra.instamini.data.repository.PostRepository;
import zlack.bra.instamini.data.repository.UserRepository;

import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@RunWith(MockitoJUnitRunner.class)
class PostServiceTest {

    private AutoCloseable autoCloseable;

    private PostService postService;
    private UserService userService;

    @Mock
    private ImageUploadService imageUploadService;

    @Mock
    private PostRepository postRepository;

    @Mock
    private UserRepository userRepository;

    @Mock
    private UserChildrenDelete userChildrenDelete;

    @Mock
    private PostChildrenDelete postChildrenDelete;

    @Mock
    private Validator validator;

    private final LocalDateTime localDateTime = LocalDateTime.of(LocalDate.of(2000, 12, 12), LocalTime.of(10, 10, 10));

    private final List<String> httpHosts = new ArrayList<>() {
        {
            add("test");
        }
    };
        
    @BeforeEach
    void setUp() {
        autoCloseable = MockitoAnnotations.openMocks(this);
        userService = new UserService(userRepository, postRepository, "test", httpHosts, userChildrenDelete);
        postService = new PostService(postRepository, userRepository, userService, imageUploadService, validator, postChildrenDelete);
    }

    @AfterEach
    void tearDown() throws Exception {
        autoCloseable.close();
    }

    @Test
    void editDescription() throws NoSuchFieldException, IllegalAccessException {
        Post postToEdit = new Post("old description", "photo-url", localDateTime,
                new User("user1", "user1", Collections.emptyList(), Collections.emptyList(), Collections.emptyList(), Collections.emptyList(), Collections.emptyList()));

        Integer id = 1;
        FieldSetter.setPostId(postToEdit, id);

        String newDescription = "new description";

        Mockito.when(postRepository.findById(id)).thenReturn(Optional.of(postToEdit));

        PostDTO editedPost;
        try {
            editedPost = postService.editDescription(id, newDescription);
        } catch (NotFoundException e) {
            throw new AssertionFailedError();
        }

        Assertions.assertNotNull(editedPost);
        AssertPostsUnitUtil.assertPostsInEditDescriptionTest(postToEdit, editedPost, userService, newDescription);

        Mockito.verify(postRepository, Mockito.times(1)).findById(id);
    }

    @Test
    void editDescriptionNotFound() {
        Integer id = 1;
        String newDescription = "new description";

        Mockito.when(postRepository.findById(id)).thenReturn(Optional.empty());

        try {
            postService.editDescription(id, newDescription);
            throw new AssertionFailedError();
        } catch (NotFoundException e) {
            Mockito.verify(postRepository, Mockito.times(1)).findById(id);
        }
    }

    @Test
    void create() throws IOException, NoSuchFieldException, IllegalAccessException {
        Integer userId = 1;
        MockFileUpload image = new MockFileUpload("image.jpg", MediaType.IMAGE_JPEG_TYPE);
        PostCreateDTO postCreateDTO = new PostCreateDTO("description", userId, image);

        String photoUrl = "random url";
        User user = new User("test", "test", null, null, null, null, null);
        FieldSetter.setUserId(user, userId);

        Integer postId = 2;
        Post post = new Post(postCreateDTO.getDescription(), photoUrl, localDateTime, user);
        FieldSetter.setPostId(post, postId);

        Mockito.when(postRepository.save(Mockito.any())).thenReturn(post);
        Mockito.when(imageUploadService.upload(postCreateDTO.getImage().getInputStream())).thenReturn(photoUrl);
        Mockito.when(userRepository.findById(userId)).thenReturn(Optional.of(user));

        PostDTO postDTO;
        try {
            postDTO = postService.create(postCreateDTO);
        } catch (NotFoundException | UploadFailException e) {
            throw new AssertionFailedError();
        }

        Assertions.assertNotNull(postDTO);
        AssertPostsUnitUtil.assertPosts(post, postDTO, userService);

        Mockito.verify(postRepository, Mockito.times(1)).save(Mockito.any());
        Mockito.verify(userRepository, Mockito.times(1)).findById(userId);
        Mockito.verify(imageUploadService, Mockito.times(1)).upload(postCreateDTO.getImage().getInputStream());
    }

    @Test
    void createPhotoIsNull() throws IOException, NoSuchFieldException, IllegalAccessException {
        Integer userId = 1;
        PostCreateDTO postCreateDTO = new PostCreateDTO("description", userId, null);

        String photoUrl = "random url";
        User user = new User("test", "test", null, null, null, null, null);
        FieldSetter.setUserId(user, userId);

        Integer postId = 2;
        Post post = new Post(postCreateDTO.getDescription(), photoUrl, localDateTime, user);
        FieldSetter.setPostId(post, postId);

        Mockito.when(postRepository.save(Mockito.any())).thenReturn(post);
        Mockito.when(userRepository.findById(userId)).thenReturn(Optional.of(user));

        try {
            postService.create(postCreateDTO);
            throw new AssertionFailedError();
        } catch (NotFoundException e) {
            throw new AssertionFailedError();
        } catch (UploadFailException e) {
            Mockito.verify(postRepository, Mockito.times(0)).save(Mockito.any());
            Mockito.verify(userRepository, Mockito.times(1)).findById(userId);
            Mockito.verify(imageUploadService, Mockito.times(0)).upload(Mockito.any());
        }
    }

    @Test
    void createUserNotFound() throws IOException, NoSuchFieldException, IllegalAccessException {
        Integer userId = 1;
        MockFileUpload image = new MockFileUpload("image.jpg", MediaType.IMAGE_JPEG_TYPE);
        PostCreateDTO postCreateDTO = new PostCreateDTO("description", userId, image);

        String photoUrl = "random url";
        User user = new User("test", "test", null, null, null, null, null);
        FieldSetter.setUserId(user, userId);

        Integer postId = 2;
        Post post = new Post(postCreateDTO.getDescription(), photoUrl, localDateTime, user);
        FieldSetter.setPostId(post, postId);

        Mockito.when(postRepository.save(Mockito.any())).thenReturn(post);
        Mockito.when(imageUploadService.upload(postCreateDTO.getImage().getInputStream())).thenReturn(photoUrl);
        Mockito.when(userRepository.findById(userId)).thenReturn(Optional.empty());

        try {
            postService.create(postCreateDTO);
            throw new AssertionFailedError();
        } catch (NotFoundException e) {
            Mockito.verify(postRepository, Mockito.times(0)).save(Mockito.any());
            Mockito.verify(userRepository, Mockito.times(1)).findById(userId);
            Mockito.verify(imageUploadService, Mockito.times(0)).upload(postCreateDTO.getImage().getInputStream());
        } catch (UploadFailException e) {
            throw new AssertionFailedError();
        }
    }

    @Test
    void createUploadIOException() throws IOException, NoSuchFieldException, IllegalAccessException {
        Integer userId = 1;
        MockFileUpload image = new MockFileUpload("image.jpg", MediaType.IMAGE_JPEG_TYPE);
        PostCreateDTO postCreateDTO = new PostCreateDTO("description", userId, image);

        String photoUrl = "random url";
        User user = new User("test", "test", null, null, null, null, null);
        FieldSetter.setUserId(user, userId);

        Integer postId = 2;
        Post post = new Post(postCreateDTO.getDescription(), photoUrl, localDateTime, user);
        FieldSetter.setPostId(post, postId);

        Mockito.when(postRepository.save(Mockito.any())).thenReturn(post);
        Mockito.when(imageUploadService.upload(postCreateDTO.getImage().getInputStream())).thenThrow(new IOException());
        Mockito.when(userRepository.findById(userId)).thenReturn(Optional.of(user));

        try {
            postService.create(postCreateDTO);
            throw new AssertionFailedError();
        } catch (NotFoundException e) {
            throw new AssertionFailedError();
        } catch (UploadFailException e) {
            Mockito.verify(postRepository, Mockito.times(0)).save(Mockito.any());
            Mockito.verify(userRepository, Mockito.times(1)).findById(userId);
            Mockito.verify(imageUploadService, Mockito.times(1)).upload(postCreateDTO.getImage().getInputStream());
        }
    }

    @Test
    void findAll() throws NoSuchFieldException, IllegalAccessException {
        List<Post> posts = new ArrayList<>();
        int numberOfItems = 3;
        for (int i = 1; i <= numberOfItems; i++) {
            User user = new User("1test" + i, "1test" + i, null, null, null, null, null);
            FieldSetter.setUserId(user, i);

            Post post = new Post("description-test" + i, "photo-url" + i, localDateTime, user);
            FieldSetter.setPostId(post, i);
            posts.add(post);
        }

        Mockito.when(postRepository.findAll()).thenReturn(posts);

        List<PostDTO> postsDTO = postService.findAll();

        Assertions.assertEquals(posts.size(), postsDTO.size());
        for (int i = 0; i < postsDTO.size(); i++) {
            AssertPostsUnitUtil.assertPosts(posts.get(i), postsDTO.get(i), userService);
        }
        Mockito.verify(postRepository, Mockito.times(1)).findAll();
    }

    @Test
    void findAllEmpty() {
        List<Post> posts = new ArrayList<>();

        Mockito.when(postRepository.findAll()).thenReturn(posts);

        List<PostDTO> postsDTO = postService.findAll();

        Assertions.assertEquals(posts.size(), postsDTO.size());

        Mockito.verify(postRepository, Mockito.times(1)).findAll();
    }

    @Test
    void findById() throws NoSuchFieldException, IllegalAccessException {
        Integer userId = 1;
        User user = new User("test", "test", null, null, null, null, null);
        FieldSetter.setUserId(user, userId);

        Integer postId = 1;
        Post post = new Post("description", "photo url", localDateTime, user);
        FieldSetter.setPostId(post, postId);

        Mockito.when(postRepository.findById(postId)).thenReturn(Optional.of(post));

        Optional<PostDTO> postDTOOptional = postService.findById(postId);

        if (postDTOOptional.isPresent()) {
            PostDTO postDTO = postDTOOptional.get();
            AssertPostsUnitUtil.assertPosts(post, postDTO, userService);
        } else {
            throw new AssertionFailedError();
        }
        Mockito.verify(postRepository, Mockito.times(1)).findById(postId);
    }

    @Test
    void findByIdNotFound() {
        Integer postId = 1;

        Mockito.when(postRepository.findById(postId)).thenReturn(Optional.empty());

        Optional<PostDTO> postDTOOptional = postService.findById(postId);

        if (postDTOOptional.isPresent()) {
            throw new AssertionFailedError();
        } else {
            Mockito.verify(postRepository, Mockito.times(1)).findById(postId);
        }
    }

    @Test
    void toDTO() throws NoSuchFieldException, IllegalAccessException {
        Integer userId = 1;
        User user = new User("test", "test", null, null, null, null, null);
        FieldSetter.setUserId(user, userId);

        Integer postId = 1;
        Post post = new Post("description", "photo url", localDateTime, user);
        FieldSetter.setPostId(post, postId);

        PostDTO postDTO = postService.toDTO(post);

        AssertPostsUnitUtil.assertPosts(post, postDTO, userService);
    }

    @Test
    void toDTOOptional() throws NoSuchFieldException, IllegalAccessException {
        Integer userId = 1;
        User user = new User("test", "test", null, null, null, null, null);
        FieldSetter.setUserId(user, userId);

        Integer postId = 1;
        Post post = new Post("description", "photo url", localDateTime, user);
        FieldSetter.setPostId(post, postId);

        Optional<PostDTO> postDTO = postService.toDTO(Optional.of(post));

        if (postDTO.isEmpty()) {
            throw new AssertionFailedError();
        }

        AssertPostsUnitUtil.assertPosts(post, postDTO.get(), userService);
    }

    @Test
    void deleteById() throws NoSuchFieldException, IllegalAccessException {
        Integer userId = 1;
        User user = new User("test", "test", null, null, null, null, null);
        FieldSetter.setUserId(user, userId);

        Integer postId = 1;
        Post post = new Post("description", "photo url", localDateTime, user);
        FieldSetter.setPostId(post, postId);

        Mockito.when(postRepository.findById(postId)).thenReturn(Optional.of(post));
        try {
            postService.deleteById(postId);
        } catch (NotFoundException e) {
            throw new AssertionFailedError();
        }

        Mockito.when(postRepository.findById(postId)).thenReturn(Optional.empty());
        try {
            postService.deleteById(postId);
            throw new AssertionFailedError();
        } catch (NotFoundException ignored) {
        }

        Mockito.verify(postChildrenDelete, Mockito.times(1)).deleteChildren(post);
        Mockito.verify(postRepository, Mockito.times(2)).findById(postId);
        Mockito.verify(postRepository, Mockito.times(1)).deleteById(postId);
    }

    @Test
    void findAllPostsOfUser() throws NoSuchFieldException, IllegalAccessException {
        Integer userId = 1;
        User user = new User("test", "test", null, null, null, null, null);
        FieldSetter.setUserId(user, userId);

        List<Post> posts = new ArrayList<>();
        int numberOfPosts = 5;
        for (int i = 1; i <= numberOfPosts; i++) {
            Integer postId = i;
            Post post = new Post("description", "photo url", localDateTime, user);
            FieldSetter.setPostId(post, postId);

            posts.add(post);
        }

        Mockito.when(postRepository.findAllPostsOfUser(userId)).thenReturn(posts);
        Mockito.when(userRepository.existsById(userId)).thenReturn(true);

        List<PostDTO> outputPosts;
        try {
            outputPosts = postService.findAllPostsOfUser(userId);
        } catch (NotFoundException e) {
            throw new AssertionFailedError();
        }

        Assertions.assertEquals(posts.size(), outputPosts.size());

        for (int i = 0; i < numberOfPosts; i++) {
            AssertPostsUnitUtil.assertPosts(posts.get(i), outputPosts.get(i), userService);
        }
        Mockito.verify(postRepository, Mockito.times(1)).findAllPostsOfUser(userId);
        Mockito.verify(userRepository, Mockito.times(1)).existsById(userId);
    }

    @Test
    void findAllPostsOfUserNotFound() {
        Integer userId = 1;
        List<Post> posts = new ArrayList<>();

        Mockito.when(postRepository.findAllPostsOfUser(userId)).thenReturn(posts);
        Mockito.when(userRepository.existsById(userId)).thenReturn(false);

        try {
            postService.findAllPostsOfUser(userId);
            throw new AssertionFailedError();
        } catch (NotFoundException e) {
            Mockito.verify(postRepository, Mockito.times(0)).findAllPostsOfUser(userId);
            Mockito.verify(userRepository, Mockito.times(1)).existsById(userId);
        }
    }

    @Test
    void findCountOfAllPostsOfUser() {
        Integer userId = 1;
        Long numberOfPosts = 5L;

        Mockito.when(postRepository.findCountOfAllPostsOfUser(userId)).thenReturn(numberOfPosts);
        Mockito.when(userRepository.existsById(userId)).thenReturn(true);

        Long numberOfPostsOutput;
        try {
            numberOfPostsOutput = postService.findCountOfAllPostsOfUser(userId);
        } catch (NotFoundException e) {
            throw new AssertionFailedError();
        }

        Assertions.assertEquals(numberOfPosts, numberOfPostsOutput);

        Mockito.verify(postRepository, Mockito.times(1)).findCountOfAllPostsOfUser(userId);
        Mockito.verify(userRepository, Mockito.times(1)).existsById(userId);
    }

    @Test
    void findCountOfAllPostsOfUserNotFound() {
        Integer userId = 1;

        Mockito.when(postRepository.findCountOfAllPostsOfUser(userId)).thenReturn(Long.valueOf(0));
        Mockito.when(userRepository.existsById(userId)).thenReturn(false);

        try {
            postService.findCountOfAllPostsOfUser(userId);
            throw new AssertionFailedError();
        } catch (NotFoundException e) {
            Mockito.verify(postRepository, Mockito.times(0)).findCountOfAllPostsOfUser(userId);
            Mockito.verify(userRepository, Mockito.times(1)).existsById(userId);
        }
    }
}
