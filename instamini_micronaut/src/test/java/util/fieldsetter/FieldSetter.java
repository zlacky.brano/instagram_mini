package util.fieldsetter;

import zlack.bra.instamini.data.entity.*;

import java.lang.reflect.Field;

public class FieldSetter {

    public static void setUserId(User user, Integer userId) throws NoSuchFieldException, IllegalAccessException {
        Field reader = User.class.getDeclaredField("id");
        reader.setAccessible(true);
        reader.set(user, userId);
    }

    public static void setCommentId(Comment comment, Integer commentId) throws NoSuchFieldException, IllegalAccessException {
        Field reader = Comment.class.getDeclaredField("id");
        reader.setAccessible(true);
        reader.set(comment, commentId);
    }

    public static void setLikeId(Like like, Integer likeId) throws NoSuchFieldException, IllegalAccessException {
        Field reader = Like.class.getDeclaredField("id");
        reader.setAccessible(true);
        reader.set(like, likeId);
    }

    public static void setFollowId(Follow follow, Integer followId) throws NoSuchFieldException, IllegalAccessException {
        Field reader = Follow.class.getDeclaredField("id");
        reader.setAccessible(true);
        reader.set(follow, followId);
    }

    public static void setPostId(Post post, Integer postId) throws NoSuchFieldException, IllegalAccessException {
        Field reader = Post.class.getDeclaredField("id");
        reader.setAccessible(true);
        reader.set(post, postId);
    }
}
