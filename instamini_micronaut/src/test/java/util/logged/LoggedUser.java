package util.logged;


import zlack.bra.instamini.business.dto.UserDTO;

import java.security.Principal;


public class LoggedUser {

    public static final UserDTO user = new UserDTO(666, "test", "test");

    public static Principal getPrincipal() {
        return user::getEmail;
    }
}
