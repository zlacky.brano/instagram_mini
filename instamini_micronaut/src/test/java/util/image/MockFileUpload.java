package util.image;

import com.sun.istack.Nullable;
import io.micronaut.http.MediaType;
import io.micronaut.http.multipart.CompletedFileUpload;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.Optional;

public class MockFileUpload implements CompletedFileUpload {

    private final String filename;

    private final MediaType mediaType;

    private final byte[] content;

    private final InputStream inputStream;

    public MockFileUpload(String filename, MediaType mediaType) {
        this(filename, mediaType, null);
    }

    public MockFileUpload(String filename, MediaType mediaType, @Nullable byte[] content) {
        this.filename = filename;
        this.mediaType = mediaType;
        this.content = (content != null ? content : new byte[0]);
        this.inputStream = new ByteArrayInputStream(this.content);
    }

    @Override
    public InputStream getInputStream() throws IOException {
        return inputStream;
    }

    @Override
    public byte[] getBytes() throws IOException {
        return content;
    }

    @Override
    public ByteBuffer getByteBuffer() throws IOException {
        return ByteBuffer.wrap(content);
    }

    @Override
    public Optional<MediaType> getContentType() {
        return Optional.of(mediaType);
    }

    @Override
    public String getName() {
        return filename;
    }

    @Override
    public String getFilename() {
        return filename;
    }

    @Override
    public long getSize() {
        return content.length;
    }

    @Override
    public long getDefinedSize() {
        return content.length;
    }

    @Override
    public boolean isComplete() {
        return true;
    }
}