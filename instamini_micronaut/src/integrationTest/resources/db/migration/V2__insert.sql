INSERT INTO User (email, username) VALUES ('john.smith@test.com', 'JohnyS');
INSERT INTO User (email, username) VALUES ('george.jones@test.com', '_Georgie_');
INSERT INTO User (email, username) VALUES ('jacob.brown@test.com', '_**JB**_');
INSERT INTO User (email, username) VALUES ('harry.davies@test.com', 'HarryDav');
INSERT INTO User (email, username) VALUES ('charlie.smith@test.com', ':Charlie:');
INSERT INTO User (email, username) VALUES ('thomas.wilson@test.com', 'WilT');
INSERT INTO User (email, username) VALUES ('joe.evans@test.com', 'JEvans');
INSERT INTO User (email, username) VALUES ('oscar.garcia@test.com', 'Oscar');
INSERT INTO User (email, username) VALUES ('william.miller@test.com', 'WillMill');
INSERT INTO User (email, username) VALUES ('olivia.johnson@test.com', 'Olivia Johnson');
INSERT INTO User (email, username) VALUES ('emily.murphy@test.com', 'Em');
INSERT INTO User (email, username) VALUES ('ava.byrne@test.com', 'Ava');

INSERT INTO Follow (user_id_who, user_id_whom) VALUES (1, 2);
INSERT INTO Follow (user_id_who, user_id_whom) VALUES (2, 1);
INSERT INTO Follow (user_id_who, user_id_whom) VALUES (6, 4);
INSERT INTO Follow (user_id_who, user_id_whom) VALUES (10, 1);

INSERT INTO Post (description, photo_url, time, user_id) VALUES ('Morbi quis tortor id nulla ultrices aliquet. Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui.', 'https://res.cloudinary.com/instamini/image/upload/v1630672586/husky-3380548_1280_rman9o.jpg', '2020-04-05 22:52:37', 6);
INSERT INTO Post (description, photo_url, time, user_id) VALUES ('Nullam orci pede, venenatis non, sodales sed, tincidunt eu, felis. Fusce posuere felis sed lacus. Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl. Nunc rhoncus dui vel sem. Sed sagittis.', 'https://res.cloudinary.com/instamini/image/upload/v1630672584/dolphin-203875_1280_n67crm.jpg', '2020-12-20 07:51:02', 10);
INSERT INTO Post (description, photo_url, time, user_id) VALUES ('In est risus, auctor sed, tristique in, tempus sit amet, sem. Felis sed interdum venenatis, turpis enim blandit mi, in porttitor pede justo eu massa. Donec dapibus. Duis at velit eu est congue elementum.', 'https://res.cloudinary.com/instamini/image/upload/v1630672583/pug-690566_1280_s4wmbu.jpg', '2021-05-25 12:12:22', 2);
INSERT INTO Post (description, photo_url, time, user_id) VALUES ('Nulla ac enim. In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem.', 'https://res.cloudinary.com/instamini/image/upload/v1630672586/husky-3380548_1280_rman9o.jpg', '2021-02-06 16:15:07', 10);

INSERT INTO Comment (text, time, post_id, user_id) VALUES ('In hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus.', '2020-05-24 08:23:35', 1, 6);
INSERT INTO Comment (text, time, post_id, user_id) VALUES ('Pellentesque ultrices mattis odio.', '2021-01-23 15:33:49', 1, 10);
INSERT INTO Comment (text, time, post_id, user_id) VALUES ('Vestibulum quam sapien, varius ut, blandit non, interdum in, ante. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Duis faucibus accumsan odio. Curabitur convallis.', '2020-09-09 07:06:13', 2, 8);
INSERT INTO Comment (text, time, post_id, user_id) VALUES ('Aenean sit amet justo. Morbi ut odio. Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim. Lorem ipsum dolor sit amet, consectetuer adipiscing elit.', '2021-01-11 11:00:57', 4, 10);

INSERT INTO "Like" (user_id, post_id) VALUES (10, 3);
INSERT INTO "Like" (user_id, post_id) VALUES (6, 3);
INSERT INTO "Like" (user_id, post_id) VALUES (1, 2);
INSERT INTO "Like" (user_id, post_id) VALUES (5, 4);
