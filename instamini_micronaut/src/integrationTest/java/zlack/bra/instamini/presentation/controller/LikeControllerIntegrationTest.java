package zlack.bra.instamini.presentation.controller;

import com.radcortez.flyway.test.annotation.DataSource;
import com.radcortez.flyway.test.annotation.FlywayTest;
import io.micronaut.http.HttpRequest;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.HttpStatus;
import io.micronaut.http.MediaType;
import io.micronaut.http.client.HttpClient;
import io.micronaut.http.client.annotation.Client;
import io.micronaut.http.client.exceptions.HttpClientResponseException;
import io.micronaut.security.token.jwt.generator.JwtTokenGenerator;
import io.micronaut.test.extensions.junit5.annotation.MicronautTest;
import jakarta.inject.Inject;
import junit.framework.AssertionFailedError;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import util.AssertLikesIntegrationUtil;
import util.token.TestTokenGenerator;
import zlack.bra.instamini.business.dto.*;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;


@MicronautTest
@FlywayTest(@DataSource(url = "jdbc:h2:mem:mydb;DB_CLOSE_DELAY=-1;DATABASE_TO_UPPER=false;MODE=MySQL", username = "zlackbra", password = "password"))
class LikeControllerIntegrationTest {

    @Inject
    @Client("/likes")
    private HttpClient client;

    @Inject
    private JwtTokenGenerator jwtTokenGenerator;

    private String loggedUserToken;

    private final UserDTO loggedUserDTOExample = new UserDTO(10, "olivia.johnson@test.com", "Olivia Johnson");

    private final UserDTO notLoggedUserDTOExample = new UserDTO(6, "thomas.wilson@test.com", "WilT");

    private final List<LikeDTO> likesInDatabase = new ArrayList<>() {
        {
            add(new LikeDTO(1, loggedUserDTOExample,
                    new PostDTO(3, "In est risus, auctor sed, tristique in, tempus sit amet, sem. Felis sed interdum venenatis, turpis enim blandit mi, in porttitor pede justo eu massa. Donec dapibus. Duis at velit eu est congue elementum.",
                            "https://res.cloudinary.com/instamini/image/upload/v1630672583/pug-690566_1280_s4wmbu.jpg",
                            LocalDateTime.of(2021, 5, 25, 12, 12, 22),
                            new UserDTO(2, "george.jones@test.com", "_Georgie_"))));
            add(new LikeDTO(2, notLoggedUserDTOExample,
                    new PostDTO(3, "In est risus, auctor sed, tristique in, tempus sit amet, sem. Felis sed interdum venenatis, turpis enim blandit mi, in porttitor pede justo eu massa. Donec dapibus. Duis at velit eu est congue elementum.",
                            "https://res.cloudinary.com/instamini/image/upload/v1630672583/pug-690566_1280_s4wmbu.jpg",
                            LocalDateTime.of(2021, 5, 25, 12, 12, 22),
                            new UserDTO(2, "george.jones@test.com", "_Georgie_"))));
            add(new LikeDTO(3, new UserDTO(1, "john.smith@test.com", "JohnyS"),
                    new PostDTO(2, "Nullam orci pede, venenatis non, sodales sed, tincidunt eu, felis. Fusce posuere felis sed lacus. Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl. Nunc rhoncus dui vel sem. Sed sagittis.",
                            "https://res.cloudinary.com/instamini/image/upload/v1630672584/dolphin-203875_1280_n67crm.jpg",
                            LocalDateTime.of(2020, 12, 20, 7, 51, 2),
                            loggedUserDTOExample)));
            add(new LikeDTO(4, new UserDTO(5, "charlie.smith@test.com", ":Charlie:"),
                    new PostDTO(4, "Nulla ac enim. In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem.",
                            "https://res.cloudinary.com/instamini/image/upload/v1630672586/husky-3380548_1280_rman9o.jpg",
                            LocalDateTime.of(2021, 2, 6, 16, 15, 7),
                            loggedUserDTOExample)));
        }
    };


    @BeforeEach
    void setUp() {
        loggedUserToken = TestTokenGenerator.generateTestToken(loggedUserDTOExample, jwtTokenGenerator);
    }

    @Test
    void all401() {
        try {
            client.toBlocking().exchange(HttpRequest.GET(""));
            throw new AssertionFailedError();
        } catch (HttpClientResponseException e) {
            Assertions.assertEquals(e.getStatus(), HttpStatus.UNAUTHORIZED);
        }
    }

    @Test
    void all200() {
        HttpResponse<LikeDTO[]> result = client.toBlocking().exchange(HttpRequest.GET("").header("Authorization", "Bearer " + loggedUserToken), LikeDTO[].class);

        Assertions.assertEquals(result.status(), HttpStatus.OK);
        Assertions.assertTrue(result.getContentType().isPresent());
        Assertions.assertEquals(result.getContentType().get(), MediaType.APPLICATION_JSON_TYPE);
        Assertions.assertTrue(result.getBody().isPresent());

        LikeDTO[] resultBody = result.getBody().get();

        Assertions.assertEquals(resultBody.length, likesInDatabase.size());
        int i = 0;
        for (LikeDTO likeDTO : resultBody) {
            AssertLikesIntegrationUtil.assertLikes(likeDTO, likesInDatabase.get(i++));
        }
    }

    @Test
    void byID401() {
        LikeDTO likeDTOToFind = likesInDatabase.get(0);

        try {
            client.toBlocking().exchange(HttpRequest.GET("/" + likeDTOToFind.getId()));
            throw new AssertionFailedError();
        } catch (HttpClientResponseException e) {
            Assertions.assertEquals(e.getStatus(), HttpStatus.UNAUTHORIZED);
        }
    }

    @Test
    void byID404() {
        try {
            client.toBlocking().exchange(HttpRequest.GET("/" + 0)
                    .header("Authorization", "Bearer " + loggedUserToken));
            throw new AssertionFailedError();
        } catch (HttpClientResponseException e) {
            Assertions.assertEquals(e.getStatus(), HttpStatus.NOT_FOUND);
        }
    }

    @Test
    void byID200() {
        LikeDTO likeDTOToFind = likesInDatabase.get(0);

        HttpResponse<LikeDTO> result = client.toBlocking().exchange(HttpRequest.GET("/" + likeDTOToFind.getId())
                .header("Authorization", "Bearer " + loggedUserToken), LikeDTO.class);

        Assertions.assertEquals(result.status(), HttpStatus.OK);
        Assertions.assertTrue(result.getContentType().isPresent());
        Assertions.assertEquals(result.getContentType().get(), MediaType.APPLICATION_JSON_TYPE);
        Assertions.assertTrue(result.getBody().isPresent());

        AssertLikesIntegrationUtil.assertLikes(result.getBody().get(), likeDTOToFind);
    }

    @Test
    void create401() {
        LikeCreateDTO likeCreateDTO = new LikeCreateDTO(notLoggedUserDTOExample.getId(), likesInDatabase.get(2).getPost().getId());

        try {
            client.toBlocking().exchange(HttpRequest.POST("/", likeCreateDTO.toJson())
                    .contentType("application/json"));
            throw new AssertionFailedError();
        } catch (HttpClientResponseException e) {
            Assertions.assertEquals(e.getStatus(), HttpStatus.UNAUTHORIZED);
        }
    }

    @Test
    void create403() {
        LikeCreateDTO likeCreateDTO = new LikeCreateDTO(notLoggedUserDTOExample.getId(), likesInDatabase.get(2).getPost().getId());

        try {
            client.toBlocking().exchange(HttpRequest.POST("/", likeCreateDTO.toJson())
                    .contentType("application/json")
                    .header("Authorization", "Bearer " + loggedUserToken));
            throw new AssertionFailedError();
        } catch (HttpClientResponseException e) {
            Assertions.assertEquals(e.getStatus(), HttpStatus.FORBIDDEN);
        }
    }

    @Test
    void create404() {
        LikeCreateDTO likeCreateDTO = new LikeCreateDTO(loggedUserDTOExample.getId(), 0);

        try {
            client.toBlocking().exchange(HttpRequest.POST("/", likeCreateDTO.toJson())
                    .contentType("application/json")
                    .header("Authorization", "Bearer " + loggedUserToken));
            throw new AssertionFailedError();
        } catch (HttpClientResponseException e) {
            Assertions.assertEquals(e.getStatus(), HttpStatus.NOT_FOUND);
        }
    }

    @Test
    void create201() {
        LikeCreateDTO likeCreateDTO = new LikeCreateDTO(loggedUserDTOExample.getId(), likesInDatabase.get(2).getPost().getId());

        HttpResponse<LikeDTO> result = client.toBlocking().exchange(HttpRequest.POST("/", likeCreateDTO.toJson())
                .contentType("application/json")
                .header("Authorization", "Bearer " + loggedUserToken), LikeDTO.class);

        Assertions.assertEquals(result.status(), HttpStatus.CREATED);
        Assertions.assertTrue(result.getContentType().isPresent());
        Assertions.assertEquals(result.getContentType().get(), MediaType.APPLICATION_JSON_TYPE);
        Assertions.assertTrue(result.getBody().isPresent());

        AssertLikesIntegrationUtil.assertLikes(result.getBody().get(), likeCreateDTO);
    }

    @Test
    void create400Valid() {
        LikeCreateDTO likeCreateDTO = new LikeCreateDTO(loggedUserDTOExample.getId(), likesInDatabase.get(2).getPost().getId());

        try {
            client.toBlocking().exchange(HttpRequest.POST("/", "{\"userId\":\"" + likeCreateDTO.getUserId() + "\"}")
                    .contentType("application/json")
                    .header("Authorization", "Bearer " + loggedUserToken));
            throw new AssertionFailedError();
        } catch (HttpClientResponseException e) {
            Assertions.assertEquals(e.getStatus(), HttpStatus.BAD_REQUEST);
        }
    }

    @Test
    void create400Unique() {
        LikeCreateDTO likeCreateDTO = new LikeCreateDTO(loggedUserDTOExample.getId(), likesInDatabase.get(0).getPost().getId());

        try {
            client.toBlocking().exchange(HttpRequest.POST("/", likeCreateDTO.toJson())
                    .contentType("application/json")
                    .header("Authorization", "Bearer " + loggedUserToken));
            throw new AssertionFailedError();
        } catch (HttpClientResponseException e) {
            Assertions.assertEquals(e.getStatus(), HttpStatus.BAD_REQUEST);
        }
    }

    @Test
    void delete401() {
        try {
            client.toBlocking().exchange(HttpRequest.DELETE("/" + likesInDatabase.get(0).getId()));
            throw new AssertionFailedError();
        } catch (HttpClientResponseException e) {
            Assertions.assertEquals(e.getStatus(), HttpStatus.UNAUTHORIZED);
        }
    }

    @Test
    void delete403() {
        try {
            client.toBlocking().exchange(HttpRequest.DELETE("/" + likesInDatabase.get(3).getId())
                    .header("Authorization", "Bearer " + loggedUserToken));
            throw new AssertionFailedError();
        } catch (HttpClientResponseException e) {
            Assertions.assertEquals(e.getStatus(), HttpStatus.FORBIDDEN);
        }
    }

    @Test
    void delete404() {
        try {
            client.toBlocking().exchange(HttpRequest.DELETE("/" + 0)
                    .header("Authorization", "Bearer " + loggedUserToken));
            throw new AssertionFailedError();
        } catch (HttpClientResponseException e) {
            Assertions.assertEquals(e.getStatus(), HttpStatus.NOT_FOUND);
        }
    }

    @Test
    void delete200() {
        HttpResponse<Object> result = client.toBlocking().exchange(HttpRequest.DELETE("/" + likesInDatabase.get(0).getId())
                    .header("Authorization", "Bearer " + loggedUserToken));

        Assertions.assertEquals(result.getStatus(), HttpStatus.OK);
    }

    @Test
    void findNumberOfLikesOnPost401() {
        try {
            client.toBlocking().exchange(HttpRequest.GET("/count?post=" + 3));
            throw new AssertionFailedError();
        } catch (HttpClientResponseException e) {
            Assertions.assertEquals(e.getStatus(), HttpStatus.UNAUTHORIZED);
        }
    }

    @Test
    void findNumberOfLikesOnPost404() {
        try {
            client.toBlocking().exchange(HttpRequest.GET("/count?post=" + 0)
                    .header("Authorization", "Bearer " + loggedUserToken));
            throw new AssertionFailedError();
        } catch (HttpClientResponseException e) {
            Assertions.assertEquals(e.getStatus(), HttpStatus.NOT_FOUND);
        }
    }

    @Test
    void findNumberOfLikesOnPost200() {
        HttpResponse<Integer> result = client.toBlocking().exchange(HttpRequest.GET("/count?post=" + 3)
                .header("Authorization", "Bearer " + loggedUserToken), Integer.class);

        Assertions.assertEquals(result.status(), HttpStatus.OK);
        Assertions.assertTrue(result.getContentType().isPresent());
        Assertions.assertEquals(result.getContentType().get(), MediaType.APPLICATION_JSON_TYPE);
        Assertions.assertTrue(result.getBody().isPresent());

        // post with id 1 has 2 likes in database
        Assertions.assertEquals(result.getBody().get(), 2);
    }

    @Test
    void findLikesOnPost401() {
        try {
            client.toBlocking().exchange(HttpRequest.GET("/onPost?post=" + 3));
            throw new AssertionFailedError();
        } catch (HttpClientResponseException e) {
            Assertions.assertEquals(e.getStatus(), HttpStatus.UNAUTHORIZED);
        }
    }

    @Test
    void findLikesOnPost404() {
        try {
            client.toBlocking().exchange(HttpRequest.GET("/onPost?post=" + 0)
                    .header("Authorization", "Bearer " + loggedUserToken));
            throw new AssertionFailedError();
        } catch (HttpClientResponseException e) {
            Assertions.assertEquals(e.getStatus(), HttpStatus.NOT_FOUND);
        }
    }

    @Test
    void findLikesOnPost200() {
        HttpResponse<LikeDTO[]> result = client.toBlocking().exchange(HttpRequest.GET("/onPost?post=" + 3)
                .header("Authorization", "Bearer " + loggedUserToken), LikeDTO[].class);

        Assertions.assertEquals(result.status(), HttpStatus.OK);
        Assertions.assertTrue(result.getContentType().isPresent());
        Assertions.assertEquals(result.getContentType().get(), MediaType.APPLICATION_JSON_TYPE);
        Assertions.assertTrue(result.getBody().isPresent());

        LikeDTO[] resultBody = result.getBody().get();

        // post with id 1 has 2 likes in database
        AssertLikesIntegrationUtil.assertLikes(resultBody[0], likesInDatabase.get(0));
        AssertLikesIntegrationUtil.assertLikes(resultBody[1], likesInDatabase.get(1));
    }

    @Test
    void findLikeIdByUserAndPost200() {
        HttpResponse<Integer> result = client.toBlocking().exchange(HttpRequest.GET("/isLiked/" + likesInDatabase.get(1).getPost().getId())
                .header("Authorization", "Bearer " + loggedUserToken), Integer.class);

        Assertions.assertEquals(result.status(), HttpStatus.OK);
        Assertions.assertTrue(result.getContentType().isPresent());
        Assertions.assertEquals(result.getContentType().get(), MediaType.APPLICATION_JSON_TYPE);
        Assertions.assertTrue(result.getBody().isPresent());

        Assertions.assertEquals(result.getBody().get(), 1);
    }

    @Test
    void findLikeIdByUserAndPost401() {
        try {
            client.toBlocking().exchange(HttpRequest.GET("/isLiked/" + likesInDatabase.get(3).getPost().getId()));
            throw new AssertionFailedError();
        } catch (HttpClientResponseException e) {
            Assertions.assertEquals(e.getStatus(), HttpStatus.UNAUTHORIZED);
        }
    }

    @Test
    void findLikeIdByUserAndPost404() {
        try {
            client.toBlocking().exchange(HttpRequest.GET("/isLiked/" + 0)
                    .header("Authorization", "Bearer " + loggedUserToken));
            throw new AssertionFailedError();
        } catch (HttpClientResponseException e) {
            Assertions.assertEquals(e.getStatus(), HttpStatus.NOT_FOUND);
        }
    }
}