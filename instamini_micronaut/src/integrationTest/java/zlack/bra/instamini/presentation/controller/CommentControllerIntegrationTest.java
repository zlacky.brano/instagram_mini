package zlack.bra.instamini.presentation.controller;

import com.radcortez.flyway.test.annotation.DataSource;
import com.radcortez.flyway.test.annotation.FlywayTest;
import io.micronaut.http.HttpRequest;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.HttpStatus;
import io.micronaut.http.MediaType;
import io.micronaut.http.client.HttpClient;
import io.micronaut.http.client.annotation.Client;
import io.micronaut.http.client.exceptions.HttpClientResponseException;
import io.micronaut.security.authentication.Authentication;
import io.micronaut.security.token.jwt.generator.JwtTokenGenerator;
import io.micronaut.test.extensions.junit5.annotation.MicronautTest;
import jakarta.inject.Inject;
import junit.framework.AssertionFailedError;
import org.jetbrains.annotations.NotNull;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import util.AssertCommentsIntegrationUtil;
import util.token.TestTokenGenerator;
import zlack.bra.instamini.business.dto.*;
import zlack.bra.instamini.controller.request.EditTextCommentRequestBody;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@MicronautTest
@FlywayTest(@DataSource(url = "jdbc:h2:mem:mydb;DB_CLOSE_DELAY=-1;DATABASE_TO_UPPER=false;MODE=MySQL", username = "zlackbra", password = "password"))
class CommentControllerIntegrationTest {

    @Inject
    @Client("/comments")
    private HttpClient client;

    @Inject
    private JwtTokenGenerator jwtTokenGenerator;

    private String loggedUserToken;

    private final UserDTO loggedUserDTOExample = new UserDTO(10, "olivia.johnson@test.com", "Olivia Johnson");

    private final UserDTO notLoggedUserDTOExample = new UserDTO(6, "thomas.wilson@test.com", "WilT");

    private final List<CommentDTO> commentsInDatabase = new ArrayList<>() {
        {
            add(new CommentDTO(1, "In hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus.",
                    LocalDateTime.of(2020, 5, 24, 8, 23, 35),
                    notLoggedUserDTOExample,
                    new PostDTO(1, "Morbi quis tortor id nulla ultrices aliquet. Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui.",
                            "https://res.cloudinary.com/instamini/image/upload/v1630672586/husky-3380548_1280_rman9o.jpg",
                            LocalDateTime.of(2020, 4, 5, 22, 52, 37),
                            notLoggedUserDTOExample)));
            add(new CommentDTO(2, "Pellentesque ultrices mattis odio.",
                    LocalDateTime.of(2021, 1, 23, 15, 33, 49),
                    loggedUserDTOExample,
                    new PostDTO(1, "Morbi quis tortor id nulla ultrices aliquet. Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui.",
                            "https://res.cloudinary.com/instamini/image/upload/v1630672586/husky-3380548_1280_rman9o.jpg",
                            LocalDateTime.of(2020, 4, 5, 22, 52, 37),
                            notLoggedUserDTOExample)));
            add(new CommentDTO(3, "Vestibulum quam sapien, varius ut, blandit non, interdum in, ante. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Duis faucibus accumsan odio. Curabitur convallis.",
                    LocalDateTime.of(2020, 9, 9, 7, 6, 13),
                    new UserDTO(8, "oscar.garcia@test.com", "Oscar"),
                    new PostDTO(2, "Nullam orci pede, venenatis non, sodales sed, tincidunt eu, felis. Fusce posuere felis sed lacus. Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl. Nunc rhoncus dui vel sem. Sed sagittis.",
                            "https://res.cloudinary.com/instamini/image/upload/v1630672584/dolphin-203875_1280_n67crm.jpg",
                            LocalDateTime.of(2020, 12, 20, 7, 51, 2),
                            loggedUserDTOExample)));
            add(new CommentDTO(4, "Aenean sit amet justo. Morbi ut odio. Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim. Lorem ipsum dolor sit amet, consectetuer adipiscing elit.",
                    LocalDateTime.of(2021, 1, 11, 11, 0, 57),
                    loggedUserDTOExample,
                    new PostDTO(4, "Nulla ac enim. In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem.",
                            "https://res.cloudinary.com/instamini/image/upload/v1630672586/husky-3380548_1280_rman9o.jpg",
                            LocalDateTime.of(2021, 2, 6, 16, 15, 7),
                            loggedUserDTOExample)));
        }
    };

    @BeforeEach
    void setUp() {
        loggedUserToken = TestTokenGenerator.generateTestToken(loggedUserDTOExample, jwtTokenGenerator);
    }

    @Test
    void all401() {
        try {
            client.toBlocking().exchange(HttpRequest.GET(""));
            throw new AssertionFailedError();
        } catch (HttpClientResponseException e) {
            Assertions.assertEquals(e.getStatus(), HttpStatus.UNAUTHORIZED);
        }
    }

    @Test
    void all200() {
        HttpResponse<CommentDTO[]> result = client.toBlocking().exchange(HttpRequest.GET("").header("Authorization", "Bearer " + loggedUserToken), CommentDTO[].class);

        Assertions.assertEquals(result.status(), HttpStatus.OK);
        Assertions.assertTrue(result.getContentType().isPresent());
        Assertions.assertEquals(result.getContentType().get(), MediaType.APPLICATION_JSON_TYPE);
        Assertions.assertTrue(result.getBody().isPresent());

        CommentDTO[] resultBody = result.getBody().get();

        Assertions.assertEquals(resultBody.length, commentsInDatabase.size());
        int i = 0;
        for (CommentDTO commentDTO : resultBody) {
            AssertCommentsIntegrationUtil.assertComments(commentDTO, commentsInDatabase.get(i++));
        }
    }

    @Test
    void byID401() {
        CommentDTO commentDTOToFind = commentsInDatabase.get(0);

        try {
            client.toBlocking().exchange(HttpRequest.GET("/" + commentDTOToFind.getId()));
            throw new AssertionFailedError();
        } catch (HttpClientResponseException e) {
            Assertions.assertEquals(e.getStatus(), HttpStatus.UNAUTHORIZED);
        }
    }

    @Test
    void byID404() {
        try {
            client.toBlocking().exchange(HttpRequest.GET("/" + 0).header("Authorization", "Bearer " + loggedUserToken));
            throw new AssertionFailedError();
        } catch (HttpClientResponseException e) {
            Assertions.assertEquals(e.getStatus(), HttpStatus.NOT_FOUND);
        }
    }

    @Test
    void byID200() {
        CommentDTO commentDTOToFind = commentsInDatabase.get(0);

        HttpResponse<CommentDTO> result = client.toBlocking().exchange(HttpRequest.GET("/" + commentDTOToFind.getId())
                .header("Authorization", "Bearer " + loggedUserToken), CommentDTO.class);

        Assertions.assertEquals(result.status(), HttpStatus.OK);
        Assertions.assertTrue(result.getContentType().isPresent());
        Assertions.assertEquals(result.getContentType().get(), MediaType.APPLICATION_JSON_TYPE);
        Assertions.assertTrue(result.getBody().isPresent());

        AssertCommentsIntegrationUtil.assertComments(result.getBody().get(), commentDTOToFind);
    }

    @Test
    void findAllCommentsOnPost401() {
        try {
            client.toBlocking().exchange(HttpRequest.GET("/onPost?post=" + loggedUserDTOExample.getId()));
            throw new AssertionFailedError();
        } catch (HttpClientResponseException e) {
            Assertions.assertEquals(e.getStatus(), HttpStatus.UNAUTHORIZED);
        }
    }

    @Test
    void findAllCommentsOnPost404() {
        try {
            client.toBlocking().exchange(HttpRequest.GET("/onPost?post=" + 0).header("Authorization", "Bearer " + loggedUserToken));
            throw new AssertionFailedError();
        } catch (HttpClientResponseException e) {
            Assertions.assertEquals(e.getStatus(), HttpStatus.NOT_FOUND);
        }
    }

    @Test
    void findAllCommentsOnPost200() {
        HttpResponse<CommentDTO[]> result = client.toBlocking().exchange(HttpRequest.GET("/onPost?post=" + 1)
                .header("Authorization", "Bearer " + loggedUserToken), CommentDTO[].class);

        Assertions.assertEquals(result.status(), HttpStatus.OK);
        Assertions.assertTrue(result.getContentType().isPresent());
        Assertions.assertEquals(result.getContentType().get(), MediaType.APPLICATION_JSON_TYPE);
        Assertions.assertTrue(result.getBody().isPresent());

        CommentDTO[] resultBody = result.getBody().get();

        // post with id 1 has 2 comments in database
        Assertions.assertEquals(resultBody.length, 2);
        AssertCommentsIntegrationUtil.assertComments(resultBody[0], commentsInDatabase.get(1));
        AssertCommentsIntegrationUtil.assertComments(resultBody[1], commentsInDatabase.get(0));
    }

    @Test
    void create401() {
        CommentCreateDTO commentCreateDTO = new CommentCreateDTO("test", notLoggedUserDTOExample.getId(), commentsInDatabase.get(2).getPost().getId());

        try {
            client.toBlocking().exchange(HttpRequest.POST("", commentCreateDTO.toJson())
                    .contentType("application/json"));
            throw new AssertionFailedError();
        } catch (HttpClientResponseException e) {
            Assertions.assertEquals(e.getStatus(), HttpStatus.UNAUTHORIZED);
        }
    }

    @Test
    void create403() {
        CommentCreateDTO commentCreateDTO = new CommentCreateDTO("test", notLoggedUserDTOExample.getId(), commentsInDatabase.get(2).getPost().getId());

        try {
            client.toBlocking().exchange(HttpRequest.POST("", commentCreateDTO.toJson())
                    .header("Authorization", "Bearer " + loggedUserToken)
                    .contentType("application/json"));
            throw new AssertionFailedError();
        } catch (HttpClientResponseException e) {
            Assertions.assertEquals(e.getStatus(), HttpStatus.FORBIDDEN);
        }

    }

    @Test
    void create404() {
        CommentCreateDTO commentCreateDTO = new CommentCreateDTO("test", loggedUserDTOExample.getId(), 0);

        try {
            client.toBlocking().exchange(HttpRequest.POST("", commentCreateDTO.toJson())
                    .header("Authorization", "Bearer " + loggedUserToken)
                    .contentType("application/json"));
            throw new AssertionFailedError();
        } catch (HttpClientResponseException e) {
            Assertions.assertEquals(e.getStatus(), HttpStatus.NOT_FOUND);
        }
    }

    @Test
    void create201() {
        CommentCreateDTO commentCreateDTO = new CommentCreateDTO("test", loggedUserDTOExample.getId(), commentsInDatabase.get(2).getPost().getId());

        HttpResponse<CommentDTO> result = client.toBlocking().exchange(HttpRequest.POST("", commentCreateDTO.toJson())
                .header("Authorization", "Bearer " + loggedUserToken)
                .contentType("application/json"), CommentDTO.class);

        Assertions.assertEquals(result.status(), HttpStatus.CREATED);
        Assertions.assertTrue(result.getContentType().isPresent());
        Assertions.assertEquals(result.getContentType().get(), MediaType.APPLICATION_JSON_TYPE);
        Assertions.assertTrue(result.getBody().isPresent());

        AssertCommentsIntegrationUtil.assertComments(result.getBody().get(), commentCreateDTO);
    }

    @Test
    void create400Valid() {
        CommentCreateDTO commentCreateDTO = new CommentCreateDTO("test", loggedUserDTOExample.getId(), commentsInDatabase.get(2).getPost().getId());

        try {
            client.toBlocking().exchange(HttpRequest.POST("", "{\"text\":\"" + commentCreateDTO.getText() + "\"}") // missing body
                    .header("Authorization", "Bearer " + loggedUserToken)
                    .contentType("application/json"));
            throw new AssertionFailedError();
        } catch (HttpClientResponseException e) {
            Assertions.assertEquals(e.getStatus(), HttpStatus.BAD_REQUEST);
        }
    }


    @Test
    void editText401() {
        EditTextCommentRequestBody requestBody = new EditTextCommentRequestBody("new text");

        try {
            client.toBlocking().exchange(HttpRequest.PUT("/" + commentsInDatabase.get(0).getId() + "/editText", requestBody.toJson())
                    .contentType("application/json"));
            throw new AssertionFailedError();
        } catch (HttpClientResponseException e) {
            Assertions.assertEquals(e.getStatus(), HttpStatus.UNAUTHORIZED);
        }
    }

    @Test
    void editText403() {
        EditTextCommentRequestBody requestBody = new EditTextCommentRequestBody("new text");

        try {
            client.toBlocking().exchange(HttpRequest.PUT("/" + commentsInDatabase.get(0).getId() + "/editText", requestBody.toJson())
                    .contentType("application/json")
                    .header("Authorization", "Bearer " + loggedUserToken));
            throw new AssertionFailedError();
        } catch (HttpClientResponseException e) {
            Assertions.assertEquals(e.getStatus(), HttpStatus.FORBIDDEN);
        }
    }

    @Test
    void editText404() {
        EditTextCommentRequestBody requestBody = new EditTextCommentRequestBody("new text");

        try {
            client.toBlocking().exchange(HttpRequest.PUT("/" + 0 + "/editText", requestBody.toJson())
                    .contentType("application/json")
                    .header("Authorization", "Bearer " + loggedUserToken));
            throw new AssertionFailedError();
        } catch (HttpClientResponseException e) {
            Assertions.assertEquals(e.getStatus(), HttpStatus.NOT_FOUND);
        }
    }

    @Test
    void editText200() {
        EditTextCommentRequestBody requestBody = new EditTextCommentRequestBody("new text");

        HttpResponse<CommentDTO> result = client.toBlocking().exchange(HttpRequest.PUT("/" + commentsInDatabase.get(1).getId() + "/editText", requestBody.toJson())
                .contentType("application/json")
                .header("Authorization", "Bearer " + loggedUserToken), CommentDTO.class);

        Assertions.assertEquals(result.status(), HttpStatus.OK);
        Assertions.assertTrue(result.getContentType().isPresent());
        Assertions.assertEquals(result.getContentType().get(), MediaType.APPLICATION_JSON_TYPE);
        Assertions.assertTrue(result.getBody().isPresent());

        AssertCommentsIntegrationUtil.assertCommentsInEditTextTest(result.getBody().get(), commentsInDatabase.get(1),  requestBody.getNewText());
    }

    @Test
    void editText400Valid() {
        try {
            client.toBlocking().exchange(HttpRequest.PUT("/" + commentsInDatabase.get(1).getId() + "/editText", "{}")
                    .contentType("application/json")
                    .header("Authorization", "Bearer " + loggedUserToken));
            throw new AssertionFailedError();
        } catch (HttpClientResponseException e) {
            Assertions.assertEquals(e.getStatus(), HttpStatus.BAD_REQUEST);
        }
    }

    @Test
    void delete401() {
        try {
            client.toBlocking().exchange(HttpRequest.DELETE("/" + commentsInDatabase.get(0).getId()));
            throw new AssertionFailedError();
        } catch (HttpClientResponseException e) {
            Assertions.assertEquals(e.getStatus(), HttpStatus.UNAUTHORIZED);
        }
    }

    @Test
    void delete403() {
        try {
            client.toBlocking().exchange(HttpRequest.DELETE("/" + commentsInDatabase.get(0).getId())
                    .header("Authorization", "Bearer " + loggedUserToken));
            throw new AssertionFailedError();
        } catch (HttpClientResponseException e) {
            Assertions.assertEquals(e.getStatus(), HttpStatus.FORBIDDEN);
        }
    }

    @Test
    void delete404() {
        try {
            client.toBlocking().exchange(HttpRequest.DELETE("/" + 0)
                    .header("Authorization", "Bearer " + loggedUserToken));
            throw new AssertionFailedError();
        } catch (HttpClientResponseException e) {
            Assertions.assertEquals(e.getStatus(), HttpStatus.NOT_FOUND);
        }
    }

    @Test
    void delete200() {
        HttpResponse<Object> result = client.toBlocking().exchange(HttpRequest.DELETE("/" + commentsInDatabase.get(1).getId())
                    .header("Authorization", "Bearer " + loggedUserToken));

        Assertions.assertEquals(result.getStatus(), HttpStatus.OK);
    }
}

