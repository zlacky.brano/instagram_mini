package zlack.bra.instamini.presentation.controller;

import com.radcortez.flyway.test.annotation.DataSource;
import com.radcortez.flyway.test.annotation.FlywayTest;
import io.micronaut.http.HttpRequest;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.HttpStatus;
import io.micronaut.http.MediaType;
import io.micronaut.http.client.HttpClient;
import io.micronaut.http.client.annotation.Client;
import io.micronaut.http.client.exceptions.HttpClientResponseException;
import io.micronaut.security.token.jwt.generator.JwtTokenGenerator;
import io.micronaut.test.extensions.junit5.annotation.MicronautTest;
import jakarta.inject.Inject;
import junit.framework.AssertionFailedError;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import util.AssertFollowsIntegrationUtil;
import util.token.TestTokenGenerator;
import zlack.bra.instamini.business.dto.*;

import java.util.ArrayList;
import java.util.List;


@MicronautTest
@FlywayTest(@DataSource(url = "jdbc:h2:mem:mydb;DB_CLOSE_DELAY=-1;DATABASE_TO_UPPER=false;MODE=MySQL", username = "zlackbra", password = "password"))
class FollowControllerIntegrationTest {

    @Inject
    @Client("/follows")
    private HttpClient client;

    @Inject
    private JwtTokenGenerator jwtTokenGenerator;

    private String loggedUserToken;

    private final UserDTO loggedUserDTOExample = new UserDTO(10, "olivia.johnson@test.com", "Olivia Johnson");

    private final UserDTO notLoggedUserDTOExample = new UserDTO(6, "thomas.wilson@test.com", "WilT");

    private final List<FollowDTO> followsInDatabase = new ArrayList<>() {
        {
            add(new FollowDTO(1,
                    new UserDTO(1, "john.smith@test.com", "JohnyS"),
                    new UserDTO(2, "george.jones@test.com", "_Georgie_")));
            add(new FollowDTO(2,
                    new UserDTO(2, "george.jones@test.com", "_Georgie_"),
                    new UserDTO(1, "john.smith@test.com", "JohnyS")));
            add(new FollowDTO(3,
                    notLoggedUserDTOExample,
                    new UserDTO(4, "harry.davies@test.com", "HarryDav")));
            add(new FollowDTO(4,
                    loggedUserDTOExample,
                    new UserDTO(1, "john.smith@test.com", "JohnyS")));
        }
    };

    @BeforeEach
    void setUp() {
        loggedUserToken = TestTokenGenerator.generateTestToken(loggedUserDTOExample, jwtTokenGenerator);
    }

    @Test
    void all401() {
        try {
            client.toBlocking().exchange(HttpRequest.GET(""));
            throw new AssertionFailedError();
        } catch (HttpClientResponseException e) {
            Assertions.assertEquals(e.getStatus(), HttpStatus.UNAUTHORIZED);
        }
    }

    @Test
    void all200() {
        HttpResponse<FollowDTO[]> result = client.toBlocking().exchange(HttpRequest.GET("").header("Authorization", "Bearer " + loggedUserToken), FollowDTO[].class);

        Assertions.assertEquals(result.status(), HttpStatus.OK);
        Assertions.assertTrue(result.getContentType().isPresent());
        Assertions.assertEquals(result.getContentType().get(), MediaType.APPLICATION_JSON_TYPE);
        Assertions.assertTrue(result.getBody().isPresent());

        FollowDTO[] resultBody = result.getBody().get();

        Assertions.assertEquals(resultBody.length, followsInDatabase.size());
        int i = 0;
        for (FollowDTO followDTO : resultBody) {
            AssertFollowsIntegrationUtil.assertFollows(followDTO, followsInDatabase.get(i++));
        }
    }

    @Test
    void byID401() {
        FollowDTO followDTOToFind = followsInDatabase.get(0);

        try {
            client.toBlocking().exchange(HttpRequest.GET("/" + followDTOToFind.getId()));
            throw new AssertionFailedError();
        } catch (HttpClientResponseException e) {
            Assertions.assertEquals(e.getStatus(), HttpStatus.UNAUTHORIZED);
        }
    }

    @Test
    void byID404() {
        try {
            client.toBlocking().exchange(HttpRequest.GET("/" + 0)
                    .header("Authorization", "Bearer " + loggedUserToken));
            throw new AssertionFailedError();
        } catch (HttpClientResponseException e) {
            Assertions.assertEquals(e.getStatus(), HttpStatus.NOT_FOUND);
        }
    }

    @Test
    void byID200() {
        FollowDTO followDTOToFind = followsInDatabase.get(0);

        HttpResponse<FollowDTO> result = client.toBlocking().exchange(HttpRequest.GET("/" + followDTOToFind.getId())
                .header("Authorization", "Bearer " + loggedUserToken), FollowDTO.class);

        Assertions.assertEquals(result.status(), HttpStatus.OK);
        Assertions.assertTrue(result.getContentType().isPresent());
        Assertions.assertEquals(result.getContentType().get(), MediaType.APPLICATION_JSON_TYPE);
        Assertions.assertTrue(result.getBody().isPresent());

        AssertFollowsIntegrationUtil.assertFollows(result.getBody().get(), followDTOToFind);
    }

    @Test
    void create401() {
        FollowCreateDTO followCreateDTO = new FollowCreateDTO(loggedUserDTOExample.getId(), loggedUserDTOExample.getId());

        try {
            client.toBlocking().exchange(HttpRequest.POST("", followCreateDTO.toJson())
                    .contentType("application/json"));
            throw new AssertionFailedError();
        } catch (HttpClientResponseException e) {
            Assertions.assertEquals(e.getStatus(), HttpStatus.UNAUTHORIZED);
        }
    }

    @Test
    void create403() {
        FollowCreateDTO followCreateDTO = new FollowCreateDTO(notLoggedUserDTOExample.getId(), loggedUserDTOExample.getId());

        try {
            client.toBlocking().exchange(HttpRequest.POST("", followCreateDTO.toJson())
                    .contentType("application/json")
                    .header("Authorization", "Bearer " + loggedUserToken));
            throw new AssertionFailedError();
        } catch (HttpClientResponseException e) {
            Assertions.assertEquals(e.getStatus(), HttpStatus.FORBIDDEN);
        }
    }

    @Test
    void create404() {
        FollowCreateDTO followCreateDTO = new FollowCreateDTO(loggedUserDTOExample.getId(), 0);

        try {
            client.toBlocking().exchange(HttpRequest.POST("", followCreateDTO.toJson())
                    .contentType("application/json")
                    .header("Authorization", "Bearer " + loggedUserToken));
            throw new AssertionFailedError();
        } catch (HttpClientResponseException e) {
            Assertions.assertEquals(e.getStatus(), HttpStatus.NOT_FOUND);
        }
    }

    @Test
    void create201() {
        FollowCreateDTO followCreateDTO = new FollowCreateDTO(loggedUserDTOExample.getId(), notLoggedUserDTOExample.getId());

        HttpResponse<FollowDTO> result = client.toBlocking().exchange(HttpRequest.POST("", followCreateDTO.toJson())
                    .contentType("application/json")
                    .header("Authorization", "Bearer " + loggedUserToken), FollowDTO.class);

        Assertions.assertEquals(result.status(), HttpStatus.CREATED);
        Assertions.assertTrue(result.getContentType().isPresent());
        Assertions.assertEquals(result.getContentType().get(), MediaType.APPLICATION_JSON_TYPE);
        Assertions.assertTrue(result.getBody().isPresent());

        AssertFollowsIntegrationUtil.assertFollows(result.getBody().get(), followCreateDTO);
    }

    @Test
    void create400Valid() {
        FollowCreateDTO followCreateDTO = new FollowCreateDTO(loggedUserDTOExample.getId(), notLoggedUserDTOExample.getId());

        try {
            client.toBlocking().exchange(HttpRequest.POST("", "{\"userWhoId\":\"" + followCreateDTO.getUserWhoId() + "\"}") // missing body
                    .contentType("application/json")
                    .header("Authorization", "Bearer " + loggedUserToken));
            throw new AssertionFailedError();
        } catch (HttpClientResponseException e) {
            Assertions.assertEquals(e.getStatus(), HttpStatus.BAD_REQUEST);
        }
    }

    @Test
    void create400FollowYourself() {
        FollowCreateDTO followCreateDTO = new FollowCreateDTO(loggedUserDTOExample.getId(), loggedUserDTOExample.getId());

        try {
            client.toBlocking().exchange(HttpRequest.POST("", followCreateDTO.toJson())
                    .contentType("application/json")
                    .header("Authorization", "Bearer " + loggedUserToken));
            throw new AssertionFailedError();
        } catch (HttpClientResponseException e) {
            Assertions.assertEquals(e.getStatus(), HttpStatus.BAD_REQUEST);
        }
    }

    @Test
    void create400Unique() {
        FollowCreateDTO followCreateDTO = new FollowCreateDTO(loggedUserDTOExample.getId(), followsInDatabase.get(3).getUserWhom().getId());

        try {
            client.toBlocking().exchange(HttpRequest.POST("", followCreateDTO.toJson())
                    .contentType("application/json")
                    .header("Authorization", "Bearer " + loggedUserToken));
            throw new AssertionFailedError();
        } catch (HttpClientResponseException e) {
            Assertions.assertEquals(e.getStatus(), HttpStatus.BAD_REQUEST);
        }
    }

    @Test
    void delete401() {
        try {
            client.toBlocking().exchange(HttpRequest.DELETE("/" + followsInDatabase.get(0).getId()));
            throw new AssertionFailedError();
        } catch (HttpClientResponseException e) {
            Assertions.assertEquals(e.getStatus(), HttpStatus.UNAUTHORIZED);
        }
    }

    @Test
    void delete403() {
        try {
            client.toBlocking().exchange(HttpRequest.DELETE("/" + followsInDatabase.get(0).getId())
                    .header("Authorization", "Bearer " + loggedUserToken));
            throw new AssertionFailedError();
        } catch (HttpClientResponseException e) {
            Assertions.assertEquals(e.getStatus(), HttpStatus.FORBIDDEN);
        }
    }

    @Test
    void delete404() {
        try {
            client.toBlocking().exchange(HttpRequest.DELETE("/" + 0)
                    .header("Authorization", "Bearer " + loggedUserToken));
            throw new AssertionFailedError();
        } catch (HttpClientResponseException e) {
            Assertions.assertEquals(e.getStatus(), HttpStatus.NOT_FOUND);
        }
    }

    @Test
    void delete200() {
        HttpResponse<Object> result = client.toBlocking().exchange(HttpRequest.DELETE("/" + 4)
                .header("Authorization", "Bearer " + loggedUserToken));

        Assertions.assertEquals(result.getStatus(), HttpStatus.OK);
    }

    @Test
    void findFollowIdByUsers200() {
        HttpResponse<Integer> result = client.toBlocking().exchange(HttpRequest.GET("/isFollowed/" + followsInDatabase.get(3).getUserWhom().getId())
                .header("Authorization", "Bearer " + loggedUserToken), Integer.class);

        Assertions.assertEquals(result.status(), HttpStatus.OK);
        Assertions.assertTrue(result.getContentType().isPresent());
        Assertions.assertEquals(result.getContentType().get(), MediaType.APPLICATION_JSON_TYPE);
        Assertions.assertTrue(result.getBody().isPresent());

        Assertions.assertEquals(result.getBody().get(), 4);
    }

    @Test
    void findFollowIdByUsers401() {
        try {
            client.toBlocking().exchange(HttpRequest.GET("/isFollowed/" + followsInDatabase.get(3).getUserWhom().getId()));
            throw new AssertionFailedError();
        } catch (HttpClientResponseException e) {
            Assertions.assertEquals(e.getStatus(), HttpStatus.UNAUTHORIZED);
        }
    }

    @Test
    void findFollowIdByUsers404() {
        try {
            client.toBlocking().exchange(HttpRequest.GET("/isFollowed/" + 0)
                    .header("Authorization", "Bearer " + loggedUserToken));
            throw new AssertionFailedError();
        } catch (HttpClientResponseException e) {
            Assertions.assertEquals(e.getStatus(), HttpStatus.NOT_FOUND);
        }
    }
}