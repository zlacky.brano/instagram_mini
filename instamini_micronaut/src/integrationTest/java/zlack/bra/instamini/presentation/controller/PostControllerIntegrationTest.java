package zlack.bra.instamini.presentation.controller;

import com.radcortez.flyway.test.annotation.DataSource;
import com.radcortez.flyway.test.annotation.FlywayTest;
import io.micronaut.http.HttpRequest;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.HttpStatus;
import io.micronaut.http.MediaType;
import io.micronaut.http.client.HttpClient;
import io.micronaut.http.client.annotation.Client;
import io.micronaut.http.client.exceptions.HttpClientResponseException;
import io.micronaut.http.client.multipart.MultipartBody;
import io.micronaut.http.multipart.CompletedFileUpload;
import io.micronaut.security.token.jwt.generator.JwtTokenGenerator;
import io.micronaut.test.extensions.junit5.annotation.MicronautTest;
import jakarta.inject.Inject;
import junit.framework.AssertionFailedError;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import util.AssertPostsIntegrationUtil;
import util.token.TestTokenGenerator;
import zlack.bra.instamini.business.dto.PostDTO;
import zlack.bra.instamini.business.dto.PostCreateDTO;
import zlack.bra.instamini.business.dto.UserDTO;
import zlack.bra.instamini.business.service.ImageUploadService;
import zlack.bra.instamini.controller.request.EditPostDescriptionRequestBody;
import util.image.GetCompletedFileUpload;

import java.io.FileInputStream;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@MicronautTest
@FlywayTest(@DataSource(url = "jdbc:h2:mem:mydb;DB_CLOSE_DELAY=-1;DATABASE_TO_UPPER=false;MODE=MySQL", username = "zlackbra", password = "password"))
class PostControllerIntegrationTest {

    @Inject
    private ImageUploadService imageUploadService;

    @Inject
    @Client("/posts")
    private HttpClient client;

    @Inject
    private JwtTokenGenerator jwtTokenGenerator;

    private String loggedUserToken;
    
    private final UserDTO loggedUserDTOExample = new UserDTO(10, "olivia.johnson@test.com", "Olivia Johnson");

    private final UserDTO notLoggedUserDTOExample = new UserDTO(6, "thomas.wilson@test.com", "WilT");

    private final List<PostDTO> postsInDatabase = new ArrayList<>() {
        {
            add(new PostDTO(1, "Morbi quis tortor id nulla ultrices aliquet. Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui.",
                    "https://res.cloudinary.com/instamini/image/upload/v1630672586/husky-3380548_1280_rman9o.jpg",
                    LocalDateTime.of(2020, 4, 5, 22, 52, 37),
                    notLoggedUserDTOExample));
            add(new PostDTO(2, "Nullam orci pede, venenatis non, sodales sed, tincidunt eu, felis. Fusce posuere felis sed lacus. Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl. Nunc rhoncus dui vel sem. Sed sagittis.",
                    "https://res.cloudinary.com/instamini/image/upload/v1630672584/dolphin-203875_1280_n67crm.jpg",
                    LocalDateTime.of(2020, 12, 20, 7, 51, 2),
                    loggedUserDTOExample));
            add(new PostDTO(3, "In est risus, auctor sed, tristique in, tempus sit amet, sem. Felis sed interdum venenatis, turpis enim blandit mi, in porttitor pede justo eu massa. Donec dapibus. Duis at velit eu est congue elementum.",
                    "https://res.cloudinary.com/instamini/image/upload/v1630672583/pug-690566_1280_s4wmbu.jpg",
                    LocalDateTime.of(2021, 5, 25, 12, 12, 22),
                    new UserDTO(2, "george.jones@test.com", "_Georgie_")));
            add(new PostDTO(4, "Nulla ac enim. In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem.",
                    "https://res.cloudinary.com/instamini/image/upload/v1630672586/husky-3380548_1280_rman9o.jpg",
                    LocalDateTime.of(2021, 2, 6, 16, 15, 7),
                    loggedUserDTOExample));
        }
    };

    @BeforeEach
    void setUp() {
        loggedUserToken = TestTokenGenerator.generateTestToken(loggedUserDTOExample, jwtTokenGenerator);
    }

    @Test
    void all401() {
        try {
            client.toBlocking().exchange(HttpRequest.GET(""));
            throw new AssertionFailedError();
        } catch (HttpClientResponseException e) {
            Assertions.assertEquals(e.getStatus(), HttpStatus.UNAUTHORIZED);
        }
    }

    @Test
    void all200() {
        HttpResponse<PostDTO[]> result = client.toBlocking().exchange(HttpRequest.GET("").header("Authorization", "Bearer " + loggedUserToken), PostDTO[].class);

        Assertions.assertEquals(result.status(), HttpStatus.OK);
        Assertions.assertTrue(result.getContentType().isPresent());
        Assertions.assertEquals(result.getContentType().get(), MediaType.APPLICATION_JSON_TYPE);
        Assertions.assertTrue(result.getBody().isPresent());

        PostDTO[] resultBody = result.getBody().get();

        Assertions.assertEquals(resultBody.length, postsInDatabase.size());
        int i = 0;
        for (PostDTO postDTO : resultBody) {
            AssertPostsIntegrationUtil.assertPosts(postDTO, postsInDatabase.get(i++));
        }
    }

    @Test
    void byID401() {
        PostDTO postDTOToFind = postsInDatabase.get(0);

        try {
            client.toBlocking().exchange(HttpRequest.GET("/" + postDTOToFind.getId()));
            throw new AssertionFailedError();
        } catch (HttpClientResponseException e) {
            Assertions.assertEquals(e.getStatus(), HttpStatus.UNAUTHORIZED);
        }
    }

    @Test
    void byID404() {
        try {
            client.toBlocking().exchange(HttpRequest.GET("/" + 0)
                    .header("Authorization", "Bearer " + loggedUserToken));
            throw new AssertionFailedError();
        } catch (HttpClientResponseException e) {
            Assertions.assertEquals(e.getStatus(), HttpStatus.NOT_FOUND);
        }
    }

    @Test
    void byID200() {
        PostDTO postDTOToFind = postsInDatabase.get(0);

        HttpResponse<PostDTO> result = client.toBlocking().exchange(HttpRequest.GET("/" + postDTOToFind.getId())
                .header("Authorization", "Bearer " + loggedUserToken), PostDTO.class);

        Assertions.assertEquals(result.status(), HttpStatus.OK);
        Assertions.assertTrue(result.getContentType().isPresent());
        Assertions.assertEquals(result.getContentType().get(), MediaType.APPLICATION_JSON_TYPE);
        Assertions.assertTrue(result.getBody().isPresent());

        AssertPostsIntegrationUtil.assertPosts(result.getBody().get(), postDTOToFind);
    }

    @Test
    void create401() throws IOException {
        String path = "/src/integrationTest/resources/image-test/test.png";
        CompletedFileUpload image = GetCompletedFileUpload.get("image.png", MediaType.IMAGE_PNG_TYPE, new FileInputStream(System.getProperty("user.dir") + path).readAllBytes());
        PostCreateDTO postCreateDTO = new PostCreateDTO("test", notLoggedUserDTOExample.getId(), image);
        MultipartBody multipartBody = MultipartBody.builder()
                .addPart("userId", postCreateDTO.getUserId().toString())
                .addPart("description", postCreateDTO.getDescription())
                .addPart("image", postCreateDTO.getImage().getName(), postCreateDTO.getImage().getBytes())
                .build();

        try {
            client.toBlocking().exchange(HttpRequest.POST("/", multipartBody)
                    .contentType("multipart/form-data"));
            throw new AssertionFailedError();
        } catch (HttpClientResponseException e) {
            Assertions.assertEquals(e.getStatus(), HttpStatus.UNAUTHORIZED);
        }
    }

    @Test
    void create403() throws IOException {
        String path = "/src/integrationTest/resources/image-test/test.png";
        CompletedFileUpload image = GetCompletedFileUpload.get("image.png", MediaType.IMAGE_PNG_TYPE, new FileInputStream(System.getProperty("user.dir") + path).readAllBytes());
        PostCreateDTO postCreateDTO = new PostCreateDTO("test", notLoggedUserDTOExample.getId(), image);
        MultipartBody multipartBody = MultipartBody.builder()
                .addPart("userId", postCreateDTO.getUserId().toString())
                .addPart("description", postCreateDTO.getDescription())
                .addPart("image", postCreateDTO.getImage().getName(), postCreateDTO.getImage().getBytes())
                .build();

        try {
            client.toBlocking().exchange(HttpRequest.POST("/", multipartBody)
                    .header("Authorization", "Bearer " + loggedUserToken)
                    .contentType("multipart/form-data"));
            throw new AssertionFailedError();
        } catch (HttpClientResponseException e) {
            Assertions.assertEquals(e.getStatus(), HttpStatus.FORBIDDEN);
        }
    }

    @Test
    void create201() throws IOException {
        String path = "/src/integrationTest/resources/image-test/test.png";
        CompletedFileUpload image = GetCompletedFileUpload.get("image.png", MediaType.IMAGE_PNG_TYPE, new FileInputStream(System.getProperty("user.dir") + path).readAllBytes());
        PostCreateDTO postCreateDTO = new PostCreateDTO("test", loggedUserDTOExample.getId(), image);
        MultipartBody multipartBody = MultipartBody.builder()
                .addPart("userId", postCreateDTO.getUserId().toString())
                .addPart("description", postCreateDTO.getDescription())
                .addPart("image", postCreateDTO.getImage().getName(), postCreateDTO.getImage().getBytes())
                .build();

        HttpResponse<PostDTO> result = client.toBlocking().exchange(HttpRequest.POST("/", multipartBody)
                .header("Authorization", "Bearer " + loggedUserToken)
                .contentType("multipart/form-data"), PostDTO.class);

        Assertions.assertEquals(result.status(), HttpStatus.CREATED);
        Assertions.assertTrue(result.getContentType().isPresent());
        Assertions.assertEquals(result.getContentType().get(), MediaType.APPLICATION_JSON_TYPE);
        Assertions.assertTrue(result.getBody().isPresent());

        try {
            AssertPostsIntegrationUtil.assertPosts(result.getBody().get(), postCreateDTO);
        } finally {
            String photoUrl = result.getBody().get().getPhotoUrl();
            String publicId = photoUrl.substring(photoUrl.lastIndexOf("/") + 1, photoUrl.lastIndexOf("."));
            imageUploadService.destroy(publicId);
        }
    }

    @Test
    void create400() throws IOException {
        String path = "/src/integrationTest/resources/image-test/not_image.txt";
        CompletedFileUpload image = GetCompletedFileUpload.get("image.png", MediaType.IMAGE_PNG_TYPE, new FileInputStream(System.getProperty("user.dir") + path).readAllBytes());
        PostCreateDTO postCreateDTO = new PostCreateDTO("test", loggedUserDTOExample.getId(), image);
        MultipartBody multipartBody = MultipartBody.builder()
                .addPart("userId", postCreateDTO.getUserId().toString())
                .addPart("description", postCreateDTO.getDescription())
                .addPart("image", postCreateDTO.getImage().getName(), postCreateDTO.getImage().getBytes())
                .build();

        try {
            client.toBlocking().exchange(HttpRequest.POST("/", multipartBody)
                    .header("Authorization", "Bearer " + loggedUserToken)
                    .contentType("multipart/form-data"));
            throw new AssertionFailedError();
        } catch (HttpClientResponseException e) {
            Assertions.assertEquals(e.getStatus(), HttpStatus.BAD_REQUEST);
        }
    }

    @Test
    void create400Valid() throws IOException {
        String path = "/src/integrationTest/resources/image-test/test.png";
        CompletedFileUpload image = GetCompletedFileUpload.get("image.png", MediaType.IMAGE_PNG_TYPE, new FileInputStream(System.getProperty("user.dir") + path).readAllBytes());
        PostCreateDTO postCreateDTO = new PostCreateDTO("test", notLoggedUserDTOExample.getId(), image);
        MultipartBody multipartBody = MultipartBody.builder()
                .addPart("description", postCreateDTO.getDescription())
                .addPart("image", postCreateDTO.getImage().getName(), postCreateDTO.getImage().getBytes())
                .build();

        try {
            client.toBlocking().exchange(HttpRequest.POST("/", multipartBody)
                    .header("Authorization", "Bearer " + loggedUserToken)
                    .contentType("multipart/form-data"));
            throw new AssertionFailedError();
        } catch (HttpClientResponseException e) {
            Assertions.assertEquals(e.getStatus(), HttpStatus.BAD_REQUEST);
        }
    }

    @Test
    void editDescription401() {
        EditPostDescriptionRequestBody requestBody = new EditPostDescriptionRequestBody("new description");

        try {
            client.toBlocking().exchange(HttpRequest.PUT("/" + postsInDatabase.get(1).getId() + "/editDescription", requestBody.toJson())
                    .contentType("application/json"));
            throw new AssertionFailedError();
        } catch (HttpClientResponseException e) {
            Assertions.assertEquals(e.getStatus(), HttpStatus.UNAUTHORIZED);
        }
    }

    @Test
    void editDescription403() {
        EditPostDescriptionRequestBody requestBody = new EditPostDescriptionRequestBody("new description");

        try {
            client.toBlocking().exchange(HttpRequest.PUT("/" + postsInDatabase.get(0).getId() + "/editDescription", requestBody.toJson())
                    .contentType("application/json")
                    .header("Authorization", "Bearer " + loggedUserToken));
            throw new AssertionFailedError();
        } catch (HttpClientResponseException e) {
            Assertions.assertEquals(e.getStatus(), HttpStatus.FORBIDDEN);
        }
    }

    @Test
    void editDescription404() {
        EditPostDescriptionRequestBody requestBody = new EditPostDescriptionRequestBody("new description");

       try {
           client.toBlocking().exchange(HttpRequest.PUT("/" + 0 + "/editDescription", requestBody.toJson())
                   .contentType("application/json")
                   .header("Authorization", "Bearer " + loggedUserToken));
           throw new AssertionFailedError();
       } catch (HttpClientResponseException e) {
           Assertions.assertEquals(e.getStatus(), HttpStatus.NOT_FOUND);
       }
    }

    @Test
    void editDescription200() {
        EditPostDescriptionRequestBody requestBody = new EditPostDescriptionRequestBody("new description");

        HttpResponse<PostDTO> result = client.toBlocking().exchange(HttpRequest.PUT("/" + postsInDatabase.get(1).getId() + "/editDescription", requestBody.toJson())
                .contentType("application/json")
                .header("Authorization", "Bearer " + loggedUserToken), PostDTO.class);

        Assertions.assertEquals(result.status(), HttpStatus.OK);
        Assertions.assertTrue(result.getContentType().isPresent());
        Assertions.assertEquals(result.getContentType().get(), MediaType.APPLICATION_JSON_TYPE);
        Assertions.assertTrue(result.getBody().isPresent());

        AssertPostsIntegrationUtil.assertPostsInEditDescriptionTest(result.getBody().get(), postsInDatabase.get(1),  requestBody.getNewDescription());
    }

    @Test
    void editDescription400Valid() {
        try {
            client.toBlocking().exchange(HttpRequest.PUT("/" + postsInDatabase.get(1).getId() + "/editDescription", "{}")
                    .contentType("application/json")
                    .header("Authorization", "Bearer " + loggedUserToken));
            throw new AssertionFailedError();
        } catch (HttpClientResponseException e) {
            Assertions.assertEquals(e.getStatus(), HttpStatus.BAD_REQUEST);
        }
    }

    @Test
    void delete401() {
        try {
            client.toBlocking().exchange(HttpRequest.DELETE("/" + postsInDatabase.get(0).getId()));
            throw new AssertionFailedError();
        } catch (HttpClientResponseException e) {
            Assertions.assertEquals(e.getStatus(), HttpStatus.UNAUTHORIZED);
        }
    }

    @Test
    void delete403() {
        try {
            client.toBlocking().exchange(HttpRequest.DELETE("/" + postsInDatabase.get(0).getId())
                    .header("Authorization", "Bearer " + loggedUserToken));
            throw new AssertionFailedError();
        } catch (HttpClientResponseException e) {
            Assertions.assertEquals(e.getStatus(), HttpStatus.FORBIDDEN);
        }
    }

    @Test
    void delete404() {
        try {
            client.toBlocking().exchange(HttpRequest.DELETE("/" + 0)
                    .header("Authorization", "Bearer " + loggedUserToken));
            throw new AssertionFailedError();
        } catch (HttpClientResponseException e) {
            Assertions.assertEquals(e.getStatus(), HttpStatus.NOT_FOUND);
        }
    }

    @Test
    void delete200() {
        HttpResponse<Object> result = client.toBlocking().exchange(HttpRequest.DELETE("/" + postsInDatabase.get(1).getId())
                    .header("Authorization", "Bearer " + loggedUserToken));

        Assertions.assertEquals(result.getStatus(), HttpStatus.OK);
    }

    @Test
    void findAllPostsOfUser401() {
        try {
            client.toBlocking().exchange(HttpRequest.GET("/ofUser?user=" + loggedUserDTOExample.getId()));
            throw new AssertionFailedError();
        } catch (HttpClientResponseException e) {
            Assertions.assertEquals(e.getStatus(), HttpStatus.UNAUTHORIZED);
        }
    }

    @Test
    void findAllPostsOfUser404() {
        try {
            client.toBlocking().exchange(HttpRequest.GET("/ofUser?user=" + 0)
                    .header("Authorization", "Bearer " + loggedUserToken));
            throw new AssertionFailedError();
        } catch (HttpClientResponseException e) {
            Assertions.assertEquals(e.getStatus(), HttpStatus.NOT_FOUND);
        }
    }

    @Test
    void findAllPostsOfUser200() {
        HttpResponse<PostDTO[]> result = client.toBlocking().exchange(HttpRequest.GET("/ofUser?user=" + loggedUserDTOExample.getId())
                .header("Authorization", "Bearer " + loggedUserToken), PostDTO[].class);

        Assertions.assertEquals(result.status(), HttpStatus.OK);
        Assertions.assertTrue(result.getContentType().isPresent());
        Assertions.assertEquals(result.getContentType().get(), MediaType.APPLICATION_JSON_TYPE);
        Assertions.assertTrue(result.getBody().isPresent());

        PostDTO[] resultBody = result.getBody().get();

        // logged user has 2 posts in database
        Assertions.assertEquals(resultBody.length, 2);
        AssertPostsIntegrationUtil.assertPosts(resultBody[0], postsInDatabase.get(3));
        AssertPostsIntegrationUtil.assertPosts(resultBody[1], postsInDatabase.get(1));
    }

    @Test
    void findCountOfAllPostsOfUser401() {
        try {
            client.toBlocking().exchange(HttpRequest.GET("/count?user=" + loggedUserDTOExample.getId()));
            throw new AssertionFailedError();
        } catch (HttpClientResponseException e) {
            Assertions.assertEquals(e.getStatus(), HttpStatus.UNAUTHORIZED);
        }
    }

    @Test
    void findCountOfAllPostsOfUser404() {
        try {
            client.toBlocking().exchange(HttpRequest.GET("/count?user=" + 0)
                    .header("Authorization", "Bearer " + loggedUserToken));
            throw new AssertionFailedError();
        } catch (HttpClientResponseException e) {
            Assertions.assertEquals(e.getStatus(), HttpStatus.NOT_FOUND);
        }
    }

    @Test
    void findCountOfAllPostsOfUser200() {
        HttpResponse<Integer> result = client.toBlocking().exchange(HttpRequest.GET("/count?user=" + loggedUserDTOExample.getId())
                .header("Authorization", "Bearer " + loggedUserToken), Integer.class);

        Assertions.assertEquals(result.status(), HttpStatus.OK);
        Assertions.assertTrue(result.getContentType().isPresent());
        Assertions.assertEquals(result.getContentType().get(), MediaType.APPLICATION_JSON_TYPE);
        Assertions.assertTrue(result.getBody().isPresent());

        // logged user has 2 posts in database
        Assertions.assertEquals(result.getBody().get(), 2);
    }
}
