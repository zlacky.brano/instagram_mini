package zlack.bra.instamini.presentation.controller;

import com.radcortez.flyway.test.annotation.DataSource;
import com.radcortez.flyway.test.annotation.FlywayTest;
import io.micronaut.http.HttpRequest;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.HttpStatus;
import io.micronaut.http.MediaType;
import io.micronaut.http.client.HttpClient;
import io.micronaut.http.client.annotation.Client;
import io.micronaut.http.client.exceptions.HttpClientResponseException;
import io.micronaut.security.token.jwt.generator.JwtTokenGenerator;
import io.micronaut.test.extensions.junit5.annotation.MicronautTest;
import jakarta.inject.Inject;
import junit.framework.AssertionFailedError;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import util.AssertUsersIntegrationUtil;
import util.token.TestTokenGenerator;
import zlack.bra.instamini.business.dto.UserDTO;
import zlack.bra.instamini.controller.request.EditUsernameRequestBody;

import java.util.ArrayList;
import java.util.List;


@MicronautTest
@FlywayTest(@DataSource(url = "jdbc:h2:mem:mydb;DB_CLOSE_DELAY=-1;DATABASE_TO_UPPER=false;MODE=MySQL", username = "zlackbra", password = "password"))
class UserControllerIntegrationTest {

    @Inject
    @Client("/users")
    private HttpClient client;

    @Inject
    private JwtTokenGenerator jwtTokenGenerator;

    private String loggedUserToken;

    private final UserDTO loggedUserDTOExample = new UserDTO(10, "olivia.johnson@test.com", "Olivia Johnson");

    private final List<UserDTO> usersInDatabse = new ArrayList<>() {
        {
            add(new UserDTO(1, "john.smith@test.com", "JohnyS"));
            add(new UserDTO(2, "george.jones@test.com", "_Georgie_"));
            add(new UserDTO(3, "jacob.brown@test.com", "_**JB**_"));
            add(new UserDTO(4, "harry.davies@test.com", "HarryDav"));
            add(new UserDTO(5, "charlie.smith@test.com", ":Charlie:"));
            add(new UserDTO(6, "thomas.wilson@test.com", "WilT"));
            add(new UserDTO(7, "joe.evans@test.com", "JEvans"));
            add(new UserDTO(8, "oscar.garcia@test.com", "Oscar"));
            add(new UserDTO(9, "william.miller@test.com", "WillMill"));
            add(new UserDTO(10, "olivia.johnson@test.com", "Olivia Johnson"));
            add(new UserDTO(11, "emily.murphy@test.com", "Em"));
            add(new UserDTO(12, "ava.byrne@test.com", "Ava"));
        }
    };

    @BeforeEach
    void setUp() {
        loggedUserToken = TestTokenGenerator.generateTestToken(loggedUserDTOExample, jwtTokenGenerator);
    }

    @Test
    void all401() {
        try {
            client.toBlocking().exchange(HttpRequest.GET(""));
            throw new AssertionFailedError();
        } catch (HttpClientResponseException e) {
            Assertions.assertEquals(e.getStatus(), HttpStatus.UNAUTHORIZED);
        }
    }

    @Test
    void all200() {
        HttpResponse<UserDTO[]> result = client.toBlocking().exchange(HttpRequest.GET("").header("Authorization", "Bearer " + loggedUserToken), UserDTO[].class);

        Assertions.assertEquals(result.status(), HttpStatus.OK);
        Assertions.assertTrue(result.getContentType().isPresent());
        Assertions.assertEquals(result.getContentType().get(), MediaType.APPLICATION_JSON_TYPE);
        Assertions.assertTrue(result.getBody().isPresent());

        UserDTO[] resultBody = result.getBody().get();

        Assertions.assertEquals(resultBody.length, usersInDatabse.size());
        int i = 0;
        for (UserDTO userDTO : resultBody) {
            AssertUsersIntegrationUtil.assertUsers(userDTO, usersInDatabse.get(i++));
        }
    }

    @Test
    void byID401() {
        UserDTO postDTOToFind = usersInDatabse.get(0);

        try {
            client.toBlocking().exchange(HttpRequest.GET("/" + postDTOToFind.getId()));
            throw new AssertionFailedError();
        } catch (HttpClientResponseException e) {
            Assertions.assertEquals(e.getStatus(), HttpStatus.UNAUTHORIZED);
        }
    }

    @Test
    void byID404() {
        try {
            client.toBlocking().exchange(HttpRequest.GET("/" + 0)
                    .header("Authorization", "Bearer " + loggedUserToken));
            throw new AssertionFailedError();
        } catch (HttpClientResponseException e) {
            Assertions.assertEquals(e.getStatus(), HttpStatus.NOT_FOUND);
        }
    }

    @Test
    void byID200() {
        UserDTO postDTOToFind = usersInDatabse.get(0);

        HttpResponse<UserDTO> result = client.toBlocking().exchange(HttpRequest.GET("/" + postDTOToFind.getId())
                .header("Authorization", "Bearer " + loggedUserToken), UserDTO.class);

        Assertions.assertEquals(result.status(), HttpStatus.OK);
        Assertions.assertTrue(result.getContentType().isPresent());
        Assertions.assertEquals(result.getContentType().get(), MediaType.APPLICATION_JSON_TYPE);
        Assertions.assertTrue(result.getBody().isPresent());

        AssertUsersIntegrationUtil.assertUsers(result.getBody().get(), postDTOToFind);
    }

    @Test
    void byPrincipal401() {
        try {
            client.toBlocking().exchange(HttpRequest.GET("/logged"));
            throw new AssertionFailedError();
        } catch (HttpClientResponseException e) {
            Assertions.assertEquals(e.getStatus(), HttpStatus.UNAUTHORIZED);
        }
    }

    @Test
    void byPrincipal200() {
        HttpResponse<UserDTO> result = client.toBlocking().exchange(HttpRequest.GET("/logged")
                .header("Authorization", "Bearer " + loggedUserToken), UserDTO.class);

        Assertions.assertEquals(result.status(), HttpStatus.OK);
        Assertions.assertTrue(result.getContentType().isPresent());
        Assertions.assertEquals(result.getContentType().get(), MediaType.APPLICATION_JSON_TYPE);
        Assertions.assertTrue(result.getBody().isPresent());

        AssertUsersIntegrationUtil.assertUsers(result.getBody().get(), loggedUserDTOExample);
    }

    @Test
    void editUsername401() {
        EditUsernameRequestBody requestBody = new EditUsernameRequestBody("new username");

        try {
            client.toBlocking().exchange(HttpRequest.PUT("/" + usersInDatabse.get(1).getId() + "/editUsername", requestBody.toJson())
                    .contentType("application/json"));
            throw new AssertionFailedError();
        } catch (HttpClientResponseException e) {
            Assertions.assertEquals(e.getStatus(), HttpStatus.UNAUTHORIZED);
        }
    }

    @Test
    void editUsername403() {
        EditUsernameRequestBody requestBody = new EditUsernameRequestBody("new username");

        try {
            client.toBlocking().exchange(HttpRequest.PUT("/" + usersInDatabse.get(0).getId() + "/editUsername", requestBody.toJson())
                    .contentType("application/json")
                    .header("Authorization", "Bearer " + loggedUserToken));
            throw new AssertionFailedError();
        } catch (HttpClientResponseException e) {
            Assertions.assertEquals(e.getStatus(), HttpStatus.FORBIDDEN);
        }
    }

    @Test
    void editUsername200() {
        EditUsernameRequestBody requestBody = new EditUsernameRequestBody("new username");

        HttpResponse<UserDTO> result = client.toBlocking().exchange(HttpRequest.PUT("/" + usersInDatabse.get(9).getId() + "/editUsername", requestBody.toJson())
                .contentType("application/json")
                .header("Authorization", "Bearer " + loggedUserToken), UserDTO.class);

        Assertions.assertEquals(result.status(), HttpStatus.OK);
        Assertions.assertTrue(result.getContentType().isPresent());
        Assertions.assertEquals(result.getContentType().get(), MediaType.APPLICATION_JSON_TYPE);
        Assertions.assertTrue(result.getBody().isPresent());

        AssertUsersIntegrationUtil.assertUsersInEditUsernameTest(result.getBody().get(), usersInDatabse.get(9),  requestBody.getUsername());
    }

    @Test
    void editUsername400Valid() {
        try {
            client.toBlocking().exchange(HttpRequest.PUT("/" + usersInDatabse.get(0).getId() + "/editUsername", "{}")
                    .contentType("application/json")
                    .header("Authorization", "Bearer " + loggedUserToken));
            throw new AssertionFailedError();
        } catch (HttpClientResponseException e) {
            Assertions.assertEquals(e.getStatus(), HttpStatus.BAD_REQUEST);
        }
    }

    @Test
    void editUsername400Unique() {
        EditUsernameRequestBody requestBody = new EditUsernameRequestBody("JEvans");

        try {
            client.toBlocking().exchange(HttpRequest.PUT("/" + usersInDatabse.get(9).getId() + "/editUsername", requestBody.toJson())
                    .contentType("application/json")
                    .header("Authorization", "Bearer " + loggedUserToken));
            throw new AssertionFailedError();
        } catch (HttpClientResponseException e) {
            Assertions.assertEquals(e.getStatus(), HttpStatus.BAD_REQUEST);
        }
    }

    @Test
    void delete401() {
        try {
            client.toBlocking().exchange(HttpRequest.DELETE("/" + usersInDatabse.get(0).getId()));
            throw new AssertionFailedError();
        } catch (HttpClientResponseException e) {
            Assertions.assertEquals(e.getStatus(), HttpStatus.UNAUTHORIZED);
        }
    }

    @Test
    void delete403() {
        try {
            client.toBlocking().exchange(HttpRequest.DELETE("/" + usersInDatabse.get(0).getId())
                    .header("Authorization", "Bearer " + loggedUserToken));
            throw new AssertionFailedError();
        } catch (HttpClientResponseException e) {
            Assertions.assertEquals(e.getStatus(), HttpStatus.FORBIDDEN);
        }
    }

    @Test
    void delete200() {
        HttpResponse<Object> result = client.toBlocking().exchange(HttpRequest.DELETE("/" + usersInDatabse.get(9).getId())
                .header("Authorization", "Bearer " + loggedUserToken));

        Assertions.assertEquals(result.getStatus(), HttpStatus.OK);
    }

    @Test
    void findAllUsersWhoLikedPost401() {
        try {
            client.toBlocking().exchange(HttpRequest.DELETE("/likedPost?post=" + 9));
            throw new AssertionFailedError();
        } catch (HttpClientResponseException e) {
            Assertions.assertEquals(e.getStatus(), HttpStatus.UNAUTHORIZED);
        }
    }

    @Test
    void findAllUsersWhoLikedPost404() {
        try {
            client.toBlocking().exchange(HttpRequest.GET("/likedPost?post=" + 0)
                    .header("Authorization", "Bearer " + loggedUserToken));
            throw new AssertionFailedError();
        } catch (HttpClientResponseException e) {
            Assertions.assertEquals(e.getStatus(), HttpStatus.NOT_FOUND);
        }
    }

    @Test
    void findAllUsersWhoLikedPost200() {
        HttpResponse<UserDTO[]> result = client.toBlocking().exchange(HttpRequest.GET("/likedPost?post=" + 3)
                .header("Authorization", "Bearer " + loggedUserToken), UserDTO[].class);

        Assertions.assertEquals(result.status(), HttpStatus.OK);
        Assertions.assertTrue(result.getContentType().isPresent());
        Assertions.assertEquals(result.getContentType().get(), MediaType.APPLICATION_JSON_TYPE);
        Assertions.assertTrue(result.getBody().isPresent());

        UserDTO[] resultBody = result.getBody().get();

        Assertions.assertEquals(resultBody.length, 2);

        // post with id 3 has 2 user's likes in database
        AssertUsersIntegrationUtil.assertUsers(resultBody[0], usersInDatabse.get(5));
        AssertUsersIntegrationUtil.assertUsers(resultBody[1], usersInDatabse.get(9));
    }

    @Test
    void findAllFollowers401() {
        try {
            client.toBlocking().exchange(HttpRequest.DELETE("/followers?user=" + 1));
            throw new AssertionFailedError();
        } catch (HttpClientResponseException e) {
            Assertions.assertEquals(e.getStatus(), HttpStatus.UNAUTHORIZED);
        }
    }

    @Test
    void findAllFollowers404() {
        try {
            client.toBlocking().exchange(HttpRequest.GET("/followers?user=" + 0)
                    .header("Authorization", "Bearer " + loggedUserToken));
            throw new AssertionFailedError();
        } catch (HttpClientResponseException e) {
            Assertions.assertEquals(e.getStatus(), HttpStatus.NOT_FOUND);
        }
    }

    @Test
    void findAllFollowers200() {
        HttpResponse<UserDTO[]> result = client.toBlocking().exchange(HttpRequest.GET("/followers?user=" + 1)
                .header("Authorization", "Bearer " + loggedUserToken), UserDTO[].class);

        Assertions.assertEquals(result.status(), HttpStatus.OK);
        Assertions.assertTrue(result.getContentType().isPresent());
        Assertions.assertEquals(result.getContentType().get(), MediaType.APPLICATION_JSON_TYPE);
        Assertions.assertTrue(result.getBody().isPresent());

        UserDTO[] resultBody = result.getBody().get();

        Assertions.assertEquals(resultBody.length, 2);

        // user with id 2 has 2 followers in database
        AssertUsersIntegrationUtil.assertUsers(resultBody[0], usersInDatabse.get(1));
        AssertUsersIntegrationUtil.assertUsers(resultBody[1], usersInDatabse.get(9));
    }

    @Test
    void findCountOfAllFollowers401() {
        try {
            client.toBlocking().exchange(HttpRequest.GET("/followers/count?user=" + 1));
            throw new AssertionFailedError();
        } catch (HttpClientResponseException e) {
            Assertions.assertEquals(e.getStatus(), HttpStatus.UNAUTHORIZED);
        }
    }

    @Test
    void findCountOfAllFollowers404() {
        try {
            client.toBlocking().exchange(HttpRequest.GET("/followers/count?user=" + 0)
                    .header("Authorization", "Bearer " + loggedUserToken));
            throw new AssertionFailedError();
        } catch (HttpClientResponseException e) {
            Assertions.assertEquals(e.getStatus(), HttpStatus.NOT_FOUND);
        }
    }

    @Test
    void findCountOfAllFollowers200() {
        HttpResponse<Integer> result = client.toBlocking().exchange(HttpRequest.GET("/followers/count?user=" + 1)
                .header("Authorization", "Bearer " + loggedUserToken), Integer.class);

        Assertions.assertEquals(result.status(), HttpStatus.OK);
        Assertions.assertTrue(result.getContentType().isPresent());
        Assertions.assertEquals(result.getContentType().get(), MediaType.APPLICATION_JSON_TYPE);
        Assertions.assertTrue(result.getBody().isPresent());

        Integer resultBody = result.getBody().get();

        Assertions.assertEquals(resultBody, 2);
    }

    @Test
    void findAllFollowing401() {
        try {
            client.toBlocking().exchange(HttpRequest.GET("/following?user=" + 1));
            throw new AssertionFailedError();
        } catch (HttpClientResponseException e) {
            Assertions.assertEquals(e.getStatus(), HttpStatus.UNAUTHORIZED);
        }
    }

    @Test
    void findAllFollowing404() {
        try {
            client.toBlocking().exchange(HttpRequest.GET("/following?user=" + 0)
                    .header("Authorization", "Bearer " + loggedUserToken));
            throw new AssertionFailedError();
        } catch (HttpClientResponseException e) {
            Assertions.assertEquals(e.getStatus(), HttpStatus.NOT_FOUND);
        }
    }

    @Test
    void findAllFollowing200() {
        HttpResponse<UserDTO[]> result = client.toBlocking().exchange(HttpRequest.GET("/following?user=" + 2)
                .header("Authorization", "Bearer " + loggedUserToken), UserDTO[].class);

        Assertions.assertEquals(result.status(), HttpStatus.OK);
        Assertions.assertTrue(result.getContentType().isPresent());
        Assertions.assertEquals(result.getContentType().get(), MediaType.APPLICATION_JSON_TYPE);
        Assertions.assertTrue(result.getBody().isPresent());

        UserDTO[] resultBody = result.getBody().get();

        Assertions.assertEquals(resultBody.length, 1);

        // user with id 2 follows user in database
        AssertUsersIntegrationUtil.assertUsers(resultBody[0], usersInDatabse.get(0));
    }

    @Test
    void findCountOfAllFollowing401() {
        try {
            client.toBlocking().exchange(HttpRequest.GET("/following/count?user=" + 2));
            throw new AssertionFailedError();
        } catch (HttpClientResponseException e) {
            Assertions.assertEquals(e.getStatus(), HttpStatus.UNAUTHORIZED);
        }
    }

    @Test
    void findCountOfAllFollowing404() {
        try {
            client.toBlocking().exchange(HttpRequest.GET("/following/count?user=" + 0)
                    .header("Authorization", "Bearer " + loggedUserToken));
            throw new AssertionFailedError();
        } catch (HttpClientResponseException e) {
            Assertions.assertEquals(e.getStatus(), HttpStatus.NOT_FOUND);
        }
    }

    @Test
    void findCountOfAllFollowing200() {
        HttpResponse<Integer> result = client.toBlocking().exchange(HttpRequest.GET("/following/count?user=" + 2)
                .header("Authorization", "Bearer " + loggedUserToken), Integer.class);

        Assertions.assertEquals(result.status(), HttpStatus.OK);
        Assertions.assertTrue(result.getContentType().isPresent());
        Assertions.assertEquals(result.getContentType().get(), MediaType.APPLICATION_JSON_TYPE);
        Assertions.assertTrue(result.getBody().isPresent());

        Integer resultBody = result.getBody().get();

        Assertions.assertEquals(resultBody, 1);
    }
}

