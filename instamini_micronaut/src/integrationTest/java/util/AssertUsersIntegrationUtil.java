package util;

import org.junit.jupiter.api.Assertions;
import zlack.bra.instamini.business.dto.PostCreateDTO;
import zlack.bra.instamini.business.dto.PostDTO;
import zlack.bra.instamini.business.dto.UserCreateDTO;
import zlack.bra.instamini.business.dto.UserDTO;


public class AssertUsersIntegrationUtil {
    public static void assertUsers(UserDTO result, UserDTO user) {
        Assertions.assertEquals(user.getId(), result.getId());
        Assertions.assertEquals(user.getUsername(), result.getUsername());
        Assertions.assertEquals(user.getEmail(), result.getEmail());
    }

    public static void assertUsers(UserDTO result, UserCreateDTO user) {
        Assertions.assertNotNull(result.getId());
        Assertions.assertEquals(user.getUsername(), result.getUsername());
        Assertions.assertEquals(user.getEmail(), result.getEmail());
    }

    public static void assertUsersInEditUsernameTest(UserDTO result, UserDTO user, String newUsername) {
        Assertions.assertEquals(user.getId(), result.getId());
        Assertions.assertEquals(newUsername, result.getUsername());
        Assertions.assertEquals(user.getEmail(), result.getEmail());
    }
}
