package util;

import org.junit.jupiter.api.Assertions;
import zlack.bra.instamini.business.dto.*;


public class AssertFollowsIntegrationUtil {
    public static void assertFollows(FollowDTO result, FollowDTO follow) {
        Assertions.assertEquals(follow.getId(), result.getId());
        Assertions.assertEquals(follow.getUserWho(), result.getUserWho());
        Assertions.assertEquals(follow.getUserWhom(), result.getUserWhom());
    }

    public static void assertFollows(FollowDTO result, FollowCreateDTO follow) {
        Assertions.assertNotNull(result.getId());
        Assertions.assertEquals(follow.getUserWhoId(), result.getUserWho().getId());
        Assertions.assertEquals(follow.getUserWhomId(), result.getUserWhom().getId());
    }
}
