package util.token;

import io.micronaut.security.authentication.Authentication;
import io.micronaut.security.token.jwt.generator.JwtTokenGenerator;
import junit.framework.AssertionFailedError;
import org.jetbrains.annotations.NotNull;
import zlack.bra.instamini.business.dto.UserDTO;

import java.util.Map;
import java.util.Optional;

public class TestTokenGenerator {

    public static String generateTestToken(UserDTO loggedUser, JwtTokenGenerator jwtTokenGenerator) {
        Authentication authentication = new Authentication() {
            @Override
            public @NotNull Map<String, Object> getAttributes() {
                return Map.of();
            }

            @Override
            public String getName() {
                return loggedUser.getEmail();
            }
        };
        Optional<String> token = jwtTokenGenerator.generateToken(authentication, 100);
        if (token.isEmpty()) {
            throw new AssertionFailedError();
        }

        return token.get();
    }
}
