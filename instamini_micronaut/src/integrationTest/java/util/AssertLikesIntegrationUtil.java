package util;

import org.junit.jupiter.api.Assertions;
import zlack.bra.instamini.business.dto.*;

public class AssertLikesIntegrationUtil {
    public static void assertLikes(LikeDTO result, LikeDTO like) {
        Assertions.assertEquals(like.getId(), result.getId());
        Assertions.assertEquals(like.getUser(), result.getUser());
        Assertions.assertEquals(like.getPost(), result.getPost());
    }

    public static void assertLikes(LikeDTO result, LikeCreateDTO like) {
        Assertions.assertNotNull(result.getId());
        Assertions.assertEquals(like.getUserId(), result.getUser().getId());
        Assertions.assertEquals(like.getPostId(), result.getPost().getId());
    }
}
