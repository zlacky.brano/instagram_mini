package util.image;

import io.micronaut.http.MediaType;
import io.micronaut.http.multipart.CompletedFileUpload;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.Optional;

public class GetCompletedFileUpload {

    public static CompletedFileUpload get(String filename, MediaType mediaType, byte[] content) {
        InputStream inputStream = new ByteArrayInputStream(content);
        ;
        return new CompletedFileUpload() {
            @Override
            public InputStream getInputStream() throws IOException {
                return inputStream;
            }

            @Override
            public byte[] getBytes() throws IOException {
                return content;
            }

            @Override
            public ByteBuffer getByteBuffer() throws IOException {
                return ByteBuffer.wrap(content);
            }

            @Override
            public Optional<MediaType> getContentType() {
                return Optional.of(mediaType);
            }

            @Override
            public String getName() {
                return filename;
            }

            @Override
            public String getFilename() {
                return filename;
            }

            @Override
            public long getSize() {
                return content.length;
            }

            @Override
            public long getDefinedSize() {
                return content.length;
            }

            @Override
            public boolean isComplete() {
                return true;
            }
        };
    }
}