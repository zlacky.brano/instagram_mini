package util;

import org.junit.jupiter.api.Assertions;
import zlack.bra.instamini.business.dto.*;

public class AssertPostsIntegrationUtil {
    public static void assertPosts(PostDTO result, PostDTO post) {
        Assertions.assertEquals(post.getId(), result.getId());
        Assertions.assertEquals(post.getDescription(), result.getDescription());
        Assertions.assertEquals(post.getTime().toString(), result.getTime().toString());
        Assertions.assertEquals(post.getUser(), result.getUser());
        Assertions.assertEquals(post.getPhotoUrl(), result.getPhotoUrl());

    }

    public static void assertPosts(PostDTO result, PostCreateDTO post) {
        Assertions.assertNotNull(result.getId());
        Assertions.assertEquals(post.getDescription(), result.getDescription());
        Assertions.assertNotNull(result.getTime().toString());
        Assertions.assertEquals(post.getUserId(), result.getUser().getId());
        Assertions.assertNotNull(result.getPhotoUrl());
    }

    public static void assertPostsInEditDescriptionTest(PostDTO result, PostDTO post, String newDescription) {
        Assertions.assertEquals(post.getId(), result.getId());
        Assertions.assertEquals(newDescription, result.getDescription());
        Assertions.assertEquals(post.getTime().toString(), result.getTime().toString());
        Assertions.assertEquals(post.getUser(), result.getUser());
        Assertions.assertEquals(post.getPhotoUrl(), result.getPhotoUrl());
    }
}
