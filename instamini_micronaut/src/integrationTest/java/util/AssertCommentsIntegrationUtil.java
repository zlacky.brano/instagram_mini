package util;

import org.junit.jupiter.api.Assertions;
import zlack.bra.instamini.business.dto.CommentCreateDTO;
import zlack.bra.instamini.business.dto.CommentDTO;

public class AssertCommentsIntegrationUtil {

    public static void assertComments(CommentDTO result, CommentDTO comment) {
        Assertions.assertEquals(comment.getId(), result.getId());
        Assertions.assertEquals(comment.getText(), result.getText());
        Assertions.assertEquals(comment.getTime().toString(), result.getTime().toString());
        Assertions.assertEquals(comment.getUser(), result.getUser());
        Assertions.assertEquals(comment.getPost(), result.getPost());

    }

    public static void assertComments(CommentDTO result, CommentCreateDTO comment) {
        Assertions.assertNotNull(result.getId());
        Assertions.assertEquals(comment.getText(), result.getText());
        Assertions.assertNotNull(result.getTime().toString());
        Assertions.assertEquals(comment.getUserId(), result.getUser().getId());
        Assertions.assertEquals(comment.getPostId(), result.getPost().getId());
    }

    public static void assertCommentsInEditTextTest(CommentDTO result, CommentDTO comment, String newText) {
        Assertions.assertEquals(comment.getId(), result.getId());
        Assertions.assertEquals(newText, result.getText());
        Assertions.assertEquals(comment.getTime().toString(), result.getTime().toString());
        Assertions.assertEquals(comment.getUser(), result.getUser());
        Assertions.assertEquals(comment.getPost(), result.getPost());

    }
}
