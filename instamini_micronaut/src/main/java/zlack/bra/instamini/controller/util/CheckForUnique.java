package zlack.bra.instamini.controller.util;

import io.micronaut.http.HttpStatus;
import io.micronaut.http.exceptions.HttpStatusException;
import lombok.extern.slf4j.Slf4j;
import org.json.simple.JSONObject;

import java.util.HashMap;
import java.util.Map;

import static zlack.bra.instamini.constant.Constants.DUPLICATE_ENTRY;

@Slf4j
public class CheckForUnique {

    /**
     * Checks, if there was ConstraintViolationException and if so returns appropriate response
     * @param e exception
     * @return ResponseStatusException
     */
    public static HttpStatusException checkForUniqueConstraintViolation(Exception e) {
        for (Throwable t = e.getCause(); t != null; t = t.getCause()) {
            log.error("Exception: " + t);
            if (t.getMessage().startsWith("Duplicate entry") || t.getMessage().startsWith("Unique index")) {
                Map<String, String> responseBody = new HashMap<>();
                responseBody.put("message", DUPLICATE_ENTRY);

                return new HttpStatusException(HttpStatus.BAD_REQUEST, new JSONObject(responseBody));
            }
        }
        return new HttpStatusException(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage());
    }
}
