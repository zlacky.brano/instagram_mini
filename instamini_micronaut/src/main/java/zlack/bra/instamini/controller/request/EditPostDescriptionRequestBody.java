package zlack.bra.instamini.controller.request;

import io.micronaut.core.annotation.Introspected;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import static zlack.bra.instamini.constant.Constants.MAX_LENGTH_DESCRIPTION_AND_TEXT;

@Introspected
@Data
@NoArgsConstructor
@AllArgsConstructor
public class EditPostDescriptionRequestBody {

    @NotBlank
    @Size(max = MAX_LENGTH_DESCRIPTION_AND_TEXT)
    private String newDescription;

    public String toJson() {
        return "{\"newDescription\": \"" + newDescription + "\"}";
    }
}
