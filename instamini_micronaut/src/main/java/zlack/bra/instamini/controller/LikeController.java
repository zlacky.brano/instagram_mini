package zlack.bra.instamini.controller;

import io.micronaut.http.HttpResponse;
import io.micronaut.http.HttpStatus;
import io.micronaut.http.annotation.*;
import io.micronaut.http.exceptions.HttpStatusException;
import io.micronaut.http.hateoas.Link;
import io.micronaut.scheduling.TaskExecutors;
import io.micronaut.scheduling.annotation.ExecuteOn;
import io.micronaut.security.annotation.Secured;
import io.micronaut.security.rules.SecurityRule;
import zlack.bra.instamini.business.dto.LikeCreateDTO;
import zlack.bra.instamini.business.dto.LikeDTO;
import zlack.bra.instamini.business.dto.UserDTO;
import zlack.bra.instamini.business.exception.NotFoundException;
import zlack.bra.instamini.business.service.impl.LikeService;
import zlack.bra.instamini.controller.util.AuthorizationUtil;

import javax.validation.Valid;
import java.security.Principal;
import java.util.List;
import java.util.Optional;

import static zlack.bra.instamini.constant.Constants.ROOT_URL;
import static zlack.bra.instamini.controller.util.CheckForUnique.checkForUniqueConstraintViolation;

@Secured(SecurityRule.IS_AUTHENTICATED)
@ExecuteOn(TaskExecutors.IO)
@Controller("/likes")
public class LikeController {

    private final LikeService likeService;

    private final AuthorizationUtil authorizationUtil;

    public LikeController(LikeService likeService, AuthorizationUtil authorizationUtil) {
        this.likeService = likeService;
        this.authorizationUtil = authorizationUtil;
    }

    /**
     * Finds all likes
     * @return all likes
     */
    @Get
    public List<LikeDTO> all() {
        return likeService.findAll();
    }

    /**
     * Finds like by id
     * @param id of like
     * @return found like or status code NOT_FOUND, if like does not exist
     */
    @Get("/{id}")
    public LikeDTO byID(@PathVariable Integer id) {
        Optional<LikeDTO> optionalLikeDTO;
        optionalLikeDTO = likeService.findById(id);

        if (optionalLikeDTO.isPresent()) {
            return optionalLikeDTO.get();
        } else {
            throw new HttpStatusException(HttpStatus.NOT_FOUND, "No such like.");
        }
    }

    /**
     * Creates new like
     * @param likeCreateDTO object with necessary values to create like
     * @param principal represents logged user
     * @return created like. NOT_FOUND, if post or user does not exist. BAD_REQUEST, if unique constraint was violated
     */
    @Post
    public HttpResponse<?> create(@Body @Valid LikeCreateDTO likeCreateDTO, Principal principal) {
        authorizationUtil.checkIfAuthorizedOrElseThrow(principal, likeCreateDTO.getUserId(),
                "Can't create like for other user.");

        LikeDTO likeDTO;
        try {
            likeDTO = likeService.create(likeCreateDTO);
        } catch (NotFoundException e) {
            throw new HttpStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (Exception e) {
            throw checkForUniqueConstraintViolation(e);
        }
        return HttpResponse.created(Link.of(ROOT_URL + "likes/" + likeDTO.getId()).toString()).body(likeDTO);
    }

    /**
     * Deletes like by id
     * @param id of like
     * @param principal represents logged user
     * Can return NOT_FOUND, if like does not exist
     */
    @Delete("/{id}")
    public void delete(@PathVariable Integer id, Principal principal) {
        Optional<LikeDTO> likeDTOOptional = likeService.findById(id);
        likeDTOOptional.ifPresent(likeDTO -> authorizationUtil.checkIfAuthorizedOrElseThrow(principal,
                likeDTO.getUser().getId(), "Can't delete like, which does not belong to you."));

        try {
            likeService.deleteById(id);
        } catch (NotFoundException e) {
            throw new HttpStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    /**
     * Finds number of likes on post
     * @param post id
     * @return number of likes on post. NOT_FOUND, if post does not exist
     */
    @Get(value = "/count")
    public Long findNumberOfLikesOnPost(@QueryValue("post") Integer post) {
        try {
            return likeService.findNumberOfLikesOnPost(post);
        } catch (NotFoundException e) {
            throw new HttpStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    /**
     * Finds likes on post
     * @param post id
     * @return likes on post. NOT_FOUND, if post does not exist
     */
    @Get(value = "/onPost")
    public List<LikeDTO> findLikesOnPost(@QueryValue("post") Integer post) {
        try {
            return likeService.findLikesOnPost(post);
        } catch (NotFoundException e) {
            throw new HttpStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    /**
     * Finds like id by logged user and post
     * @param postId id of post
     * @param principal represents logged user
     * @return found like id. NOT_FOUND, if like does not exist
     */
    @Get("/isLiked/{postId}")
    public Integer findLikeIdByUserAndPost(@PathVariable Integer postId, Principal principal) {
        UserDTO user = authorizationUtil.getUserFromPrincipal(principal);
        try {
            return likeService.findLikeIdByUserAndPost(user.getId(), postId);
        } catch (NotFoundException e) {
            throw new HttpStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
}
