package zlack.bra.instamini.controller;

import io.micronaut.http.HttpResponse;
import io.micronaut.http.HttpStatus;
import io.micronaut.http.MediaType;
import io.micronaut.http.annotation.*;
import io.micronaut.http.exceptions.HttpStatusException;
import io.micronaut.http.hateoas.Link;
import io.micronaut.http.multipart.CompletedFileUpload;
import io.micronaut.scheduling.TaskExecutors;
import io.micronaut.scheduling.annotation.ExecuteOn;
import io.micronaut.security.annotation.Secured;
import io.micronaut.security.rules.SecurityRule;
import zlack.bra.instamini.business.dto.PostCreateDTO;
import zlack.bra.instamini.business.dto.PostDTO;
import zlack.bra.instamini.business.exception.NotFoundException;
import zlack.bra.instamini.business.exception.UploadFailException;
import zlack.bra.instamini.business.service.impl.PostService;
import zlack.bra.instamini.controller.request.EditPostDescriptionRequestBody;
import zlack.bra.instamini.controller.util.AuthorizationUtil;

import javax.validation.Valid;
import java.security.Principal;
import java.util.List;
import java.util.Optional;

import static zlack.bra.instamini.constant.Constants.NOT_AN_IMAGE;
import static zlack.bra.instamini.constant.Constants.ROOT_URL;

@Secured(SecurityRule.IS_AUTHENTICATED)
@ExecuteOn(TaskExecutors.IO)
@Controller("/posts")
public class PostController {

    private final PostService postService;

    private final AuthorizationUtil authorizationUtil;

    public PostController(PostService postService, AuthorizationUtil authorizationUtil) {
        this.postService = postService;
        this.authorizationUtil = authorizationUtil;
    }

    /**
     * Finds all posts
     * @return all posts
     */
    @Get
    public List<PostDTO> all() {
        return postService.findAll();
    }

    /**
     * Finds post by id
     * @param id of post
     * @return found post. NOT_FOUND, if post does not exist
     */
    @Get("/{id}")
    public PostDTO byID(@PathVariable Integer id) throws HttpStatusException {
        Optional<PostDTO> optionalPostDTO;
        optionalPostDTO = postService.findById(id);

        if (optionalPostDTO.isPresent()) {
            return optionalPostDTO.get();
        }
        else {
            throw new HttpStatusException(HttpStatus.NOT_FOUND, "No such post.");
        }
    }

    /**
     * Creates new post
     * @param image of post
     * @param description of post
     * @param userId owner of post
     * @param principal represents logged user
     * @return created post. NOT_FOUND, if user was not found. BAD_REQUEST, if file is not an image
     */
    @Post(consumes = { MediaType.MULTIPART_FORM_DATA })
    public HttpResponse<?> create(CompletedFileUpload image, String description, Integer userId, Principal principal) {
        authorizationUtil.checkIfAuthorizedOrElseThrow(principal, userId,
                "Can't create post on profile, which does not belong to you.");

        PostCreateDTO postCreateDTO = new PostCreateDTO(description, userId, image);
        PostDTO postDTO;
        try {
            postDTO = postService.create(postCreateDTO);
        } catch (NotFoundException e) {
            throw new HttpStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UploadFailException e) {
            if (e.getMessage().equals(NOT_AN_IMAGE)) {
                throw new HttpStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
            }
            throw new HttpStatusException(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage());
        }
        return HttpResponse.created(Link.of(ROOT_URL + "posts/" + postDTO.getId()).toString()).body(postDTO);
    }

    /**
     * Edits description of post
     * @param id of post
     * @param requestBody new description
     * @param principal represents logged user
     * @return updated post. NOT_FOUND, if post does not exist
     */
    @Put("/{id}/editDescription")
    public PostDTO editDescription(@PathVariable Integer id,
                                   @Body @Valid EditPostDescriptionRequestBody requestBody,
                                   Principal principal) {
        Optional<PostDTO> postDTOOptional = postService.findById(id);
        postDTOOptional.ifPresent(postDTO -> authorizationUtil.checkIfAuthorizedOrElseThrow(principal,
                postDTO.getUser().getId(), "Can't edit description on post, which does not belong to you."));

        try {
            return postService.editDescription(id, requestBody.getNewDescription());
        } catch (NotFoundException e) {
            throw new HttpStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    /**
     * Deletes post by id
     * @param id of post
     * @param principal represents logged user
     * Can return NOT_FOUND, if post does not exist
     */
    @Delete("/{id}")
    public void delete(@PathVariable Integer id, Principal principal) {
        Optional<PostDTO> postDTOOptional = postService.findById(id);
        postDTOOptional.ifPresent(postDTO -> authorizationUtil.checkIfAuthorizedOrElseThrow(principal,
                postDTO.getUser().getId(), "Can't delete post, which does not belong to you."));

        try {
            postService.deleteById(id);
        } catch (NotFoundException e) {
            throw new HttpStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    /**
     * Finds all posts of user
     * @param user id
     * @return found posts. NOT_FOUND, if user was not found
     */
    @Get(value = "/ofUser")
    public List<PostDTO> findAllPostsOfUser(@QueryValue("user") Integer user) {
        try {
            return postService.findAllPostsOfUser(user);
        } catch (NotFoundException e) {
            throw new HttpStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    /**
     * Finds number of all posts of user
     * @param user id
     * @return found number of posts of user
     */
    @Get(value = "/count")
    public Long findCountOfAllPostsOfUser(@QueryValue("user") Integer user) {
        try {
            return postService.findCountOfAllPostsOfUser(user);
        } catch (NotFoundException e) {
            throw new HttpStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
}
