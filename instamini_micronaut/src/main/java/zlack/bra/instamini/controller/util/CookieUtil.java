package zlack.bra.instamini.controller.util;

import io.micronaut.http.HttpRequest;
import io.micronaut.http.cookie.Cookie;
import io.micronaut.http.cookie.Cookies;

import java.util.Optional;

/**
 * Util for helping with cookies (getting it from request and creating new one)
 */
public class CookieUtil {

    public static Optional<Cookie> getCookie(HttpRequest<?> request, String name) {
        Cookies cookies = request.getCookies();
        return cookies.findCookie(name);
    }

    public static Cookie createCookie(String name, String value, int maxAge) {
        Cookie cookie = Cookie.of(name, value);
        cookie.path("/");
        cookie.httpOnly(true);
        cookie.maxAge(maxAge);
        return cookie;
    }
}