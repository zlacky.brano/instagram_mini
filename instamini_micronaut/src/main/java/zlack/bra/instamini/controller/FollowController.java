package zlack.bra.instamini.controller;

import io.micronaut.http.HttpResponse;
import io.micronaut.http.HttpStatus;
import io.micronaut.http.annotation.*;
import io.micronaut.http.exceptions.HttpStatusException;
import io.micronaut.http.hateoas.Link;
import io.micronaut.scheduling.TaskExecutors;
import io.micronaut.scheduling.annotation.ExecuteOn;
import io.micronaut.security.annotation.Secured;
import io.micronaut.security.rules.SecurityRule;
import zlack.bra.instamini.business.dto.FollowCreateDTO;
import zlack.bra.instamini.business.dto.FollowDTO;
import zlack.bra.instamini.business.dto.UserDTO;
import zlack.bra.instamini.business.exception.FollowYourselfException;
import zlack.bra.instamini.business.exception.NotFoundException;
import zlack.bra.instamini.business.service.impl.FollowService;
import zlack.bra.instamini.controller.util.AuthorizationUtil;

import javax.validation.Valid;
import java.security.Principal;
import java.util.List;
import java.util.Optional;

import static zlack.bra.instamini.constant.Constants.ROOT_URL;
import static zlack.bra.instamini.controller.util.CheckForUnique.checkForUniqueConstraintViolation;

@Secured(SecurityRule.IS_AUTHENTICATED)
@ExecuteOn(TaskExecutors.IO)
@Controller("/follows")
public class FollowController {

    private final FollowService followService;

    private final AuthorizationUtil authorizationUtil;

    public FollowController(FollowService followService, AuthorizationUtil authorizationUtil) {
        this.followService = followService;
        this.authorizationUtil = authorizationUtil;
    }

    /**
     * Finds all follows
     * @return all follows
     */
    @Get
    public List<FollowDTO> all() {
        return followService.findAll();
    }

    /**
     * Finds follow by id
     * @param id of follow
     * @return found follow or status code NOT_FOUND, if follow does not exist
     */
    @Get("/{id}")
    public FollowDTO byID(@PathVariable Integer id) {
        Optional<FollowDTO> optionalFollowDTO;
        optionalFollowDTO = followService.findById(id);

        if (optionalFollowDTO.isPresent()) {
            return optionalFollowDTO.get();
        }
        else {
            throw new HttpStatusException(HttpStatus.NOT_FOUND, "No such follow.");
        }
    }

    /**
     * Creates new follow
     * @param followCreateDTO object with necessary values to create follow
     * @param principal represents logged user
     * @return created follow. NOT_FOUND, if any of users does not exist. BAD_REQUEST, if both users are same or unique constraint was violated
     */
    @Post
    public HttpResponse<?> create(@Body @Valid FollowCreateDTO followCreateDTO, Principal principal) {
        authorizationUtil.checkIfAuthorizedOrElseThrow(principal, followCreateDTO.getUserWhoId(),
                "Can't create follow for other user.");

        FollowDTO followDTO;
        try {
            followDTO = followService.create(followCreateDTO);
        } catch (NotFoundException e) {
            throw new HttpStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (FollowYourselfException e) {
            throw new HttpStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        } catch (Exception e) {
            throw checkForUniqueConstraintViolation(e);
        }
        return HttpResponse.created(Link.of(ROOT_URL + "follows/" + followDTO.getId()).toString()).body(followDTO);
    }

    /**
     * Deletes follow by id
     * @param id of user
     * @param principal represents logged user
     * Can return NOT_FOUND, if follow does not exist
     */
    @Delete("/{id}")
    public void delete(@PathVariable Integer id, Principal principal) {
        Optional<FollowDTO> followDTOOptional = followService.findById(id);
        followDTOOptional.ifPresent(followDTO -> authorizationUtil.checkIfAuthorizedOrElseThrow(principal,
                followDTO.getUserWho().getId(), "Can't delete follow, which does not belong to you."));

        try {
            followService.deleteById(id);
        } catch (NotFoundException e) {
            throw new HttpStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    /**
     * Finds follow id by principal's id and userIdWhom
     * @param userIdWhom od userWhom
     * @param principal represents logged user
     * @return follow id or NOT_FOUND, if follow does not exist
     */
    @Get("/isFollowed/{userIdWhom}")
    public Integer findFollowIdByUsers(@PathVariable Integer userIdWhom, Principal principal) {
        UserDTO user = authorizationUtil.getUserFromPrincipal(principal);
        try {
            return followService.findFollowIdByUsers(user.getId(), userIdWhom);
        } catch (NotFoundException e) {
            throw new HttpStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
}
