package zlack.bra.instamini.controller.util;

import io.micronaut.http.HttpStatus;
import io.micronaut.http.exceptions.HttpStatusException;
import jakarta.inject.Singleton;
import zlack.bra.instamini.business.dto.UserDTO;
import zlack.bra.instamini.business.service.impl.UserService;

import java.security.Principal;
import java.util.Optional;

@Singleton
public class AuthorizationUtil {

    private final UserService userService;

    public AuthorizationUtil(UserService userService) {
        this.userService = userService;
    }

    /**
     * Gets user from principal
     * @param principal logged user
     * @return UserDTO
     */
    public UserDTO getUserFromPrincipal(Principal principal) {
        Optional<UserDTO> userDTOOptional = userService.findByEmail(principal.getName());
        if (userDTOOptional.isPresent()) {
            return userDTOOptional.get();
        }
        throw new HttpStatusException(HttpStatus.BAD_REQUEST, "User associated with JWT was deleted.");
    }

    /**
     * Throws ForbiddenException, if user is not allowed to use resource
     * @param principal logged user
     * @param userId owner's id
     * @param message error message, if exception is hthrown
     */
    public void checkIfAuthorizedOrElseThrow(Principal principal, Integer userId, String message) {
        Optional<UserDTO> userOptional = userService.findByEmail(principal.getName());
        if (userOptional.isPresent() && !userOptional.get().getId().equals(userId)) {
            throw new HttpStatusException(HttpStatus.FORBIDDEN, message);
        }
    }
}
