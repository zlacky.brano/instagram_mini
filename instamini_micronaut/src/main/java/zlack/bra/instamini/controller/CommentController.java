package zlack.bra.instamini.controller;

import io.micronaut.http.HttpResponse;
import io.micronaut.http.HttpStatus;
import io.micronaut.http.annotation.*;
import io.micronaut.http.exceptions.HttpStatusException;
import io.micronaut.http.hateoas.Link;
import io.micronaut.scheduling.TaskExecutors;
import io.micronaut.scheduling.annotation.ExecuteOn;
import io.micronaut.security.annotation.Secured;
import io.micronaut.security.rules.SecurityRule;
import zlack.bra.instamini.business.dto.CommentCreateDTO;
import zlack.bra.instamini.business.dto.CommentDTO;
import zlack.bra.instamini.business.exception.NotFoundException;
import zlack.bra.instamini.business.service.impl.CommentService;
import zlack.bra.instamini.controller.request.EditTextCommentRequestBody;
import zlack.bra.instamini.controller.util.AuthorizationUtil;

import javax.validation.Valid;
import java.security.Principal;
import java.util.List;
import java.util.Optional;

import static zlack.bra.instamini.constant.Constants.ROOT_URL;

@Secured(SecurityRule.IS_AUTHENTICATED)
@ExecuteOn(TaskExecutors.IO)
@Controller("/comments")
public class CommentController {

    private final CommentService commentService;

    private final AuthorizationUtil authorizationUtil;

    public CommentController(CommentService commentService, AuthorizationUtil authorizationUtil) {
        this.commentService = commentService;
        this.authorizationUtil = authorizationUtil;
    }

    /**
     * Returns all comments
     * @return all comments
     */
    @Get
    public List<CommentDTO> all() {
        return commentService.findAll();
    }

    /**
     * Finds comment by id
     * @param id of comment
     * @return found comment or status code NOT_FOUND, if it does not exist
     */
    @Get("/{id}")
    public CommentDTO byID(@PathVariable Integer id) {
        Optional<CommentDTO> optionalCommentDTO;
        optionalCommentDTO = commentService.findById(id);

        if (optionalCommentDTO.isPresent()) {
            return optionalCommentDTO.get();
        }
        else {
            throw new HttpStatusException(HttpStatus.NOT_FOUND, "No such comment.");
        }
    }

    /**
     * Creates new comment
     * @param commentCreateDTO object with necessary values to create comment
     * @param principal represents logged user
     * @return created comment or status code NOT_FOUND, if user or post does not exist
     */
    @Post
    public HttpResponse<?> create(@Body @Valid CommentCreateDTO commentCreateDTO, Principal principal) {
        authorizationUtil.checkIfAuthorizedOrElseThrow(principal, commentCreateDTO.getUserId(),
                "Can't create comment for other user.");

        CommentDTO commentDTO;
        try {
            commentDTO = commentService.create(commentCreateDTO);
        } catch (NotFoundException e) {
            throw new HttpStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
        return HttpResponse.created(Link.of(ROOT_URL + "comments/" + commentDTO.getId()).toString()).body(commentDTO);
    }

    /**
     * Edits text of comment
     * @param id of comment
     * @param requestBody new text
     * @param principal represents logged user
     * @return updated comment or status code NOT_FOUND, if comment does not exist
     */
    @Put("/{id}/editText")
    public CommentDTO editText(@PathVariable Integer id, @Body @Valid EditTextCommentRequestBody requestBody, Principal principal) {
        Optional<CommentDTO> commentDTOOptional = commentService.findById(id);
        commentDTOOptional.ifPresent(commentDTO -> authorizationUtil.checkIfAuthorizedOrElseThrow(principal,
                commentDTO.getUser().getId(), "Can't edit text of comment, which does not belong to you."));

        try {
            return commentService.editText(id, requestBody.getNewText());
        } catch (NotFoundException e) {
            throw new HttpStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    /**
     * Deletes comment by id
     * @param id of comment
     * @param principal represents logged user
     * Can return NOT_FOUND, if comment does not exist
     */
    @Delete("/{id}")
    public void delete(@PathVariable Integer id, Principal principal) {
        Optional<CommentDTO> commentDTOOptional = commentService.findById(id);
        commentDTOOptional.ifPresent(commentDTO -> authorizationUtil.checkIfAuthorizedOrElseThrow(principal,
                commentDTO.getUser().getId(), "Can't delete comment, which does not belong to you."));

        try {
            commentService.deleteById(id);
        } catch (NotFoundException e) {
            throw new HttpStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    /**
     * Finds all comments on post
     * @param post id
     * @return found comments or status code NOT_FOUND, if post does not exist
     */
    @Get(value = "/onPost")
    public List<CommentDTO> findAllCommentsOnPost(@QueryValue("post") Integer post) {
        try {
            return commentService.findAllCommentsOnPost(post);
        } catch (NotFoundException e) {
            throw new HttpStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
}
