package zlack.bra.instamini.controller;

import io.micronaut.http.HttpStatus;
import io.micronaut.http.annotation.*;
import io.micronaut.http.exceptions.HttpStatusException;
import io.micronaut.scheduling.TaskExecutors;
import io.micronaut.scheduling.annotation.ExecuteOn;
import io.micronaut.security.annotation.Secured;
import io.micronaut.security.rules.SecurityRule;
import zlack.bra.instamini.business.dto.UserDTO;
import zlack.bra.instamini.business.exception.NotFoundException;
import zlack.bra.instamini.business.service.impl.UserService;
import zlack.bra.instamini.controller.request.EditUsernameRequestBody;
import zlack.bra.instamini.controller.util.AuthorizationUtil;

import javax.validation.Valid;
import java.security.Principal;
import java.util.List;
import java.util.Optional;

import static zlack.bra.instamini.controller.util.CheckForUnique.checkForUniqueConstraintViolation;

@Secured(SecurityRule.IS_AUTHENTICATED)
@ExecuteOn(TaskExecutors.IO)
@Controller("/users")
public class UserController {

    private final UserService userService;

    private final AuthorizationUtil authorizationUtil;

    public UserController(UserService userService, AuthorizationUtil authorizationUtil) {
        this.userService = userService;
        this.authorizationUtil = authorizationUtil;
    }

    /**
     * Finds all users
     * @return all users
     */
    @Get
    public List<UserDTO> all() {
        return userService.findAll();
    }

    /**
     * Finds users by prefix
     * @param prefix
     * @return found users
     */
    @Get("/search")
    public List<UserDTO> searchUsers(@QueryValue("prefix") String prefix) {
        return userService.searchUsers(prefix);
    }

    /**
     * Finds user by id
     * @param id of user
     * @return found user. NOT_FOUND, if user does not exist.
     */
    @Get("/{id}")
    public UserDTO byID(@PathVariable Integer id) {
        Optional<UserDTO> optionalUserDTO;
        optionalUserDTO = userService.findById(id);

        if (optionalUserDTO.isPresent()) {
            return optionalUserDTO.get();
        }
        else {
            throw new HttpStatusException(HttpStatus.NOT_FOUND, "No such user.");
        }
    }

    /**
     * Finds user by email
     * @param email
     * @return found user. NOT_FOUND, if user does not exist.
     */
    @Get(value = "/byEmail")
    public UserDTO byEmail(@QueryValue("email") String email) {
        Optional<UserDTO> optionalUserDTO;
        optionalUserDTO = userService.findByEmail(email);

        if (optionalUserDTO.isPresent()) {
            return optionalUserDTO.get();
        }
        else {
            throw new HttpStatusException(HttpStatus.NOT_FOUND, "No such user.");
        }
    }

    /**
     * Finds user by principal (logged user)
     * @param principal represents logged user
     * @return found user. NOT_FOUND, if user does not exist.
     */
    @Get(value = "/logged")
    public UserDTO byPrincipal(Principal principal) {
        String userEmail = principal.getName();
        if (userEmail == null) {
            throw new HttpStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Principal's email is null.");
        }
        Optional<UserDTO> optionalUserDTO;
        optionalUserDTO = userService.findByEmail(userEmail);

        if (optionalUserDTO.isPresent()) {
            return optionalUserDTO.get();
        }
        else {
            throw new HttpStatusException(HttpStatus.NOT_FOUND, "No such user.");
        }
    }

    /**
     * Edits username of user
     * @param id of user
     * @param requestBody new username
     * @param principal represents logged user
     * @return updated user
     */
    @Put("/{id}/editUsername")
    public UserDTO editUsername(@PathVariable Integer id,
                                @Body @Valid EditUsernameRequestBody requestBody,
                                Principal principal) {
        authorizationUtil.checkIfAuthorizedOrElseThrow(principal, id,
                "Can't edit username on profile, which does not belong to you.");
        try {
            return userService.editUsername(id, requestBody.getUsername());
        } catch (NotFoundException e) {
            throw new HttpStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (Exception e) {
            throw checkForUniqueConstraintViolation(e);
        }
    }

    /**
     * Deletes user by id
     * @param id of user
     * @param principal represents logged user
     * Can return NOT_FOUND, if user does not exist
     */
    @Delete("/{id}")
    public void delete(@PathVariable Integer id, Principal principal) {
        authorizationUtil.checkIfAuthorizedOrElseThrow(principal, id,
                "Can't delete profile, which does not belong to you.");
        try {
            userService.deleteById(id);
        } catch (NotFoundException e) {
            throw new HttpStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    /**
     * Finds all users who liked post
     * @param post id
     * @return found users. NOT_FOUND, if post does not exist
     */
    @Get(value = "/likedPost")
    public List<UserDTO> findAllUsersWhoLikedPost(@QueryValue("post") Integer post) {
        try {
            return userService.findAllUsersWhoLikedPost(post);
        } catch (NotFoundException e) {
            throw new HttpStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    /**
     * Finds all followers of user
     * @param user id
     * @return found users. NOT_FOUND, if user does not exist
     */
    @Get(value = "/followers")
    public List<UserDTO> findAllFollowers(@QueryValue("user") Integer user) {
        try {
            return userService.findAllFollowers(user);
        } catch (NotFoundException e) {
            throw new HttpStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    /**
     * Finds all users, who user is following
     * @param user id
     * @return found users. NOT_FOUND, if user does not exist
     */
    @Get(value = "/following")
    public List<UserDTO> findAllFollowing(@QueryValue("user") Integer user) {
        try {
            return userService.findAllFollowing(user);
        } catch (NotFoundException e) {
            throw new HttpStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    /**
     * Finds count of all followers
     * @param user id
     * @return found count. NOT_FOUND, if user does not exist
     */
    @Get(value = "/followers/count")
    public Long findCountOfAllFollowers(@QueryValue("user") Integer user) {
        try {
            return userService.findCountOfAllFollowers(user);
        } catch (NotFoundException e) {
            throw new HttpStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    /**
     * Finds count of all users, who user is following
     * @param user id
     * @return found count. NOT_FOUND, if user does not exist
     */
    @Get(value = "/following/count")
    public Long findCountOfAllFollowing(@QueryValue("user") Integer user) {
        try {
            return userService.findCountOfAllFollowing(user);
        } catch (NotFoundException e) {
            throw new HttpStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
}
