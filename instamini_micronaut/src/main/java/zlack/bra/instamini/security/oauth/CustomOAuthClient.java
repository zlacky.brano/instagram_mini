package zlack.bra.instamini.security.oauth;

import io.micronaut.context.annotation.EachBean;
import io.micronaut.context.annotation.Replaces;
import io.micronaut.context.annotation.Requires;
import io.micronaut.http.HttpRequest;
import io.micronaut.http.MutableHttpResponse;
import io.micronaut.security.authentication.AuthenticationResponse;
import io.micronaut.security.oauth2.client.DefaultOauthClient;
import io.micronaut.security.oauth2.client.OauthClient;
import io.micronaut.security.oauth2.endpoint.token.response.OauthAuthenticationMapper;
import org.reactivestreams.Publisher;
import reactor.core.publisher.Flux;
import zlack.bra.instamini.controller.util.CookieUtil;

import java.util.Map;

import static zlack.bra.instamini.constant.Constants.COOKIE_EXPIRE;
import static zlack.bra.instamini.constant.Constants.REDIRECT_URI_PARAM_COOKIE_NAME;

@EachBean(OauthAuthenticationMapper.class)
@Replaces(OauthClient.class)
public class CustomOAuthClient implements OauthClient {

    private final DefaultOauthClient defaultOauthClient;

    private final String defaultRedirectUrl = "/";

    public CustomOAuthClient(DefaultOauthClient defaultOauthClient) {
        this.defaultOauthClient = defaultOauthClient;
    }

    @Override
    public String getName() {
        return defaultOauthClient.getName();
    }

    /**
     * Replacing DefaultOauthClient, so I can add cookie with redirect uri to authorization request
     * @param originating request
     * @return response with redirect to google server
     */
    @Override
    public Publisher<MutableHttpResponse<?>> authorizationRedirect(HttpRequest<?> originating) {
        Publisher<MutableHttpResponse<?>> mutableHttpResponsePublisher = defaultOauthClient.authorizationRedirect(originating);

        String redirectUrl = originating.getParameters().get(REDIRECT_URI_PARAM_COOKIE_NAME);

        if (redirectUrl == null) {
            redirectUrl = defaultRedirectUrl;
        }

        String finalRedirectUrl = redirectUrl;
        return Flux.from(mutableHttpResponsePublisher)
                .map(mutableHttpResponse -> mutableHttpResponse.cookie(
                        CookieUtil.createCookie(REDIRECT_URI_PARAM_COOKIE_NAME, finalRedirectUrl, COOKIE_EXPIRE)
                        )
                );
    }

    @Override
    public Publisher<AuthenticationResponse> onCallback(HttpRequest<Map<String, Object>> request) {
        return defaultOauthClient.onCallback(request);
    }
}
