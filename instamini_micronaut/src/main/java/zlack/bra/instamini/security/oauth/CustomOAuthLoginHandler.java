package zlack.bra.instamini.security.oauth;

import io.micronaut.context.annotation.Value;
import io.micronaut.http.HttpRequest;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.MutableHttpResponse;
import io.micronaut.http.cookie.Cookie;
import io.micronaut.http.uri.UriBuilder;
import io.micronaut.security.authentication.Authentication;
import io.micronaut.security.authentication.AuthenticationResponse;
import io.micronaut.security.handlers.RedirectingLoginHandler;
import io.micronaut.security.token.jwt.generator.JwtTokenGenerator;
import jakarta.inject.Singleton;
import lombok.extern.slf4j.Slf4j;
import zlack.bra.instamini.business.dto.UserCreateDTO;
import zlack.bra.instamini.business.dto.UserDTO;
import zlack.bra.instamini.business.service.impl.UserService;
import zlack.bra.instamini.controller.util.CookieUtil;
import zlack.bra.instamini.security.exception.JwtGenerateException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Optional;

import static zlack.bra.instamini.constant.Constants.REDIRECT_URI_PARAM_COOKIE_NAME;

@Singleton
@Slf4j
public class CustomOAuthLoginHandler implements RedirectingLoginHandler {

    private final UserService userService;
    private final JwtTokenGenerator jwtTokenGenerator;

    private final Integer tokenExpirationSec;
    private final String serverPort;
    private final String loginUri;

    private final String defaultTargetUrl = "/";

    public CustomOAuthLoginHandler(UserService userService,
                                   JwtTokenGenerator jwtTokenGenerator,
                                   @Value("${jwt.token-expiration}") Integer tokenExpirationSec,
                                   @Value("${micronaut.server.port}") String serverPort,
                                   @Value("${micronaut.security.oauth2.login-uri}") String loginUri) {
        this.userService = userService;
        this.jwtTokenGenerator = jwtTokenGenerator;
        this.tokenExpirationSec = tokenExpirationSec;
        this.serverPort = serverPort;
        this.loginUri = loginUri;
    }

    /**
     * Handler after successful login. Registers new user if necessary
     * @param authentication of user
     * @param request
     * @return response with redirection to client
     */
    @Override
    public MutableHttpResponse<?> loginSuccess(Authentication authentication, HttpRequest<?> request) {
        URI targetUrl = determineTargetUrl(request, authentication);

        String email = authentication.getName();
        Optional<UserDTO> userDTO = userService.findByEmail(email);

        if (userDTO.isEmpty()) {
            log.info("Registering new user: " + email + ".");
            userService.create(new UserCreateDTO(email, email));
        } else {
            log.info("User: " + userDTO.get().getEmail() + " already registered.");
        }
        return HttpResponse.temporaryRedirect(targetUrl);
    }

    @Override
    public MutableHttpResponse<?> loginRefresh(Authentication authentication, String refreshToken, HttpRequest<?> request) {
        try {
            return HttpResponse.temporaryRedirect(new URI("http://localhost:" + serverPort + loginUri));
        } catch (URISyntaxException e) {
            throw new RuntimeException("Error: " + e);
        }
    }

    /**
     * Handler of failed login
     * @param authenticationResponse
     * @param request
     * @return unauthorized response
     */
    @Override
    public MutableHttpResponse<?> loginFailed(AuthenticationResponse authenticationResponse, HttpRequest<?> request) {
        return HttpResponse.unauthorized();
    }

    /**
     * Adds jwt to redirect uri and gets redirect uri from cookie
     * @param request
     * @param authentication of user
     * @return target uri
     */
    private URI determineTargetUrl(HttpRequest<?> request, Authentication authentication) {
        Optional<String> redirectUri = CookieUtil.getCookie(request, REDIRECT_URI_PARAM_COOKIE_NAME).map(Cookie::getValue);
        String targetUrl = redirectUri.orElse(defaultTargetUrl);
        Optional<String> token = jwtTokenGenerator.generateToken(authentication, tokenExpirationSec);
        if (token.isEmpty()) {
            throw new JwtGenerateException("Jwt token is null", null);
        }
        return UriBuilder.of(targetUrl).queryParam("token", token.get()).build();
    }
}
