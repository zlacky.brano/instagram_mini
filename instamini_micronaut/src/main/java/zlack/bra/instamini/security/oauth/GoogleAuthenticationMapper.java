package zlack.bra.instamini.security.oauth;

import io.micronaut.security.authentication.AuthenticationResponse;
import io.micronaut.security.oauth2.endpoint.authorization.state.State;
import io.micronaut.security.oauth2.endpoint.token.response.OauthAuthenticationMapper;
import io.micronaut.security.oauth2.endpoint.token.response.TokenResponse;
import jakarta.inject.Named;
import jakarta.inject.Singleton;
import lombok.extern.slf4j.Slf4j;
import org.reactivestreams.Publisher;
import reactor.core.publisher.Flux;

import java.util.List;

@Named("google")
@Singleton
@Slf4j
public class GoogleAuthenticationMapper implements OauthAuthenticationMapper {

    private final GoogleApiClient googleApiClient;

    public GoogleAuthenticationMapper(GoogleApiClient googleApiClient) {
        this.googleApiClient = googleApiClient;
    }

    /**
     * Handles response with access token from google server
     * @param tokenResponse holds access token
     * @param state of authorization flow
     * @return authentication response with fetched user ifno
     */
    @Override
    public Publisher<AuthenticationResponse> createAuthenticationResponse(TokenResponse tokenResponse, State state) {
        return Flux.from(googleApiClient.getUser("Bearer " + tokenResponse.getAccessToken()))
                .map(user -> AuthenticationResponse.success(user.getEmail(), List.of()));
    }
}
