package zlack.bra.instamini.security.exception;

public class JwtGenerateException extends RuntimeException {
    public JwtGenerateException(String errorMessage, Throwable err) {
        super(errorMessage, err);
    }
}
