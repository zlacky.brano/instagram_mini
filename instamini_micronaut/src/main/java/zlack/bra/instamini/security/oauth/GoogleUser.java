package zlack.bra.instamini.security.oauth;

import io.micronaut.core.annotation.Introspected;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Introspected
@Getter
@AllArgsConstructor
public class GoogleUser {

    private final Integer id;
    private final String name;
    private final String email;
}
