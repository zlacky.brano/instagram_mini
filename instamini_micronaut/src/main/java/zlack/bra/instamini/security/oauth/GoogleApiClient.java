package zlack.bra.instamini.security.oauth;

import io.micronaut.http.annotation.Get;
import io.micronaut.http.annotation.Header;
import io.micronaut.http.client.annotation.Client;
import org.reactivestreams.Publisher;

@Client("${google-api.url}")
public interface GoogleApiClient {

    /**
     * Gets user info from google server
     * @param authorization access token
     * @return user info
     */
    @Get("/userinfo")
    Publisher<GoogleUser> getUser(@Header("Authorization") String authorization);
}
