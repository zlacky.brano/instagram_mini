package zlack.bra.instamini;

import io.micronaut.runtime.Micronaut;

public class InstaminiApplication {

    public static void main(String[] args) {
        Micronaut.run(InstaminiApplication.class, args);
    }
}
