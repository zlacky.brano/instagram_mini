package zlack.bra.instamini.constant;

public class Constants {
    public static final String ROOT_URL = "http://localhost:8080/";
    public static final String REDIRECT_URI_PARAM_COOKIE_NAME = "redirect_uri";
    public static final Integer COOKIE_EXPIRE = 300;
    public static final String DUPLICATE_ENTRY = "Duplicate entry!";
    public static final int MAX_LENGTH_USERNAME_AND_EMAIL = 50;
    public static final int MAX_LENGTH_DESCRIPTION_AND_TEXT = 500;
    public static final int MAX_LENGTH_PHOTO_URL = 2048;
    public static final String NOT_AN_IMAGE = "File for upload is not an image!";
}
