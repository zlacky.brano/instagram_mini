package zlack.bra.instamini.data.entity;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Setter
@Getter
@NoArgsConstructor
@Entity
@Table(name = "Follow", uniqueConstraints={
        @UniqueConstraint(columnNames = {"user_id_who", "user_id_whom"})})
public class Follow {

    @Setter(value = AccessLevel.NONE)
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "user_id_who", nullable = false)
    private User userWho;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "user_id_whom", nullable = false)
    private User userWhom;

    public Follow(User userWho, User userWhom) {
        this.userWho = userWho;
        this.userWhom = userWhom;
    }

    @Override
    public String toString() {
        return "Follow{" +
                "id=" + id +
                ", userWho=" + userWho +
                ", userWhom=" + userWhom +
                '}';
    }
}
