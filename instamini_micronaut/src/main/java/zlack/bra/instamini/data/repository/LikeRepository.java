package zlack.bra.instamini.data.repository;

import io.micronaut.data.annotation.Query;
import io.micronaut.data.annotation.Repository;
import io.micronaut.data.repository.CrudRepository;
import zlack.bra.instamini.data.entity.Like;

import java.util.List;
import java.util.Optional;

@Repository
public interface LikeRepository extends CrudRepository<Like, Integer> {

    @Query("SELECT COUNT(l) FROM Like as l WHERE l.post.id = :idPost")
    Long findNumberOfLikesOnPost(Integer idPost);

    @Query("SELECT l FROM Like as l WHERE l.post.id = :idPost")
    List<Like> findLikesOnPost(Integer idPost);

    @Query("SELECT l.id FROM Like as l WHERE l.user.id = :userId AND l.post.id = :postId")
    Optional<Integer> findLikeIdByUserAndPost(Integer userId, Integer postId);
}
