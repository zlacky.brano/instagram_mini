package zlack.bra.instamini.data.repository;

import io.micronaut.data.annotation.Query;
import io.micronaut.data.annotation.Repository;
import io.micronaut.data.repository.CrudRepository;
import zlack.bra.instamini.data.entity.User;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository extends CrudRepository<User, Integer> {

    @Query("SELECT u FROM Like as l JOIN User as u ON(l.user.id = u.id) WHERE l.post.id = :idPost")
    List<User> findAllUsersWhoLikedPost(Integer idPost);

    @Query("SELECT u FROM Follow as f JOIN User as u ON(f.userWho.id = u.id) WHERE f.userWhom.id = :id")
    List<User> findAllFollowers(Integer id);

    @Query("SELECT u FROM Follow as f JOIN User as u ON(f.userWhom.id = u.id) WHERE f.userWho.id = :id")
    List<User> findAllFollowing(Integer id);

    @Query("SELECT u FROM User as u WHERE u.email = :email")
    Optional<User> findByEmail(String email);

    @Query("SELECT COUNT(f) FROM Follow as f JOIN User as u ON(f.userWho.id = u.id) WHERE f.userWhom.id = :id")
    Long findCountOfAllFollowers(Integer id);

    @Query("SELECT COUNT(f) FROM Follow as f JOIN User as u ON(f.userWhom.id = u.id) WHERE f.userWho.id = :id")
    Long findCountOfAllFollowing(Integer id);
}
