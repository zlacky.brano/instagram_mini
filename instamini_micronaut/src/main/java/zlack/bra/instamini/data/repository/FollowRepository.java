package zlack.bra.instamini.data.repository;

import io.micronaut.data.annotation.Query;
import io.micronaut.data.annotation.Repository;
import io.micronaut.data.repository.CrudRepository;
import zlack.bra.instamini.data.entity.Follow;

import java.util.Optional;

@Repository
public interface FollowRepository extends CrudRepository<Follow, Integer> {

    @Query("SELECT f.id FROM Follow as f WHERE f.userWho.id = :userIdWho AND f.userWhom.id = :userIdWhom")
    Optional<Integer> findFollowIdByUsers(Integer userIdWho, Integer userIdWhom);
}
