package zlack.bra.instamini.data.repository;

import io.micronaut.data.annotation.Query;
import io.micronaut.data.annotation.Repository;
import io.micronaut.data.repository.CrudRepository;
import zlack.bra.instamini.data.entity.Post;

import java.util.List;

@Repository
public interface PostRepository extends CrudRepository<Post, Integer> {

    @Query("SELECT p FROM Post as p WHERE p.user.id = :idUser ORDER BY p.time DESC")
    List<Post> findAllPostsOfUser(Integer idUser);

    @Query("SELECT COUNT(p) FROM Post as p WHERE p.user.id = :idUser")
    Long findCountOfAllPostsOfUser(Integer idUser);
}
