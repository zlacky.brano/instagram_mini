package zlack.bra.instamini.data.entity;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;
import java.util.List;

import static zlack.bra.instamini.constant.Constants.*;

@Setter
@Getter
@NoArgsConstructor
@Entity
@Table(name = "Post")
public class Post {

    @Setter(value = AccessLevel.NONE)
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @NotBlank
    @Size(max = MAX_LENGTH_DESCRIPTION_AND_TEXT)
    @Column(nullable = false)
    private String description;

    @NotNull
    @Size(max = MAX_LENGTH_PHOTO_URL)
    @Column(name = "photo_url", nullable = false)
    private String photoUrl;

    @NotNull
    @Column(columnDefinition = "TIMESTAMP", nullable = false)
    private LocalDateTime time;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "user_id", nullable = false)
    private User user;

    @OneToMany(mappedBy = "post")
    private List<Like> likes;

    @OneToMany(mappedBy = "post")
    private List<Comment> comments;

    public Post(String description, String photoUrl, LocalDateTime time, User user) {
        this.description = description;
        this.photoUrl = photoUrl;
        this.time = time;
        this.user = user;
    }

    @Override
    public String toString() {
        return "Post{" +
                "id=" + id +
                ", description='" + description + '\'' +
                ", photoUrl='" + photoUrl + '\'' +
                ", time=" + time +
                ", user=" + user +
                '}';
    }
}
