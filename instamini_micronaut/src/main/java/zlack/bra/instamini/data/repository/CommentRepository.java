package zlack.bra.instamini.data.repository;

import io.micronaut.data.annotation.Query;
import io.micronaut.data.annotation.Repository;
import io.micronaut.data.repository.CrudRepository;
import io.micronaut.transaction.annotation.ReadOnly;
import jakarta.inject.Singleton;
import zlack.bra.instamini.data.entity.Comment;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Repository
public interface CommentRepository extends CrudRepository<Comment, Integer> {

    @Query("SELECT c FROM Comment as c WHERE c.post.id = :idPost ORDER BY c.time DESC")
    List<Comment> findAllCommentsOnPost(Integer idPost);
}
