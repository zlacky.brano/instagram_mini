package zlack.bra.instamini.business.service.util;

import jakarta.inject.Singleton;
import zlack.bra.instamini.data.entity.Comment;
import zlack.bra.instamini.data.entity.Like;
import zlack.bra.instamini.data.entity.Post;
import zlack.bra.instamini.data.repository.CommentRepository;
import zlack.bra.instamini.data.repository.LikeRepository;

import java.util.List;

@Singleton
public class PostChildrenDelete {

    private final CommentRepository commentRepository;
    private final LikeRepository likeRepository;

    public PostChildrenDelete(CommentRepository commentRepository, LikeRepository likeRepository) {
        this.commentRepository = commentRepository;
        this.likeRepository = likeRepository;
    }

    public void deleteChildren(Post post) {
        deleteLikes(post.getLikes());
        deleteComments(post.getComments());
    }

    private void deleteLikes(List<Like> likes) {
        for(Like like : likes) {
            likeRepository.deleteById(like.getId());
        }
    }

    private void deleteComments(List<Comment> comments) {
        for(Comment comment : comments) {
            commentRepository.deleteById(comment.getId());
        }
    }
}
