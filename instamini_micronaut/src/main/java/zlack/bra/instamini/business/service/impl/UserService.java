package zlack.bra.instamini.business.service.impl;

import co.elastic.clients.elasticsearch.ElasticsearchClient;
import co.elastic.clients.elasticsearch._types.query_dsl.PrefixQuery;
import co.elastic.clients.elasticsearch.core.SearchResponse;
import co.elastic.clients.elasticsearch.core.search.Hit;
import co.elastic.clients.json.jackson.JacksonJsonpMapper;
import co.elastic.clients.transport.ElasticsearchTransport;
import co.elastic.clients.transport.rest_client.RestClientTransport;
import io.micronaut.context.annotation.Value;
import io.micronaut.runtime.http.scope.RequestScope;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.HttpHost;
import org.elasticsearch.ElasticsearchException;
import org.elasticsearch.client.RestClient;
import zlack.bra.instamini.business.dto.UserCreateDTO;
import zlack.bra.instamini.business.dto.UserDTO;
import zlack.bra.instamini.business.exception.NotFoundException;
import zlack.bra.instamini.business.service.UserServiceInt;
import zlack.bra.instamini.business.service.util.UserChildrenDelete;
import zlack.bra.instamini.data.entity.*;
import zlack.bra.instamini.data.repository.*;

import javax.transaction.Transactional;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Slf4j
@RequestScope
public class UserService implements UserServiceInt {

    private final UserRepository userRepository;
    private final PostRepository postRepository;

    private final UserChildrenDelete userChildrenDelete;

    private final String usersIndex;

    private final ElasticsearchClient client;

    public UserService(UserRepository userRepository, PostRepository postRepository,
                       @Value("${elasticsearch-index}") String usersIndex,
                       @Value("${elasticsearch.httpHosts}") List<String> httpHosts,
                       UserChildrenDelete userChildrenDelete) {
        this.userRepository = userRepository;
        this.postRepository = postRepository;
        this.usersIndex = usersIndex;
        this.userChildrenDelete = userChildrenDelete;

        RestClient restClient = RestClient.builder(
                HttpHost.create(httpHosts.get(0))).build();
        ElasticsearchTransport transport = new RestClientTransport(
                restClient, new JacksonJsonpMapper());
        client = new ElasticsearchClient(transport);
    }

    @Override
    public List<UserDTO> searchUsers(String usernamePrefix) {
        log.info("Collecting users with elastic search by given prefix.");
        SearchResponse<UserDTO> searchResponse;
        try {
            searchResponse = client.search(rq -> rq
                            .index(usersIndex)
                            .query(q -> q.prefix(
                                            new PrefixQuery.Builder()
                                                    .field("username")
                                                    .value(usernamePrefix)
                                                    .caseInsensitive(true)
                                                    .build())
                            ),
                    UserDTO.class);
        } catch (IOException e) {
            log.warn("Exception in searching users with elastic search.");
            throw new ElasticsearchException(e);
        }

        List<UserDTO> result = new ArrayList<>();
        for (Hit<UserDTO> hit : searchResponse.hits().hits()) {
            result.add(hit.source());
        }

        return result;
    }

    @Override
    @Transactional
    public UserDTO editUsername(Integer id, String username) throws NotFoundException {
        Optional<User> userOptional = userRepository.findById(id);
        if (userOptional.isEmpty()) {
            log.warn("Trying to edit username to user, which does not exist.");
            throw new NotFoundException("No such user.");
        }

        User user = userOptional.get();
        user.setUsername(username);

        log.info("Username of user: " + userOptional.get().getEmail() + " is going to be edited.");

        return toDTO(user);
    }

    @Override
    public UserDTO create(UserCreateDTO userCreateDTO) {
        User persistedUser = userRepository.save(new User(userCreateDTO.getEmail(), userCreateDTO.getUsername(), null, null, null, null, null));

        log.info("User " + userCreateDTO.getEmail() + " has been created.");

        return toDTO(persistedUser);
    }

    @Override
    public List<UserDTO> findAll() {
        log.info("Collecting all users.");
        return StreamSupport.stream(userRepository.findAll().spliterator(), false).map(this::toDTO).collect(Collectors.toList());
    }

    @Override
    public Optional<UserDTO> findById(Integer id) {
        log.info("Finding one specified user by id.");
        return toDTO(userRepository.findById(id));
    }

    @Override
    public Optional<UserDTO> findByEmail(String email) {
        log.info("Finding one specified user by email.");
        return toDTO(userRepository.findByEmail(email));
    }

    @Override
    public UserDTO toDTO(User user) {
        return new UserDTO(user.getId(), user.getEmail(), user.getUsername());
    }

    @Override
    public Optional<UserDTO> toDTO(Optional<User> user) {
        if (user.isEmpty()) {
            return Optional.empty();
        }
        return Optional.of(toDTO(user.get()));
    }

    @Override
    @Transactional
    public void deleteById(Integer id) throws NotFoundException {
        Optional<User> userOptional = userRepository.findById(id);
        if (userOptional.isPresent()) {
            userChildrenDelete.deleteChildren(userOptional.get());
            userRepository.deleteById(id);
            log.info("User with id: " + id + " has been deleted.");
        } else {
            log.warn("Trying to delete user, that does not exist.");
            throw new NotFoundException("No such user.");
        }
    }

    @Override
    public List<UserDTO> findAllUsersWhoLikedPost(Integer idPost) throws NotFoundException {
        if (postRepository.existsById(idPost)) {
            log.info("Collecting all users, which liked post: " + idPost + ".");
            return userRepository.findAllUsersWhoLikedPost(idPost).stream().map(this::toDTO).collect(Collectors.toList());
        }
        log.warn("Trying to find all users, which liked post");
        throw new NotFoundException("No such post.");
    }

    @Override
    public List<UserDTO> findAllFollowers(Integer id) throws NotFoundException {
        if (!userRepository.existsById(id)) {
            throw new NotFoundException("No such user.");
        }
        log.info("Collecting all users, which follow user with id: " + id + ".");
        return userRepository.findAllFollowers(id).stream().map(this::toDTO).collect(Collectors.toList());
    }

    @Override
    public List<UserDTO> findAllFollowing(Integer id) throws NotFoundException {
        if (!userRepository.existsById(id)) {
            throw new NotFoundException("No such user.");
        }
        log.info("Collecting all users, that user with id: " + id + " follows.");
        return userRepository.findAllFollowing(id).stream().map(this::toDTO).collect(Collectors.toList());
    }

    @Override
    public Long findCountOfAllFollowers(Integer id) throws NotFoundException {
        if (!userRepository.existsById(id)) {
            throw new NotFoundException("No such user.");
        }
        log.info("Finding count of all users, which follow user with id: " + id + ".");
        return userRepository.findCountOfAllFollowers(id);
    }

    @Override
    public Long findCountOfAllFollowing(Integer id) throws NotFoundException {
        if (!userRepository.existsById(id)) {
            throw new NotFoundException("No such user.");
        }
        log.info("Finding count of all users, that user with id: " + id + " follows.");
        return userRepository.findCountOfAllFollowing(id);
    }
}