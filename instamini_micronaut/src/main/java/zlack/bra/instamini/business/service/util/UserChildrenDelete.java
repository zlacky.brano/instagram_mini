package zlack.bra.instamini.business.service.util;

import jakarta.inject.Singleton;
import zlack.bra.instamini.data.entity.*;
import zlack.bra.instamini.data.repository.CommentRepository;
import zlack.bra.instamini.data.repository.FollowRepository;
import zlack.bra.instamini.data.repository.LikeRepository;
import zlack.bra.instamini.data.repository.PostRepository;

import java.util.List;

@Singleton
public class UserChildrenDelete {

    private final PostChildrenDelete postChildrenDelete;
    private final FollowRepository followRepository;
    private final CommentRepository commentRepository;
    private final LikeRepository likeRepository;
    private final PostRepository postRepository;

    public UserChildrenDelete(PostChildrenDelete postChildrenDelete, FollowRepository followRepository,
                              CommentRepository commentRepository, LikeRepository likeRepository, PostRepository postRepository) {
        this.postChildrenDelete = postChildrenDelete;
        this.followRepository = followRepository;
        this.commentRepository = commentRepository;
        this.likeRepository = likeRepository;
        this.postRepository = postRepository;
    }

    public void deleteChildren(User user) {
        deleteComments(user.getComments());
        deleteFollows(user.getFollowed());
        deleteFollows(user.getFollowing());
        deletePosts(user.getPosts());
        deleteLikes(user.getLikes());
    }

    private void deletePosts(List<Post> posts) {
        for(Post post : posts) {
            postChildrenDelete.deleteChildren(post);
            postRepository.deleteById(post.getId());
        }
    }

    private void deleteFollows(List<Follow> follows) {
        for(Follow follow : follows) {
            followRepository.deleteById(follow.getId());
        }
    }

    private void deleteLikes(List<Like> likes) {
        for(Like like : likes) {
            likeRepository.deleteById(like.getId());
        }
    }

    private void deleteComments(List<Comment> comments) {
        for(Comment comment : comments) {
            commentRepository.deleteById(comment.getId());
        }
    }
}
