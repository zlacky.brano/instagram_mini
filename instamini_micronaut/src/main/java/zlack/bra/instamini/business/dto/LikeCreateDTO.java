package zlack.bra.instamini.business.dto;

import io.micronaut.core.annotation.Introspected;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@Introspected
@Data
@NoArgsConstructor
@AllArgsConstructor
public class LikeCreateDTO {

    @NotNull
    private Integer userId;

    @NotNull
    private Integer postId;

    public String toJson() {
        return "{" +
                "\"userId\": \"" + userId + "\"," +
                "\"postId\": \"" + postId + "\"" +
                "}";
    }
}
