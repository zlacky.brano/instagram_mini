package zlack.bra.instamini.business.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import static zlack.bra.instamini.constant.Constants.MAX_LENGTH_USERNAME_AND_EMAIL;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserCreateDTO {

    @Size(max = MAX_LENGTH_USERNAME_AND_EMAIL)
    @Email
    @NotBlank
    private String email;

    @Size(max = MAX_LENGTH_USERNAME_AND_EMAIL)
    @NotBlank
    private String username;
}
