package zlack.bra.instamini.business.dto;

import io.micronaut.core.annotation.Introspected;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import static zlack.bra.instamini.constant.Constants.MAX_LENGTH_DESCRIPTION_AND_TEXT;

@Introspected
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CommentCreateDTO {

    @Size(max = MAX_LENGTH_DESCRIPTION_AND_TEXT)
    @NotBlank
    private String text;

    @NotNull
    private Integer userId;

    @NotNull
    private Integer postId;

    public String toJson() {
        return "{" +
                "\"text\": \"" + text + "\"," +
                "\"userId\": \"" + userId + "\"," +
                "\"postId\": \"" + postId + "\"}";
    }
}
