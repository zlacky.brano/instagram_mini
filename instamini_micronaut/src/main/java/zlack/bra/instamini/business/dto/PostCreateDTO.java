package zlack.bra.instamini.business.dto;

import io.micronaut.core.annotation.Introspected;
import io.micronaut.http.multipart.CompletedFileUpload;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import static zlack.bra.instamini.constant.Constants.MAX_LENGTH_DESCRIPTION_AND_TEXT;

@Introspected
@Data
@NoArgsConstructor
@AllArgsConstructor
public class PostCreateDTO {

    @NotBlank
    @Size(max = MAX_LENGTH_DESCRIPTION_AND_TEXT)
    private String description;

    @NotNull
    private Integer userId;

    @NotNull
    private CompletedFileUpload image;
}
