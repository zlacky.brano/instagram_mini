package zlack.bra.instamini.business.service.impl;

import io.micronaut.runtime.http.scope.RequestScope;
import io.micronaut.validation.validator.Validator;
import lombok.extern.slf4j.Slf4j;
import zlack.bra.instamini.business.dto.PostCreateDTO;
import zlack.bra.instamini.business.dto.PostDTO;
import zlack.bra.instamini.business.exception.NotFoundException;
import zlack.bra.instamini.business.exception.UploadFailException;
import zlack.bra.instamini.business.service.ImageUploadService;
import zlack.bra.instamini.business.service.PostServiceInt;
import zlack.bra.instamini.business.service.util.PostChildrenDelete;
import zlack.bra.instamini.data.entity.Post;
import zlack.bra.instamini.data.entity.User;
import zlack.bra.instamini.data.repository.PostRepository;
import zlack.bra.instamini.data.repository.UserRepository;

import javax.transaction.Transactional;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Slf4j
@RequestScope
public class PostService implements PostServiceInt {

    private final PostRepository postRepository;
    private final UserRepository userRepository;
    private final UserService userService;
    private final ImageUploadService imageUploadService;

    private final Validator validator;

    private final PostChildrenDelete postChildrenDelete;

    public PostService(PostRepository postRepository, UserRepository userRepository,
                       UserService userService, ImageUploadService imageUploadService, Validator validator, PostChildrenDelete postChildrenDelete) {
        this.postRepository = postRepository;
        this.userRepository = userRepository;
        this.userService = userService;
        this.imageUploadService = imageUploadService;
        this.validator = validator;
        this.postChildrenDelete = postChildrenDelete;
    }

    @Override
    @Transactional
    public PostDTO editDescription(Integer id, String description) throws NotFoundException {
        Optional<Post> postOptional = postRepository.findById(id);
        if (postOptional.isEmpty()) {
            throw new NotFoundException("No such post.");
        }

        Post post = postOptional.get();

        post.setDescription(description);

        return toDTO(post);
    }

    @Override
    public PostDTO create(PostCreateDTO postCreateDTO) throws NotFoundException, UploadFailException {
        validatePostCreateDTO(postCreateDTO);

        Optional<User> optionalUser = userRepository.findById(postCreateDTO.getUserId());
        String photoUrl;

        if (optionalUser.isEmpty()) {
            log.warn("Trying to create post, which should belong to user, which does not exist.");
            throw new NotFoundException("No such user.");
        }

        try {
            if (postCreateDTO.getImage() == null) {
                log.warn("While trying to create post, there was photo file null.");
                throw new UploadFailException("Photo file is null.");
            }
            photoUrl = imageUploadService.upload(postCreateDTO.getImage().getInputStream());
        } catch (IOException e) {
            log.warn("While uploading photo on Cloudinary, there was an error: \n" + e);
            throw new UploadFailException(e.getMessage());
        }

        if (photoUrl == null) {
            log.warn("After successful upload URL of the photo is null.");
            throw new UploadFailException("Photo URL is null after upload.");
        }

        Post persistedPost = postRepository.save(new Post(postCreateDTO.getDescription(), photoUrl, LocalDateTime.of(LocalDate.now(), LocalTime.now()), optionalUser.get()));

        log.info("Post, which belongs to user: " + optionalUser.get().getEmail() + " has been created.");

        return toDTO(persistedPost);
    }

    // Manual validation of postCreateDTO
    private void validatePostCreateDTO(PostCreateDTO postCreateDTO) {
        final Set<ConstraintViolation<PostCreateDTO>> constraintViolations = validator.validate(postCreateDTO);
        if (!constraintViolations.isEmpty()) {
            throw new ConstraintViolationException(constraintViolations);
        }
    }

    @Override
    public List<PostDTO> findAll() {
        log.info("Collecting all posts.");
        return StreamSupport.stream(postRepository.findAll().spliterator(), false).map(this::toDTO).collect(Collectors.toList());
    }

    @Override
    public Optional<PostDTO> findById(Integer id) {
        log.info("Finding one specified post.");
        return toDTO(postRepository.findById(id));
    }

    @Override
    public PostDTO toDTO(Post post) {
        return new PostDTO(post.getId(), post.getDescription(), post.getPhotoUrl(), post.getTime(), userService.toDTO(post.getUser()));
    }

    @Override
    public Optional<PostDTO> toDTO(Optional<Post> post) {
        if (post.isEmpty()) {
            return Optional.empty();
        }
        return Optional.of(toDTO(post.get()));
    }

    @Override
    @Transactional
    public void deleteById(Integer id) throws NotFoundException {
        Optional<Post> postOptional = postRepository.findById(id);
        if (postOptional.isPresent()) {
            postChildrenDelete.deleteChildren(postOptional.get());
            postRepository.deleteById(id);
            log.info("Post with id: " + id + " has been deleted.");
        }
        else {
            log.warn("Trying to delete post, which does not exist.");
            throw new NotFoundException("No such post.");
        }
    }

    @Override
    public List<PostDTO> findAllPostsOfUser(Integer idUser) throws NotFoundException {
        if (userRepository.existsById(idUser)) {
            log.info("Collecting all posts of user with id: " + idUser + ".");
            return postRepository.findAllPostsOfUser(idUser).stream().map(this::toDTO).collect(Collectors.toList());
        }
        log.warn("Trying to find posts of user, which does not exist.");
        throw new NotFoundException("No such user.");
    }

    @Override
    public Long findCountOfAllPostsOfUser(Integer idUser) throws NotFoundException {
        if (userRepository.existsById(idUser)) {
            log.info("Finding count of posts of user with id: " + idUser + ".");
            return postRepository.findCountOfAllPostsOfUser(idUser);
        }
        log.warn("Trying to find count of posts of user, which does not exist.");
        throw new NotFoundException("No such user.");
    }
}
