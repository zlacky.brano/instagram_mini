package zlack.bra.instamini.business.service.impl;

import io.micronaut.runtime.http.scope.RequestScope;
import lombok.extern.slf4j.Slf4j;
import zlack.bra.instamini.business.dto.CommentCreateDTO;
import zlack.bra.instamini.business.dto.CommentDTO;
import zlack.bra.instamini.business.exception.NotFoundException;
import zlack.bra.instamini.business.service.CommentServiceInt;
import zlack.bra.instamini.data.entity.Comment;
import zlack.bra.instamini.data.entity.Post;
import zlack.bra.instamini.data.entity.User;
import zlack.bra.instamini.data.repository.CommentRepository;
import zlack.bra.instamini.data.repository.PostRepository;
import zlack.bra.instamini.data.repository.UserRepository;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Slf4j
@RequestScope
public class CommentService implements CommentServiceInt {

    private final CommentRepository commentRepository;
    private final UserService userService;
    private final PostService postService;
    private final PostRepository postRepository;
    private final UserRepository userRepository;

    public CommentService(CommentRepository commentRepository, UserService userService, PostService postService, PostRepository postRepository, UserRepository userRepository) {
        this.commentRepository = commentRepository;
        this.userService = userService;
        this.postService = postService;
        this.postRepository = postRepository;
        this.userRepository = userRepository;
    }

    @Override
    @Transactional
    public CommentDTO editText(Integer id, String text) throws NotFoundException {
        Optional<Comment> optionalComment = commentRepository.findById(id);
        if (optionalComment.isEmpty()) {
            log.warn("Trying to edit text on comment, which does not exist.");
            throw new NotFoundException("No such comment.");
        }

        Comment comment = optionalComment.get();
        comment.setText(text);

        log.info("Text of comment with id: " + comment.getId() + " has been edited.");
        return toDTO(comment);
    }

    @Override
    public CommentDTO create(CommentCreateDTO commentCreateDTO) throws NotFoundException {
        Optional<Post> optionalPost = postRepository.findById(commentCreateDTO.getPostId());
        Optional<User> optionalUser = userRepository.findById(commentCreateDTO.getUserId());
        if (optionalPost.isEmpty()) {
            log.warn("Trying to create comment on post, which does not exist.");
            throw new NotFoundException("No such post.");
        } else if (optionalUser.isEmpty()) {
            log.warn("Trying to create comment with user, which does not exist.");
            throw new NotFoundException("No such user.");
        }

        Comment persistedComment = commentRepository.save(new Comment(commentCreateDTO.getText(), LocalDateTime.of(LocalDate.now(), LocalTime.now()), optionalUser.get(), optionalPost.get()));

        log.info("Comment of user: " + optionalUser.get().getEmail() +
                " has been created on post: " + optionalPost.get().getId() + ".");

        return toDTO(persistedComment);
    }

    @Override
    public List<CommentDTO> findAll() {
        log.info("Collecting all comments.");
        return StreamSupport.stream(commentRepository.findAll().spliterator(), false).map(this::toDTO).collect(Collectors.toList());
    }

    @Override
    public Optional<CommentDTO> findById(Integer id) {
        log.info("Finding one specified comment.");
        return toDTO(commentRepository.findById(id));
    }

    @Override
    public CommentDTO toDTO(Comment comment) {
        return new CommentDTO(comment.getId(), comment.getText(), comment.getTime(), userService.toDTO(comment.getUser()), postService.toDTO(comment.getPost()));
    }

    @Override
    public Optional<CommentDTO> toDTO(Optional<Comment> comment) {
        if (comment.isEmpty()) {
            return Optional.empty();
        }
        return Optional.of(toDTO(comment.get()));
    }

    @Override
    @Transactional
    public void deleteById(Integer id) throws NotFoundException {
        if (commentRepository.existsById(id)) {
            commentRepository.deleteById(id);
            log.info("Comment with id: " + id + " has been deleted.");
        }
        else {
            log.warn("Trying to delete comment, which is not in database.");
            throw new NotFoundException("No such comment.");
        }
    }

    @Override
    public List<CommentDTO> findAllCommentsOnPost(Integer idPost) throws NotFoundException {
        if (postRepository.existsById(idPost)) {
            log.info("Collecting all comments on post with id: " + idPost + ".");
            return commentRepository.findAllCommentsOnPost(idPost).stream().map(this::toDTO).collect(Collectors.toList());
        }
        log.warn("Trying to find comments on post, which does not exist.");
        throw new NotFoundException("No such post.");
    }
}
