#!/bin/bash

rm -f ./docker-compose.yml

if [ -z "$1" ]; then
   echo "You have to choose between micronat quarkus or spring! Add it after script call as argument."
elif [ "$1" == "micronaut" ]; then
   cp ./docker_compose/docker-compose-micronaut.yml ./docker-compose.yml
elif [ "$1" == "quarkus" ]; then
   cp ./docker_compose/docker-compose-quarkus.yml ./docker-compose.yml
elif [ "$1" == "spring" ]; then
   cp ./docker_compose/docker-compose-spring.yml ./docker-compose.yml
else
   echo "You have to choose between micronat quarkus or spring! Add it after script call as argument."
fi
