package util;

import org.mockito.Mockito;
import zlack.bra.instamini.business.dto.UserDTO;
import zlack.bra.instamini.security.jwt.JwtUtil;

public class MockJwtAuthenticationUtil {

    private final JwtUtil jwtUtil;

    private final String mockedToken;

    private final UserDTO mockedLoggedUserDTO;

    public MockJwtAuthenticationUtil(JwtUtil jwtUtil, UserDTO mockedLoggedUserDTO, String token) {
        this.jwtUtil = jwtUtil;
        this.mockedLoggedUserDTO = mockedLoggedUserDTO;
        this.mockedToken = token;
    }

    public void mockReturns() {
        Mockito.when(jwtUtil.validateToken(mockedToken)).thenReturn(true);
        Mockito.when(jwtUtil.extractEmail(mockedToken)).thenReturn(mockedLoggedUserDTO.getEmail());
    }

    public void verify(int totalValidations) {
        Mockito.verify(jwtUtil, Mockito.times(totalValidations)).validateToken(mockedToken);
        Mockito.verify(jwtUtil, Mockito.times(totalValidations)).extractEmail(mockedToken);
    }

    public String getMockedToken() {
        return mockedToken;
    }
}
