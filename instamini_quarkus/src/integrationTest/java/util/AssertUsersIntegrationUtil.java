package util;

import io.restassured.path.json.JsonPath;
import org.apache.commons.lang3.ObjectUtils;
import org.junit.jupiter.api.Assertions;
import zlack.bra.instamini.business.dto.UserCreateDTO;
import zlack.bra.instamini.business.dto.UserDTO;


public class AssertUsersIntegrationUtil {

    public static void assertNestedUsers(JsonPath jsonPath, String partPath, UserDTO user) {
        Assertions.assertEquals(user.getId(), jsonPath.get(partPath + ".id"));
        Assertions.assertEquals(user.getUsername(), jsonPath.get(partPath +".username"));
        Assertions.assertEquals(user.getEmail(), jsonPath.get(partPath + ".email"));
    }

    public static void assertUsers(String result, UserDTO user, int indexInResult) {
        JsonPath jsonPath = new JsonPath(result);

        Assertions.assertEquals(user.getId(), jsonPath.get("[" + indexInResult + "].id"));
        Assertions.assertEquals(user.getEmail(), jsonPath.get("[" + indexInResult + "].email"));
        Assertions.assertEquals(user.getUsername(), jsonPath.get("[" + indexInResult + "].username"));
    }

    public static void assertUsers(String result, UserDTO user) {
        JsonPath jsonPath = new JsonPath(result);

        Assertions.assertEquals(user.getId(), jsonPath.get("id"));
        Assertions.assertEquals(user.getEmail(), jsonPath.get("email"));
        Assertions.assertEquals(user.getUsername(), jsonPath.get("username"));
    }

    public static void assertUsers(String result, UserCreateDTO user) {
        JsonPath jsonPath = new JsonPath(result);

        Assertions.assertFalse(ObjectUtils.isEmpty(jsonPath.get("id")));
        Assertions.assertEquals(user.getEmail(), jsonPath.get("email"));
        Assertions.assertEquals(user.getUsername(), jsonPath.get("username"));
    }

    public static void assertUsersInEditUsernameTest(String result, UserDTO user, String newUsername) {
        JsonPath jsonPath = new JsonPath(result);

        Assertions.assertEquals(user.getId(), jsonPath.get("id"));
        Assertions.assertEquals(user.getEmail(), jsonPath.get("email"));
        Assertions.assertEquals(newUsername, jsonPath.get("username"));
    }
}
