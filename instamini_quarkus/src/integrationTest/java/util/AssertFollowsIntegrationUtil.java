package util;

import io.restassured.path.json.JsonPath;
import org.apache.commons.lang3.ObjectUtils;
import org.junit.jupiter.api.Assertions;
import zlack.bra.instamini.business.dto.*;


public class AssertFollowsIntegrationUtil {

    public static void assertFollows(String result, FollowDTO follow, int indexInResult) {
        JsonPath jsonPath = new JsonPath(result);

        Assertions.assertEquals(follow.getId(), jsonPath.get("[" + indexInResult + "].id"));
        AssertUsersIntegrationUtil.assertNestedUsers(jsonPath, "[" + indexInResult + "].userWho", follow.getUserWho());
        AssertUsersIntegrationUtil.assertNestedUsers(jsonPath, "[" + indexInResult + "].userWhom", follow.getUserWhom());
    }

    public static void assertFollows(String result, FollowDTO follow) {
        JsonPath jsonPath = new JsonPath(result);

        Assertions.assertEquals(follow.getId(), jsonPath.get("id"));
        AssertUsersIntegrationUtil.assertNestedUsers(jsonPath, "userWho", follow.getUserWho());
        AssertUsersIntegrationUtil.assertNestedUsers(jsonPath, "userWhom", follow.getUserWhom());
    }

    public static void assertFollows(String result, FollowCreateDTO followCreateDTO) {
        JsonPath jsonPath = new JsonPath(result);

        Assertions.assertFalse(ObjectUtils.isEmpty(jsonPath.get("id")));
        Assertions.assertEquals(jsonPath.get("userWho.id"), followCreateDTO.getUserWhoId());
        Assertions.assertEquals(jsonPath.get("userWhom.id"), followCreateDTO.getUserWhomId());
    }
}
