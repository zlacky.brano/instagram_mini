package util;

import io.restassured.path.json.JsonPath;
import org.apache.commons.lang3.ObjectUtils;
import org.junit.jupiter.api.Assertions;
import zlack.bra.instamini.business.dto.PostCreateDTO;
import zlack.bra.instamini.business.dto.PostDTO;
import zlack.bra.instamini.business.dto.UserCreateDTO;
import zlack.bra.instamini.business.dto.UserDTO;

public class AssertPostsIntegrationUtil {

    public static void assertNestedPosts(JsonPath jsonPath, String partPath, PostDTO post) {
        Assertions.assertEquals(post.getId(), jsonPath.get(partPath + ".id"));
        Assertions.assertEquals(post.getTime().toString(), jsonPath.get(partPath + ".time"));
        Assertions.assertEquals(post.getDescription(), jsonPath.get(partPath + ".description"));
        Assertions.assertEquals(post.getPhotoUrl(), jsonPath.get(partPath + ".photoUrl"));
        AssertUsersIntegrationUtil.assertNestedUsers(jsonPath, partPath + ".user", post.getUser());
    }

    public static void assertPosts(String result, PostDTO post, int indexInResult) {
        JsonPath jsonPath = new JsonPath(result);

        Assertions.assertEquals(post.getId(), jsonPath.get("[" + indexInResult + "].id"));
        Assertions.assertEquals(post.getPhotoUrl(), jsonPath.get("[" + indexInResult + "].photoUrl"));
        Assertions.assertEquals(post.getTime().toString(), jsonPath.get("[" + indexInResult + "].time"));
        Assertions.assertEquals(post.getDescription(), jsonPath.get("[" + indexInResult + "].description"));
        AssertUsersIntegrationUtil.assertNestedUsers(jsonPath, "[" + indexInResult + "].user", post.getUser());
    }

    public static void assertPosts(String result, PostDTO post) {
        JsonPath jsonPath = new JsonPath(result);

        Assertions.assertEquals(post.getId(), jsonPath.get("id"));
        Assertions.assertEquals(post.getPhotoUrl(), jsonPath.get("photoUrl"));
        Assertions.assertEquals(post.getTime().toString(), jsonPath.get("time"));
        Assertions.assertEquals(post.getDescription(), jsonPath.get("description"));
        AssertUsersIntegrationUtil.assertNestedUsers(jsonPath, "user", post.getUser());

    }

    public static void assertPosts(String result, PostCreateDTO post) {
        JsonPath jsonPath = new JsonPath(result);

        Assertions.assertFalse(ObjectUtils.isEmpty(jsonPath.get("id")));
        Assertions.assertFalse(ObjectUtils.isEmpty(jsonPath.get("photoUrl")));
        Assertions.assertFalse(ObjectUtils.isEmpty(jsonPath.get("time")));
        Assertions.assertEquals(post.getDescription(), jsonPath.get("description"));
        Assertions.assertEquals(post.getUserId(), jsonPath.get("user.id"));
    }

    public static void assertPostsInEditDescriptionTest(String result, PostDTO post, String newDescription) {
        JsonPath jsonPath = new JsonPath(result);

        Assertions.assertEquals(post.getId(), jsonPath.get("id"));
        Assertions.assertEquals(post.getPhotoUrl(), jsonPath.get("photoUrl"));
        Assertions.assertEquals(post.getTime().toString(), jsonPath.get("time"));
        Assertions.assertEquals(newDescription, jsonPath.get("description"));
        AssertUsersIntegrationUtil.assertNestedUsers(jsonPath, "user", post.getUser());
    }
}
