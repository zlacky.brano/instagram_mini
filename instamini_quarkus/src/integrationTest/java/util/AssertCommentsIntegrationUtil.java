package util;

import io.restassured.path.json.JsonPath;
import org.apache.commons.lang3.ObjectUtils;
import org.junit.jupiter.api.Assertions;
import zlack.bra.instamini.business.dto.CommentCreateDTO;
import zlack.bra.instamini.business.dto.CommentDTO;

public class AssertCommentsIntegrationUtil {

    public static void assertComments(String result, CommentDTO comment, int indexInResult) {
        JsonPath jsonPath = new JsonPath(result);

        Assertions.assertEquals(comment.getId(), jsonPath.get("[" + indexInResult + "].id"));
        Assertions.assertEquals(comment.getText(), jsonPath.get("[" + indexInResult + "].text"));
        Assertions.assertEquals(comment.getTime().toString(), jsonPath.get("[" + indexInResult + "].time"));
        AssertUsersIntegrationUtil.assertNestedUsers(jsonPath, "[" + indexInResult + "].user", comment.getUser());
        AssertPostsIntegrationUtil.assertNestedPosts(jsonPath, "[" + indexInResult + "].post", comment.getPost());
    }

    public static void assertComments(String result, CommentDTO comment) {
        JsonPath jsonPath = new JsonPath(result);

        Assertions.assertEquals(comment.getId(), jsonPath.get("id"));
        Assertions.assertEquals(comment.getText(), jsonPath.get("text"));
        Assertions.assertEquals(comment.getTime().toString(), jsonPath.get("time"));
        AssertUsersIntegrationUtil.assertNestedUsers(jsonPath, "user", comment.getUser());
        AssertPostsIntegrationUtil.assertNestedPosts(jsonPath, "post", comment.getPost());
    }

    public static void assertComments(String result, CommentCreateDTO comment) {
        JsonPath jsonPath = new JsonPath(result);

        Assertions.assertFalse(ObjectUtils.isEmpty(jsonPath.get("id")));
        Assertions.assertEquals(comment.getText(), jsonPath.get("text"));
        Assertions.assertFalse(ObjectUtils.isEmpty(jsonPath.get("time")));
        Assertions.assertEquals(jsonPath.get("user.id"), comment.getUserId());
        Assertions.assertEquals(jsonPath.get("post.id"), comment.getPostId());
    }

    public static void assertCommentsInEditTextTest(String result, CommentDTO comment, String newText) {
        JsonPath jsonPath = new JsonPath(result);

        Assertions.assertEquals(comment.getId(), jsonPath.get("id"));
        Assertions.assertEquals(newText, jsonPath.get("text"));
        Assertions.assertEquals(comment.getTime().toString(), jsonPath.get("time"));
        AssertUsersIntegrationUtil.assertNestedUsers(jsonPath, "user", comment.getUser());
        AssertPostsIntegrationUtil.assertNestedPosts(jsonPath, "post", comment.getPost());
    }

}/*

    public static void assertComments(ResultActions result, CommentDTO comment) throws Exception {
        JSONObject json = new JSONObject(result.andReturn().getResponse().getContentAsString());
        JSONObject postObject = json.getJSONObject("post");
        JSONObject userOfPostObject = postObject.getJSONObject("user");

        Assertions.assertEquals(postObject.get("id"), comment.getPost().getId());
        Assertions.assertEquals(postObject.get("photoUrl"), comment.getPost().getPhotoUrl());
        Assertions.assertEquals(postObject.get("description"), comment.getPost().getDescription());
        Assertions.assertEquals(postObject.get("time"), comment.getPost().getTime().toString());

        Assertions.assertEquals(userOfPostObject.get("id"), comment.getPost().getUser().getId());
        Assertions.assertEquals(userOfPostObject.get("username"), comment.getPost().getUser().getUsername());
        Assertions.assertEquals(userOfPostObject.get("email"), comment.getPost().getUser().getEmail());

        result.andExpect(jsonPath("$.id").value(comment.getId()))
                .andExpect(jsonPath("$.text").value(comment.getText()))
                .andExpect(jsonPath("$.time").value(comment.getTime().toString()))
                .andExpect(jsonPath("$.user").value(comment.getUser()));
    }

    public static void assertComments(ResultActions result, CommentCreateDTO commentCreateDTO) throws Exception {
        result.andExpect(jsonPath("$.id").isNotEmpty())
                .andExpect(jsonPath("$.text").value(commentCreateDTO.getText()))
                .andExpect(jsonPath("$.time").isNotEmpty())
                .andExpect(jsonPath("$.user.id").value(commentCreateDTO.getUserId()))
                .andExpect(jsonPath("$.post.id").value(commentCreateDTO.getPostId()));
    }

    public static void assertPostsInEditTextTest(ResultActions result, CommentDTO comment, String newText) throws Exception {
        JSONObject json = new JSONObject(result.andReturn().getResponse().getContentAsString());
        JSONObject postObject = json.getJSONObject("post");
        JSONObject userOfPostObject = postObject.getJSONObject("user");

        Assertions.assertEquals(postObject.get("id"), comment.getPost().getId());
        Assertions.assertEquals(postObject.get("photoUrl"), comment.getPost().getPhotoUrl());
        Assertions.assertEquals(postObject.get("description"), comment.getPost().getDescription());
        Assertions.assertEquals(postObject.get("time"), comment.getPost().getTime().toString());

        Assertions.assertEquals(userOfPostObject.get("id"), comment.getPost().getUser().getId());
        Assertions.assertEquals(userOfPostObject.get("username"), comment.getPost().getUser().getUsername());
        Assertions.assertEquals(userOfPostObject.get("email"), comment.getPost().getUser().getEmail());

        result.andExpect(jsonPath("$.id").value(comment.getId()))
                .andExpect(jsonPath("$.text").value(newText))
                .andExpect(jsonPath("$.time").value(comment.getTime().toString()))
                .andExpect(jsonPath("$.user").value(comment.getUser()));
    }
}
*/
