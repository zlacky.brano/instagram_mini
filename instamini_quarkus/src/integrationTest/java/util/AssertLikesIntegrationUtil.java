package util;

import io.restassured.path.json.JsonPath;
import org.apache.commons.lang3.ObjectUtils;
import org.junit.jupiter.api.Assertions;
import zlack.bra.instamini.business.dto.*;

public class AssertLikesIntegrationUtil {

    public static void assertLikes(String result, LikeDTO like, int indexInResult) {
        JsonPath jsonPath = new JsonPath(result);

        Assertions.assertEquals(like.getId(), jsonPath.get("[" + indexInResult + "].id"));
        AssertUsersIntegrationUtil.assertNestedUsers(jsonPath, "[" + indexInResult + "].user", like.getUser());
        AssertPostsIntegrationUtil.assertNestedPosts(jsonPath, "[" + indexInResult + "].post", like.getPost());
    }

    public static void assertLikes(String result, LikeDTO like) {
        JsonPath jsonPath = new JsonPath(result);

        Assertions.assertEquals(like.getId(), jsonPath.get("id"));
        AssertUsersIntegrationUtil.assertNestedUsers(jsonPath, "user", like.getUser());
        AssertPostsIntegrationUtil.assertNestedPosts(jsonPath, "post", like.getPost());
    }

    public static void assertLikes(String result, LikeCreateDTO like) {
        JsonPath jsonPath = new JsonPath(result);

        Assertions.assertFalse(ObjectUtils.isEmpty(jsonPath.get("id")));
        Assertions.assertEquals(jsonPath.get("user.id"), like.getUserId());
        Assertions.assertEquals(jsonPath.get("post.id"), like.getPostId());
    }
}
