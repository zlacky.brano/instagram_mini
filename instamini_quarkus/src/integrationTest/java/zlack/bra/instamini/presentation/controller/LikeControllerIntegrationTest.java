package zlack.bra.instamini.presentation.controller;

import com.radcortez.flyway.test.annotation.DataSource;
import com.radcortez.flyway.test.annotation.FlywayTest;
import io.quarkus.test.common.QuarkusTestResource;
import io.quarkus.test.h2.H2DatabaseTestResource;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.junit.mockito.InjectMock;
import io.restassured.RestAssured;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import util.AssertLikesIntegrationUtil;
import util.MockJwtAuthenticationUtil;
import util.db.QuarkusDataSourceProvider;
import zlack.bra.instamini.business.dto.*;
import zlack.bra.instamini.security.jwt.JwtUtil;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;


@QuarkusTest
@QuarkusTestResource(H2DatabaseTestResource.class)
@FlywayTest(@DataSource(QuarkusDataSourceProvider.class))
class LikeControllerIntegrationTest {

    private MockJwtAuthenticationUtil mockJwtAuthenticationUtil;

    @InjectMock
    private JwtUtil jwtUtil;

    private final UserDTO loggedUserDTOExample = new UserDTO(10, "olivia.johnson@test.com", "Olivia Johnson");

    private final UserDTO notLoggedUserDTOExample = new UserDTO(6, "thomas.wilson@test.com", "WilT");

    private final List<LikeDTO> likesInDatabase = new ArrayList<>() {
        {
            add(new LikeDTO(1, loggedUserDTOExample,
                    new PostDTO(3, "In est risus, auctor sed, tristique in, tempus sit amet, sem. Felis sed interdum venenatis, turpis enim blandit mi, in porttitor pede justo eu massa. Donec dapibus. Duis at velit eu est congue elementum.",
                            "https://res.cloudinary.com/instamini/image/upload/v1630672583/pug-690566_1280_s4wmbu.jpg",
                            LocalDateTime.of(2021, 5, 25, 12, 12, 22),
                            new UserDTO(2, "george.jones@test.com", "_Georgie_"))));
            add(new LikeDTO(2, notLoggedUserDTOExample,
                    new PostDTO(3, "In est risus, auctor sed, tristique in, tempus sit amet, sem. Felis sed interdum venenatis, turpis enim blandit mi, in porttitor pede justo eu massa. Donec dapibus. Duis at velit eu est congue elementum.",
                            "https://res.cloudinary.com/instamini/image/upload/v1630672583/pug-690566_1280_s4wmbu.jpg",
                            LocalDateTime.of(2021, 5, 25, 12, 12, 22),
                            new UserDTO(2, "george.jones@test.com", "_Georgie_"))));
            add(new LikeDTO(3, new UserDTO(1, "john.smith@test.com", "JohnyS"),
                    new PostDTO(2, "Nullam orci pede, venenatis non, sodales sed, tincidunt eu, felis. Fusce posuere felis sed lacus. Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl. Nunc rhoncus dui vel sem. Sed sagittis.",
                            "https://res.cloudinary.com/instamini/image/upload/v1630672584/dolphin-203875_1280_n67crm.jpg",
                            LocalDateTime.of(2020, 12, 20, 7, 51, 2),
                            loggedUserDTOExample)));
            add(new LikeDTO(4, new UserDTO(5, "charlie.smith@test.com", ":Charlie:"),
                    new PostDTO(4, "Nulla ac enim. In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem.",
                            "https://res.cloudinary.com/instamini/image/upload/v1630672586/husky-3380548_1280_rman9o.jpg",
                            LocalDateTime.of(2021, 2, 6, 16, 15, 7),
                            loggedUserDTOExample)));
        }
    };


    @BeforeEach
    void setUp() {
        mockJwtAuthenticationUtil = new MockJwtAuthenticationUtil(jwtUtil, loggedUserDTOExample, "exists");
    }

    @Test
    void all401() {
        RestAssured.given().when().get("/likes").then().statusCode(401);

        mockJwtAuthenticationUtil.verify(0);
    }

    @Test
    void all200() {
        mockJwtAuthenticationUtil.mockReturns();

        String result = RestAssured.given()
                    .header("Authorization", "Bearer " + mockJwtAuthenticationUtil.getMockedToken())
                .when()
                    .get("/likes")
                .then()
                    .statusCode(200)
                    .contentType("application/json")
                    .extract().body().asString();

        for (int i = 0; i < likesInDatabase.size(); i++) {
            AssertLikesIntegrationUtil.assertLikes(result, likesInDatabase.get(i), i);
        }

        mockJwtAuthenticationUtil.verify(1);
    }

    @Test
    void byID401() {
        LikeDTO likeDTOToFind = likesInDatabase.get(0);

        RestAssured.given()
                .when()
                    .get("/likes/{id}", likeDTOToFind.getId())
                .then()
                    .statusCode(401);

        mockJwtAuthenticationUtil.verify(0);
    }

    @Test
    void byID404() {
        mockJwtAuthenticationUtil.mockReturns();

        RestAssured.given()
                    .header("Authorization", "Bearer " + mockJwtAuthenticationUtil.getMockedToken())
                .when()
                    .get("/likes/{id}", 0)
                .then()
                    .statusCode(404);

        mockJwtAuthenticationUtil.verify(1);
    }

    @Test
    void byID200() {
        LikeDTO likeDTOToFind = likesInDatabase.get(0);

        mockJwtAuthenticationUtil.mockReturns();

        String result = RestAssured.given()
                    .header("Authorization", "Bearer " + mockJwtAuthenticationUtil.getMockedToken())
                .when()
                    .get("/likes/{id}", likeDTOToFind.getId())
                .then()
                    .statusCode(200)
                    .contentType("application/json")
                    .extract().body().asString();

        AssertLikesIntegrationUtil.assertLikes(result, likeDTOToFind);

        mockJwtAuthenticationUtil.verify(1);
    }

    @Test
    void create401() {
        LikeCreateDTO likeCreateDTO = new LikeCreateDTO(notLoggedUserDTOExample.getId(), likesInDatabase.get(2).getPost().getId());

        RestAssured.given()
                    .contentType("application/json")
                    .body(likeCreateDTO.toJson())
                .when()
                    .post("/likes")
                .then()
                    .statusCode(401);

        mockJwtAuthenticationUtil.verify(0);
    }

    @Test
    void create403() {
        LikeCreateDTO likeCreateDTO = new LikeCreateDTO(notLoggedUserDTOExample.getId(), likesInDatabase.get(2).getPost().getId());

        mockJwtAuthenticationUtil.mockReturns();

        RestAssured.given()
                    .header("Authorization", "Bearer " + mockJwtAuthenticationUtil.getMockedToken())
                    .contentType("application/json")
                    .body(likeCreateDTO.toJson())
                .when()
                    .post("/likes")
                .then()
                    .statusCode(403);

        mockJwtAuthenticationUtil.verify(1);
    }

    @Test
    void create404() {
        LikeCreateDTO likeCreateDTO = new LikeCreateDTO(loggedUserDTOExample.getId(), 0);

        mockJwtAuthenticationUtil.mockReturns();

        RestAssured.given()
                    .header("Authorization", "Bearer " + mockJwtAuthenticationUtil.getMockedToken())
                    .contentType("application/json")
                    .body(likeCreateDTO.toJson())
                .when()
                    .post("/likes")
                .then()
                    .statusCode(404);

        mockJwtAuthenticationUtil.verify(1);
    }

    @Test
    void create201() {
        LikeCreateDTO likeCreateDTO = new LikeCreateDTO(loggedUserDTOExample.getId(), likesInDatabase.get(2).getPost().getId());

        mockJwtAuthenticationUtil.mockReturns();

        String result = RestAssured.given()
                    .header("Authorization", "Bearer " + mockJwtAuthenticationUtil.getMockedToken())
                    .contentType("application/json")
                    .body(likeCreateDTO.toJson())
                .when()
                    .post("/likes")
                .then()
                    .statusCode(201)
                    .contentType("application/json")
                    .extract().body().asString();

        AssertLikesIntegrationUtil.assertLikes(result, likeCreateDTO);

        mockJwtAuthenticationUtil.verify(1);
    }

    @Test
    void create400Valid() {
        LikeCreateDTO likeCreateDTO = new LikeCreateDTO(loggedUserDTOExample.getId(), likesInDatabase.get(2).getPost().getId());

        mockJwtAuthenticationUtil.mockReturns();

        RestAssured.given()
                    .header("Authorization", "Bearer " + mockJwtAuthenticationUtil.getMockedToken())
                    .contentType("application/json")
                    .body("{\"userId\":\"" + likeCreateDTO.getUserId() + "\"}") // missing body
                .when()
                    .post("/likes")
                .then()
                    .statusCode(400);

        mockJwtAuthenticationUtil.verify(1);
    }

    @Test
    void create400Unique() {
        LikeCreateDTO likeCreateDTO = new LikeCreateDTO(loggedUserDTOExample.getId(), likesInDatabase.get(0).getPost().getId());

        mockJwtAuthenticationUtil.mockReturns();

        RestAssured.given()
                    .header("Authorization", "Bearer " + mockJwtAuthenticationUtil.getMockedToken())
                    .contentType("application/json")
                    .body(likeCreateDTO.toJson())
                .when()
                    .post("/likes")
                .then()
                    .statusCode(400);

        mockJwtAuthenticationUtil.verify(1);
    }

    @Test
    void delete401() {
        RestAssured.given()
                .when()
                    .delete("/likes/{id}", likesInDatabase.get(0).getId())
                .then()
                    .statusCode(401);

        mockJwtAuthenticationUtil.verify(0);
    }

    @Test
    void delete403() {
        mockJwtAuthenticationUtil.mockReturns();

        RestAssured.given()
                    .header("Authorization", "Bearer " + mockJwtAuthenticationUtil.getMockedToken())
                .when()
                    .delete("/likes/{id}", likesInDatabase.get(3).getId())
                .then()
                    .statusCode(403);

        mockJwtAuthenticationUtil.verify(1);
    }

    @Test
    void delete404() {
        mockJwtAuthenticationUtil.mockReturns();

        RestAssured.given()
                    .header("Authorization", "Bearer " + mockJwtAuthenticationUtil.getMockedToken())
                .when()
                    .delete("/likes/{id}", 0)
                .then()
                    .statusCode(404);

        mockJwtAuthenticationUtil.verify(1);
    }

    @Test
    void delete204() {
        mockJwtAuthenticationUtil.mockReturns();

        RestAssured.given()
                    .header("Authorization", "Bearer " + mockJwtAuthenticationUtil.getMockedToken())
                .when()
                    .delete("/likes/{id}", likesInDatabase.get(0).getId())
                .then()
                    .statusCode(204);

        mockJwtAuthenticationUtil.verify(1);
    }

    @Test
    void findNumberOfLikesOnPost401() {
        RestAssured.given()
                    .param("post", "3")
                .when()
                    .get("/likes/count")
                .then()
                    .statusCode(401);

        mockJwtAuthenticationUtil.verify(0);
    }

    @Test
    void findNumberOfLikesOnPost404() {
        mockJwtAuthenticationUtil.mockReturns();

        RestAssured.given()
                    .param("post", "0")
                    .header("Authorization", "Bearer " + mockJwtAuthenticationUtil.getMockedToken())
                .when()
                    .get("/likes/count")
                .then()
                    .statusCode(404);

        mockJwtAuthenticationUtil.verify(1);
    }

    @Test
    void findNumberOfLikesOnPost200() {
        mockJwtAuthenticationUtil.mockReturns();

        String result = RestAssured.given()
                    .param("post", "3")
                    .header("Authorization", "Bearer " + mockJwtAuthenticationUtil.getMockedToken())
                .when()
                    .get("/likes/count")
                .then()
                    .statusCode(200)
                    .contentType("application/json")
                    .extract().body().asString();

        // post with id 1 has 2 likes in database
        Assertions.assertEquals(result, "2");

        mockJwtAuthenticationUtil.verify(1);
    }

    @Test
    void findLikesOnPost401() {
        RestAssured.given()
                    .param("post", "3")
                .when()
                    .get("/likes/onPost")
                .then()
                    .statusCode(401);

        mockJwtAuthenticationUtil.verify(0);
    }

    @Test
    void findLikesOnPost404() {
        mockJwtAuthenticationUtil.mockReturns();

        RestAssured.given()
                    .param("post", "0")
                    .header("Authorization", "Bearer " + mockJwtAuthenticationUtil.getMockedToken())
                .when()
                    .get("/likes/onPost")
                .then()
                    .statusCode(404);

        mockJwtAuthenticationUtil.verify(1);
    }

    @Test
    void findLikesOnPost200() {
        mockJwtAuthenticationUtil.mockReturns();

        String result = RestAssured.given()
                    .param("post", "3")
                    .header("Authorization", "Bearer " + mockJwtAuthenticationUtil.getMockedToken())
                .when()
                    .get("/likes/onPost")
                .then()
                    .statusCode(200)
                    .contentType("application/json")
                    .extract().body().asString();

        // post with id 1 has 2 likes in database
        AssertLikesIntegrationUtil.assertLikes(result, likesInDatabase.get(0), 1);
        AssertLikesIntegrationUtil.assertLikes(result, likesInDatabase.get(1), 0);

        mockJwtAuthenticationUtil.verify(1);
    }

    @Test
    void findLikeIdByUserAndPost200() {
        mockJwtAuthenticationUtil.mockReturns();

        String result = RestAssured.given()
                    .header("Authorization", "Bearer " + mockJwtAuthenticationUtil.getMockedToken())
                .when()
                    .get("/likes/isLiked/{postId}", likesInDatabase.get(0).getPost().getId())
                .then()
                    .statusCode(200)
                    .contentType("application/json")
                    .extract().body().asString();

        Assertions.assertEquals(result, "1");

        mockJwtAuthenticationUtil.verify(1);
    }

    @Test
    void findLikeIdByUserAndPost401() {
        RestAssured.given()
                .when()
                    .get("/likes/isLiked/{postId}", likesInDatabase.get(0).getPost().getId())
                .then()
                    .statusCode(401);

        mockJwtAuthenticationUtil.verify(0);
    }

    @Test
    void findLikeIdByUserAndPost404() {
        mockJwtAuthenticationUtil.mockReturns();

        RestAssured.given()
                    .header("Authorization", "Bearer " + mockJwtAuthenticationUtil.getMockedToken())
                .when()
                    .get("/likes/isLiked/{postId}", 0)
                .then()
                    .statusCode(404);

        mockJwtAuthenticationUtil.verify(1);
    }
}
