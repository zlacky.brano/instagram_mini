package zlack.bra.instamini.presentation.controller;

import com.radcortez.flyway.test.annotation.DataSource;
import com.radcortez.flyway.test.annotation.FlywayTest;
import io.quarkus.test.common.QuarkusTestResource;
import io.quarkus.test.h2.H2DatabaseTestResource;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.junit.mockito.InjectMock;
import io.restassured.RestAssured;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import util.AssertUsersIntegrationUtil;
import util.MockJwtAuthenticationUtil;
import util.db.QuarkusDataSourceProvider;
import zlack.bra.instamini.business.dto.UserDTO;
import zlack.bra.instamini.controller.request.EditUsernameRequestBody;
import zlack.bra.instamini.security.jwt.JwtUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;


@QuarkusTest
@QuarkusTestResource(H2DatabaseTestResource.class)
@FlywayTest(@DataSource(QuarkusDataSourceProvider.class))
class UserControllerIntegrationTest {

    private MockJwtAuthenticationUtil mockJwtAuthenticationUtil;

    @InjectMock
    private JwtUtil jwtUtil;

    private final UserDTO loggedUserDTOExample = new UserDTO(10, "olivia.johnson@test.com", "Olivia Johnson");

    private final List<UserDTO> usersInDatabse = new ArrayList<>() {
        {
            add(new UserDTO(1, "john.smith@test.com", "JohnyS"));
            add(new UserDTO(2, "george.jones@test.com", "_Georgie_"));
            add(new UserDTO(3, "jacob.brown@test.com", "_**JB**_"));
            add(new UserDTO(4, "harry.davies@test.com", "HarryDav"));
            add(new UserDTO(5, "charlie.smith@test.com", ":Charlie:"));
            add(new UserDTO(6, "thomas.wilson@test.com", "WilT"));
            add(new UserDTO(7, "joe.evans@test.com", "JEvans"));
            add(new UserDTO(8, "oscar.garcia@test.com", "Oscar"));
            add(new UserDTO(9, "william.miller@test.com", "WillMill"));
            add(new UserDTO(10, "olivia.johnson@test.com", "Olivia Johnson"));
            add(new UserDTO(11, "emily.murphy@test.com", "Em"));
            add(new UserDTO(12, "ava.byrne@test.com", "Ava"));
        }
    };

    @BeforeEach
    void setUp() {
        mockJwtAuthenticationUtil = new MockJwtAuthenticationUtil(jwtUtil, loggedUserDTOExample, "exists");
    }

    @Test
    void all401() {
        RestAssured.given()
                .when()
                    .get("/users")
                .then()
                    .statusCode(401);

        mockJwtAuthenticationUtil.verify(0);
    }

    @Test
    void all200() {
        mockJwtAuthenticationUtil.mockReturns();

        String result = RestAssured.given()
                    .header("Authorization", "Bearer " + mockJwtAuthenticationUtil.getMockedToken())
                .when()
                    .get("/users")
                .then()
                    .contentType("application/json")
                    .statusCode(200)
                    .extract().body().asString();

        for (int i = 0; i < usersInDatabse.size(); i++) {
            AssertUsersIntegrationUtil.assertUsers(result, usersInDatabse.get(i), i);
        }

        mockJwtAuthenticationUtil.verify(1);
    }

    @Test
    void byEmail401() {
        UserDTO userDTOToFind = usersInDatabse.get(0);

        RestAssured.given()
                    .param("email", userDTOToFind.getEmail())
                .when()
                    .get("/users/byEmail")
                .then()
                    .statusCode(401);

        mockJwtAuthenticationUtil.verify(0);
    }

    @Test
    void byEmail404() {
        mockJwtAuthenticationUtil.mockReturns();

        RestAssured.given()
                    .param("email", UUID.randomUUID().toString())
                    .header("Authorization", "Bearer " + mockJwtAuthenticationUtil.getMockedToken())
                .when()
                    .get("/users/byEmail")
                .then()
                    .statusCode(404);

        mockJwtAuthenticationUtil.verify(1);
    }

    @Test
    void byEmail200() {
        UserDTO userDTOToFind = usersInDatabse.get(0);

        mockJwtAuthenticationUtil.mockReturns();

        String result = RestAssured.given()
                    .param("email", userDTOToFind.getEmail())
                    .header("Authorization", "Bearer " + mockJwtAuthenticationUtil.getMockedToken())
                .when()
                    .get("/users/byEmail")
                .then()
                    .contentType("application/json")
                    .statusCode(200)
                    .extract().body().asString();

        AssertUsersIntegrationUtil.assertUsers(result, userDTOToFind);

        mockJwtAuthenticationUtil.verify(1);
    }

    @Test
    void byID401() {
        UserDTO userDTOToFind = usersInDatabse.get(0);

        RestAssured.given()
                .when()
                    .get("/users/{id}", userDTOToFind.getId())
                .then()
                    .statusCode(401);

        mockJwtAuthenticationUtil.verify(0);
    }

    @Test
    void byID404() {
        mockJwtAuthenticationUtil.mockReturns();

        RestAssured.given()
                    .header("Authorization", "Bearer " + mockJwtAuthenticationUtil.getMockedToken())
                .when()
                    .get("/users/{id}", 0)
                .then()
                    .statusCode(404);

        mockJwtAuthenticationUtil.verify(1);
    }

    @Test
    void byID200() {
        UserDTO userDTOToFind = usersInDatabse.get(0);

        mockJwtAuthenticationUtil.mockReturns();

        String result = RestAssured.given()
                    .header("Authorization", "Bearer " + mockJwtAuthenticationUtil.getMockedToken())
                .when()
                    .get("/users/{id}", userDTOToFind.getId())
                .then()
                    .contentType("application/json")
                    .statusCode(200)
                    .extract().body().asString();

        AssertUsersIntegrationUtil.assertUsers(result, userDTOToFind);

        mockJwtAuthenticationUtil.verify(1);
    }

    @Test
    void byPrincipal401() {
        RestAssured.given()
                .when()
                    .get("/users/logged")
                .then()
                    .statusCode(401);

        mockJwtAuthenticationUtil.verify(0);
    }

    @Test
    void byPrincipal200() {
        mockJwtAuthenticationUtil.mockReturns();

        String result = RestAssured.given()
                    .header("Authorization", "Bearer " + mockJwtAuthenticationUtil.getMockedToken())
                .when()
                    .get("/users/logged")
                .then()
                    .contentType("application/json")
                    .statusCode(200)
                    .extract().body().asString();

        AssertUsersIntegrationUtil.assertUsers(result, loggedUserDTOExample);

        mockJwtAuthenticationUtil.verify(1);
    }

    @Test
    void editUsername401() {
        EditUsernameRequestBody requestBody = new EditUsernameRequestBody("new username");

        RestAssured.given()
                    .contentType("application/json")
                    .body(requestBody.toJson())
                .when()
                    .put("/users/{id}/editUsername", usersInDatabse.get(0).getId())
                .then()
                    .statusCode(401);

        mockJwtAuthenticationUtil.verify(0);
    }

    @Test
    void editUsername403() {
        EditUsernameRequestBody requestBody = new EditUsernameRequestBody("new username");

        mockJwtAuthenticationUtil.mockReturns();

        RestAssured.given()
                    .contentType("application/json")
                    .body(requestBody.toJson())
                    .header("Authorization", "Bearer " + mockJwtAuthenticationUtil.getMockedToken())
                .when()
                    .put("/users/{id}/editUsername", usersInDatabse.get(0).getId()) // Editing username, which does not belong to logged user
                .then()
                    .statusCode(403);

        mockJwtAuthenticationUtil.verify(1);
    }

    @Test
    void editUsername200() {
        EditUsernameRequestBody requestBody = new EditUsernameRequestBody("new username");

        mockJwtAuthenticationUtil.mockReturns();

        String result = RestAssured.given()
                    .contentType("application/json")
                    .body(requestBody.toJson())
                    .header("Authorization", "Bearer " + mockJwtAuthenticationUtil.getMockedToken())
                .when()
                    .put("/users/{id}/editUsername", usersInDatabse.get(9).getId())
                .then()
                    .statusCode(200)
                    .contentType("application/json")
                    .extract().body().asString();

        AssertUsersIntegrationUtil.assertUsersInEditUsernameTest(result, usersInDatabse.get(9), requestBody.getUsername());

        mockJwtAuthenticationUtil.verify(1);
    }

    @Test
    void editUsername400Valid() {
        mockJwtAuthenticationUtil.mockReturns();

        RestAssured.given()
                    .contentType("application/json")
                    .body("{}") // missing body
                    .header("Authorization", "Bearer " + mockJwtAuthenticationUtil.getMockedToken())
                .when()
                    .put("/users/{id}/editUsername", usersInDatabse.get(1).getId())
                .then()
                    .statusCode(400);

        mockJwtAuthenticationUtil.verify(1);
    }

    @Test
    void editUsername400Unique() {
        EditUsernameRequestBody requestBody = new EditUsernameRequestBody("JEvans");

        mockJwtAuthenticationUtil.mockReturns();

        RestAssured.given()
                    .contentType("application/json")
                    .body(requestBody.toJson())
                    .header("Authorization", "Bearer " + mockJwtAuthenticationUtil.getMockedToken())
                .when()
                    .put("/users/{id}/editUsername", usersInDatabse.get(9).getId())
                .then()
                    .statusCode(400);

        mockJwtAuthenticationUtil.verify(1);
    }

    @Test
    void delete401() {
        RestAssured.given()
                .when()
                    .delete("/users/{id}", usersInDatabse.get(0).getId())
                .then()
                    .statusCode(401);

        mockJwtAuthenticationUtil.verify(0);
    }

    @Test
    void delete403() {
        mockJwtAuthenticationUtil.mockReturns();

        RestAssured.given()
                    .header("Authorization", "Bearer " + mockJwtAuthenticationUtil.getMockedToken())
                .when()
                    .delete("/users/{id}", usersInDatabse.get(0).getId()) // Deleting user, which does not belong to logged user
                .then()
                    .statusCode(403);

        mockJwtAuthenticationUtil.verify(1);
    }

    @Test
    void delete204() {
        mockJwtAuthenticationUtil.mockReturns();

        RestAssured.given()
                    .header("Authorization", "Bearer " + mockJwtAuthenticationUtil.getMockedToken())
                .when()
                    .delete("/users/{id}", usersInDatabse.get(9).getId())
                .then()
                    .statusCode(204);

        mockJwtAuthenticationUtil.verify(1);
    }

    @Test
    void findAllUsersWhoLikedPost401() {
        RestAssured.given()
                    .param("post", "3")
                .when()
                    .get("/users/likedPost")
                .then()
                    .statusCode(401);

        mockJwtAuthenticationUtil.verify(0);
    }

    @Test
    void findAllUsersWhoLikedPost404() {
        mockJwtAuthenticationUtil.mockReturns();

        RestAssured.given()
                    .header("Authorization", "Bearer " + mockJwtAuthenticationUtil.getMockedToken())
                    .param("post", "0")
                .when()
                    .get("/users/likedPost")
                .then()
                    .statusCode(404);

        mockJwtAuthenticationUtil.verify(1);
    }

    @Test
    void findAllUsersWhoLikedPost200() {
        mockJwtAuthenticationUtil.mockReturns();

        String result = RestAssured.given()
                    .header("Authorization", "Bearer " + mockJwtAuthenticationUtil.getMockedToken())
                    .param("post", "3")
                .when()
                    .get("/users/likedPost")
                .then()
                    .statusCode(200)
                    .contentType("application/json")
                    .extract().body().asString();

        // post with id 3 has 2 user's likes in database
        AssertUsersIntegrationUtil.assertUsers(result, usersInDatabse.get(5), 0);
        AssertUsersIntegrationUtil.assertUsers(result, usersInDatabse.get(9), 1);

        mockJwtAuthenticationUtil.verify(1);
    }

    @Test
    void findAllFollowers401() {
        RestAssured.given()
                    .param("user", "1")
                .when()
                    .get("/users/followers")
                .then()
                    .statusCode(401);

        mockJwtAuthenticationUtil.verify(0);
    }

    @Test
    void findAllFollowers404() {
        mockJwtAuthenticationUtil.mockReturns();

        RestAssured.given()
                    .param("user", "0")
                    .header("Authorization", "Bearer " + mockJwtAuthenticationUtil.getMockedToken())
                .when()
                    .get("/users/followers")
                .then()
                    .statusCode(404);

        mockJwtAuthenticationUtil.verify(1);
    }

    @Test
    void findAllFollowers200() {
        mockJwtAuthenticationUtil.mockReturns();

        String result = RestAssured.given()
                    .param("user", "1")
                    .header("Authorization", "Bearer " + mockJwtAuthenticationUtil.getMockedToken())
                .when()
                    .get("/users/followers")
                .then()
                    .statusCode(200)
                    .contentType("application/json")
                    .extract().body().asString();

        // user with id 2 has 2 followers in database
        AssertUsersIntegrationUtil.assertUsers(result, usersInDatabse.get(1), 0);
        AssertUsersIntegrationUtil.assertUsers(result, usersInDatabse.get(9), 1);

        mockJwtAuthenticationUtil.verify(1);
    }

    @Test
    void findCountOfAllFollowers401() {
        RestAssured.given()
                    .param("user", "2")
                .when()
                    .get("/users/followers/count")
                .then()
                    .statusCode(401);

        mockJwtAuthenticationUtil.verify(0);
    }

    @Test
    void findCountOfAllFollowers404() {
        mockJwtAuthenticationUtil.mockReturns();

        RestAssured.given()
                    .param("user", "0")
                    .header("Authorization", "Bearer " + mockJwtAuthenticationUtil.getMockedToken())
                .when()
                    .get("/users/followers/count")
                .then()
                    .statusCode(404);

        mockJwtAuthenticationUtil.verify(1);
    }

    @Test
    void findCountOfAllFollowers200() {
        mockJwtAuthenticationUtil.mockReturns();

        String result = RestAssured.given()
                    .param("user", "1")
                    .header("Authorization", "Bearer " + mockJwtAuthenticationUtil.getMockedToken())
                .when()
                    .get("/users/followers/count")
                .then()
                    .statusCode(200)
                    .contentType("application/json")
                    .extract().body().asString();

        // user with id 2 follows user in database
        Assertions.assertEquals("2", result);

        mockJwtAuthenticationUtil.verify(1);
    }

    @Test
    void findAllFollowing401() {
        RestAssured.given()
                    .param("user", "2")
                .when()
                    .get("/users/following")
                .then()
                    .statusCode(401);

        mockJwtAuthenticationUtil.verify(0);
    }

    @Test
    void findAllFollowing404() {
        mockJwtAuthenticationUtil.mockReturns();

        RestAssured.given()
                    .param("user", "0")
                    .header("Authorization", "Bearer " + mockJwtAuthenticationUtil.getMockedToken())
                .when()
                    .get("/users/following")
                .then()
                    .statusCode(404);

        mockJwtAuthenticationUtil.verify(1);
    }

    @Test
    void findAllFollowing200() {
        mockJwtAuthenticationUtil.mockReturns();

        String result = RestAssured.given()
                    .param("user", "2")
                    .header("Authorization", "Bearer " + mockJwtAuthenticationUtil.getMockedToken())
                .when()
                    .get("/users/following")
                .then()
                    .statusCode(200)
                    .contentType("application/json")
                    .extract().body().asString();

        // user with id 2 follows user in database
        AssertUsersIntegrationUtil.assertUsers(result, usersInDatabse.get(0), 0);

        mockJwtAuthenticationUtil.verify(1);
    }

    @Test
    void findCountOfAllFollowing401() {
        RestAssured.given()
                    .param("user", "2")
                .when()
                    .get("/users/following/count")
                .then()
                    .statusCode(401);

        mockJwtAuthenticationUtil.verify(0);
    }

    @Test
    void findCountOfAllFollowing404() {
        mockJwtAuthenticationUtil.mockReturns();

        RestAssured.given()
                    .param("user", "0")
                    .header("Authorization", "Bearer " + mockJwtAuthenticationUtil.getMockedToken())
                .when()
                    .get("/users/following/count")
                .then()
                    .statusCode(404);

        mockJwtAuthenticationUtil.verify(1);
    }

    @Test
    void findCountOfAllFollowing200() {
        mockJwtAuthenticationUtil.mockReturns();

        String result = RestAssured.given()
                    .param("user", "2")
                    .header("Authorization", "Bearer " + mockJwtAuthenticationUtil.getMockedToken())
                .when()
                    .get("/users/following/count")
                .then()
                    .statusCode(200)
                    .contentType("application/json")
                    .extract().body().asString();

        // user with id 2 follows user in database
        Assertions.assertEquals(result, "1");

        mockJwtAuthenticationUtil.verify(1);
    }
}

