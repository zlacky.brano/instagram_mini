package zlack.bra.instamini.presentation.controller;

import com.radcortez.flyway.test.annotation.DataSource;
import com.radcortez.flyway.test.annotation.FlywayTest;
import io.quarkus.test.common.QuarkusTestResource;
import io.quarkus.test.h2.H2DatabaseTestResource;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.junit.mockito.InjectMock;
import io.restassured.RestAssured;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import util.AssertFollowsIntegrationUtil;
import util.MockJwtAuthenticationUtil;
import util.db.QuarkusDataSourceProvider;
import zlack.bra.instamini.business.dto.*;
import zlack.bra.instamini.security.jwt.JwtUtil;

import java.util.ArrayList;
import java.util.List;


@QuarkusTest
@QuarkusTestResource(H2DatabaseTestResource.class)
@FlywayTest(@DataSource(QuarkusDataSourceProvider.class))
class FollowControllerIntegrationTest {

    private MockJwtAuthenticationUtil mockJwtAuthenticationUtil;

    @InjectMock
    private JwtUtil jwtUtil;

    private final UserDTO loggedUserDTOExample = new UserDTO(10, "olivia.johnson@test.com", "Olivia Johnson");

    private final UserDTO notLoggedUserDTOExample = new UserDTO(6, "thomas.wilson@test.com", "WilT");

    private final List<FollowDTO> followsInDatabase = new ArrayList<>() {
        {
            add(new FollowDTO(1,
                    new UserDTO(1, "john.smith@test.com", "JohnyS"),
                    new UserDTO(2, "george.jones@test.com", "_Georgie_")));
            add(new FollowDTO(2,
                    new UserDTO(2, "george.jones@test.com", "_Georgie_"),
                    new UserDTO(1, "john.smith@test.com", "JohnyS")));
            add(new FollowDTO(3,
                    notLoggedUserDTOExample,
                    new UserDTO(4, "harry.davies@test.com", "HarryDav")));
            add(new FollowDTO(4,
                    loggedUserDTOExample,
                    new UserDTO(1, "john.smith@test.com", "JohnyS")));
        }
    };

    @BeforeEach
    void setUp() {
        mockJwtAuthenticationUtil = new MockJwtAuthenticationUtil(jwtUtil, loggedUserDTOExample, "exists");
    }

    @Test
    void all401() {
        RestAssured.given().when().get("/follows").then().statusCode(401);

        mockJwtAuthenticationUtil.verify(0);
    }

    @Test
    void all200() {
        mockJwtAuthenticationUtil.mockReturns();

        String result = RestAssured.given()
                    .header("Authorization", "Bearer " + mockJwtAuthenticationUtil.getMockedToken())
                .when()
                    .get("/follows")
                .then()
                    .statusCode(200)
                    .contentType("application/json")
                    .extract().body().asString();

        for (int i = 0; i < followsInDatabase.size(); i++) {
            AssertFollowsIntegrationUtil.assertFollows(result, followsInDatabase.get(i), i);
        }

        mockJwtAuthenticationUtil.verify(1);
    }

    @Test
    void byID401() {
        FollowDTO followDTOToFind = followsInDatabase.get(0);

        RestAssured.given()
                .when()
                    .get("/follows/{id}", followDTOToFind.getId())
                .then()
                    .statusCode(401);

        mockJwtAuthenticationUtil.verify(0);
    }

    @Test
    void byID404() {
        mockJwtAuthenticationUtil.mockReturns();

        RestAssured.given()
                    .header("Authorization", "Bearer " + mockJwtAuthenticationUtil.getMockedToken())
                .when()
                    .get("/follows/{id}", 0)
                .then()
                    .statusCode(404);

        mockJwtAuthenticationUtil.verify(1);
    }

    @Test
    void byID200() {
        FollowDTO followDTOToFind = followsInDatabase.get(0);

        mockJwtAuthenticationUtil.mockReturns();

        String result = RestAssured.given()
                    .header("Authorization", "Bearer " + mockJwtAuthenticationUtil.getMockedToken())
                .when()
                    .get("/follows/{id}", followDTOToFind.getId())
                .then()
                    .statusCode(200)
                    .contentType("application/json")
                    .extract().body().asString();

        AssertFollowsIntegrationUtil.assertFollows(result, followDTOToFind);

        mockJwtAuthenticationUtil.verify(1);
    }

    @Test
    void create401() {
        FollowCreateDTO followCreateDTO = new FollowCreateDTO(loggedUserDTOExample.getId(), loggedUserDTOExample.getId());

        RestAssured.given()
                    .contentType("application/json")
                    .body(followCreateDTO.toJson())
                .when()
                    .post("/follows")
                .then()
                    .statusCode(401);

        mockJwtAuthenticationUtil.verify(0);
    }

    @Test
    void create403() {
        FollowCreateDTO followCreateDTO = new FollowCreateDTO(notLoggedUserDTOExample.getId(), loggedUserDTOExample.getId());

        mockJwtAuthenticationUtil.mockReturns();

        RestAssured.given()
                    .header("Authorization", "Bearer " + mockJwtAuthenticationUtil.getMockedToken())
                    .contentType("application/json")
                    .body(followCreateDTO.toJson())
                .when()
                    .post("/follows")
                .then()
                    .statusCode(403);

        mockJwtAuthenticationUtil.verify(1);
    }

    @Test
    void create404() {
        FollowCreateDTO followCreateDTO = new FollowCreateDTO(loggedUserDTOExample.getId(), 0);

        mockJwtAuthenticationUtil.mockReturns();

        RestAssured.given()
                    .header("Authorization", "Bearer " + mockJwtAuthenticationUtil.getMockedToken())
                    .contentType("application/json")
                    .body(followCreateDTO.toJson())
                .when()
                    .post("/follows")
                .then()
                    .statusCode(404);

        mockJwtAuthenticationUtil.verify(1);
    }

    @Test
    void create201() {
        FollowCreateDTO followCreateDTO = new FollowCreateDTO(loggedUserDTOExample.getId(), notLoggedUserDTOExample.getId());

        mockJwtAuthenticationUtil.mockReturns();

        String result = RestAssured.given()
                    .header("Authorization", "Bearer " + mockJwtAuthenticationUtil.getMockedToken())
                    .contentType("application/json")
                    .body(followCreateDTO.toJson())
                .when()
                    .post("/follows")
                .then()
                    .statusCode(201)
                    .contentType("application/json")
                    .extract().body().asString();

        AssertFollowsIntegrationUtil.assertFollows(result, followCreateDTO);

        mockJwtAuthenticationUtil.verify(1);
    }

    @Test
    void create400Valid() {
        FollowCreateDTO followCreateDTO = new FollowCreateDTO(loggedUserDTOExample.getId(), notLoggedUserDTOExample.getId());

        mockJwtAuthenticationUtil.mockReturns();

        RestAssured.given()
                    .header("Authorization", "Bearer " + mockJwtAuthenticationUtil.getMockedToken())
                    .contentType("application/json")
                    .body("{\"userWhoId\":\"" + followCreateDTO.getUserWhoId() + "\"}") // missing body
                .when()
                    .post("/follows")
                .then()
                    .statusCode(400);

        mockJwtAuthenticationUtil.verify(1);
    }

    @Test
    void create400FollowYourself() {
        FollowCreateDTO followCreateDTO = new FollowCreateDTO(loggedUserDTOExample.getId(), loggedUserDTOExample.getId());

        mockJwtAuthenticationUtil.mockReturns();

        RestAssured.given()
                    .header("Authorization", "Bearer " + mockJwtAuthenticationUtil.getMockedToken())
                    .contentType("application/json")
                    .body(followCreateDTO.toJson())
                .when()
                    .post("/follows")
                .then()
                    .statusCode(400);

        mockJwtAuthenticationUtil.verify(1);
    }

    @Test
    void create400Unique() {
        FollowCreateDTO followCreateDTO = new FollowCreateDTO(loggedUserDTOExample.getId(), followsInDatabase.get(3).getUserWhom().getId());

        mockJwtAuthenticationUtil.mockReturns();

        RestAssured.given()
                    .header("Authorization", "Bearer " + mockJwtAuthenticationUtil.getMockedToken())
                    .contentType("application/json")
                    .body(followCreateDTO.toJson())
                .when()
                    .post("/follows")
                .then()
                    .statusCode(400);

        mockJwtAuthenticationUtil.verify(1);
    }

    @Test
    void delete401() {
        RestAssured.given()
                .when()
                    .delete("/follows/{id}", followsInDatabase.get(0).getId())
                .then()
                    .statusCode(401);

        mockJwtAuthenticationUtil.verify(0);
    }

    @Test
    void delete403() {
        mockJwtAuthenticationUtil.mockReturns();

        RestAssured.given()
                    .header("Authorization", "Bearer " + mockJwtAuthenticationUtil.getMockedToken())
                .when()
                    .delete("/follows/{id}", followsInDatabase.get(0).getId())
                .then()
                    .statusCode(403);

        mockJwtAuthenticationUtil.verify(1);
    }

    @Test
    void delete404() {
        mockJwtAuthenticationUtil.mockReturns();

        RestAssured.given()
                    .header("Authorization", "Bearer " + mockJwtAuthenticationUtil.getMockedToken())
                .when()
                    .delete("/follows/{id}", 0)
                .then()
                    .statusCode(404);

        mockJwtAuthenticationUtil.verify(1);
    }

    @Test
    void delete204() {
        mockJwtAuthenticationUtil.mockReturns();

        RestAssured.given()
                    .header("Authorization", "Bearer " + mockJwtAuthenticationUtil.getMockedToken())
                .when()
                    .delete("/follows/{id}", followsInDatabase.get(3).getId())
                .then()
                    .statusCode(204);

        mockJwtAuthenticationUtil.verify(1);
    }

    @Test
    void findFollowIdByUsers200() {
        mockJwtAuthenticationUtil.mockReturns();

        String result = RestAssured.given()
                    .header("Authorization", "Bearer " + mockJwtAuthenticationUtil.getMockedToken())
                .when()
                    .get("/follows/isFollowed/{userIdWhom}", followsInDatabase.get(3).getUserWhom().getId())
                .then()
                    .statusCode(200)
                    .contentType("application/json")
                    .extract().body().asString();

        Assertions.assertEquals(result, "4");

        mockJwtAuthenticationUtil.verify(1);
    }

    @Test
    void findFollowIdByUsers401() {
        RestAssured.given()
                .when()
                    .get("/follows/isFollowed/{userIdWhom}", followsInDatabase.get(3).getUserWhom().getId())
                .then()
                    .statusCode(401);

        mockJwtAuthenticationUtil.verify(0);
    }

    @Test
    void findFollowIdByUsers404() {
        mockJwtAuthenticationUtil.mockReturns();

        RestAssured.given()
                    .header("Authorization", "Bearer " + mockJwtAuthenticationUtil.getMockedToken())
                .when()
                    .get("/follows/isFollowed/{userIdWhom}", 0  )
                .then()
                    .statusCode(404);

        mockJwtAuthenticationUtil.verify(1);
    }
}
