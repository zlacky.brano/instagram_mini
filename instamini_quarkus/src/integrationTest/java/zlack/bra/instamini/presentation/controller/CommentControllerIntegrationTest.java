package zlack.bra.instamini.presentation.controller;

import com.radcortez.flyway.test.annotation.DataSource;
import com.radcortez.flyway.test.annotation.FlywayTest;
import io.quarkus.test.TestTransaction;
import io.quarkus.test.common.QuarkusTestResource;
import io.quarkus.test.h2.H2DatabaseTestResource;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.junit.mockito.InjectMock;
import io.restassured.RestAssured;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import util.AssertCommentsIntegrationUtil;
import util.MockJwtAuthenticationUtil;
import util.db.QuarkusDataSourceProvider;
import zlack.bra.instamini.business.dto.*;
import zlack.bra.instamini.controller.request.EditTextCommentRequestBody;
import zlack.bra.instamini.security.jwt.JwtUtil;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@QuarkusTest
@QuarkusTestResource(H2DatabaseTestResource.class)
@FlywayTest(@DataSource(QuarkusDataSourceProvider.class))
class CommentControllerIntegrationTest {

    private MockJwtAuthenticationUtil mockJwtAuthenticationUtil;

    @InjectMock
    private JwtUtil jwtUtil;

    private final UserDTO loggedUserDTOExample = new UserDTO(10, "olivia.johnson@test.com", "Olivia Johnson");

    private final UserDTO notLoggedUserDTOExample = new UserDTO(6, "thomas.wilson@test.com", "WilT");

    private final List<CommentDTO> commentsInDatabase = new ArrayList<>() {
        {
            add(new CommentDTO(1, "In hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus.",
                    LocalDateTime.of(2020, 5, 24, 8, 23, 35),
                    notLoggedUserDTOExample,
                    new PostDTO(1, "Morbi quis tortor id nulla ultrices aliquet. Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui.",
                            "https://res.cloudinary.com/instamini/image/upload/v1630672586/husky-3380548_1280_rman9o.jpg",
                            LocalDateTime.of(2020, 4, 5, 22, 52, 37),
                            notLoggedUserDTOExample)));
            add(new CommentDTO(2, "Pellentesque ultrices mattis odio.",
                    LocalDateTime.of(2021, 1, 23, 15, 33, 49),
                    loggedUserDTOExample,
                    new PostDTO(1, "Morbi quis tortor id nulla ultrices aliquet. Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui.",
                            "https://res.cloudinary.com/instamini/image/upload/v1630672586/husky-3380548_1280_rman9o.jpg",
                            LocalDateTime.of(2020, 4, 5, 22, 52, 37),
                            notLoggedUserDTOExample)));
            add(new CommentDTO(3, "Vestibulum quam sapien, varius ut, blandit non, interdum in, ante. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Duis faucibus accumsan odio. Curabitur convallis.",
                    LocalDateTime.of(2020, 9, 9, 7, 6, 13),
                    new UserDTO(8, "oscar.garcia@test.com", "Oscar"),
                    new PostDTO(2, "Nullam orci pede, venenatis non, sodales sed, tincidunt eu, felis. Fusce posuere felis sed lacus. Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl. Nunc rhoncus dui vel sem. Sed sagittis.",
                            "https://res.cloudinary.com/instamini/image/upload/v1630672584/dolphin-203875_1280_n67crm.jpg",
                            LocalDateTime.of(2020, 12, 20, 7, 51, 2),
                            loggedUserDTOExample)));
            add(new CommentDTO(4, "Aenean sit amet justo. Morbi ut odio. Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim. Lorem ipsum dolor sit amet, consectetuer adipiscing elit.",
                    LocalDateTime.of(2021, 1, 11, 11, 0, 57),
                    loggedUserDTOExample,
                    new PostDTO(4, "Nulla ac enim. In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem.",
                            "https://res.cloudinary.com/instamini/image/upload/v1630672586/husky-3380548_1280_rman9o.jpg",
                            LocalDateTime.of(2021, 2, 6, 16, 15, 7),
                            loggedUserDTOExample)));
        }
    };

    @BeforeEach
    void setUp() {
        mockJwtAuthenticationUtil = new MockJwtAuthenticationUtil(jwtUtil, loggedUserDTOExample, "exists");
    }

    @Test
    void all401() {
        RestAssured.given().when().get("/comments").then().statusCode(401);

        mockJwtAuthenticationUtil.verify(0);
    }

    @Test
    void all200() {
        mockJwtAuthenticationUtil.mockReturns();

        String result = RestAssured.given()
                    .header("Authorization", "Bearer " + mockJwtAuthenticationUtil.getMockedToken())
                .when()
                    .get("/comments")
                .then()
                    .statusCode(200)
                    .contentType("application/json")
                    .extract().body().asString();

        for (int i = 0; i < commentsInDatabase.size(); i++) {
            AssertCommentsIntegrationUtil.assertComments(result, commentsInDatabase.get(i), i);
        }

        mockJwtAuthenticationUtil.verify(1);
    }

    @Test
    void byID401() {
        CommentDTO commentDTOToFind = commentsInDatabase.get(0);

        RestAssured.given()
                .when()
                    .get("/comments/{id}", commentDTOToFind.getId())
                .then()
                    .statusCode(401);

        mockJwtAuthenticationUtil.verify(0);
    }

    @Test
    void byID404() {
        mockJwtAuthenticationUtil.mockReturns();

        RestAssured.given()
                    .header("Authorization", "Bearer " + mockJwtAuthenticationUtil.getMockedToken())
                .when()
                    .get("/comments/{id}", 0)
                .then()
                    .statusCode(404);

        mockJwtAuthenticationUtil.verify(1);
    }

    @Test
    void byID200() {
        CommentDTO commentDTOToFind = commentsInDatabase.get(0);

        mockJwtAuthenticationUtil.mockReturns();

        String result = RestAssured.given()
                    .header("Authorization", "Bearer " + mockJwtAuthenticationUtil.getMockedToken())
                .when()
                    .get("/comments/{id}", commentDTOToFind.getId())
                .then()
                    .statusCode(200)
                    .contentType("application/json")
                    .extract().body().asString();

        AssertCommentsIntegrationUtil.assertComments(result, commentDTOToFind);

        mockJwtAuthenticationUtil.verify(1);
    }

    @Test
    void findAllCommentsOnPost401() {
        RestAssured.given()
                  .param("post", loggedUserDTOExample.getId())
                .when()
                    .get("/comments/onPost")
                .then()
                    .statusCode(401);

        mockJwtAuthenticationUtil.verify(0);
    }

    @Test
    void findAllCommentsOnPost404() {
        mockJwtAuthenticationUtil.mockReturns();

        RestAssured.given()
                    .param("post", 0)
                    .header("Authorization", "Bearer " + mockJwtAuthenticationUtil.getMockedToken())
                .when()
                    .get("/comments/onPost")
                .then()
                    .statusCode(404);

        mockJwtAuthenticationUtil.verify(1);
    }

    @Test
    void findAllCommentsOnPost200() {
        mockJwtAuthenticationUtil.mockReturns();

        String result = RestAssured.given()
                    .param("post", 1)
                    .header("Authorization", "Bearer " + mockJwtAuthenticationUtil.getMockedToken())
                .when()
                    .get("/comments/onPost")
                .then()
                    .statusCode(200)
                    .contentType("application/json")
                    .extract().body().asString();

        // post with id 1 has 2 comments in database
        AssertCommentsIntegrationUtil.assertComments(result, commentsInDatabase.get(0), 1);
        AssertCommentsIntegrationUtil.assertComments(result, commentsInDatabase.get(1), 0);

        mockJwtAuthenticationUtil.verify(1);
    }

    @Test
    void create401() {
        CommentCreateDTO commentCreateDTO = new CommentCreateDTO("test", notLoggedUserDTOExample.getId(), commentsInDatabase.get(2).getPost().getId());

        RestAssured.given()
                    .contentType("application/json")
                    .body(commentCreateDTO.toJson())
                .when()
                    .post("/comments")
                .then()
                    .statusCode(401);

        mockJwtAuthenticationUtil.verify(0);
    }

    @Test
    void create403() {
        CommentCreateDTO commentCreateDTO = new CommentCreateDTO("test", notLoggedUserDTOExample.getId(), commentsInDatabase.get(2).getPost().getId());

        mockJwtAuthenticationUtil.mockReturns();

        RestAssured.given()
                    .header("Authorization", "Bearer " + mockJwtAuthenticationUtil.getMockedToken())
                    .contentType("application/json")
                    .body(commentCreateDTO.toJson())
                .when()
                    .post("/comments")
                .then()
                    .statusCode(403);

        mockJwtAuthenticationUtil.verify(1);
    }

    @Test
    void create404() {
        CommentCreateDTO commentCreateDTO = new CommentCreateDTO("test", loggedUserDTOExample.getId(), 0);

        mockJwtAuthenticationUtil.mockReturns();

        RestAssured.given()
                    .header("Authorization", "Bearer " + mockJwtAuthenticationUtil.getMockedToken())
                    .contentType("application/json")
                    .body(commentCreateDTO.toJson())
                .when()
                    .post("/comments")
                .then()
                    .statusCode(404 );

        mockJwtAuthenticationUtil.verify(1);
    }

    @Test
    void create201() {
        CommentCreateDTO commentCreateDTO = new CommentCreateDTO("test", loggedUserDTOExample.getId(), commentsInDatabase.get(2).getPost().getId());

        mockJwtAuthenticationUtil.mockReturns();

        String result = RestAssured.given()
                    .header("Authorization", "Bearer " + mockJwtAuthenticationUtil.getMockedToken())
                    .contentType("application/json")
                    .body(commentCreateDTO.toJson())
                .when()
                    .post("/comments")
                .then()
                    .statusCode(201)
                    .contentType("application/json")
                    .extract().body().asString();

        AssertCommentsIntegrationUtil.assertComments(result, commentCreateDTO);

        mockJwtAuthenticationUtil.verify(1);
    }

    @Test
    void create400Valid() {
        CommentCreateDTO commentCreateDTO = new CommentCreateDTO("test", loggedUserDTOExample.getId(), commentsInDatabase.get(2).getPost().getId());

        mockJwtAuthenticationUtil.mockReturns();

        RestAssured.given()
                    .header("Authorization", "Bearer " + mockJwtAuthenticationUtil.getMockedToken())
                    .contentType("application/json")
                    .body("{\"text\":\"" + commentCreateDTO.getText() + "\"}") // missing body
                .when()
                    .post("/comments")
                .then()
                    .statusCode(400);

        mockJwtAuthenticationUtil.verify(1);
    }

    @Test
    void editText401() {
        EditTextCommentRequestBody requestBody = new EditTextCommentRequestBody("new text");

        RestAssured.given()
                    .contentType("application/json")
                    .body(requestBody.toJson())
                .when()
                    .put("/comments/{id}/editText", commentsInDatabase.get(0).getId())
                .then()
                    .statusCode(401);

        mockJwtAuthenticationUtil.verify(0);
    }

    @Test
    void editText403() {
        EditTextCommentRequestBody requestBody = new EditTextCommentRequestBody("new text");

        mockJwtAuthenticationUtil.mockReturns();

        RestAssured.given()
                    .header("Authorization", "Bearer " + mockJwtAuthenticationUtil.getMockedToken())
                    .contentType("application/json")
                    .body(requestBody.toJson())
                .when()
                    .put("/comments/{id}/editText", commentsInDatabase.get(0).getId())
                .then()
                    .statusCode(403);

        mockJwtAuthenticationUtil.verify(1);
    }

    @Test
    void editText404() {
        EditTextCommentRequestBody requestBody = new EditTextCommentRequestBody("new text");

        mockJwtAuthenticationUtil.mockReturns();

        RestAssured.given()
                    .header("Authorization", "Bearer " + mockJwtAuthenticationUtil.getMockedToken())
                    .contentType("application/json")
                    .body(requestBody.toJson())
                .when()
                    .put("/comments/{id}/editText", 0)
                .then()
                    .statusCode(404);

        mockJwtAuthenticationUtil.verify(1);
    }

    @Test
    void editText200() {
        EditTextCommentRequestBody requestBody = new EditTextCommentRequestBody("new text");

        mockJwtAuthenticationUtil.mockReturns();

        String result = RestAssured.given()
                    .header("Authorization", "Bearer " + mockJwtAuthenticationUtil.getMockedToken())
                    .contentType("application/json")
                    .body(requestBody.toJson())
                .when()
                    .put("/comments/{id}/editText", commentsInDatabase.get(1).getId())
                .then()
                    .statusCode(200)
                    .contentType("application/json")
                    .extract().body().asString();

        AssertCommentsIntegrationUtil.assertCommentsInEditTextTest(result, commentsInDatabase.get(1), requestBody.getNewText());

        mockJwtAuthenticationUtil.verify(1);
    }

    @Test
    void editText400Valid() {
        mockJwtAuthenticationUtil.mockReturns();

        RestAssured.given()
                    .header("Authorization", "Bearer " + mockJwtAuthenticationUtil.getMockedToken())
                    .contentType("application/json")
                    .body("{}")
                .when()
                    .put("/comments/{id}/editText", commentsInDatabase.get(0).getId())
                .then()
                    .statusCode(400);

        mockJwtAuthenticationUtil.verify(1);
    }

    @Test
    void delete401() {
        RestAssured.given()
                .when()
                    .delete("/comments/{id}", commentsInDatabase.get(0).getId())
                .then()
                    .statusCode(401);

        mockJwtAuthenticationUtil.verify(0);
    }

    @Test
    void delete403() {
        mockJwtAuthenticationUtil.mockReturns();

        RestAssured.given()
                    .header("Authorization", "Bearer " + mockJwtAuthenticationUtil.getMockedToken())
                .when()
                    .delete("/comments/{id}", commentsInDatabase.get(0).getId())
                .then()
                    .statusCode(403);

        mockJwtAuthenticationUtil.verify(1);
    }

    @Test
    void delete404() {
        mockJwtAuthenticationUtil.mockReturns();

        RestAssured.given()
                    .header("Authorization", "Bearer " + mockJwtAuthenticationUtil.getMockedToken())
                .when()
                    .delete("/comments/{id}", 0)
                .then()
                    .statusCode(404);

        mockJwtAuthenticationUtil.verify(1);
    }

    @Test
    void delete204() {
        mockJwtAuthenticationUtil.mockReturns();

        RestAssured.given()
                    .header("Authorization", "Bearer " + mockJwtAuthenticationUtil.getMockedToken())
                .when()
                    .delete("/comments/{id}", commentsInDatabase.get(1).getId())
                .then()
                    .statusCode(204);

        mockJwtAuthenticationUtil.verify(1);
    }
}

