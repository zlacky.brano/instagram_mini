package zlack.bra.instamini.presentation.controller;

import com.radcortez.flyway.test.annotation.DataSource;
import com.radcortez.flyway.test.annotation.FlywayTest;
import io.quarkus.test.common.QuarkusTestResource;
import io.quarkus.test.h2.H2DatabaseTestResource;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.junit.mockito.InjectMock;
import io.restassured.RestAssured;
import io.restassured.builder.MultiPartSpecBuilder;
import io.restassured.path.json.JsonPath;
import io.restassured.specification.MultiPartSpecification;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import util.AssertPostsIntegrationUtil;
import util.MockJwtAuthenticationUtil;
import util.db.QuarkusDataSourceProvider;
import zlack.bra.instamini.business.dto.PostCreateDTO;
import zlack.bra.instamini.business.dto.PostDTO;
import zlack.bra.instamini.business.dto.UserDTO;
import zlack.bra.instamini.business.service.ImageUploadService;
import zlack.bra.instamini.controller.request.EditPostDescriptionRequestBody;
import zlack.bra.instamini.security.jwt.JwtUtil;

import javax.inject.Inject;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@QuarkusTest
@QuarkusTestResource(H2DatabaseTestResource.class)
@FlywayTest(@DataSource(QuarkusDataSourceProvider.class))
class PostControllerIntegrationTest {

    @Inject
    ImageUploadService imageUploadService;

    private MockJwtAuthenticationUtil mockJwtAuthenticationUtil;

    @InjectMock
    private JwtUtil jwtUtil;

    private final UserDTO loggedUserDTOExample = new UserDTO(10, "olivia.johnson@test.com", "Olivia Johnson");

    private final UserDTO notLoggedUserDTOExample = new UserDTO(6, "thomas.wilson@test.com", "WilT");

    private final List<PostDTO> postsInDatabase = new ArrayList<>() {
        {
            add(new PostDTO(1, "Morbi quis tortor id nulla ultrices aliquet. Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui.",
                    "https://res.cloudinary.com/instamini/image/upload/v1630672586/husky-3380548_1280_rman9o.jpg",
                    LocalDateTime.of(2020, 4, 5, 22, 52, 37),
                    notLoggedUserDTOExample));
            add(new PostDTO(2, "Nullam orci pede, venenatis non, sodales sed, tincidunt eu, felis. Fusce posuere felis sed lacus. Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl. Nunc rhoncus dui vel sem. Sed sagittis.",
                    "https://res.cloudinary.com/instamini/image/upload/v1630672584/dolphin-203875_1280_n67crm.jpg",
                    LocalDateTime.of(2020, 12, 20, 7, 51, 2),
                    loggedUserDTOExample));
            add(new PostDTO(3, "In est risus, auctor sed, tristique in, tempus sit amet, sem. Felis sed interdum venenatis, turpis enim blandit mi, in porttitor pede justo eu massa. Donec dapibus. Duis at velit eu est congue elementum.",
                    "https://res.cloudinary.com/instamini/image/upload/v1630672583/pug-690566_1280_s4wmbu.jpg",
                    LocalDateTime.of(2021, 5, 25, 12, 12, 22),
                    new UserDTO(2, "george.jones@test.com", "_Georgie_")));
            add(new PostDTO(4, "Nulla ac enim. In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem.",
                    "https://res.cloudinary.com/instamini/image/upload/v1630672586/husky-3380548_1280_rman9o.jpg",
                    LocalDateTime.of(2021, 2, 6, 16, 15, 7),
                    loggedUserDTOExample));
        }
    };

    @BeforeEach
    void setUp() {
        mockJwtAuthenticationUtil = new MockJwtAuthenticationUtil(jwtUtil, loggedUserDTOExample, "exists");
    }

    @Test
    void all401() {
        RestAssured.given()
                .when()
                    .get("/posts")
                .then().statusCode(401);

        mockJwtAuthenticationUtil.verify(0);
    }

    @Test
    void all200() {
        mockJwtAuthenticationUtil.mockReturns();

        String result = RestAssured.given()
                  .header("Authorization", "Bearer " + mockJwtAuthenticationUtil.getMockedToken())
                .when()
                    .get("/posts")
                .then()
                    .statusCode(200)
                    .contentType("application/json")
                    .extract().body().asString();

        for (int i = 0; i < postsInDatabase.size(); i++) {
            AssertPostsIntegrationUtil.assertPosts(result, postsInDatabase.get(i), i);
        }

        mockJwtAuthenticationUtil.verify(1);
    }

    @Test
    void byID401() {
        PostDTO postDTOToFind = postsInDatabase.get(0);

        RestAssured.given()
                .when()
                    .get("/posts/{id}", postDTOToFind.getId())
                .then()
                    .statusCode(401);

        mockJwtAuthenticationUtil.verify(0);
    }

    @Test
    void byID404() {
        mockJwtAuthenticationUtil.mockReturns();

        RestAssured.given()
                    .header("Authorization", "Bearer " + mockJwtAuthenticationUtil.getMockedToken())
                .when()
                    .get("/posts/{id}", 0)
                .then()
                    .statusCode(404);

        mockJwtAuthenticationUtil.verify(1);
    }

    @Test
    void byID200() {
        PostDTO postDTOToFind = postsInDatabase.get(0);

        mockJwtAuthenticationUtil.mockReturns();

        String result = RestAssured.given()
                    .header("Authorization", "Bearer " + mockJwtAuthenticationUtil.getMockedToken())
                .when()
                    .get("/posts/{id}", postDTOToFind.getId())
                .then()
                    .statusCode(200)
                    .contentType("application/json")
                    .extract().body().asString();

        AssertPostsIntegrationUtil.assertPosts(result, postDTOToFind);

        mockJwtAuthenticationUtil.verify(1);
    }

    @Test
    void create401() throws FileNotFoundException {
        String path = "/src/integrationTest/resources/image-test/test.png";
        PostCreateDTO postCreateDTO = new PostCreateDTO("test", notLoggedUserDTOExample.getId(), new FileInputStream(System.getProperty("user.dir") + path));

        RestAssured.given()
                    .multiPart(getMultiPart(path))
                    .formParam("userId", postCreateDTO.getUserId())
                    .formParam("description", postCreateDTO.getDescription())
                .when()
                    .post("/posts")
                .then()
                    .statusCode(401);

        mockJwtAuthenticationUtil.verify(0);
    }

    @Test
    void create403() throws FileNotFoundException {
        String path = "/src/integrationTest/resources/image-test/test.png";
        PostCreateDTO postCreateDTO = new PostCreateDTO("test", notLoggedUserDTOExample.getId(), new FileInputStream(System.getProperty("user.dir") + path));

        mockJwtAuthenticationUtil.mockReturns();

        RestAssured.given()
                    .header("Authorization", "Bearer " + mockJwtAuthenticationUtil.getMockedToken())
                    .multiPart(getMultiPart(path))
                    .formParam("userId", postCreateDTO.getUserId())
                    .formParam("description", postCreateDTO.getDescription())
                .when()
                    .post("/posts")
                .then()
                    .statusCode(403);

        mockJwtAuthenticationUtil.verify(1);
    }

    @Test
    void create201() throws IOException {
        String path = "/src/integrationTest/resources/image-test/test.png";
        PostCreateDTO postCreateDTO = new PostCreateDTO("test", loggedUserDTOExample.getId(), new FileInputStream(System.getProperty("user.dir") + path));

        mockJwtAuthenticationUtil.mockReturns();

        String result = RestAssured.given()
                    .header("Authorization", "Bearer " + mockJwtAuthenticationUtil.getMockedToken())
                    .multiPart(getMultiPart(path))
                    .formParam("userId", postCreateDTO.getUserId())
                    .formParam("description", postCreateDTO.getDescription())
                .when()
                    .post("/posts")
                .then()
                    .statusCode(201)
                    .contentType("application/json")
                    .extract().body().asString();

        try {
            AssertPostsIntegrationUtil.assertPosts(result, postCreateDTO);

            mockJwtAuthenticationUtil.verify(1);
        } finally {
            JsonPath jsonPath = new JsonPath(result);
            String photoUrl = jsonPath.getString("photoUrl");
            String publicId = photoUrl.substring(photoUrl.lastIndexOf("/") + 1, photoUrl.lastIndexOf("."));
            imageUploadService.destroy(publicId);
        }
    }

    @Test
    void create400() throws FileNotFoundException {
        String path = "/src/integrationTest/resources/image-test/not_image.txt";
        PostCreateDTO postCreateDTO = new PostCreateDTO("test", loggedUserDTOExample.getId(), new FileInputStream(System.getProperty("user.dir") + path));

        mockJwtAuthenticationUtil.mockReturns();

        RestAssured.given()
                    .header("Authorization", "Bearer " + mockJwtAuthenticationUtil.getMockedToken())
                    .multiPart(getMultiPart(path))
                    .formParam("userId", postCreateDTO.getUserId())
                    .formParam("description", postCreateDTO.getDescription())
                .when()
                    .post("/posts")
                .then()
                    .statusCode(400);

        mockJwtAuthenticationUtil.verify(1);
    }

    @Test
    void create400Valid() throws FileNotFoundException {
        String path = "/src/integrationTest/resources/image-test/test.png";
        PostCreateDTO postCreateDTO = new PostCreateDTO("test", loggedUserDTOExample.getId(), new FileInputStream(System.getProperty("user.dir") + path));

        mockJwtAuthenticationUtil.mockReturns();

        RestAssured.given()
                    .header("Authorization", "Bearer " + mockJwtAuthenticationUtil.getMockedToken())
                    .multiPart(getMultiPart(path))
                    .formParam("description", postCreateDTO.getDescription())
                .when()
                    .post("/posts")
                .then()
                    .statusCode(400);

        mockJwtAuthenticationUtil.verify(1);
    }

    @Test
    void editDescription401() {
        EditPostDescriptionRequestBody requestBody = new EditPostDescriptionRequestBody("new description");

        RestAssured.given()
                    .contentType("application/json")
                    .body(requestBody.toJson())
                .when()
                    .put("/posts/{id}/editDescription", postsInDatabase.get(0).getId())
                .then()
                    .statusCode(401);

        mockJwtAuthenticationUtil.verify(0);
    }

    @Test
    void editDescription403() {
        EditPostDescriptionRequestBody requestBody = new EditPostDescriptionRequestBody("new description");

        mockJwtAuthenticationUtil.mockReturns();

        RestAssured.given()
                    .contentType("application/json")
                    .body(requestBody.toJson())
                    .header("Authorization", "Bearer " + mockJwtAuthenticationUtil.getMockedToken())
                .when()
                    .put("/posts/{id}/editDescription", postsInDatabase.get(0).getId()) // Editing post, which does not belong to logged user
                .then()
                    .statusCode(403);

        mockJwtAuthenticationUtil.verify(1);
    }

    @Test
    void editDescription404() {
        EditPostDescriptionRequestBody requestBody = new EditPostDescriptionRequestBody("new description");

        mockJwtAuthenticationUtil.mockReturns();

        RestAssured.given()
                    .contentType("application/json")
                    .body(requestBody.toJson())
                    .header("Authorization", "Bearer " + mockJwtAuthenticationUtil.getMockedToken())
                .when()
                    .put("/posts/{id}/editDescription", 0)
                .then()
                    .statusCode(404);

        mockJwtAuthenticationUtil.verify(1);
    }

    @Test
    void editDescription200() {
        EditPostDescriptionRequestBody requestBody = new EditPostDescriptionRequestBody("new description");

        mockJwtAuthenticationUtil.mockReturns();


        String result = RestAssured.given()
                    .contentType("application/json")
                    .body(requestBody.toJson())
                    .header("Authorization", "Bearer " + mockJwtAuthenticationUtil.getMockedToken())
                .when()
                    .put("/posts/{id}/editDescription", postsInDatabase.get(1).getId())
                .then()
                    .statusCode(200)
                    .contentType("application/json")
                    .extract().body().asString();

        AssertPostsIntegrationUtil.assertPostsInEditDescriptionTest(result, postsInDatabase.get(1), requestBody.getNewDescription());

        mockJwtAuthenticationUtil.verify(1);
    }

    @Test
    void editDescription400Valid() {
        mockJwtAuthenticationUtil.mockReturns();

        RestAssured.given()
                    .contentType("application/json")
                    .body("{}") // missing body
                    .header("Authorization", "Bearer " + mockJwtAuthenticationUtil.getMockedToken())
                .when()
                    .put("/posts/{id}/editDescription", postsInDatabase.get(1).getId())
                .then()
                    .statusCode(400);

        mockJwtAuthenticationUtil.verify(1);
    }

    @Test
    void delete401() {
        RestAssured.given()
                .when()
                    .delete("/posts/{id}", postsInDatabase.get(0).getId())
                .then()
                    .statusCode(401);

        mockJwtAuthenticationUtil.verify(0);
    }

    @Test
    void delete403() {
        mockJwtAuthenticationUtil.mockReturns();

        RestAssured.given()
                    .header("Authorization", "Bearer " + mockJwtAuthenticationUtil.getMockedToken())
                .when()
                    .delete("/posts/{id}", postsInDatabase.get(0).getId()) // Deleting post, which does not belong to logged user
                .then()
                    .statusCode(403);

        mockJwtAuthenticationUtil.verify(1);
    }

    @Test
    void delete404() {
        mockJwtAuthenticationUtil.mockReturns();

        RestAssured.given()
                    .header("Authorization", "Bearer " + mockJwtAuthenticationUtil.getMockedToken())
                .when()
                    .delete("/posts/{id}", 0)
                .then()
                    .statusCode(404);

        mockJwtAuthenticationUtil.verify(1);
    }

    @Test
    void delete204() {
        mockJwtAuthenticationUtil.mockReturns();

        RestAssured.given()
                    .header("Authorization", "Bearer " + mockJwtAuthenticationUtil.getMockedToken())
                .when()
                    .delete("/posts/{id}", postsInDatabase.get(1).getId())
                .then()
                    .statusCode(204);

        mockJwtAuthenticationUtil.verify(1);
    }

    @Test
    void findAllPostsOfUser401() {
        RestAssured.given()
                    .param("user", loggedUserDTOExample.getId().toString())
                .when()
                    .get("/posts/ofUser")
                .then()
                    .statusCode(401);

        mockJwtAuthenticationUtil.verify(0);
    }

    @Test
    void findAllPostsOfUser404() {
        mockJwtAuthenticationUtil.mockReturns();

        RestAssured.given()
                    .param("user", "0")
                    .header("Authorization", "Bearer " + mockJwtAuthenticationUtil.getMockedToken())
                .when()
                    .get("/posts/ofUser")
                .then()
                    .statusCode(404);

        mockJwtAuthenticationUtil.verify(1);
    }

    @Test
    void findAllPostsOfUser200() {
        mockJwtAuthenticationUtil.mockReturns();

        String result = RestAssured.given()
                    .param("user", loggedUserDTOExample.getId().toString())
                    .header("Authorization", "Bearer " + mockJwtAuthenticationUtil.getMockedToken())
                .when()
                    .get("/posts/ofUser")
                .then()
                    .statusCode(200)
                    .contentType("application/json")
                    .extract().body().asString();

        // logged user has 2 posts in database
        AssertPostsIntegrationUtil.assertPosts(result, postsInDatabase.get(3), 0);
        AssertPostsIntegrationUtil.assertPosts(result, postsInDatabase.get(1), 1);

        mockJwtAuthenticationUtil.verify(1);
    }

    @Test
    void findCountOfAllPostsOfUser401() {
        RestAssured.given()
                    .param("user", loggedUserDTOExample.getId().toString())
                .when()
                    .get("/posts/count")
                .then()
                    .statusCode(401);

        mockJwtAuthenticationUtil.verify(0);
    }

    @Test
    void findCountOfAllPostsOfUser404() {
        mockJwtAuthenticationUtil.mockReturns();

        RestAssured.given()
                    .param("user", "0")
                    .header("Authorization", "Bearer " + mockJwtAuthenticationUtil.getMockedToken())
                .when()
                    .get("/posts/count")
                .then()
                    .statusCode(404);

        mockJwtAuthenticationUtil.verify(1);
    }

    @Test
    void findCountOfAllPostsOfUser200() {
        mockJwtAuthenticationUtil.mockReturns();

        String result = RestAssured.given()
                    .param("user", loggedUserDTOExample.getId().toString())
                    .header("Authorization", "Bearer " + mockJwtAuthenticationUtil.getMockedToken())
                .when()
                    .get("/posts/count")
                .then()
                    .statusCode(200)
                    .contentType("application/json")
                    .extract().body().asString();

        Assertions.assertEquals(result, "2"); // logged user has 2 posts in database

        mockJwtAuthenticationUtil.verify(1);
    }

    private MultiPartSpecification getMultiPart(String path) throws FileNotFoundException {
        return new MultiPartSpecBuilder(new FileInputStream(System.getProperty("user.dir") + path))
                .fileName("image")
                .controlName("image")
                .mimeType("image/png")
                .build();
    }
}
