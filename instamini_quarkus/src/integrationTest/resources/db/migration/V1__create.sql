CREATE TABLE User
(
    id       int         NOT NULL AUTO_INCREMENT,
    email    varchar(50) NOT NULL,
    username varchar(50) NOT NULL,

    PRIMARY KEY (id),
    UNIQUE KEY ind_55 (email),
    UNIQUE KEY ind_56 (username)
);

CREATE TABLE Follow
(
    id           int NOT NULL AUTO_INCREMENT,
    user_id_who  int NOT NULL,
    user_id_whom int NOT NULL,

    PRIMARY KEY (id),
    UNIQUE KEY ind_84 (user_id_who, user_id_whom),
    FOREIGN KEY (user_id_who) REFERENCES User (id),
    FOREIGN KEY (user_id_whom) REFERENCES User (id)
);

CREATE TABLE Post
(
    id          int           NOT NULL AUTO_INCREMENT,
    description varchar(500)  NOT NULL,
    photo_url   varchar(2048) NOT NULL,
    time        datetime      NOT NULL,
    user_id     int           NOT NULL,

    PRIMARY KEY (id),
    FOREIGN KEY (user_id) REFERENCES User (id)
);

CREATE TABLE Comment
(
    id      int          NOT NULL AUTO_INCREMENT,
    text    varchar(500) NOT NULL,
    time    datetime     NOT NULL,
    post_id int          NOT NULL,
    user_id int          NOT NULL,

    PRIMARY KEY (id),
    FOREIGN KEY (post_id) REFERENCES Post (id),
    FOREIGN KEY (user_id) REFERENCES User (id)
);

CREATE TABLE "Like"

(
    id      int NOT NULL AUTO_INCREMENT,
    user_id int NOT NULL,
    post_id int NOT NULL,

    PRIMARY KEY (id),
    UNIQUE KEY ind_88 (post_id, user_id),
    FOREIGN KEY (user_id) REFERENCES User (id),
    FOREIGN KEY (post_id) REFERENCES Post (id)
);