package util;

import org.junit.jupiter.api.Assertions;
import zlack.bra.instamini.business.dto.PostDTO;
import zlack.bra.instamini.business.service.impl.UserService;
import zlack.bra.instamini.data.entity.Post;

public class AssertPostsUnitUtil {

    public static void assertPosts(PostDTO post1, PostDTO post2) {
        Assertions.assertEquals(post1.getId(), post2.getId());
        Assertions.assertEquals(post1.getDescription(), post2.getDescription());
        Assertions.assertEquals(post1.getTime(), post2.getTime());
        Assertions.assertEquals(post1.getPhotoUrl(), post2.getPhotoUrl());
        Assertions.assertEquals(post1.getUser(), post2.getUser());
    }

    public static void assertPosts(Post post1, PostDTO post2, UserService userService) {
        Assertions.assertEquals(post1.getId(), post2.getId());
        Assertions.assertEquals(post1.getDescription(), post2.getDescription());
        Assertions.assertFalse(post2.getTime().toString().isEmpty());
        Assertions.assertEquals(post1.getPhotoUrl(), post2.getPhotoUrl());
        Assertions.assertEquals(userService.toDTO(post1.getUser()), post2.getUser());
    }

    public static void assertPostsInEditDescriptionTest(Post post1, PostDTO post2, UserService userService, String newDescription) {
        Assertions.assertEquals(post1.getId(), post2.getId());
        Assertions.assertEquals(newDescription, post2.getDescription());
        Assertions.assertEquals(post1.getTime(), post2.getTime());
        Assertions.assertEquals(post1.getPhotoUrl(), post2.getPhotoUrl());
        Assertions.assertEquals(userService.toDTO(post1.getUser()), post2.getUser());
    }
}
