package util;

import org.junit.jupiter.api.Assertions;
import zlack.bra.instamini.business.dto.LikeDTO;
import zlack.bra.instamini.business.service.impl.PostService;
import zlack.bra.instamini.business.service.impl.UserService;
import zlack.bra.instamini.data.entity.Like;

public class AssertLikesUnitUtil {

    public static void assertLikes(Like like1, LikeDTO like2, UserService userService, PostService postService) {
        Assertions.assertEquals(like1.getId(), like2.getId());
        Assertions.assertEquals(userService.toDTO(like1.getUser()), like2.getUser());
        Assertions.assertEquals(postService.toDTO(like1.getPost()), like2.getPost());
    }

    public static void assertLikes(LikeDTO like1, LikeDTO like2) {
        Assertions.assertEquals(like1.getId(), like2.getId());
        Assertions.assertEquals(like1.getUser(), like2.getUser());
        Assertions.assertEquals(like1.getPost(), like2.getPost());
    }
}
