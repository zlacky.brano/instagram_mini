package util;

import org.junit.jupiter.api.Assertions;
import zlack.bra.instamini.business.dto.CommentCreateDTO;
import zlack.bra.instamini.business.dto.CommentDTO;
import zlack.bra.instamini.business.service.impl.PostService;
import zlack.bra.instamini.business.service.impl.UserService;
import zlack.bra.instamini.data.entity.Comment;

public class AssertCommentsUnitUtil {

    public static void assertComments(Comment comment1, CommentDTO comment2, UserService userService, PostService postService) {
        Assertions.assertEquals(comment1.getId(), comment2.getId());
        Assertions.assertEquals(comment1.getText(), comment2.getText());
        Assertions.assertFalse(comment2.getTime().toString().isEmpty());
        Assertions.assertEquals(userService.toDTO(comment1.getUser()), comment2.getUser());
        Assertions.assertEquals(postService.toDTO(comment1.getPost()), comment2.getPost());
    }

    public static void assertCommentsInEditTextTest(Comment comment1, CommentDTO comment2,
                                                        UserService userService, PostService postService, String newText) {
        Assertions.assertEquals(comment1.getId(), comment2.getId());
        Assertions.assertEquals(newText, comment2.getText());
        Assertions.assertEquals(comment1.getTime(), comment2.getTime());
        Assertions.assertEquals(userService.toDTO(comment1.getUser()), comment2.getUser());
        Assertions.assertEquals(postService.toDTO(comment1.getPost()), comment2.getPost());
    }

    public static void assertComments(CommentDTO comment1, CommentDTO comment2) {
        Assertions.assertEquals(comment1.getId(), comment2.getId());
        Assertions.assertEquals(comment1.getText(), comment2.getText());
        Assertions.assertEquals(comment1.getTime(), comment2.getTime());
        Assertions.assertEquals(comment1.getUser(), comment2.getUser());
        Assertions.assertEquals(comment1.getPost(), comment2.getPost());
    }
}
