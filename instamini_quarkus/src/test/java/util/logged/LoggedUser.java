package util.logged;

import zlack.bra.instamini.business.dto.UserDTO;

import javax.ws.rs.core.SecurityContext;
import java.security.Principal;


public class LoggedUser {

    public static final UserDTO user = new UserDTO(666, "test", "test");

    public static final SecurityContext securityContext = new SecurityContext() {
        @Override
        public Principal getUserPrincipal() {
            return new Principal() {
                @Override
                public String getName() {
                    return user.getId().toString();
                }
            };
        }

        @Override
        public boolean isUserInRole(String role) {
            return false;
        }

        @Override
        public boolean isSecure() {
            return false;
        }

        @Override
        public String getAuthenticationScheme() {
            return "Bearer";
        }
    };
}
