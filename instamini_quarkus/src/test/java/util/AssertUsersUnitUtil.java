package util;

import org.junit.jupiter.api.Assertions;
import zlack.bra.instamini.business.dto.UserDTO;
import zlack.bra.instamini.data.entity.User;

public class AssertUsersUnitUtil {

    public static void assertUsers(User user1, UserDTO user2) {
        Assertions.assertEquals(user1.getId(), user2.getId());
        Assertions.assertEquals(user1.getUsername(), user2.getUsername());
        Assertions.assertEquals(user1.getEmail(), user2.getEmail());
    }

    public static void assertUsersInEditUsernameTest(User user1, UserDTO user2, String newUsername) {
        Assertions.assertEquals(user1.getId(), user2.getId());
        Assertions.assertEquals(newUsername, user2.getUsername());
        Assertions.assertEquals(user1.getEmail(), user2.getEmail());
    }

    public static void assertUsers(UserDTO user1, UserDTO user2) {
        Assertions.assertEquals(user1.getId(), user2.getId());
        Assertions.assertEquals(user1.getUsername(), user2.getUsername());
        Assertions.assertEquals(user1.getEmail(), user2.getEmail());
    }
}
