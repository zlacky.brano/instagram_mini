package zlack.bra.instamini.business.service.impl;

import io.quarkus.hibernate.orm.panache.PanacheQuery;
import junit.framework.AssertionFailedError;
import org.elasticsearch.client.RestHighLevelClient;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import util.AssertFollowsUnitUtil;
import util.fieldsetter.FieldSetter;
import zlack.bra.instamini.business.dto.FollowCreateDTO;
import zlack.bra.instamini.business.dto.FollowDTO;
import zlack.bra.instamini.business.exception.FollowYourselfException;
import zlack.bra.instamini.business.exception.NotFoundException;
import zlack.bra.instamini.data.entity.Follow;
import zlack.bra.instamini.data.entity.User;
import zlack.bra.instamini.data.repository.FollowRepository;
import zlack.bra.instamini.data.repository.PostRepository;
import zlack.bra.instamini.data.repository.UserRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


@RunWith(MockitoJUnitRunner.class)
class FollowServiceTest {

    private AutoCloseable autoCloseable;

    private UserService userService;
    private FollowService followService;

    @Mock
    private FollowRepository followRepository;

    @Mock
    private UserRepository userRepository;

    @Mock
    private PostRepository postRepository;

    @Mock
    private RestHighLevelClient highLevelClient;

    @Mock
    private PanacheQuery<Follow> followQuery;

    @BeforeEach
    void setUp() {
        autoCloseable = MockitoAnnotations.openMocks(this);
        userService = new UserService(userRepository, postRepository, "test", highLevelClient);
        followService = new FollowService(followRepository, userRepository, userService);
    }

    @AfterEach
    void tearDown() throws Exception {
        autoCloseable.close();
    }

    @Test
    void create() throws NoSuchFieldException, IllegalAccessException {
        Integer userWhoId = 1;
        Integer userWhomId = 2;
        FollowCreateDTO followCreateDTO = new FollowCreateDTO(userWhoId, userWhomId);

        User userWho = new User("test", "test", null, null, null, null, null);
        User userWhom = new User("test", "test", null, null, null, null, null);

        FieldSetter.setUserId(userWho, userWhoId);
        FieldSetter.setUserId(userWhom, userWhomId);

        Follow follow = new Follow(userWho, userWhom);

        Mockito.when(userRepository.findByIdOptional(userWhoId)).thenReturn(Optional.of(userWho));
        Mockito.when(userRepository.findByIdOptional(userWhomId)).thenReturn(Optional.of(userWhom));

        FollowDTO followDTO;
        try {
            followDTO = followService.create(followCreateDTO);
        } catch (FollowYourselfException | NotFoundException e) {
            throw new AssertionFailedError();
        }

        Assertions.assertNotNull(followDTO);
        AssertFollowsUnitUtil.assertFollows(follow, followDTO, userService);

        Mockito.verify(followRepository, Mockito.times(1)).persistAndFlush(Mockito.any());
        Mockito.verify(userRepository, Mockito.times(1)).findByIdOptional(userWhoId);
        Mockito.verify(userRepository, Mockito.times(1)).findByIdOptional(userWhomId);
    }

    @Test
    void createNotFoundUserWho() throws NoSuchFieldException, IllegalAccessException {
        Integer userWhoId = 1;
        Integer userWhomId = 2;
        FollowCreateDTO followCreateDTO = new FollowCreateDTO(userWhoId, userWhomId);

        User userWho = new User("test", "test", null, null, null, null, null);
        User userWhom = new User("test", "test", null, null, null, null, null);

        FieldSetter.setUserId(userWho, userWhoId);
        FieldSetter.setUserId(userWhom, userWhomId);

        Mockito.when(userRepository.findByIdOptional(userWhoId)).thenReturn(Optional.empty());
        Mockito.when(userRepository.findByIdOptional(userWhomId)).thenReturn(Optional.of(userWhom));

        try {
            followService.create(followCreateDTO);
            throw new AssertionFailedError();
        } catch (FollowYourselfException e) {
            throw new AssertionFailedError();
        } catch (NotFoundException e) {
            Mockito.verify(followRepository, Mockito.times(0)).persistAndFlush(Mockito.any());
            Mockito.verify(userRepository, Mockito.times(1)).findByIdOptional(userWhoId);
            Mockito.verify(userRepository, Mockito.times(1)).findByIdOptional(userWhomId);
        }
    }

    @Test
    void createNotFoundUserWhom() throws NoSuchFieldException, IllegalAccessException {
        Integer userWhoId = 1;
        Integer userWhomId = 2;
        FollowCreateDTO followCreateDTO = new FollowCreateDTO(userWhoId, userWhomId);

        User userWho = new User("test", "test", null, null, null, null, null);
        User userWhom = new User("test", "test", null, null, null, null, null);

        FieldSetter.setUserId(userWho, userWhoId);
        FieldSetter.setUserId(userWhom, userWhomId);

        Mockito.when(userRepository.findByIdOptional(userWhoId)).thenReturn(Optional.of(userWho));
        Mockito.when(userRepository.findByIdOptional(userWhomId)).thenReturn(Optional.empty());

        try {
            followService.create(followCreateDTO);
            throw new AssertionFailedError();
        } catch (FollowYourselfException e) {
            throw new AssertionFailedError();
        } catch (NotFoundException e) {
            Mockito.verify(followRepository, Mockito.times(0)).persistAndFlush(Mockito.any());
            Mockito.verify(userRepository, Mockito.times(1)).findByIdOptional(userWhoId);
            Mockito.verify(userRepository, Mockito.times(1)).findByIdOptional(userWhomId);
        }
    }

    @Test
    void createFollowYourself() throws NoSuchFieldException, IllegalAccessException {
        Integer userId = 2;
        FollowCreateDTO followCreateDTO = new FollowCreateDTO(userId, userId);

        User user = new User("test", "test", null, null, null, null, null);

        FieldSetter.setUserId(user, userId);
        
        Mockito.when(userRepository.findByIdOptional(userId)).thenReturn(Optional.of(user));

        try {
            followService.create(followCreateDTO);
            throw new AssertionFailedError();
        } catch (FollowYourselfException e) {
            Mockito.verify(followRepository, Mockito.times(0)).persistAndFlush(Mockito.any());
            Mockito.verify(userRepository, Mockito.times(2)).findByIdOptional(userId);
        } catch (NotFoundException e) {
            throw new AssertionFailedError();
        }
    }

    @Test
    void findAll() throws NoSuchFieldException, IllegalAccessException {
        List<Follow> follows = new ArrayList<>();
        int numberOfItems = 3;
        int idOfUser = 1;

        for (int i = 1; i <= numberOfItems; i++) {
            User userWho = new User("1test" + i, "1test" + i, null, null, null, null, null);
            User userWhom = new User("2test" + i, "2test" + i, null, null, null, null, null);
            FieldSetter.setUserId(userWho, idOfUser++);
            FieldSetter.setUserId(userWhom, idOfUser++);

            Follow follow = new Follow(userWho, userWhom);
            FieldSetter.setFollowId(follow, i);
            follows.add(follow);
        }

        Mockito.when(followQuery.stream()).thenReturn(follows.stream());
        Mockito.when(followRepository.findAll()).thenReturn(followQuery);

        List<FollowDTO> followsDTO = followService.findAll();

        Assertions.assertEquals(follows.size(), followsDTO.size());
        for (int i = 0; i < followsDTO.size(); i++) {
            AssertFollowsUnitUtil.assertFollows(follows.get(i), followsDTO.get(i), userService);
        }
        Mockito.verify(followRepository, Mockito.times(1)).findAll();
        Mockito.verify(followQuery, Mockito.times(1)).stream();
    }

    @Test
    void findAllEmpty() {
        List<Follow> follows = new ArrayList<>();

        Mockito.when(followQuery.stream()).thenReturn(follows.stream());
        Mockito.when(followRepository.findAll()).thenReturn(followQuery);

        List<FollowDTO> followsDTO = followService.findAll();

        Assertions.assertEquals(follows.size(), followsDTO.size());
        Mockito.verify(followRepository, Mockito.times(1)).findAll();
        Mockito.verify(followQuery, Mockito.times(1)).stream();
    }

    @Test
    void findById() throws NoSuchFieldException, IllegalAccessException {
        Integer userWhoId = 1;
        Integer userWhomId = 2;
        User userWho = new User("1test", "1test", null, null, null, null, null);
        User userWhom = new User("2test", "2test", null, null, null, null, null);
        FieldSetter.setUserId(userWho, userWhoId);
        FieldSetter.setUserId(userWhom, userWhomId);

        Integer followId = 1;
        Follow follow = new Follow(userWho, userWhom);
        FieldSetter.setFollowId(follow, followId);

        Mockito.when(followRepository.findByIdOptional(followId)).thenReturn(Optional.of(follow));

        Optional<FollowDTO> followDTOOptional = followService.findById(followId);

        if (followDTOOptional.isPresent()) {
            FollowDTO followDTO = followDTOOptional.get();
            AssertFollowsUnitUtil.assertFollows(follow, followDTO, userService);
        }
        else {
            throw new AssertionFailedError();
        }
        Mockito.verify(followRepository, Mockito.times(1)).findByIdOptional(followId);
    }

    @Test
    void findByIdNotFound() {
        Integer followId = 1;

        Mockito.when(followRepository.findByIdOptional(followId)).thenReturn(Optional.empty());

        Optional<FollowDTO> followDTOOptional = followService.findById(followId);

        if (followDTOOptional.isPresent()) {
            throw new AssertionFailedError();
        } else {
            Mockito.verify(followRepository, Mockito.times(1)).findByIdOptional(followId);
        }
    }

    @Test
    void toDTO() throws NoSuchFieldException, IllegalAccessException {
        Integer userWhoId = 1;
        Integer userWhomId = 2;
        User userWho = new User("1test", "1test", null, null, null, null, null);
        User userWhom = new User("2test", "2test", null, null, null, null, null);
        FieldSetter.setUserId(userWho, userWhoId);
        FieldSetter.setUserId(userWhom, userWhomId);

        Integer followId = 1;
        Follow follow = new Follow(userWho, userWhom);
        FieldSetter.setFollowId(follow, followId);

        FollowDTO followDTO = followService.toDTO(follow);

        AssertFollowsUnitUtil.assertFollows(follow, followDTO, userService);
    }

    @Test
    void toDTOOptional() throws NoSuchFieldException, IllegalAccessException {
        Integer userWhoId = 1;
        Integer userWhomId = 2;
        User userWho = new User("1test", "1test", null, null, null, null, null);
        User userWhom = new User("2test", "2test", null, null, null, null, null);
        FieldSetter.setUserId(userWho, userWhoId);
        FieldSetter.setUserId(userWhom, userWhomId);

        Integer followId = 1;
        Follow follow = new Follow(userWho, userWhom);
        FieldSetter.setFollowId(follow, followId);

        Optional<FollowDTO> followDTO = followService.toDTO(Optional.of(follow));

        if (followDTO.isEmpty()) {
            throw new AssertionFailedError();
        }

        AssertFollowsUnitUtil.assertFollows(follow, followDTO.get(), userService);
    }

    @Test
    void deleteById() {
        Integer followId = 1;

        Mockito.when(followRepository.findByIdOptional(followId)).thenReturn(Optional.of(new Follow()));
        try {
            followService.deleteById(followId);
        } catch (NotFoundException e) {
            throw new AssertionFailedError();
        }

        Mockito.when(followRepository.findByIdOptional(followId)).thenReturn(Optional.empty());
        try {
            followService.deleteById(followId);
            throw new AssertionFailedError();
        } catch (NotFoundException ignored) { }

        Mockito.verify(followRepository, Mockito.times(2)).findByIdOptional(followId);
        Mockito.verify(followRepository, Mockito.times(1)).deleteById(followId);
    }

    @Test
    void findFollowIdByUsers() {
        Integer followId = 1;
        Integer userId1 = 1;
        Integer userId2 = 2;

        Mockito.when(followRepository.findFollowIdByUsers(userId1, userId2)).thenReturn(Optional.of(followId));

        Integer resultId;
        try {
            resultId = followService.findFollowIdByUsers(userId1, userId2);
        } catch (NotFoundException e) {
            throw new AssertionFailedError();
        }

        Assertions.assertEquals(followId, resultId);
        Mockito.verify(followRepository, Mockito.times(1)).findFollowIdByUsers(userId1, userId2);
    }

    @Test
    void findFollowIdByUsersNotFound() {
        Integer userId1 = 1;
        Integer userId2 = 2;

        Mockito.when(followRepository.findFollowIdByUsers(userId1, userId2)).thenReturn(Optional.empty());

        try {
            followService.findFollowIdByUsers(userId1, userId2);
            throw new AssertionFailedError();
        } catch (NotFoundException e) {
            Mockito.verify(followRepository, Mockito.times(1)).findFollowIdByUsers(userId1, userId2);
        }
    }
}
