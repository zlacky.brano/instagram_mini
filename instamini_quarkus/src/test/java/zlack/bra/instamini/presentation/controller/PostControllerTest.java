package zlack.bra.instamini.presentation.controller;

import junit.framework.AssertionFailedError;
import org.apache.commons.io.IOUtils;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import util.AssertPostsUnitUtil;
import util.logged.LoggedUser;
import zlack.bra.instamini.business.dto.PostCreateDTO;
import zlack.bra.instamini.business.dto.PostDTO;
import zlack.bra.instamini.business.dto.UserDTO;
import zlack.bra.instamini.business.exception.ForbiddenException;
import zlack.bra.instamini.business.exception.NotFoundException;
import zlack.bra.instamini.business.exception.UploadFailException;
import zlack.bra.instamini.business.service.impl.PostService;
import zlack.bra.instamini.controller.PostController;
import zlack.bra.instamini.controller.request.EditPostDescriptionRequestBody;

import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static zlack.bra.instamini.constant.Constants.NOT_AN_IMAGE;

@RunWith(MockitoJUnitRunner.class)
class PostControllerTest {

    private AutoCloseable autoCloseable;

    @InjectMocks
    private PostController postController;

    @Mock
    private PostService postService;

    private final LocalDateTime localDateTime = LocalDateTime.of(LocalDate.of(2000, 12, 12), LocalTime.of(10, 10, 10));

    private UserDTO loggedUser;

    @BeforeEach
    void setUp() {
        autoCloseable = MockitoAnnotations.openMocks(this);
        loggedUser = LoggedUser.user;
    }

    @AfterEach
    void tearDown() throws Exception {
        autoCloseable.close();
    }

    @Test
    void all() {
        List<PostDTO> posts = new ArrayList<>();
        int numberOfItems = 3;

        for (int i = 1; i <= numberOfItems; i++) {
            PostDTO post = new PostDTO(i, "description-test" + i, "photo-url" + i, localDateTime,
                    new UserDTO(i, "user" + i, "user" + i));
            posts.add(post);
        }

        Mockito.when(postService.findAll()).thenReturn(posts);

        List<PostDTO> postsOutput = postController.all();

        Assertions.assertEquals(posts.size(), postsOutput.size());
        for (int i = 0; i < postsOutput.size(); i++) {
            AssertPostsUnitUtil.assertPosts(posts.get(i), postsOutput.get(i));
        }
        Mockito.verify(postService, Mockito.times(1)).findAll();
    }

    @Test
    void allEmpty() {
        List<PostDTO> posts = new ArrayList<>();

        Mockito.when(postService.findAll()).thenReturn(posts);

        List<PostDTO> postsOutput = postController.all();

        Assertions.assertEquals(posts.size(), postsOutput.size());

        Mockito.verify(postService, Mockito.times(1)).findAll();
    }

    @Test
    void byID() {
        UserDTO user = new UserDTO(1, "test", "test");
        PostDTO post = new PostDTO(1, "description", "photo url", localDateTime, user);

        Mockito.when(postService.findById(post.getId())).thenReturn(Optional.of(post));

        PostDTO postOutput = postController.byID(post.getId());

        AssertPostsUnitUtil.assertPosts(post, postOutput);

        Mockito.verify(postService, Mockito.times(1)).findById(post.getId());
    }

    @Test
    void byIDNotFound() {
        Integer postId = 1;

        Mockito.when(postService.findById(postId)).thenReturn(Optional.empty());

        try {
            postController.byID(postId);
            throw new AssertionFailedError();
        } catch (javax.ws.rs.NotFoundException e) {
            Mockito.verify(postService, Mockito.times(1)).findById(postId);
        }
    }

    @Test
    void create() throws UploadFailException, NotFoundException {
        InputStream mockImage = IOUtils.toInputStream("image", StandardCharsets.UTF_8);
        PostCreateDTO postCreateDTO = new PostCreateDTO("test", loggedUser.getId(), mockImage);

        Integer postId = 1;
        String photoUrl = "random url";

        PostDTO postDTO = new PostDTO(postId, postCreateDTO.getDescription(), photoUrl, localDateTime, loggedUser);

        Mockito.when(postService.create(postCreateDTO)).thenReturn(postDTO);

        PostDTO postDTOOutput;
        try {
            postDTOOutput = (PostDTO) postController.create(postCreateDTO, LoggedUser.securityContext).getEntity();
        } catch (ForbiddenException e) {
            throw new AssertionFailedError();
        }

        Assertions.assertNotNull(postDTOOutput);
        AssertPostsUnitUtil.assertPosts(postDTO, postDTOOutput);

        Mockito.verify(postService, Mockito.times(1)).create(postCreateDTO);
    }

    @Test
    void createNotFound() throws UploadFailException, NotFoundException {
        InputStream mockImage = IOUtils.toInputStream("image", StandardCharsets.UTF_8);
        PostCreateDTO postCreateDTO = new PostCreateDTO("test", loggedUser.getId(), mockImage);

        Mockito.when(postService.create(postCreateDTO)).thenThrow(new NotFoundException(""));

        try {
            postController.create(postCreateDTO, LoggedUser.securityContext);
            throw new AssertionFailedError();
        } catch (javax.ws.rs.NotFoundException  e) {
            Mockito.verify(postService, Mockito.times(1)).create(postCreateDTO);
        } catch (ForbiddenException e) {
            throw new AssertionFailedError();
        }
    }

    @Test
    void createForbidden() throws UploadFailException, NotFoundException {
        Integer userId = 1;
        InputStream mockImage = IOUtils.toInputStream("image", StandardCharsets.UTF_8);
        PostCreateDTO postCreateDTO = new PostCreateDTO("test", userId, mockImage);

        Integer postId = 1;
        String photoUrl = "random url";
        UserDTO userDTO = new UserDTO(userId, "test", "test");

        PostDTO postDTO = new PostDTO(postId, postCreateDTO.getDescription(), photoUrl, localDateTime, userDTO);

        Mockito.when(postService.create(postCreateDTO)).thenReturn(postDTO);

        try {
            postController.create(postCreateDTO, LoggedUser.securityContext);
            throw new AssertionFailedError();
        } catch (ForbiddenException e) {
            Mockito.verify(postService, Mockito.times(0)).create(postCreateDTO);
        }
    }

    @Test
    void createBadRequest() throws UploadFailException, NotFoundException {
        InputStream mockImage = IOUtils.toInputStream("image", StandardCharsets.UTF_8);
        PostCreateDTO postCreateDTO = new PostCreateDTO("test", loggedUser.getId(), mockImage);

        Mockito.when(postService.create(postCreateDTO)).thenThrow(new UploadFailException(NOT_AN_IMAGE));

        try {
            postController.create(postCreateDTO, LoggedUser.securityContext);
            throw new AssertionFailedError();
        } catch (javax.ws.rs.BadRequestException e) {
            Assertions.assertEquals(e.getMessage(), NOT_AN_IMAGE);
            Mockito.verify(postService, Mockito.times(1)).create(postCreateDTO);
        } catch (ForbiddenException e) {
            throw new AssertionFailedError();
        }
    }

    @Test
    void editDescription() throws NotFoundException {
        Integer postId = 1;
        PostDTO postDTO = new PostDTO(postId, "test", "test", localDateTime, loggedUser);
        EditPostDescriptionRequestBody requestBody = new EditPostDescriptionRequestBody("new description");

        PostDTO newPostDTO = new PostDTO(postId, requestBody.getNewDescription(), postDTO.getPhotoUrl(), postDTO.getTime(), postDTO.getUser());

        Mockito.when(postService.findById(postId)).thenReturn(Optional.of(postDTO));
        Mockito.when(postService.editDescription(postId, requestBody.getNewDescription())).thenReturn(newPostDTO);

        PostDTO postDTOOutput;
        try {
            postDTOOutput = postController.editDescription(postId, requestBody, LoggedUser.securityContext);
        } catch (ForbiddenException e) {
            throw new AssertionFailedError();
        }

        Assertions.assertNotNull(postDTOOutput);
        AssertPostsUnitUtil.assertPosts(newPostDTO, postDTOOutput);

        Mockito.verify(postService, Mockito.times(1)).editDescription(postId, requestBody.getNewDescription());
        Mockito.verify(postService, Mockito.times(1)).findById(postId);
    }

    @Test
    void editDescriptionNotFound() throws NotFoundException {
        Integer postId = 1;
        EditPostDescriptionRequestBody requestBody = new EditPostDescriptionRequestBody("new description");

        Mockito.when(postService.findById(postId)).thenReturn(Optional.empty());
        Mockito.when(postService.editDescription(postId, requestBody.getNewDescription())).thenThrow(new NotFoundException(""));

        try {
            postController.editDescription(postId, requestBody, LoggedUser.securityContext);
            throw new AssertionFailedError();
        } catch (javax.ws.rs.NotFoundException e) {
            Mockito.verify(postService, Mockito.times(1)).editDescription(postId, requestBody.getNewDescription());
            Mockito.verify(postService, Mockito.times(1)).findById(postId);
        } catch (ForbiddenException e) {
            throw new AssertionFailedError();
        }
    }

    @Test
    void editDescriptionForbidden() throws NotFoundException {
        Integer postId = 1;
        Integer userId = 1;
        UserDTO userDTO = new UserDTO(userId, "test", "test");
        PostDTO postDTO = new PostDTO(postId, "test", "test", localDateTime, userDTO);
        EditPostDescriptionRequestBody requestBody = new EditPostDescriptionRequestBody("new description");

        PostDTO newPostDTO = new PostDTO(postId, requestBody.getNewDescription(), postDTO.getPhotoUrl(), postDTO.getTime(), postDTO.getUser());

        Mockito.when(postService.findById(postId)).thenReturn(Optional.of(postDTO));
        Mockito.when(postService.editDescription(postId, requestBody.getNewDescription())).thenReturn(newPostDTO);

        try {
            postController.editDescription(postId, requestBody, LoggedUser.securityContext);
            throw new AssertionFailedError();
        } catch (ForbiddenException e) {
            Mockito.verify(postService, Mockito.times(0)).editDescription(postId, requestBody.getNewDescription());
            Mockito.verify(postService, Mockito.times(1)).findById(postId);
        }
    }

    @Test
    void delete() throws NotFoundException {
        Integer postId = 1;
        Optional<PostDTO> postDTOOptional = Optional.of(new PostDTO(postId, "test", "test", localDateTime, loggedUser));

        Mockito.when(postService.findById(postId)).thenReturn(postDTOOptional);
        try {
            postController.delete(postId, LoggedUser.securityContext);
        } catch (ForbiddenException e) {
            throw new AssertionFailedError();
        }

        Mockito.verify(postService, Mockito.times(1)).findById(postId);
        Mockito.verify(postService, Mockito.times(1)).deleteById(postId);
    }

    @Test
    void deleteNotFound() throws NotFoundException {
        Integer postId = 1;

        Mockito.when(postService.findById(postId)).thenReturn(Optional.empty());
        Mockito.doThrow(new NotFoundException("")).when(postService).deleteById(postId);
        try {
            postController.delete(postId, LoggedUser.securityContext);
            throw new AssertionFailedError();
        } catch (javax.ws.rs.NotFoundException e) {
            Mockito.verify(postService, Mockito.times(1)).findById(postId);
            Mockito.verify(postService, Mockito.times(1)).deleteById(postId);
        } catch (ForbiddenException e) {
            throw new AssertionFailedError();
        }
    }

    @Test
    void deleteForbidden() throws NotFoundException {
        Integer postId = 1;
        Integer userId = 1;

        UserDTO userDTO = new UserDTO(userId, "test", "test");

        Optional<PostDTO> postDTOOptional = Optional.of(new PostDTO(postId, "test", "test", localDateTime, userDTO));

        Mockito.when(postService.findById(postId)).thenReturn(postDTOOptional);
        try {
            postController.delete(postId, LoggedUser.securityContext);
            throw new AssertionFailedError();
        } catch (ForbiddenException e) {
            Mockito.verify(postService, Mockito.times(1)).findById(postId);
            Mockito.verify(postService, Mockito.times(0)).deleteById(postId);
        }
    }

    @Test
    void findAllPostsOfUser() throws NotFoundException {
        Integer userId = 1;
        UserDTO userDTO = new UserDTO(userId, "test", "test");

        List<PostDTO> postsDTO = new ArrayList<>();
        int numberOfPosts = 5;
        for (int i = 1; i <= numberOfPosts; i++) {
            PostDTO postDTO = new PostDTO(i, "description", "photo url", localDateTime, userDTO);
            postsDTO.add(postDTO);
        }

        Mockito.when(postService.findAllPostsOfUser(userId)).thenReturn(postsDTO);

        List<PostDTO> outputPostsDTO = postController.findAllPostsOfUser(userId);

        Assertions.assertEquals(postsDTO.size(), outputPostsDTO.size());

        for (int i = 0; i < numberOfPosts; i++) {
            AssertPostsUnitUtil.assertPosts(outputPostsDTO.get(i), postsDTO.get(i));
        }
        Mockito.verify(postService, Mockito.times(1)).findAllPostsOfUser(userId);
    }

    @Test
    void findAllPostsOfUserNotFound() throws NotFoundException {
        Integer userId = 1;

        Mockito.when(postService.findAllPostsOfUser(userId)).thenThrow(new NotFoundException(""));

        try {
            postController.findAllPostsOfUser(userId);
            throw new AssertionFailedError();
        } catch (javax.ws.rs.NotFoundException e) {
            Mockito.verify(postService, Mockito.times(1)).findAllPostsOfUser(userId);
        }
    }

    @Test
    void findCountOfAllPostsOfUser() throws NotFoundException {
        Integer userId = 1;
        Integer numberOfPosts = 5;

        Mockito.when(postService.findCountOfAllPostsOfUser(userId)).thenReturn(numberOfPosts);

        Integer numberOfPostsOutput = postController.findCountOfAllPostsOfUser(userId);

        Assertions.assertEquals(numberOfPosts, numberOfPostsOutput);

        Mockito.verify(postService, Mockito.times(1)).findCountOfAllPostsOfUser(userId);
    }

    @Test
    void findCountOfAllPostsOfUserNotFound() throws NotFoundException {
        Integer userId = 1;

        Mockito.when(postService.findCountOfAllPostsOfUser(userId)).thenThrow(new NotFoundException(""));
        try {
            postController.findCountOfAllPostsOfUser(userId);
            throw new AssertionFailedError();
        } catch (javax.ws.rs.NotFoundException e) {
            Mockito.verify(postService, Mockito.times(1)).findCountOfAllPostsOfUser(userId);
        }
    }
}
