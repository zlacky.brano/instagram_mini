package zlack.bra.instamini.presentation.controller.util;

import junit.framework.AssertionFailedError;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import util.logged.LoggedUser;
import zlack.bra.instamini.business.exception.ForbiddenException;
import zlack.bra.instamini.controller.util.AuthorizationUtil;


class AuthorizationUtilTest {

    private final String message = "Test";

    @Test
    void checkIfAuthorizedOrElseThrow() {
        try {
            AuthorizationUtil.checkIfAuthorizedOrElseThrow(LoggedUser.securityContext, 666, message);
        } catch (ForbiddenException e) {
            throw new AssertionFailedError();
        }
    }

    @Test
    void checkIfAuthorizedOrElseThrowForbidden() {
        try {
            AuthorizationUtil.checkIfAuthorizedOrElseThrow(LoggedUser.securityContext, 1, message);
            throw new AssertionFailedError();
        } catch (ForbiddenException e) {
            Assertions.assertEquals(e.getMessage(), message);
        }
    }
}
