package zlack.bra.instamini.presentation.controller;

import junit.framework.AssertionFailedError;
import org.hibernate.exception.ConstraintViolationException;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import util.AssertUsersUnitUtil;
import util.logged.LoggedUser;
import zlack.bra.instamini.business.dto.UserDTO;
import zlack.bra.instamini.business.exception.DuplicateEntryException;
import zlack.bra.instamini.business.exception.ForbiddenException;
import zlack.bra.instamini.business.exception.NotFoundException;
import zlack.bra.instamini.business.service.impl.UserService;
import zlack.bra.instamini.controller.UserController;
import zlack.bra.instamini.controller.request.EditUsernameRequestBody;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static zlack.bra.instamini.constant.Constants.DUPLICATE_ENTRY;


@RunWith(MockitoJUnitRunner.class)
class UserControllerTest {

    private AutoCloseable autoCloseable;

    @InjectMocks
    private UserController userController;

    @Mock
    private UserService userService;
    
    private UserDTO loggedUser;

    @BeforeEach
    void setUp() {
        autoCloseable = MockitoAnnotations.openMocks(this);
        loggedUser = LoggedUser.user;
    }

    @AfterEach
    void tearDown() throws Exception {
        autoCloseable.close();
    }

    @Test
    void all() {
        List<UserDTO> users = new ArrayList<>();
        int numberOfItems = 3;

        for (int i = 1; i <= numberOfItems; i++) {
            UserDTO user = new UserDTO(i, "1user" + i, "1user" + i);
            users.add(user);
        }

        Mockito.when(userService.findAll()).thenReturn(users);

        List<UserDTO> usersDTO = userController.all();

        Assertions.assertEquals(users.size(), usersDTO.size());
        for (int i = 0; i < usersDTO.size(); i++) {
            AssertUsersUnitUtil.assertUsers(users.get(i), usersDTO.get(i));
        }
        Mockito.verify(userService, Mockito.times(1)).findAll();
    }

    @Test
    void allEmpty() {
        List<UserDTO> users = new ArrayList<>();

        Mockito.when(userService.findAll()).thenReturn(users);

        List<UserDTO> usersDTO = userController.all();

        Assertions.assertEquals(users.size(), usersDTO.size());
        Mockito.verify(userService, Mockito.times(1)).findAll();
    }

    @Test
    void searchUsers() {
        List<UserDTO> users = new ArrayList<>();
        int numberOfItems = 3;

        for (int i = 1; i <= numberOfItems; i++) {
            UserDTO user = new UserDTO(i, "1user" + i, "1user" + i);
            users.add(user);
        }

        String prefix = "";

        Mockito.when(userService.searchUsers(prefix)).thenReturn(users);

        List<UserDTO> usersDTO = userController.searchUsers(prefix);

        Assertions.assertEquals(users.size(), usersDTO.size());
        for (int i = 0; i < usersDTO.size(); i++) {
            AssertUsersUnitUtil.assertUsers(users.get(i), usersDTO.get(i));
        }
        Mockito.verify(userService, Mockito.times(1)).searchUsers(prefix);
    }

    @Test
    void byID() {
        UserDTO user = new UserDTO(1, "1user", "1user");
        
        Mockito.when(userService.findById(user.getId())).thenReturn(Optional.of(user));

        UserDTO userResult = userController.byID(user.getId());

        AssertUsersUnitUtil.assertUsers(user, userResult);

        Mockito.verify(userService, Mockito.times(1)).findById(user.getId());
    }

    @Test
    void byIDNotFound() {
        Integer userId = 1;

        Mockito.when(userService.findById(userId)).thenReturn(Optional.empty());

        try {
            userController.byID(userId);
            throw new AssertionFailedError();
        } catch (javax.ws.rs.NotFoundException e) {
            Mockito.verify(userService, Mockito.times(1)).findById(userId);
        }
    }

    @Test
    void byEmail() {
        UserDTO user = new UserDTO(1, "1user", "1user");

        Mockito.when(userService.findByEmail(user.getEmail())).thenReturn(Optional.of(user));

        UserDTO userResult = userController.byEmail(user.getEmail());

        AssertUsersUnitUtil.assertUsers(user, userResult);

        Mockito.verify(userService, Mockito.times(1)).findByEmail(user.getEmail());
    }

    @Test
    void byEmailNotFound() {
        String email = "email";

        Mockito.when(userService.findByEmail(email)).thenReturn(Optional.empty());

        try {
            userController.byEmail(email);
            throw new AssertionFailedError();
        } catch (javax.ws.rs.NotFoundException e) {
            Mockito.verify(userService, Mockito.times(1)).findByEmail(email);
        }
    }

    @Test
    void byPrincipal() {
        Mockito.when(userService.findById(loggedUser.getId())).thenReturn(Optional.of(loggedUser));

        UserDTO userResult = userController.byPrincipal(LoggedUser.securityContext);

        AssertUsersUnitUtil.assertUsers(loggedUser, userResult);

        Mockito.verify(userService, Mockito.times(1)).findById(loggedUser.getId());
    }
    
    @Test
    void byPrincipalNotFound() {
        Mockito.when(userService.findById(loggedUser.getId())).thenReturn(Optional.empty());

        try {
            userController.byPrincipal(LoggedUser.securityContext);
            throw new AssertionFailedError();
        } catch (javax.ws.rs.NotFoundException e) {
            Mockito.verify(userService, Mockito.times(1)).findById(loggedUser.getId());
        }
    }

    @Test
    void editUsername() throws NotFoundException {
        EditUsernameRequestBody requestBody = new EditUsernameRequestBody("new username");

        UserDTO newUserDTO = new UserDTO(loggedUser.getId(), requestBody.getUsername(), loggedUser.getEmail());

        Mockito.when(userService.editUsername(loggedUser.getId(), requestBody.getUsername())).thenReturn(newUserDTO);

        UserDTO userDTOOutput;
        try {
            userDTOOutput = userController.editUsername(loggedUser.getId(), requestBody, LoggedUser.securityContext);
        } catch (Exception e) {
            throw new AssertionFailedError();
        }

        Assertions.assertNotNull(userDTOOutput);
        AssertUsersUnitUtil.assertUsers(newUserDTO, userDTOOutput);

        Mockito.verify(userService, Mockito.times(1)).editUsername(loggedUser.getId(), requestBody.getUsername());
    }

    @Test
    void editUsernameNotFound() throws NotFoundException {
        EditUsernameRequestBody requestBody = new EditUsernameRequestBody("new username");

        Mockito.when(userService.editUsername(loggedUser.getId(), requestBody.getUsername())).thenThrow(new NotFoundException(""));

        try {
            userController.editUsername(loggedUser.getId(), requestBody, LoggedUser.securityContext);
            throw new AssertionFailedError();
        } catch (javax.ws.rs.NotFoundException e) {
            Mockito.verify(userService, Mockito.times(1)).editUsername(loggedUser.getId(), requestBody.getUsername());
        } catch (Exception e) {
            throw new AssertionFailedError();
        }
    }

    @Test
    void editUsernameForbidden() throws NotFoundException {
        EditUsernameRequestBody requestBody = new EditUsernameRequestBody("new username");

        Integer notLoggedUserId = 1;
        UserDTO newUserDTO = new UserDTO(notLoggedUserId, requestBody.getUsername(), "test");

        Mockito.when(userService.editUsername(notLoggedUserId, requestBody.getUsername())).thenReturn(newUserDTO);

        try {
            userController.editUsername(notLoggedUserId, requestBody, LoggedUser.securityContext);
            throw new AssertionFailedError();
        } catch (ForbiddenException e) {
            Mockito.verify(userService, Mockito.times(0)).editUsername(notLoggedUserId, requestBody.getUsername());
        } catch (Exception e) {
            throw new AssertionFailedError();
        }
    }

    @Test
    void editUsernameDuplicateEntry() throws NotFoundException {
        EditUsernameRequestBody requestBody = new EditUsernameRequestBody("new username");

        Mockito.when(userService.editUsername(loggedUser.getId(), requestBody.getUsername())).thenThrow(new ConstraintViolationException("test", new SQLException("Duplicate entry"), null));

        try {
            userController.editUsername(loggedUser.getId(), requestBody, LoggedUser.securityContext);
            throw new AssertionFailedError();
        } catch (DuplicateEntryException e) {
            Assertions.assertEquals(e.getMessage(), DUPLICATE_ENTRY);
            Mockito.verify(userService, Mockito.times(1)).editUsername(loggedUser.getId(), requestBody.getUsername());
        } catch (Exception e) {
            throw new AssertionFailedError();
        }
    }

    @Test
    void delete() throws NotFoundException, ForbiddenException {
        userController.delete(loggedUser.getId(), LoggedUser.securityContext);

        Mockito.verify(userService, Mockito.times(1)).deleteById(loggedUser.getId());
    }

    @Test
    void deleteNotFound() throws NotFoundException {
        Mockito.doThrow(new NotFoundException("")).when(userService).deleteById(loggedUser.getId());

        try {
            userController.delete(loggedUser.getId(), LoggedUser.securityContext);
            throw new AssertionFailedError();
        } catch (javax.ws.rs.NotFoundException e) {
            Mockito.verify(userService, Mockito.times(1)).deleteById(loggedUser.getId());
        } catch (ForbiddenException e) {
            throw new AssertionFailedError();
        }
    }

    @Test
    void deleteForbidden() throws NotFoundException {
        Integer notLoggedUserId = 5;

        try {
            userController.delete(notLoggedUserId, LoggedUser.securityContext);
            throw new AssertionFailedError();
        } catch (ForbiddenException e) {
            Mockito.verify(userService, Mockito.times(0)).deleteById(notLoggedUserId);
        }
    }

    @Test
    void findAllUsersWhoLikedPost() throws NotFoundException {
        Integer postId = 1;

        List<UserDTO> usersDTO = new ArrayList<>();
        int numberOfUsers = 5;
        for (int i = 1; i <= numberOfUsers; i++) {
            UserDTO userDTO = new UserDTO(i, "test" + i, "test" + i);
            usersDTO.add(userDTO);
        }

        Mockito.when(userService.findAllUsersWhoLikedPost(postId)).thenReturn(usersDTO);

        List<UserDTO> outputUsersDTO = userController.findAllUsersWhoLikedPost(postId);

        Assertions.assertEquals(usersDTO.size(), outputUsersDTO.size());

        for (int i = 0; i < numberOfUsers; i++) {
            AssertUsersUnitUtil.assertUsers(outputUsersDTO.get(i), usersDTO.get(i));
        }
        Mockito.verify(userService, Mockito.times(1)).findAllUsersWhoLikedPost(postId);
    }

    @Test
    void findAllUsersWhoLikedPostNotFound() throws NotFoundException {
        Integer postId = 1;

        Mockito.when(userService.findAllUsersWhoLikedPost(postId)).thenThrow(new NotFoundException(""));

        try {
            userController.findAllUsersWhoLikedPost(postId);
        } catch (javax.ws.rs.NotFoundException e) {
            Mockito.verify(userService, Mockito.times(1)).findAllUsersWhoLikedPost(postId);
        }
    }

    @Test
    void findAllFollowers() throws NotFoundException {
        Integer userId = 1;

        List<UserDTO> usersDTO = new ArrayList<>();
        int numberOfUsers = 5;
        for (int i = 1; i <= numberOfUsers; i++) {
            UserDTO userDTO = new UserDTO(i, "test" + i, "test" + i);
            usersDTO.add(userDTO);
        }

        Mockito.when(userService.findAllFollowers(userId)).thenReturn(usersDTO);

        List<UserDTO> outputUsersDTO = userController.findAllFollowers(userId);

        Assertions.assertEquals(usersDTO.size(), outputUsersDTO.size());

        for (int i = 0; i < numberOfUsers; i++) {
            AssertUsersUnitUtil.assertUsers(outputUsersDTO.get(i), usersDTO.get(i));
        }
        Mockito.verify(userService, Mockito.times(1)).findAllFollowers(userId);
    }

    @Test
    void findAllFollowersNotFound() throws NotFoundException {
        Integer userId = 1;

        Mockito.when(userService.findAllFollowers(userId)).thenThrow(new NotFoundException(""));

        try {
            userController.findAllFollowers(userId);
        } catch (javax.ws.rs.NotFoundException e) {
            Mockito.verify(userService, Mockito.times(1)).findAllFollowers(userId);
        }
    }

    @Test
    void findCountOfAllFollowers() throws NotFoundException {
        Integer userId = 1;

        int numberOfUsers = 5;

        Mockito.when(userService.findCountOfAllFollowers(userId)).thenReturn(numberOfUsers);

        Integer output = userController.findCountOfAllFollowers(userId);

        Assertions.assertEquals(numberOfUsers, output);

        Mockito.verify(userService, Mockito.times(1)).findCountOfAllFollowers(userId);
    }

    @Test
    void findCountOfAllFollowersNotFound() throws NotFoundException {
        Integer userId = 1;

        Mockito.when(userService.findCountOfAllFollowers(userId)).thenThrow(new NotFoundException(""));

        try {
            userController.findCountOfAllFollowers(userId);
        } catch (javax.ws.rs.NotFoundException e) {
            Mockito.verify(userService, Mockito.times(1)).findCountOfAllFollowers(userId);
        }
    }

    @Test
    void findAllFollowing() throws NotFoundException {
        Integer userId = 1;

        List<UserDTO> usersDTO = new ArrayList<>();
        int numberOfUsers = 5;
        for (int i = 1; i <= numberOfUsers; i++) {
            UserDTO userDTO = new UserDTO(i, "test" + i, "test" + i);
            usersDTO.add(userDTO);
        }

        Mockito.when(userService.findAllFollowing(userId)).thenReturn(usersDTO);

        List<UserDTO> outputUsersDTO = userController.findAllFollowing(userId);

        Assertions.assertEquals(usersDTO.size(), outputUsersDTO.size());

        for (int i = 0; i < numberOfUsers; i++) {
            AssertUsersUnitUtil.assertUsers(outputUsersDTO.get(i), usersDTO.get(i));
        }
        Mockito.verify(userService, Mockito.times(1)).findAllFollowing(userId);
    }

    @Test
    void findAllFollowingNotFound() throws NotFoundException {
        Integer userId = 1;

        Mockito.when(userService.findAllFollowing(userId)).thenThrow(new NotFoundException(""));

        try {
            userController.findAllFollowing(userId);
        } catch (javax.ws.rs.NotFoundException e) {
            Mockito.verify(userService, Mockito.times(1)).findAllFollowing(userId);
        }
    }

    @Test
    void findCountOfAllFollowing() throws NotFoundException {
        Integer userId = 1;

        int numberOfUsers = 5;

        Mockito.when(userService.findCountOfAllFollowing(userId)).thenReturn(numberOfUsers);

        Integer output = userController.findCountOfAllFollowing(userId);

        Assertions.assertEquals(numberOfUsers, output);

        Mockito.verify(userService, Mockito.times(1)).findCountOfAllFollowing(userId);
    }

    @Test
    void findCountOfAllFollowingNotFound() throws NotFoundException {
        Integer userId = 1;

        Mockito.when(userService.findCountOfAllFollowing(userId)).thenThrow(new NotFoundException(""));

        try {
            userController.findCountOfAllFollowing(userId);
        } catch (javax.ws.rs.NotFoundException e) {
            Mockito.verify(userService, Mockito.times(1)).findCountOfAllFollowing(userId);
        }
    }
}
