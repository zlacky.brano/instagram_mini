package zlack.bra.instamini.business.service.impl;

import com.cloudinary.Cloudinary;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import zlack.bra.instamini.business.service.ImageUploadService;

import javax.enterprise.context.RequestScoped;
import javax.imageio.ImageIO;
import javax.inject.Singleton;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Collections;
import java.util.Map;
import java.util.UUID;

import static zlack.bra.instamini.constant.Constants.NOT_AN_IMAGE;

@Slf4j
@Singleton
public class CloudinaryService implements ImageUploadService {

    private final Cloudinary cloudinary;

    public CloudinaryService(@ConfigProperty(name = "cloudinary.cloud_name") String cloudName, @ConfigProperty(name = "cloudinary.api_key") String apiKey,
                             @ConfigProperty(name = "cloudinary.api_secret") String apiSecret) {
        cloudinary = new Cloudinary(Cloudinary.asMap(
                "cloud_name", cloudName,
                "api_key", apiKey,
                "api_secret", apiSecret
        ));
    }

    @Override
    public String upload(InputStream file, Map options) throws IOException {
        File tempFile = new File(System.getProperty("user.dir") + "/src/main/resources" + File.separator + "tempFile-" + UUID.randomUUID());
        FileUtils.writeByteArrayToFile(tempFile, file.readAllBytes());

        if(ImageIO.read(tempFile) == null) {
            tempFile.delete();
            throw new IOException(NOT_AN_IMAGE);
        }

        Map result;
        try {
            result = cloudinary.uploader().upload(tempFile, options);
            log.info("Photo was uploaded on Cloudinary.");
        } finally {
            tempFile.delete();
        }

        return result.get("url").toString();
    }

    @Override
    public String upload(InputStream file) throws IOException {
        return upload(file, Collections.emptyMap());
    }

    @Override
    public void destroy(String publicId, Map options) throws IOException {
        cloudinary.uploader().destroy(publicId, options);
        log.info("Photo was destroyed on Cloudinary.");
    }

    @Override
    public void destroy(String publicId) throws IOException {
        destroy(publicId, Collections.emptyMap());
    }
}
