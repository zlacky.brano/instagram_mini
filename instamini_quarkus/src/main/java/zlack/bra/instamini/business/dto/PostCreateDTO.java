package zlack.bra.instamini.business.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.jboss.resteasy.annotations.jaxrs.FormParam;
import org.jboss.resteasy.annotations.providers.multipart.PartType;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.ws.rs.core.MediaType;
import java.io.InputStream;

import static zlack.bra.instamini.constant.Constants.MAX_LENGTH_DESCRIPTION_AND_TEXT;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PostCreateDTO {

    @NotBlank
    @FormParam("description")
    @PartType(MediaType.TEXT_PLAIN)
    @Size(max = MAX_LENGTH_DESCRIPTION_AND_TEXT)
    private String description;

    @NotNull
    @FormParam("userId")
    @PartType(MediaType.TEXT_PLAIN)
    private Integer userId;

    @NotNull
    @FormParam("image")
    @PartType(MediaType.APPLICATION_OCTET_STREAM)
    private InputStream image;
}
