package zlack.bra.instamini.business.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PostDTO {
    
    private Integer id;
    private String description;
    private String photoUrl;
    private LocalDateTime time;
    private UserDTO user;
}
