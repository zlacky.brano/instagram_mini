package zlack.bra.instamini.business.exception;

public class FollowYourselfException extends Exception {
    public FollowYourselfException(String errorMessage) {
        super(errorMessage);
    }
}
