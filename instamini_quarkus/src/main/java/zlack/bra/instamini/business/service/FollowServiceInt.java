package zlack.bra.instamini.business.service;

import zlack.bra.instamini.business.dto.FollowCreateDTO;
import zlack.bra.instamini.business.dto.FollowDTO;
import zlack.bra.instamini.business.exception.FollowYourselfException;
import zlack.bra.instamini.business.exception.NotFoundException;
import zlack.bra.instamini.data.entity.Follow;

import java.util.List;
import java.util.Optional;

public interface FollowServiceInt {

    /**
     * Creates new follow
     * @param followCreateDTO object with necessary values to create follow
     * @return created follow
     * @throws NotFoundException if users were not found
     * @throws FollowYourselfException if both users are same
     */
    FollowDTO create(FollowCreateDTO followCreateDTO) throws NotFoundException, FollowYourselfException;

    /**
     * Finds all follows
     * @return all follows
     */
    List<FollowDTO> findAll();

    /**
     * Finds follow by id
     * @param id with this we will find follow
     * @return found follow
     */
    Optional<FollowDTO> findById(Integer id);

    /**
     * Converts Follow to FollowDTO
     * @param follow to be converted
     * @return converted follow
     */
    FollowDTO toDTO(Follow follow);

    /**
     * Converts Optional-Follow to Optional-FollowDTO
     * @param follow to be converted
     * @return converted follow
     */
    Optional<FollowDTO> toDTO(Optional<Follow> follow);

    /**
     * Deletes follow by id
     * @param id with this we will find and delete follow
     * @throws NotFoundException if follow with given id does not exist
     */
    void deleteById(Integer id) throws NotFoundException;

    /**
     * Find follow id by given users
     * @param userIdWho user, who is in position, who follows
     * @param userIdWhom user, who is in position, who is being followed
     * @return found id
     * @throws NotFoundException if follow does not exist
     */
    Integer findFollowIdByUsers(Integer userIdWho, Integer userIdWhom) throws NotFoundException;
}
