package zlack.bra.instamini.business.service.impl;

import lombok.extern.slf4j.Slf4j;
import zlack.bra.instamini.business.dto.FollowCreateDTO;
import zlack.bra.instamini.business.dto.FollowDTO;
import zlack.bra.instamini.business.exception.FollowYourselfException;
import zlack.bra.instamini.business.exception.NotFoundException;
import zlack.bra.instamini.business.service.FollowServiceInt;
import zlack.bra.instamini.data.entity.Follow;
import zlack.bra.instamini.data.entity.User;
import zlack.bra.instamini.data.repository.FollowRepository;
import zlack.bra.instamini.data.repository.UserRepository;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Slf4j
@RequestScoped
@Transactional
public class FollowService implements FollowServiceInt {

    private final FollowRepository followRepository;
    private final UserRepository userRepository;
    private final UserService userService;

    @Inject
    public FollowService(FollowRepository followRepository, UserRepository userRepository, UserService userService) {
        this.followRepository = followRepository;
        this.userRepository = userRepository;
        this.userService = userService;
    }

    @Override
    public FollowDTO create(FollowCreateDTO followCreateDTO) throws NotFoundException, FollowYourselfException {
        Optional<User> optionalUserWho = userRepository.findByIdOptional(followCreateDTO.getUserWhoId());
        Optional<User> optionalUserWhom = userRepository.findByIdOptional(followCreateDTO.getUserWhomId());

        if (optionalUserWho.isEmpty()) {
            log.warn("Trying to create follow with 'who' user, which does not exist.");
            throw new NotFoundException("No such user_who.");
        }
        else if (optionalUserWhom.isEmpty()) {
            log.warn("Trying to create follow with 'whom' user, which does not exist.");
            throw new NotFoundException("No such user_whom.");
        } else if (followCreateDTO.getUserWhoId().equals(followCreateDTO.getUserWhomId())) {
            log.warn("Trying to follow yourself.");
            throw new FollowYourselfException("Can't follow yourself.");
        }
        Follow persistedFollow = new Follow(optionalUserWho.get(), optionalUserWhom.get());

        followRepository.persistAndFlush(persistedFollow);

        log.info("User: " + optionalUserWho.get().getEmail() + " is now following: " + optionalUserWhom.get().getEmail() + ".");

        return toDTO(persistedFollow);
    }

    @Override
    public List<FollowDTO> findAll() {
        log.info("Collecting all follows.");
        return followRepository.findAll().stream().map(this::toDTO).collect(Collectors.toList());
    }

    @Override
    public Optional<FollowDTO> findById(Integer id) {
        log.info("Finding one specified follow.");
        return toDTO(followRepository.findByIdOptional(id));
    }

    @Override
    public FollowDTO toDTO(Follow follow) {
        return new FollowDTO(follow.getId(), userService.toDTO(follow.getUserWho()), userService.toDTO(follow.getUserWhom()));
    }

    @Override
    public Optional<FollowDTO> toDTO(Optional<Follow> follow) {
        if (follow.isEmpty()) {
            return Optional.empty();
        }
        return Optional.of(toDTO(follow.get()));
    }

    @Override
    public void deleteById(Integer id) throws NotFoundException {
        if (followRepository.findByIdOptional(id).isPresent()) {
            followRepository.deleteById(id);
            log.info("Follow with id: " + id + " has been deleted.");
        }
        else {
            log.warn("Trying to delete follow, which does not exist.");
            throw new NotFoundException("No such follow.");
        }
    }

    @Override
    public Integer findFollowIdByUsers(Integer userIdWho, Integer userIdWhom) throws NotFoundException {
        log.info("Checking if user with id: " + userIdWho + " is following user with id: " + userIdWhom + ".");
        Optional<Integer> followIdOptional = followRepository.findFollowIdByUsers(userIdWho, userIdWhom);
        if (followIdOptional.isEmpty()) {
            log.warn("Trying to find follow by users, which does not exist.");
            throw new NotFoundException("No such follow.");
        }
        return followIdOptional.get();
    }
}
