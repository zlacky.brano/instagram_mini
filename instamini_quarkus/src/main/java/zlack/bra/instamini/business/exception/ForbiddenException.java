package zlack.bra.instamini.business.exception;

public class ForbiddenException extends Exception {

    public ForbiddenException(String errorMessage) {
        super(errorMessage);
    }
}
