package zlack.bra.instamini.business.service.impl;

import lombok.extern.slf4j.Slf4j;
import zlack.bra.instamini.business.dto.LikeCreateDTO;
import zlack.bra.instamini.business.dto.LikeDTO;
import zlack.bra.instamini.business.exception.NotFoundException;
import zlack.bra.instamini.business.service.LikeServiceInt;
import zlack.bra.instamini.data.entity.Like;
import zlack.bra.instamini.data.entity.Post;
import zlack.bra.instamini.data.entity.User;
import zlack.bra.instamini.data.repository.LikeRepository;
import zlack.bra.instamini.data.repository.PostRepository;
import zlack.bra.instamini.data.repository.UserRepository;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Slf4j
@RequestScoped
@Transactional
public class LikeService implements LikeServiceInt {

    private final LikeRepository likeRepository;
    private final PostRepository postRepository;
    private final UserRepository userRepository;
    private final PostService postService;
    private final UserService userService;

    @Inject
    public LikeService(LikeRepository likeRepository, PostRepository postRepository, UserRepository userRepository, PostService postService, UserService userService) {
        this.likeRepository = likeRepository;
        this.postRepository = postRepository;
        this.userRepository = userRepository;
        this.postService = postService;
        this.userService = userService;
    }

    @Override
    public LikeDTO create(LikeCreateDTO likeCreateDTO) throws NotFoundException {
        Optional<Post> optionalPost = postRepository.findByIdOptional(likeCreateDTO.getPostId());
        Optional<User> optionalUser = userRepository.findByIdOptional(likeCreateDTO.getUserId());

        if (optionalPost.isEmpty()) {
            log.warn("Trying to add like on post, which does not exist.");
            throw new NotFoundException("No such post.");
        } else if (optionalUser.isEmpty()) {
            log.warn("Trying to add like on post by user, which does not exist.");
            throw new NotFoundException("No such user.");
        }
        Like persistedLike = new Like(optionalUser.get(), optionalPost.get());
        likeRepository.persistAndFlush(persistedLike);

        log.info("Like of user: " + optionalUser.get().getEmail() + " has been added on post: " + optionalPost.get().getId() + ".");

        return toDTO(persistedLike);
    }

    @Override
    public List<LikeDTO> findAll() {
        log.info("Collecting all likes.");
        return likeRepository.findAll().stream().map(this::toDTO).collect(Collectors.toList());
    }

    @Override
    public Optional<LikeDTO> findById(Integer id) {
        log.info("Finding one specified like.");
        return toDTO(likeRepository.findByIdOptional(id));
    }

    @Override
    public LikeDTO toDTO(Like like) {
        return new LikeDTO(like.getId(), userService.toDTO(like.getUser()), postService.toDTO(like.getPost()));
    }

    @Override
    public Optional<LikeDTO> toDTO(Optional<Like> like) {
        if (like.isEmpty()) {
            return Optional.empty();
        }
        return Optional.of(toDTO(like.get()));
    }

    @Override
    public void deleteById(Integer id) throws NotFoundException {
        if (likeRepository.findByIdOptional(id).isPresent()) {
            likeRepository.deleteById(id);
            log.info("Like with id: " + id + " has been deleted.");
        } else {
            log.warn("Trying to delete like, which does not exist.");
            throw new NotFoundException("No such like.");
        }
    }

    @Override
    public Integer findNumberOfLikesOnPost(Integer idPost) throws NotFoundException {
        if (postRepository.findByIdOptional(idPost).isPresent()) {
            log.info("Finding number of likes on post: " + idPost + ".");
            return likeRepository.findNumberOfLikesOnPost(idPost);
        }
        log.warn("Trying to find number of likes on post, which does not exist.");
        throw new NotFoundException("Not such post.");
    }

    @Override
    public List<LikeDTO> findLikesOnPost(Integer idPost) throws NotFoundException {
        if (postRepository.findByIdOptional(idPost).isPresent()) {
            log.info("Collecting likes on post: " + idPost + ".");
            return likeRepository.findLikesOnPost(idPost).stream().map(this::toDTO).collect(Collectors.toList());
        }
        log.warn("Trying to collect likes on post, which does not exist.");
        throw new NotFoundException("Not such post.");
    }

    @Override
    public Integer findLikeIdByUserAndPost(Integer userId, Integer postId) throws NotFoundException {
        log.info("Checking if user with id: " + userId + " liked post with id: " + postId + ".");
        Optional<Integer> likeIdOptional = likeRepository.findLikeIdByUserAndPost(userId, postId);
        if (likeIdOptional.isEmpty()) {
            log.warn("Trying to find like by user and post, which does not exist.");
            throw new NotFoundException("No such like.");
        }
        return likeIdOptional.get();
    }

}
