package zlack.bra.instamini.business.exception;

public class DuplicateEntryException extends Exception {

    public DuplicateEntryException(String errorMessage) {
        super(errorMessage);
    }
}
