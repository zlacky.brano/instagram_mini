package zlack.bra.instamini.business.exception;

public class UploadFailException extends Exception {
    public UploadFailException(String errorMessage) {
        super(errorMessage);
    }
}
