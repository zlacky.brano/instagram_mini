package zlack.bra.instamini.business.service;

import java.io.IOException;
import java.io.InputStream;
import java.util.Map;

public interface ImageUploadService {

    /**
     * Uploads image to Cloudinary
     * @param file MultipartFile
     * @param options options of uploading
     * @return image URL after upload
     * @throws IOException if error occurs during upload
     */
    String upload(InputStream file, Map options) throws IOException;

    /**
     * Uploads image to Cloudinary
     * @param file MultipartFile
     * @return image URL after upload
     * @throws IOException if error occurs during upload
     */
    String upload(InputStream file) throws IOException;

    /**
     * Deletes image on Cloudinary
     * @param publicId id of image
     * @param options options of destroying
     * @throws IOException if error occurs during destroy
     */
    void destroy(String publicId, Map options) throws IOException;

    /**
     * Deletes image on Cloudinary
     * @param publicId id of image
     * @throws IOException if error occurs during destroy
     */
    void destroy(String publicId) throws IOException;
}
