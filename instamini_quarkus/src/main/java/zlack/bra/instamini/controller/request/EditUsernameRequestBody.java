package zlack.bra.instamini.controller.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import static zlack.bra.instamini.constant.Constants.MAX_LENGTH_USERNAME_AND_EMAIL;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class EditUsernameRequestBody {

    @Size(max = MAX_LENGTH_USERNAME_AND_EMAIL)
    @NotBlank
    private String username;

    public String toJson() {
        return "{\"username\": \"" + username + "\"}";
    }
}
