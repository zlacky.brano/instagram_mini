package zlack.bra.instamini.controller.util;


import zlack.bra.instamini.business.exception.ForbiddenException;

import javax.ws.rs.core.SecurityContext;
import java.security.Principal;

public class AuthorizationUtil {

    /**
     * Throws ForbiddenException, if user is not allowed to use resource
     * @param securityContext logged user
     * @param userId owner's id
     * @param message error message, if exception is hthrown
     */
    public static void checkIfAuthorizedOrElseThrow(SecurityContext securityContext, Integer userId, String message) throws ForbiddenException {
        Principal principal = securityContext.getUserPrincipal();

        if (!principal.getName().equals(userId.toString())) {
            throw new ForbiddenException(message);
        }
    }
}

