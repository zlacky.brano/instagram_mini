package zlack.bra.instamini.controller;

import org.jboss.resteasy.annotations.providers.multipart.MultipartForm;
import zlack.bra.instamini.business.dto.PostCreateDTO;
import zlack.bra.instamini.business.dto.PostDTO;
import zlack.bra.instamini.business.exception.UploadFailException;
import zlack.bra.instamini.business.service.impl.PostService;
import zlack.bra.instamini.controller.request.EditPostDescriptionRequestBody;
import zlack.bra.instamini.controller.util.AuthorizationUtil;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.*;
import java.security.Principal;
import java.util.List;
import java.util.Optional;

import static zlack.bra.instamini.constant.Constants.NOT_AN_IMAGE;
import static zlack.bra.instamini.constant.Constants.ROOT_URL;

@Path("/posts")
@ApplicationScoped
public class PostController {

    private final PostService postService;

    @Inject
    public PostController(PostService postService) {
        this.postService = postService;
    }

    /**
     * Finds all posts
     * @return all posts
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<PostDTO> all() {
        return postService.findAll();
    }

    /**
     * Finds post by id
     * @param id of post
     * @return found post. NOT_FOUND, if post does not exist
     */
    @Path("{id}")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public PostDTO byID(@PathParam("id") Integer id) {
        Optional<PostDTO> optionalPostDTO;
        optionalPostDTO = postService.findById(id);

        if (optionalPostDTO.isPresent()) {
            return optionalPostDTO.get();
        }
        else {
            throw new javax.ws.rs.NotFoundException("No such post.");
        }
    }

    /**
     * Creates new post
     * @param postCreateDTO object with necessary values to create post
     * @param sec represents logged user
     * @return created post. NOT_FOUND, if user was not found. BAD_REQUEST, if file is not an image
     */
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public Response create(@MultipartForm @Valid PostCreateDTO postCreateDTO, @Context SecurityContext sec) throws zlack.bra.instamini.business.exception.ForbiddenException {
        AuthorizationUtil.checkIfAuthorizedOrElseThrow(sec, postCreateDTO.getUserId(),
                "Can't create post on profile, which does not belong to you.");

        PostDTO postDTO;
        try {
            postDTO = postService.create(postCreateDTO);
        } catch (zlack.bra.instamini.business.exception.NotFoundException e) {
            throw new javax.ws.rs.NotFoundException(e.getMessage());
        } catch (UploadFailException e) {
            if (e.getMessage().equals(NOT_AN_IMAGE)) {
                throw new javax.ws.rs.BadRequestException(e.getMessage());
            }
            throw new javax.ws.rs.InternalServerErrorException(e.getMessage());
        }
        return Response.created(UriBuilder.fromUri(ROOT_URL + "posts/" + postDTO.getId()).build()).entity(postDTO).build();
    }

    /**
     * Edits description of post
     * @param id of post
     * @param requestBody new description
     * @param sec represents logged user
     * @return updated post. NOT_FOUND, if post does not exist
     */
    @Path("{id}/editDescription")
    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public PostDTO editDescription(@PathParam("id") Integer id,
                                   @Valid EditPostDescriptionRequestBody requestBody,
                                   @Context SecurityContext sec) throws zlack.bra.instamini.business.exception.ForbiddenException {
        Optional<PostDTO> postDTOOptional = postService.findById(id);
        if (postDTOOptional.isPresent()) {
            AuthorizationUtil.checkIfAuthorizedOrElseThrow(sec,
                    postDTOOptional.get().getUser().getId(), "Can't edit description on post, which does not belong to you.");
        }

        try {
            return postService.editDescription(id, requestBody.getNewDescription());
        } catch (zlack.bra.instamini.business.exception.NotFoundException e) {
            throw new javax.ws.rs.NotFoundException(e.getMessage());
        }
    }

    /**
     * Deletes post by id
     * @param id of post
     * @param sec represents logged user
     * Can return NOT_FOUND, if post does not exist
     */
    @Path("{id}")
    @DELETE
    @Produces(MediaType.APPLICATION_JSON)
    public void delete(@PathParam("id") Integer id, @Context SecurityContext sec) throws zlack.bra.instamini.business.exception.ForbiddenException {
        Optional<PostDTO> postDTOOptional = postService.findById(id);
        if (postDTOOptional.isPresent()) {
            AuthorizationUtil.checkIfAuthorizedOrElseThrow(sec,
                    postDTOOptional.get().getUser().getId(), "Can't delete post, which does not belong to you.");
        }

        try {
            postService.deleteById(id);
        } catch (zlack.bra.instamini.business.exception.NotFoundException e) {
            throw new javax.ws.rs.NotFoundException(e.getMessage());
        }
    }

    /**
     * Finds all posts of user
     * @param user id
     * @return found posts. NOT_FOUND, if user was not found
     */
    @Path("ofUser")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<PostDTO> findAllPostsOfUser(@QueryParam("user") Integer user) {
        try {
            return postService.findAllPostsOfUser(user);
        } catch (zlack.bra.instamini.business.exception.NotFoundException e) {
            throw new javax.ws.rs.NotFoundException(e.getMessage());
        }
    }

    /**
     * Finds number of all posts of user
     * @param user id
     * @return found number of posts of user
     */
    @Path("count")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Integer findCountOfAllPostsOfUser(@QueryParam("user") Integer user) {
        try {
            return postService.findCountOfAllPostsOfUser(user);
        } catch (zlack.bra.instamini.business.exception.NotFoundException e) {
            throw new javax.ws.rs.NotFoundException(e.getMessage());
        }
    }
}
