package zlack.bra.instamini.controller;

import zlack.bra.instamini.business.dto.LikeCreateDTO;
import zlack.bra.instamini.business.dto.LikeDTO;
import zlack.bra.instamini.business.service.impl.LikeService;
import zlack.bra.instamini.controller.util.AuthorizationUtil;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.*;
import java.security.Principal;
import java.util.List;
import java.util.Optional;

import static zlack.bra.instamini.constant.Constants.ROOT_URL;
import static zlack.bra.instamini.controller.util.CheckForUnique.checkForUniqueConstraintViolation;

@Path("/likes")
@ApplicationScoped
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class LikeController {

    private final LikeService likeService;

    @Inject
    public LikeController(LikeService likeService) {
        this.likeService = likeService;
    }

    /**
     * Finds all likes
     * @return all likes
     */
    @GET
    public List<LikeDTO> all() {
        return likeService.findAll();
    }

    /**
     * Finds like by id
     * @param id of like
     * @return found like or status code NOT_FOUND, if like does not exist
     */
    @Path("{id}")
    @GET
    public LikeDTO byID(@PathParam("id") Integer id) {
        Optional<LikeDTO> optionalLikeDTO;
        optionalLikeDTO = likeService.findById(id);

        if (optionalLikeDTO.isPresent()) {
            return optionalLikeDTO.get();
        } else {
            throw new javax.ws.rs.NotFoundException("No such like.");
        }
    }

    /**
     * Creates new like
     * @param likeCreateDTO object with necessary values to create like
     * @param sec represents logged user
     * @return created like. NOT_FOUND, if post or user does not exist. BAD_REQUEST, if unique constraint was violated
     */
    @POST
    public Response create(@Valid LikeCreateDTO likeCreateDTO, @Context SecurityContext sec) throws Exception {
        AuthorizationUtil.checkIfAuthorizedOrElseThrow(sec, likeCreateDTO.getUserId(),
                "Can't create like for other user.");

        LikeDTO likeDTO;
        try {
            likeDTO = likeService.create(likeCreateDTO);
        } catch (zlack.bra.instamini.business.exception.NotFoundException e) {
            throw new javax.ws.rs.NotFoundException(e.getMessage());
        } catch (Exception e) {
            throw checkForUniqueConstraintViolation(e);
        }
        return Response.created(UriBuilder.fromUri(ROOT_URL + "likes/" + likeDTO.getId()).build()).entity(likeDTO).build();
    }

    /**
     * Deletes like by id
     * @param id of like
     * @param sec represents logged user
     * Can return NOT_FOUND, if like does not exist
     */
    @Path("{id}")
    @DELETE
    public void delete(@PathParam("id") Integer id, @Context SecurityContext sec) throws zlack.bra.instamini.business.exception.ForbiddenException {
        Optional<LikeDTO> likeDTOOptional = likeService.findById(id);
        if (likeDTOOptional.isPresent()) {
            AuthorizationUtil.checkIfAuthorizedOrElseThrow(sec,
                    likeDTOOptional.get().getUser().getId(), "Can't delete like, which does not belong to you.");
        }

        try {
            likeService.deleteById(id);
        } catch (zlack.bra.instamini.business.exception.NotFoundException e) {
            throw new javax.ws.rs.NotFoundException(e.getMessage());
        }
    }

    /**
     * Finds number of likes on post
     * @param post id
     * @return number of likes on post. NOT_FOUND, if post does not exist
     */
    @Path("count")
    @GET
    public Integer findNumberOfLikesOnPost(@QueryParam("post") Integer post) {
        try {
            return likeService.findNumberOfLikesOnPost(post);
        } catch (zlack.bra.instamini.business.exception.NotFoundException e) {
            throw new javax.ws.rs.NotFoundException(e.getMessage());
        }
    }

    /**
     * Finds likes on post
     * @param post id
     * @return likes on post. NOT_FOUND, if post does not exist
     */
    @Path("onPost")
    @GET
    public List<LikeDTO> findLikesOnPost(@QueryParam("post") Integer post) {
        try {
            return likeService.findLikesOnPost(post);
        } catch (zlack.bra.instamini.business.exception.NotFoundException e) {
            throw new javax.ws.rs.NotFoundException(e.getMessage());
        }
    }

    /**
     * Finds like id by logged user and post
     * @param postId id of post
     * @param sec represents logged user
     * @return found like id. NOT_FOUND, if like does not exist
     */
    @Path("/isLiked/{postId}")
    @GET
    public Integer findLikeIdByUserAndPost(@PathParam("postId") Integer postId, @Context SecurityContext sec) {
        try {
            return likeService.findLikeIdByUserAndPost(Integer.parseInt(sec.getUserPrincipal().getName()), postId);
        } catch (zlack.bra.instamini.business.exception.NotFoundException e) {
            throw new javax.ws.rs.NotFoundException(e.getMessage());
        }
    }
}
