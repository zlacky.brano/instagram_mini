package zlack.bra.instamini.controller;

import zlack.bra.instamini.business.dto.FollowCreateDTO;
import zlack.bra.instamini.business.dto.FollowDTO;
import zlack.bra.instamini.business.exception.FollowYourselfException;
import zlack.bra.instamini.business.service.impl.FollowService;
import zlack.bra.instamini.controller.util.AuthorizationUtil;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.*;
import java.util.List;
import java.util.Optional;

import static zlack.bra.instamini.constant.Constants.ROOT_URL;
import static zlack.bra.instamini.controller.util.CheckForUnique.checkForUniqueConstraintViolation;

@Path("/follows")
@ApplicationScoped
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class FollowController {

    private final FollowService followService;

    @Inject
    public FollowController(FollowService followService) {
        this.followService = followService;
    }

    /**
     * Finds all follows
     * @return all follows
     */
    @GET
    public List<FollowDTO> all() {
        return followService.findAll();
    }

    /**
     * Finds follow by id
     * @param id of follow
     * @return found follow or status code NOT_FOUND, if follow does not exist
     */
    @Path("{id}")
    @GET
    public FollowDTO byID(@PathParam("id") Integer id) {
        Optional<FollowDTO> optionalFollowDTO;
        optionalFollowDTO = followService.findById(id);

        if (optionalFollowDTO.isPresent()) {
            return optionalFollowDTO.get();
        }
        else {
            throw new javax.ws.rs.NotFoundException("No such follow.");
        }
    }

    /**
     * Creates new follow
     * @param followCreateDTO object with necessary values to create follow
     * @param sec represents logged user
     * @return created follow. NOT_FOUND, if any of users does not exist. BAD_REQUEST, if both users are same or unique constraint was violated
     */
    @POST
    public Response create(@Valid FollowCreateDTO followCreateDTO, @Context SecurityContext sec) throws Exception {
        AuthorizationUtil.checkIfAuthorizedOrElseThrow(sec, followCreateDTO.getUserWhoId(),
                "Can't create follow for other user.");

        FollowDTO followDTO;
        try {
            followDTO = followService.create(followCreateDTO);
        } catch (zlack.bra.instamini.business.exception.NotFoundException e) {
            throw new javax.ws.rs.NotFoundException(e.getMessage());
        } catch (FollowYourselfException e) {
            throw new javax.ws.rs.BadRequestException(e.getMessage());
        } catch (Exception e) {
            throw checkForUniqueConstraintViolation(e);
        }
        return Response.created(UriBuilder.fromUri(ROOT_URL + "follows/" + followDTO.getId()).build()).entity(followDTO).build();
    }

    /**
     * Deletes follow by id
     * @param id of user
     * @param sec represents logged user
     * Can return NOT_FOUND, if follow does not exist
     */
    @Path("{id}")
    @DELETE
    public void delete(@PathParam("id") Integer id, @Context SecurityContext sec) throws zlack.bra.instamini.business.exception.ForbiddenException {
        Optional<FollowDTO> followDTOOptional = followService.findById(id);
        if (followDTOOptional.isPresent()) {
            AuthorizationUtil.checkIfAuthorizedOrElseThrow(sec,
                    followDTOOptional.get().getUserWho().getId(), "Can't delete follow, which does not belong to you.");
        }
        try {
            followService.deleteById(id);
        } catch (zlack.bra.instamini.business.exception.NotFoundException e) {
            throw new javax.ws.rs.NotFoundException(e.getMessage());
        }
    }

    /**
     * Finds follow id by principal's id and userIdWhom
     * @param userIdWhom od userWhom
     * @param sec represents logged user
     * @return follow id or NOT_FOUND, if follow does not exist
     */
    @GET
    @Path("/isFollowed/{userIdWhom}")
    public Integer findFollowIdByUsers(@PathParam("userIdWhom") Integer userIdWhom, @Context SecurityContext sec) {
        try {
            return followService.findFollowIdByUsers(Integer.parseInt(sec.getUserPrincipal().getName()), userIdWhom);
        } catch (zlack.bra.instamini.business.exception.NotFoundException e) {
            throw new javax.ws.rs.NotFoundException(e.getMessage());
        }
    }
}
