package zlack.bra.instamini.controller;

import zlack.bra.instamini.business.dto.CommentCreateDTO;
import zlack.bra.instamini.business.dto.CommentDTO;
import zlack.bra.instamini.business.service.impl.CommentService;
import zlack.bra.instamini.controller.request.EditTextCommentRequestBody;
import zlack.bra.instamini.controller.util.AuthorizationUtil;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.*;
import java.security.Principal;
import java.util.List;
import java.util.Optional;

import static zlack.bra.instamini.constant.Constants.ROOT_URL;

@Path("/comments")
@ApplicationScoped
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class CommentController {

    private final CommentService commentService;

    @Inject
    public CommentController(CommentService commentService) {
        this.commentService = commentService;
    }

    /**
     * Returns all comments
     * @return all comments
     */
    @GET
    public List<CommentDTO> all() {
        return commentService.findAll();
    }

    /**
     * Finds comment by id
     * @param id of comment
     * @return found comment or status code NOT_FOUND, if it does not exist
     */
    @Path("{id}")
    @GET
    public CommentDTO byID(@PathParam("id") Integer id) {
        Optional<CommentDTO> optionalCommentDTO;
        optionalCommentDTO = commentService.findById(id);

        if (optionalCommentDTO.isPresent()) {
            return optionalCommentDTO.get();
        } else {
            throw new javax.ws.rs.NotFoundException("No such comment.");
        }
    }

    /**
     * Creates new comment
     * @param commentCreateDTO object with necessary values to create comment
     * @param sec represents logged user
     * @return created comment or status code NOT_FOUND, if user or post does not exist
     */
    @POST
    public Response create(@Valid CommentCreateDTO commentCreateDTO, @Context SecurityContext sec) throws zlack.bra.instamini.business.exception.ForbiddenException {
        AuthorizationUtil.checkIfAuthorizedOrElseThrow(sec, commentCreateDTO.getUserId(),
                "Can't create comment for other user.");

        CommentDTO commentDTO;
        try {
            commentDTO = commentService.create(commentCreateDTO);
        } catch (zlack.bra.instamini.business.exception.NotFoundException e) {
            throw new javax.ws.rs.NotFoundException(e.getMessage());
        }
        return Response.created(UriBuilder.fromUri(ROOT_URL + "comments/" + commentDTO.getId()).build()).entity(commentDTO).build();
    }

    /**
     * Edits text of comment
     * @param id of comment
     * @param requestBody new text
     * @param sec represents logged user
     * @return updated comment or status code NOT_FOUND, if comment does not exist
     */
    @Path("{id}/editText")
    @PUT
    public CommentDTO editText(@PathParam("id") Integer id, @Valid EditTextCommentRequestBody requestBody, @Context SecurityContext sec) throws zlack.bra.instamini.business.exception.ForbiddenException {
        Optional<CommentDTO> commentDTOOptional = commentService.findById(id);
        if (commentDTOOptional.isPresent()) { AuthorizationUtil.checkIfAuthorizedOrElseThrow(sec,
                commentDTOOptional.get().getUser().getId(), "Can't edit text of comment, which does not belong to you.");
        }

        try {
            return commentService.editText(id, requestBody.getNewText());
        } catch (zlack.bra.instamini.business.exception.NotFoundException e) {
            throw new javax.ws.rs.NotFoundException(e.getMessage());
        }
    }

    /**
     * Deletes comment by id
     * @param id of comment
     * @param sec represents logged user
     * Can return NOT_FOUND, if comment does not exist
     */
    @Path("{id}")
    @DELETE
    public void delete(@PathParam("id") Integer id, @Context SecurityContext sec) throws zlack.bra.instamini.business.exception.ForbiddenException {
        Optional<CommentDTO> commentDTOOptional = commentService.findById(id);
        if (commentDTOOptional.isPresent()) {AuthorizationUtil.checkIfAuthorizedOrElseThrow(sec,
                commentDTOOptional.get().getUser().getId(), "Can't delete comment, which does not belong to you.");
        }

        try {
            commentService.deleteById(id);
        } catch (zlack.bra.instamini.business.exception.NotFoundException e) {
            throw new javax.ws.rs.NotFoundException(e.getMessage());
        }
    }

    /**
     * Finds all comments on post
     * @param post id
     * @return found comments or status code NOT_FOUND, if post does not exist
     */
    @Path("onPost")
    @GET
    public List<CommentDTO> findAllCommentsOnPost(@QueryParam("post") Integer post) {
        try {
            return commentService.findAllCommentsOnPost(post);
        } catch (zlack.bra.instamini.business.exception.NotFoundException e) {
            throw new javax.ws.rs.NotFoundException(e.getMessage());
        }
    }
}
