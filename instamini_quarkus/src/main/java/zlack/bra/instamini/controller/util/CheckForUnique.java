package zlack.bra.instamini.controller.util;


import lombok.extern.slf4j.Slf4j;
import zlack.bra.instamini.business.exception.DuplicateEntryException;

import static zlack.bra.instamini.constant.Constants.DUPLICATE_ENTRY;


@Slf4j
public class CheckForUnique {

    /**
     * Checks, if there was ConstraintViolationException and if so returns appropriate response
     * @param e exception
     * @return ResponseStatusException
     */
    public static Exception checkForUniqueConstraintViolation(Exception e) {
        for (Throwable t = e.getCause(); t != null; t = t.getCause()) {
            log.error("Exception: " + t);
            if (t.getMessage().startsWith("Duplicate entry") || t.getMessage().startsWith("Unique index")) {
                return new DuplicateEntryException(DUPLICATE_ENTRY);
            }
        }
        return e;
    }
}

