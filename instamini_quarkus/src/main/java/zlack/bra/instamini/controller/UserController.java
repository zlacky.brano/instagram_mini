package zlack.bra.instamini.controller;

import io.quarkus.security.Authenticated;
import zlack.bra.instamini.business.dto.UserDTO;
import zlack.bra.instamini.business.service.impl.UserService;
import zlack.bra.instamini.controller.request.EditUsernameRequestBody;
import zlack.bra.instamini.controller.util.AuthorizationUtil;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.SecurityContext;
import java.util.List;
import java.util.Optional;

import static zlack.bra.instamini.controller.util.CheckForUnique.checkForUniqueConstraintViolation;


@Path("/users")
@ApplicationScoped
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class UserController {

    private final UserService userService;

    @Inject
    public UserController(UserService userService) {
        this.userService = userService;
    }

    /**
     * Finds all users
     * @return all users
     */
    @GET
    public List<UserDTO> all() {
        return userService.findAll();
    }

    /**
     * Finds users by prefix
     * @param prefix
     * @return found users
     */
    @Path("search")
    @GET
    public List<UserDTO> searchUsers(@QueryParam("prefix") String prefix) {
        return userService.searchUsers(prefix);
    }

    /**
     * Finds user by id
     * @param id of user
     * @return found user. NOT_FOUND, if user does not exist.
     */
    @Path("{id}")
    @GET
    public UserDTO byID(@PathParam("id") Integer id) {
        Optional<UserDTO> optionalUserDTO;
        optionalUserDTO = userService.findById(id);

        if (optionalUserDTO.isPresent()) {
            return optionalUserDTO.get();
        }
        else {
            throw new javax.ws.rs.NotFoundException("No such user.");
        }
    }

    /**
     * Finds user by email
     * @param email
     * @return found user. NOT_FOUND, if user does not exist.
     */
    @Path("byEmail")
    @GET
    public UserDTO byEmail(@QueryParam("email") String email) {
        Optional<UserDTO> optionalUserDTO;
        optionalUserDTO = userService.findByEmail(email);

        if (optionalUserDTO.isPresent()) {
            return optionalUserDTO.get();
        }
        else {
            throw new javax.ws.rs.NotFoundException("No such user.");
        }
    }

    /**
     * Finds user by principal (logged user)
     * @param sec represents logged user
     * @return found user. NOT_FOUND, if user does not exist.
     */
    @GET
    @Path("/logged")
    public UserDTO byPrincipal(@Context SecurityContext sec) {
        Optional<UserDTO> optionalUserDTO;
        optionalUserDTO = userService.findById(Integer.parseInt(sec.getUserPrincipal().getName()));

        if (optionalUserDTO.isPresent()) {
            return optionalUserDTO.get();
        }
        else {
            throw new javax.ws.rs.NotFoundException("No such user.");
        }
    }

    /**
     * Edits username of user
     * @param id of user
     * @param requestBody new username
     * @param sec represents logged user
     * @return updated user
     */
    @Path("{id}/editUsername")
    @PUT
    public UserDTO editUsername(@PathParam("id") Integer id,
                                @Valid EditUsernameRequestBody requestBody,
                                @Context SecurityContext sec) throws Exception {
        AuthorizationUtil.checkIfAuthorizedOrElseThrow(sec, id,
                "Can't edit username on profile, which does not belong to you.");
        try {
            return userService.editUsername(id, requestBody.getUsername());
        } catch (zlack.bra.instamini.business.exception.NotFoundException e) {
            throw new javax.ws.rs.NotFoundException(e.getMessage());
        } catch (Exception e) {
            throw checkForUniqueConstraintViolation(e);
        }
    }

    /**
     * Deletes user by id
     * @param id of user
     * @param sec represents logged user
     * Can return NOT_FOUND, if user does not exist
     */
    @Path("{id}")
    @DELETE
    public void delete(@PathParam("id") Integer id, @Context SecurityContext sec) throws zlack.bra.instamini.business.exception.ForbiddenException {
        AuthorizationUtil.checkIfAuthorizedOrElseThrow(sec, id,
                "Can't delete profile, which does not belong to you.");
        try {
            userService.deleteById(id);
        } catch (zlack.bra.instamini.business.exception.NotFoundException e) {
            throw new javax.ws.rs.NotFoundException(e.getMessage());
        }
    }

    /**
     * Finds all users who liked post
     * @param post id
     * @return found users. NOT_FOUND, if post does not exist
     */
    @Path("likedPost")
    @GET
    public List<UserDTO> findAllUsersWhoLikedPost(@QueryParam("post") Integer post) {
        try {
            return userService.findAllUsersWhoLikedPost(post);
        } catch (zlack.bra.instamini.business.exception.NotFoundException e) {
            throw new javax.ws.rs.NotFoundException(e.getMessage());
        }
    }

    /**
     * Finds all followers of user
     * @param user id
     * @return found users. NOT_FOUND, if user does not exist
     */
    @Path("followers")
    @GET
    public List<UserDTO> findAllFollowers(@QueryParam("user") Integer user) {
        try {
            return userService.findAllFollowers(user);
        } catch (zlack.bra.instamini.business.exception.NotFoundException e) {
            throw new javax.ws.rs.NotFoundException(e.getMessage());
        }
    }

    /**
     * Finds all users, who user is following
     * @param user id
     * @return found users. NOT_FOUND, if user does not exist
     */
    @Path("following")
    @GET
    public List<UserDTO> findAllFollowing(@QueryParam("user") Integer user) {
        try {
            return userService.findAllFollowing(user);
        } catch (zlack.bra.instamini.business.exception.NotFoundException e) {
            throw new javax.ws.rs.NotFoundException(e.getMessage());
        }
    }

    /**
     * Finds count of all followers
     * @param user id
     * @return found count. NOT_FOUND, if user does not exist
     */
    @Path("followers/count")
    @GET
    public Integer findCountOfAllFollowers(@QueryParam("user") Integer user) {
        try {
            return userService.findCountOfAllFollowers(user);
        } catch (zlack.bra.instamini.business.exception.NotFoundException e) {
            throw new javax.ws.rs.NotFoundException(e.getMessage());
        }
    }

    /**
     * Finds count of all users, who user is following
     * @param user id
     * @return found count. NOT_FOUND, if user does not exist
     */
    @Path("following/count")
    @GET
    public Integer findCountOfAllFollowing(@QueryParam("user") Integer user) {
        try {
            return userService.findCountOfAllFollowing(user);
        } catch (zlack.bra.instamini.business.exception.NotFoundException e) {
            throw new javax.ws.rs.NotFoundException(e.getMessage());
        }
    }
}
