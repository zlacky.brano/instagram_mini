package zlack.bra.instamini.data.repository;

import io.quarkus.hibernate.orm.panache.PanacheRepositoryBase;
import zlack.bra.instamini.data.entity.Comment;

import javax.enterprise.context.ApplicationScoped;
import java.util.List;

@ApplicationScoped
public class CommentRepository implements PanacheRepositoryBase<Comment, Integer> {

    public List<Comment> findAllCommentsOnPost(Integer idPost) {
        return list("SELECT c FROM Comment as c WHERE c.post.id = ?1 ORDER BY c.time DESC", idPost);
    }
}
