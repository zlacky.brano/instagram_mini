package zlack.bra.instamini.data.repository;

import io.quarkus.hibernate.orm.panache.PanacheRepositoryBase;
import zlack.bra.instamini.data.entity.Post;

import javax.enterprise.context.ApplicationScoped;
import java.util.List;

@ApplicationScoped
public class PostRepository implements PanacheRepositoryBase<Post, Integer> {

    public List<Post> findAllPostsOfUser(Integer idUser) {
        return list("SELECT p FROM Post as p WHERE p.user.id = ?1 ORDER BY p.time DESC", idUser);
    }

    public Integer findCountOfAllPostsOfUser(Integer idUser) {
        return (int)find("SELECT p FROM Post as p WHERE p.user.id = ?1 ORDER BY p.time DESC", idUser).count();
    }
}
