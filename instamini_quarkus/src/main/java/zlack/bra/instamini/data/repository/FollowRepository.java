package zlack.bra.instamini.data.repository;

import io.quarkus.hibernate.orm.panache.PanacheRepositoryBase;
import zlack.bra.instamini.data.entity.Follow;

import javax.enterprise.context.ApplicationScoped;
import java.util.Optional;

@ApplicationScoped
public class FollowRepository implements PanacheRepositoryBase<Follow, Integer> {

    public Optional<Integer> findFollowIdByUsers(Integer userIdWho, Integer userIdWhom) {
        Optional<Follow> optionalFollow = find("user_id_who = ?1 and user_id_whom = ?2", userIdWho, userIdWhom).firstResultOptional();
        if (optionalFollow.isEmpty()) {
            return Optional.empty();
        } else {
            return Optional.of(optionalFollow.get().getId());
        }
    }
}
