package zlack.bra.instamini.data.repository;

import io.quarkus.hibernate.orm.panache.PanacheRepositoryBase;
import zlack.bra.instamini.data.entity.Like;

import javax.enterprise.context.ApplicationScoped;
import java.util.List;
import java.util.Optional;

@ApplicationScoped
public class LikeRepository implements PanacheRepositoryBase<Like, Integer> {

    public Integer findNumberOfLikesOnPost(Integer idPost) {
        return (int)find("SELECT l FROM Like as l WHERE l.post.id = ?1", idPost).count();
    }

    public List<Like> findLikesOnPost(Integer idPost) {
        return list("SELECT l FROM Like as l WHERE l.post.id = ?1", idPost);
    }

    public Optional<Integer> findLikeIdByUserAndPost(Integer userId, Integer postId) {
        Optional<Like> optionalLike = find("SELECT l FROM Like as l WHERE user.id = ?1 AND post.id = ?2", userId, postId).firstResultOptional();
        if (optionalLike.isEmpty()) {
            return Optional.empty();
        } else {
            return Optional.of(optionalLike.get().getId());
        }
    }
}
