package zlack.bra.instamini.data.repository;

import io.quarkus.hibernate.orm.panache.PanacheRepositoryBase;
import zlack.bra.instamini.data.entity.User;

import javax.enterprise.context.ApplicationScoped;
import java.util.List;
import java.util.Optional;

@ApplicationScoped
public class UserRepository implements PanacheRepositoryBase<User, Integer> {

    public List<User> findAllUsersWhoLikedPost(Integer idPost) {
        return list("SELECT u FROM Like as l JOIN User as u ON(l.user.id = u.id) WHERE l.post.id = ?1", idPost);
    }

    public List<User> findAllFollowers(Integer id) {
        return list("SELECT u FROM Follow as f JOIN User as u ON(f.userWho.id = u.id) WHERE f.userWhom.id = ?1", id);
    }

    public List<User> findAllFollowing(Integer id) {
        return list("SELECT u FROM Follow as f JOIN User as u ON(f.userWhom.id = u.id) WHERE f.userWho.id = ?1", id);
    }

    public Optional<User> findByEmail(String email) {
       return find("email", email).firstResultOptional();
    }

    public Integer findCountOfAllFollowers(Integer id) {
        return (int)find("SELECT u FROM Follow as f JOIN User as u ON(f.userWho.id = u.id) WHERE f.userWhom.id = ?1", id).count();
    }

    public Integer findCountOfAllFollowing(Integer id) {
        return (int)find("SELECT u FROM Follow as f JOIN User as u ON(f.userWhom.id = u.id) WHERE f.userWho.id = ?1", id).count();
    }
}
