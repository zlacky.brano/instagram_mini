package zlack.bra.instamini;

import io.quarkus.runtime.Quarkus;
import io.quarkus.runtime.annotations.QuarkusMain;

@QuarkusMain
public class InstaminiApplication {

    public static void main(String[] args) {
        Quarkus.run(args);
    }
}