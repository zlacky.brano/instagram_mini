package zlack.bra.instamini.security.login.exception;

public class ObtainUserInfoException extends Exception {

    public ObtainUserInfoException(String errorMessage) {
        super(errorMessage);
    }
}
