package zlack.bra.instamini.security.login.controller;

import io.quarkus.security.Authenticated;
import org.apache.commons.lang.StringUtils;
import zlack.bra.instamini.security.login.service.LoginService;

import javax.annotation.security.PermitAll;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.*;
import java.net.URISyntaxException;

import static javax.ws.rs.core.Cookie.DEFAULT_VERSION;
import static zlack.bra.instamini.constant.Constants.COOKIE_EXPIRE;
import static zlack.bra.instamini.constant.Constants.REDIRECT_URI_PARAM_COOKIE_NAME;

@ApplicationScoped
@Path("/oauth2/authorization/google")
public class LoginController {

    private final LoginService loginService;

    @Inject
    public LoginController(LoginService loginService) {
        this.loginService = loginService;
    }

    /**
     * Endpoint for user login
     * @param redirectURI query param, so application can redirect user on desired uri after successful login
     * @return response with redirection to google authorization server
     * @throws URISyntaxException
     */
    @GET
    @PermitAll
    public Response login(@QueryParam(REDIRECT_URI_PARAM_COOKIE_NAME) String redirectURI) throws URISyntaxException {
        if (StringUtils.isNotBlank(redirectURI)) {
            return Response.seeOther(loginService.getGoogleAuthorizationCodeRequestURI())
                    .cookie(new NewCookie(REDIRECT_URI_PARAM_COOKIE_NAME, redirectURI, "/",
                            null, DEFAULT_VERSION, null, COOKIE_EXPIRE, null, false, false))
                    .build();
        }
        return Response.seeOther(loginService.getGoogleAuthorizationCodeRequestURI()).build();
    }
}