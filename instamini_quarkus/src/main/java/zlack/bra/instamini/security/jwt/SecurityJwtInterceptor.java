package zlack.bra.instamini.security.jwt;

import lombok.extern.slf4j.Slf4j;
import org.jboss.resteasy.core.Headers;
import org.jboss.resteasy.core.ResourceMethodInvoker;
import org.jboss.resteasy.core.ServerResponse;
import zlack.bra.instamini.business.dto.UserDTO;
import zlack.bra.instamini.business.service.impl.UserService;

import javax.annotation.security.DenyAll;
import javax.annotation.security.PermitAll;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.SecurityContext;
import javax.ws.rs.ext.Provider;
import java.io.IOException;
import java.lang.reflect.Method;
import java.security.Principal;
import java.util.Optional;

@Provider
@Slf4j
public class SecurityJwtInterceptor implements ContainerRequestFilter {

    private final UserService userService;

    private final JwtUtil jwtUtil;

    private static final ServerResponse ACCESS_DENIED = new ServerResponse("Access denied for this resource", 401, new Headers<>());
    private static final ServerResponse ACCESS_FORBIDDEN = new ServerResponse("Nobody can access this resource", 403, new Headers<>());

    @Inject
    public SecurityJwtInterceptor(UserService userService, JwtUtil jwtUtil) {
        this.userService = userService;
        this.jwtUtil = jwtUtil;
    }

    /**
     * Filters request and sets security context, if user is authenticated.
     * All resources are protected expect the ones, which are annotated with PermitAll.
     * @param requestContext
     * @throws IOException
     */
    @Override
    public void filter(ContainerRequestContext requestContext) throws IOException {
        ResourceMethodInvoker methodInvoker = (ResourceMethodInvoker) requestContext.getProperty("org.jboss.resteasy.core.ResourceMethodInvoker");
        Method method = methodInvoker.getMethod();

        if (!method.isAnnotationPresent(PermitAll.class)) {
            if (method.isAnnotationPresent(DenyAll.class)) {
                requestContext.abortWith(ACCESS_FORBIDDEN);
                return;
            }

            String authorizationHeader = requestContext.getHeaderString("Authorization");

            String email = null;
            String jwt = null;

            if (authorizationHeader != null && authorizationHeader.startsWith("Bearer ")) {
                jwt = authorizationHeader.substring(7);
                if (jwtUtil.validateToken(jwt)) {
                    email = jwtUtil.extractEmail(jwt);
                }
            }

            if (email != null && requestContext.getSecurityContext().getUserPrincipal() == null) {
                Optional<UserDTO> userDTOOptional = userService.findByEmail(email);
                if (userDTOOptional.isPresent()) {
                    requestContext.setSecurityContext(new SecurityContext() {
                        @Override
                        public Principal getUserPrincipal() {
                            return () -> {
                                return userDTOOptional.get().getId().toString(); // ID of user
                            };
                        }

                        @Override
                        public boolean isUserInRole(String s) {
                            return false;
                        }

                        @Override
                        public boolean isSecure() {
                            return false;
                        }

                        @Override
                        public String getAuthenticationScheme() {
                            return "Bearer";
                        }
                    });
                } else {
                    log.warn("Could not set user authentication in security context.");
                    requestContext.abortWith(ACCESS_DENIED);
                }
            } else {
                log.warn("Could not set user authentication in security context.");
                requestContext.abortWith(ACCESS_DENIED);
            }
        }
    }
}
