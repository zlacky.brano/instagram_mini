package zlack.bra.instamini.security.login.service;

import com.google.api.client.auth.oauth2.AuthorizationCodeResponseUrl;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeRequestUrl;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeTokenRequest;
import com.google.api.client.googleapis.auth.oauth2.GoogleTokenResponse;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.gson.GsonFactory;
import lombok.extern.slf4j.Slf4j;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import zlack.bra.instamini.business.dto.UserCreateDTO;
import zlack.bra.instamini.business.dto.UserDTO;
import zlack.bra.instamini.business.service.impl.UserService;
import zlack.bra.instamini.security.jwt.JwtUtil;
import zlack.bra.instamini.security.login.exception.AuthorizationDeniedException;
import zlack.bra.instamini.security.login.exception.MissingQueryParams;
import zlack.bra.instamini.security.login.exception.ObtainUserInfoException;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.core.Cookie;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.core.UriInfo;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.List;
import java.util.Optional;

@RequestScoped
@Slf4j
public class LoginService {

    private final UserService userService;

    private final JwtUtil jwtUtil;

    private final List<String> scopes;

    private final String clientId;

    private final String secret;

    private final String userInfoURL;

    private final String redirectUri = "http://localhost:8080/login/oauth2/code/google";

    private final String defaultTargetUrl = "/";

    @Inject
    public LoginService(@ConfigProperty(name = "google-api.scopes") List<String> scopes,
                        @ConfigProperty(name = "google-api.client-id") String clientId,
                        @ConfigProperty(name = "google-api.secret") String secret,
                        @ConfigProperty(name = "google-api.user-info-url") String userInfoURL,
                        UserService userService, JwtUtil jwtUtil) {
        this.scopes = scopes;
        this.clientId = clientId;
        this.secret = secret;
        this.userInfoURL = userInfoURL;
        this.userService = userService;
        this.jwtUtil = jwtUtil;
    }

    /**
     * Creating authorization redirect request
     * @return created URI
     * @throws URISyntaxException
     */
    public URI getGoogleAuthorizationCodeRequestURI() throws URISyntaxException {
        return new URI(new GoogleAuthorizationCodeRequestUrl(
                clientId,
                redirectUri,
                scopes).setState("/profile").build());
    }

    /**
     * Handles authorization callback and returns redirect URI, if everything was successful
     * @param info for accessing Query params
     * @param cookie for accessing request uri
     * @return redirect URI
     * @throws MissingQueryParams is thrown, when query param state or code is missing
     * @throws AuthorizationDeniedException is thrown, when request for authorization code was declined
     * @throws IOException is thrown, when request failed to execute or there was error, while configuring HttpURLConnection
     * @throws ObtainUserInfoException is thrown, when obtaining user info failed
     */
    public URI handleAuthorizationCallback(UriInfo info, Cookie cookie) throws MissingQueryParams, AuthorizationDeniedException, IOException, ObtainUserInfoException, ParseException {
        AuthorizationCodeResponseUrl authResponse = getAuthorizationCodeResponseUrl(info);
        GoogleTokenResponse tokenResponse = getGoogleTokenResponse(authResponse.getCode());

        JSONObject userInfoJson = getUserInfo(tokenResponse.getAccessToken());
        String email = (String) userInfoJson.get("email");

        registerUser(email);

        return getTargetURI(cookie, email);
    }

    private void registerUser(String email) {
        Optional<UserDTO> userDTO = userService.findByEmail(email);

        if (userDTO.isEmpty()) {
            log.info("Registering new user: " + email + ".");
            userService.create(new UserCreateDTO(email, email));
        }
        else {
            log.info("User: " + email + " already registered.");
        }
    }

    private URI getTargetURI(Cookie cookie, String email) {
        String targetUrl = cookie != null ? cookie.getValue() : defaultTargetUrl;

        String token = jwtUtil.generateToken(email);

        return UriBuilder.fromUri(targetUrl).queryParam("token", token).build();
    }

    private void checkQueryParams(UriInfo info) throws MissingQueryParams {
        MultivaluedMap<String, String> queryParams = info.getQueryParameters();
        if (queryParams.get("state") == null || queryParams.get("code") == null) {
            throw new MissingQueryParams("Error: Missing query params 'state' or 'code'");
        }
    }

    private AuthorizationCodeResponseUrl getAuthorizationCodeResponseUrl(UriInfo info) throws MissingQueryParams, AuthorizationDeniedException {
        checkQueryParams(info);

        AuthorizationCodeResponseUrl authResponse = new AuthorizationCodeResponseUrl(info.getRequestUri().toString());
        if (authResponse.getError() != null) {
            throw new AuthorizationDeniedException("Error: " + authResponse.getError());
        }
        return authResponse;
    }

    private GoogleTokenResponse getGoogleTokenResponse(String code) throws IOException {
        return new GoogleAuthorizationCodeTokenRequest(
                new NetHttpTransport(), new GsonFactory(),
                clientId, secret,
                code, redirectUri)
                .execute();
    }

    private JSONObject getUserInfo(String accessToken) throws IOException, ObtainUserInfoException, ParseException {
        URL url = new URL(userInfoURL);
        HttpURLConnection con = (HttpURLConnection) url.openConnection();
        con.setRequestMethod("GET");
        con.setRequestProperty("Authorization", "Bearer " + accessToken);

        if (con.getResponseCode() == 200) {
            BufferedReader in = new BufferedReader(
                    new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuilder content = new StringBuilder();
            while ((inputLine = in.readLine()) != null) {
                content.append(inputLine);
            }
            in.close();
            con.disconnect();

            return (JSONObject) new JSONParser().parse(content.toString());
        } else {
            con.disconnect();
            throw new ObtainUserInfoException("Request for obtaining user info ended with status code: " + con.getResponseCode() + ".");
        }
    }
}
