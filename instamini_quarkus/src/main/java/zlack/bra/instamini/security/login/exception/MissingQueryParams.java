package zlack.bra.instamini.security.login.exception;

public class MissingQueryParams extends Exception {
    public MissingQueryParams(String errorMessage) {
        super(errorMessage);
    }
}
