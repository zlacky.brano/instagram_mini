package zlack.bra.instamini.security.login.controller;

import com.google.api.client.auth.oauth2.TokenResponseException;
import io.quarkus.security.Authenticated;
import org.json.simple.parser.ParseException;
import zlack.bra.instamini.security.login.exception.AuthorizationDeniedException;
import zlack.bra.instamini.security.login.exception.MissingQueryParams;
import zlack.bra.instamini.security.login.exception.ObtainUserInfoException;
import zlack.bra.instamini.security.login.service.LoginService;

import javax.annotation.security.PermitAll;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.CookieParam;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Cookie;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.io.IOException;
import java.net.URI;

import static zlack.bra.instamini.constant.Constants.REDIRECT_URI_PARAM_COOKIE_NAME;

@ApplicationScoped
@Path("/login/oauth2/code/google")
public class AfterLoginController {

    private final LoginService loginService;

    @Inject
    public AfterLoginController(LoginService loginService) {
        this.loginService = loginService;
    }

    /**
     * Callback endpoint from google authorization server
     * @param info Uri info
     * @param cookie holds redirect uri
     * @return response with appropriate status code
     * @throws IOException
     */
    @GET
    @PermitAll
    public Response afterLogin(@Context UriInfo info, @CookieParam(REDIRECT_URI_PARAM_COOKIE_NAME) Cookie cookie) throws IOException {
        try {
            URI redirectURI = loginService.handleAuthorizationCallback(info, cookie);
            return Response.status(Response.Status.FOUND).location(redirectURI).cookie().build();
        } catch (MissingQueryParams e) {
            return Response.status(400, e.getMessage()).build();
        } catch (AuthorizationDeniedException e) {
            return Response.status(401, e.getMessage()).build();
        } catch (TokenResponseException | ObtainUserInfoException | ParseException e) {
            return Response.status(500, e.getMessage()).build();
        }
    }
}
