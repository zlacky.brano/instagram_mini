package zlack.bra.instamini.security.login.exception;

public class AuthorizationDeniedException extends Exception {

    public AuthorizationDeniedException(String errorMessage) {
        super(errorMessage);
    }
}
