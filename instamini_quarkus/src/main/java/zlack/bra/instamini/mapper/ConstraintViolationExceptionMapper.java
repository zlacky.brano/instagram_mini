package zlack.bra.instamini.mapper;

import lombok.extern.slf4j.Slf4j;
import org.json.simple.JSONObject;

import javax.ws.rs.ext.Provider;
import javax.validation.ConstraintViolationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import java.util.HashMap;
import java.util.Map;

@Provider
@Slf4j
public class ConstraintViolationExceptionMapper implements ExceptionMapper<ConstraintViolationException> {

    /**
     * Maps ConstraintViolationException to BadRequest response
     * @param exception
     * @return BadRequest response
     */
    @Override
    public Response toResponse(ConstraintViolationException exception) {
        log.warn("ConstraintViolationException: " + exception.getMessage());

        Map<String, String> responseBody = new HashMap<>();
        responseBody.put("message", exception.getMessage());

        return Response.status(Response.Status.BAD_REQUEST).entity(new JSONObject(responseBody)).build();
    }
}

