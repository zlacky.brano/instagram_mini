package zlack.bra.instamini.mapper;

import lombok.extern.slf4j.Slf4j;
import org.json.simple.JSONObject;
import zlack.bra.instamini.business.exception.ForbiddenException;

import javax.ws.rs.ext.Provider;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import java.util.HashMap;
import java.util.Map;

@Provider
@Slf4j
public class ForbiddenExceptionMapper implements ExceptionMapper<ForbiddenException> {

    /**
     * Maps ForbiddenException to Forbidden response
     * @param exception
     * @return BadRequest response
     */
    @Override
    public Response toResponse(ForbiddenException exception) {
        log.warn("ForbiddenException: " + exception.getMessage());

        Map<String, String> responseBody = new HashMap<>();
        responseBody.put("message", exception.getMessage());

        return Response.status(Response.Status.FORBIDDEN).entity(new JSONObject(responseBody)).build();
    }
}

