package zlack.bra.instamini.mapper;

import lombok.extern.slf4j.Slf4j;
import org.json.simple.JSONObject;
import zlack.bra.instamini.business.exception.DuplicateEntryException;

import javax.ws.rs.ext.Provider;
import javax.validation.ConstraintViolationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import java.util.HashMap;
import java.util.Map;

@Provider
@Slf4j
public class DuplicateEntryExceptionMapper implements ExceptionMapper<DuplicateEntryException> {

    /**
     * Maps DuplicateEntryException to BadRequest response
     * @param exception
     * @return BadRequest response
     */
    @Override
    public Response toResponse(DuplicateEntryException exception) {
        log.warn("DuplicateEntryException: " + exception.getMessage());

        Map<String, String> responseBody = new HashMap<>();
        responseBody.put("message", exception.getMessage());

        return Response.status(Response.Status.BAD_REQUEST).entity(new JSONObject(responseBody)).build();
    }
}

